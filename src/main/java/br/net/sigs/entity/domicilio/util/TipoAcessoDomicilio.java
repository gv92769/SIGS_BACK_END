package br.net.sigs.entity.domicilio.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "TIPOACESSODOMICILIO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class TipoAcessoDomicilio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6664271135469156914L;

	@Id
    private Long codigo;
    
    @Column(nullable = false)
    private String descricao;
	
}
