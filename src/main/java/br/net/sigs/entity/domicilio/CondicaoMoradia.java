package br.net.sigs.entity.domicilio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.net.sigs.entity.domicilio.util.AbastecimentoAgua;
import br.net.sigs.entity.domicilio.util.AguaConsumoDomicilio;
import br.net.sigs.entity.domicilio.util.CondicaoPosseUsoTerra;
import br.net.sigs.entity.domicilio.util.DestinoLixo;
import br.net.sigs.entity.domicilio.util.FormaEscoamentoBanheiroSanitario;
import br.net.sigs.entity.domicilio.util.LocalizacaoMoradia;
import br.net.sigs.entity.domicilio.util.MaterialPredominanteConstrucao;
import br.net.sigs.entity.domicilio.util.SituacaoMoradia;
import br.net.sigs.entity.domicilio.util.TipoAcessoDomicilio;
import br.net.sigs.entity.domicilio.util.TipoDomicilio;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "CONDICAOMORADIA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CondicaoMoradia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7956608800484599928L;

	@Id
    private Long id;
	
	@ManyToOne
	@JoinColumn(name="abastecimentoagua_codigo", referencedColumnName="codigo")
	private AbastecimentoAgua abastecimentoAgua;
	
	@Transient
	private Long codAbastecimentoAgua;
	
	@ManyToOne
	@JoinColumn(name="posseuso_codigo", referencedColumnName="codigo")
	private CondicaoPosseUsoTerra posseUso;
	
	@Transient
	private Long codPosseUso;
	
	@ManyToOne
	@JoinColumn(name="destinolixo_codigo", referencedColumnName="codigo")
	private DestinoLixo destinoLixo;
	
	@Transient
	private Long codDestinoLixo;
	
	@ManyToOne
	@JoinColumn(name="formaescoamentobanheiro_codigo", referencedColumnName="codigo")
	private FormaEscoamentoBanheiroSanitario formaEscoamentoBanheiro;
	
	@Transient
	private Long codFormaEscoamentoBanheiro;
	
	@Enumerated(EnumType.STRING)
    @Column(name = "Localizacaomoradia", nullable = false)
	private LocalizacaoMoradia localizacaoMoradia;
	
	@ManyToOne
	@JoinColumn(name="materialpredominante_codigo", referencedColumnName="codigo")
	private MaterialPredominanteConstrucao materialPredominante;
	
	@Transient
	private Long codMaterialPredominante;
	
	@Column(name = "nucomodos")
	private String nuComodos;
	
	@Column(name = "numoradores")
	private String nuMoradores;
	
	@ManyToOne
	@JoinColumn(name="situacaomoradia_codigo", referencedColumnName="codigo")
	private SituacaoMoradia situacaoMoradia;
	
	@Transient
	private Long codSituacaoMoradia;
	
	@Column(name = "stdisponibilidadeenergiaeletrica")
	private Boolean stDisponibilidadeEnergiaEletrica;
	
	@ManyToOne
	@JoinColumn(name="tipoacessodomicilio_codigo", referencedColumnName="codigo")
	private TipoAcessoDomicilio tipoAcessoDomicilio;
	
	@Transient
	private Long codTipoAcessoDomicilio;
	
	@ManyToOne
	@JoinColumn(name="tipodomicilio_codigo", referencedColumnName="codigo")
	private TipoDomicilio tipoDomicilio;
	
	@Transient
	private Long codTipoDomicilio;
	
	@ManyToOne
	@JoinColumn(name="aguadomicilio_codigo", referencedColumnName="codigo")
	private AguaConsumoDomicilio aguaDomicilio;
	
	@Transient
	private Long codAguaDomicilio;
	
	@JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private Domicilio domicilio;
	
}
