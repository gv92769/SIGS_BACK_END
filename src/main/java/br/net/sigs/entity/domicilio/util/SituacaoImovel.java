/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.domicilio.util;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Embeddable
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class SituacaoImovel implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -7413274524497364387L;

	public enum SituacaoImovelTipo {
        PROPRIO,
        ALUGADO,
        FINANCIADO,
        OUTROS
    }
    
    @Enumerated(EnumType.STRING)
    private SituacaoImovelTipo tipo;
    
    private String descricao;

}
