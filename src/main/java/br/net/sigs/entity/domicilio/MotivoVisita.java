package br.net.sigs.entity.domicilio;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "MOTIVOVISITA")
@NoArgsConstructor
@AllArgsConstructor @Getter @Setter
public class MotivoVisita implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8592394324831313883L;

	@Id
    private Long codigo;

    @Column(length = 255)
    private String descricao;
}
