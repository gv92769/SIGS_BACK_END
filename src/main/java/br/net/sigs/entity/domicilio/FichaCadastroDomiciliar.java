package br.net.sigs.entity.domicilio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "FICHACADASTRODOMICILIAR")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaCadastroDomiciliar implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7566249388017417277L;

	@Id
	@Column(length = 44)
	private String uuid;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="profissional_id", referencedColumnName="id")
	private Profissional profissionalResponsavel;
	
	@ManyToOne(optional = false)
	private Cbo cbo;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="unidadebasicasaude_id", referencedColumnName="id")
	private UnidadeBasicaSaude ubs;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="domicilio_id", referencedColumnName="id")
	private Domicilio domicilio;
	
	@Column(name="uuidorigem", length = 44, nullable = false)
	private String uuidFichaOriginadora;
	
	@Column(length = 10)
	private String ine;
	
	@Column(name="fichaatualizada", nullable = false)
	private Boolean fichaAtualizada;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(nullable = false)
	private Date data;
	
	@Column(name="statustermorecusa", nullable = false)
	private Boolean statusTermoRecusa;
	
	@Column(nullable = false)
	private Boolean importada;
	
}
