package br.net.sigs.entity.domicilio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "INSTITUICAOPERMANENCIA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class InstituicaoPermanencia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4300942160699858150L;

	@Id
    private Long id;
	
	@Column(name = "nomeinstituicaopermanencia", length = 100)
	private String nomeInstituicaoPermanencia;
	
	@Column(name = "stoutrosprofissionaisvinculados")
	private Boolean stOutrosProfissionaisVinculados;
	
	@Column(name = "nomeresponsaveltecnico")
	private String nomeResponsavelTecnico;
	
	@Column(name = "cnsresponsaveltecnico", length = 15)
	private String cnsResponsavelTecnico;
	
	@Column(name = "cargoinstituicao", length = 100)
	private String cargoInstituicao;
	
	@Column(name = "telefoneresponsaveltecnico")
	private String telefoneResponsavelTecnico;
	
	@JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private Domicilio domicilio;
	
}
