package br.net.sigs.entity.domicilio;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DESFECHO")
@NoArgsConstructor
@AllArgsConstructor @Getter @Setter
public class Desfecho implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -4366761342229016429L;

	@Id
    private Long codigo;

    @Column(length = 45)
    private String descricao;
    
}
