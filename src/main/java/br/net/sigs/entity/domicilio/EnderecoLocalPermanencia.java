package br.net.sigs.entity.domicilio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.net.sigs.entity.domicilio.util.TipoLogradouro;
import br.net.sigs.entity.util.Municipio;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "ENDERECOLOCALPERMANENCIA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class EnderecoLocalPermanencia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4707885507485047083L;

	@Id
    private Long id;
	
	private String bairro;
	
	@Column(length = 8)
	private String cep;
	
	@ManyToOne
	@JoinColumn(name="codigoibge", referencedColumnName="codigoibge")
	private Municipio municipio;
	
	private String complemento;
	
	@Column(name = "nomelogradouro")
	private String nomeLogradouro;
	
	private String numero;
	
	@Column(name="estado")
    private String estado;
	
	@Column(name = "telefonecontato")
	private String telefoneContato;
	
	@Column(name = "telefoneresidencia")
	private String telefoneResidencia;
	
	@ManyToOne
	@JoinColumn(name="tipologradouro_codigo", referencedColumnName="codigo")
	private TipoLogradouro tipoLogradouro;
	
	@Column(name = "stsemnumero")
	private Boolean stSemNumero;
	
	@Column(name = "pontoreferencia")
	private String pontoReferencia;
	
	@Column(length = 2)
	private String microarea;
	
	@Column(name = "stforaarea")
	private Boolean stForaArea;
	
	@JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private Domicilio domicilio;
	
	public EnderecoLocalPermanencia(Long id, String nomeLogradouro, String numero, String complemento, String bairro, String microarea) {
		this.id = id;
		this.nomeLogradouro = nomeLogradouro;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
		this.microarea = microarea == null ? "FA" : microarea;
	}
	
}
