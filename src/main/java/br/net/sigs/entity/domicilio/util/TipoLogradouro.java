package br.net.sigs.entity.domicilio.util;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "TIPOLOGRADOURO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class TipoLogradouro implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8803464259743743931L;

	@Id
	private String codigo;
	
	private String descricao;
	
}
