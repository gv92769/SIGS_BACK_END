package br.net.sigs.entity.domicilio;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.net.sigs.entity.domicilio.util.Animal;
import br.net.sigs.entity.domicilio.util.TipoImovel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "DOMICILIO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Domicilio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8330200214735602330L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@ManyToMany
	@JoinTable(
	    name = "ANIMAL_DOMICILIO", 
	    joinColumns = @JoinColumn(
	      name = "domicilio_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "animal_codigo", referencedColumnName = "codigo"))
	private List<Animal> animaisDomicilio;
	
	@Column(name="quantosanimaisdomicilio")
	private Integer quantosAnimaisDomicilio;
	
	@Column(name="stanimaisdomicilio")
	private Boolean stAnimaisDomicilio;
	
	@ManyToOne
	@JoinColumn(name="tipoimovel_codigo", referencedColumnName="codigo")
	private TipoImovel tipoImovel;
	
	private String ficha;
	
	private String ultimaficha;
	
    @OneToOne(mappedBy = "domicilio", cascade = CascadeType.ALL)
	private EnderecoLocalPermanencia endereco;
    
    @OneToOne(mappedBy = "domicilio", cascade = CascadeType.ALL)
	private CondicaoMoradia condicao;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="domicilio_id", referencedColumnName="id")
	private List<Familia> familias;
	
    @OneToOne(mappedBy = "domicilio", cascade = CascadeType.ALL)
	private InstituicaoPermanencia instituicao;
	
}
