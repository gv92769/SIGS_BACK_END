package br.net.sigs.entity.domicilio.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "CONDICAOPOSSEUSOTERRA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CondicaoPosseUsoTerra implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1318664369824578499L;

	@Id
    private Long codigo;
    
    @Column(nullable = false)
    private String descricao;
	
}
