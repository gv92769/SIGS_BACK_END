package br.net.sigs.entity.domicilio.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
*
* @author Gabriel Vinicius
*/
@AllArgsConstructor @Getter
public enum LocalizacaoMoradia {
	Urbana(83 ,"Urbana"),
	Rural(84, "Rural");
	
	private final int value;
    private final String nome;
    
    public static LocalizacaoMoradia getLocalizacaoMoradiaPorNome(String nome){
        for(LocalizacaoMoradia localizacao : values()){
            if(localizacao.getNome().equalsIgnoreCase(nome))
                return localizacao;
        }
        return null;
    }
	
}
