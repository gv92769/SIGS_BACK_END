package br.net.sigs.entity.domicilio;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "FICHAVISITADOMICILIAR")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaVisitaDomiciliar implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3365093236916208244L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="uuid", length = 44, nullable = false)
    private String uuid;

    @Column(nullable = false)
    private Long turno;

    @Column(name="status_visita_compartilhada_outro_profissional")
    private Boolean statusOutroProfissional;

    @ManyToOne(optional = false)
    @JoinColumn(name="cidadao_id", referencedColumnName="id")
    private Cidadao cidadao;

    @JsonFormat(pattern="yyyy-MM-dd")
    @Column(nullable = false)
    private Date data;

    @Column(length = 10)
    private String ine;

    @Column(nullable = false)
    private Boolean importada;

    @ManyToOne(optional = false)
    @JoinColumn(name="unidade_basica_saude_id", referencedColumnName="id")
    private UnidadeBasicaSaude ubs;

    @ManyToOne(optional = false)
    @JoinColumn(name="profissional_id", referencedColumnName="id")
    private Profissional profissionalResponsavel;

    @ManyToOne(optional = false)
    private Cbo cbo;

    @ManyToMany
    @JoinTable(
            name = "FICHAVISITADOMICILIAR_MOTIVOVISITA",
            joinColumns = @JoinColumn(
                    name = "fichavisitadomiciliar_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "motivovisita_codigo", referencedColumnName = "codigo"))
    private List<MotivoVisita> motivos;

    @ManyToOne(optional = false)
    @JoinColumn(name="desfecho_codigo", referencedColumnName="codigo")
    private Desfecho desfecho;
}
