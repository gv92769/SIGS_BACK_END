/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.domicilio;

import java.io.Serializable;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 *
 * @author Naisses
 */
@Embeddable
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Endereco implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -2287674340699974504L;

	private String tipoLogradouro;
    
    private String logradouro;
    
    private String numero;
    
    private String complemento;
    
    private String bairro;
    
    private String cidade;
    
    private String uf;
    
    private String cep;

}
