package br.net.sigs.entity.domicilio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.domicilio.util.RendaFamiliar;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "FAMILIA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Familia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3256839467008614421L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@ManyToOne
	@JoinColumn(name="cidadao_id", referencedColumnName="id")
	private Cidadao cidadao;
	
	@Column(name="numeromembrosfamilia")
	private Integer numeroMembrosFamilia;
	
	@Column(name="numeroprontuario", length = 30)
	private String numeroProntuario;
	
	@ManyToOne
	@JoinColumn(name="rendafamiliar_codigo", referencedColumnName="codigo")
	private RendaFamiliar rendaFamiliar;
	
	@Transient
	private Long codRendaFamiliar;
	
	@JsonFormat(pattern="yyyy-MM-dd")
    @Column(name = "residedesde")
	private Date resideDesde;
	
	@Column(name = "stmudanca")
	private Boolean stMudanca;
	
}
