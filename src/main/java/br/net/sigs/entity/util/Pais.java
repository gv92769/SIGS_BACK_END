package br.net.sigs.entity.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "PAIS")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Pais implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -200960025894514409L;
		
	@Id
    private Long codigo;
    
    @Column(nullable = false)
    private String nome;
    
}
