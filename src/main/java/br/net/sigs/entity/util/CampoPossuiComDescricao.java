/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Embeddable
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CampoPossuiComDescricao implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -2709058008381357148L;

	@Column(nullable = false)
    private boolean possui;
    
    private String descricao;
 
}
