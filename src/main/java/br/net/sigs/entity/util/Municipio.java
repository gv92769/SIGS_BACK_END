package br.net.sigs.entity.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "MUNICIPIO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Municipio implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2477814869301002759L;

	@Id
	@Column(name = "codigoibge")
	private Long codigoIbge;
	
	private String nome;
	
	private String uf;
	
}
