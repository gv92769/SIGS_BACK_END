/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Gabriel Vinicius
 */
@Entity
@Table(name = "CBO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Cbo implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 6892016339695519740L;

	@Id
    private Long id;
	
    @Column(length = 6)
    private String codigo;
    
    private String titulo;
    
    @Column(name = "stlotacao")
    private Boolean stLotacao;
    
}
