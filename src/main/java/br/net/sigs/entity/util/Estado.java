/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.util;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Naisses
 */
@AllArgsConstructor @Getter 
public enum Estado {
    ACRE("01", "Acre", "AC"),
    ALAGOAS("02", "Alagoas", "AL"),
    AMAPA("03", "Amapá", "AP"),
    AMAZONAS("04", "Amazonas", "AM"),
    BAHIA("05", "Bahia", "BA"),
    CEARA("06", "Ceará", "CE"),
    DISTRITO_FEDERAL("07", "Distrito Federal", "DF"),
    ESPIRITO_SANTO("08", "Espírito Santo", "ES"),
    GOIAS("10", "Goiás", "GO"),
    MARANHAO("11", "Maranhão", "MA"),
    MATO_GROSSO("12", "Mato Grosso", "MT"),
    MATO_GROSSO_DO_SUL("13", "Mato Grosso do Sul", "MS"),
    MINAS_GERAIS("14", "Minas Gerais", "MG"),
    PARA("15", "Pará", "PA"),
    PARAIBA("16", "Paraíba", "PB"),
    PARANA("17", "Paraná", "PR"),
    PERNAMBUCO("18", "Pernambuco", "PE"),
    PIAUI("19", "Piauí", "PI"),
    RIO_DE_JANEIRO("20", "Rio de Janeiro", "RJ"),
    RIO_GRANDE_DO_NORTE("21", "Rio Grande do Norte", "RN"),
    RIO_GRANDE_DO_SUL("22", "Rio Grande do Sul", "RS"),
    RONDONIA("23", "Rondônia", "RO"),
    RORAIMA("09", "Roraima", "RR"),
    SANTA_CATARINA("25", "Santa Catarina", "SC"),
    SAO_PAULO("26", "São Paulo", "SP"),
    SERGIPE("27", "Sergipe", "SE"),
    TOCANTINS("24", "Tocantins", "TO");

	private final String dne;
    
	private final String nome;
    
	private final String sigla;
    
    public static Estado getEstadoPorSigla(String sigla){
        for (Estado estado : values()) {
            if(estado.getSigla().equalsIgnoreCase(sigla))
                return estado;
        }
        return null;
    }
    
    public static Estado getEstadoPorNome(String nome){
        for (Estado estado : values()) {
            if(estado.getNome().equalsIgnoreCase(nome))
                return estado;
        }
        return null;
    }
    
    public static String[] valuesForSigla(){
        return Arrays.stream(Estado.values()).map(Estado::getSigla).toArray(String[]::new);
    }
    
    public static void main(String[] args){
        Estado e = Estado.getEstadoPorNome("São Paulo");
        System.out.println(e);
        System.out.println(Arrays.toString(Estado.valuesForSigla()));
    }

}
