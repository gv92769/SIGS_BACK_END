package br.net.sigs.entity.saude.procedimento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "PROCEDIMENTOFORMAORGANIZACIONAL")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ProcedimentoFormaOrganizacional implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1223497286055527474L;

	@Id
    private Long id;
	
	@Column(nullable = false)
	private String codigo;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="grupo", referencedColumnName="id")
	private ProcedimentoGrupo grupo;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="subgrupo", referencedColumnName="id")
	private ProcedimentoSubGrupo subGrupo;
    
    @Column(nullable = false)
    private String descricao;
    
    @Column(nullable = false)
    private String dtcompetencia;
	
}
