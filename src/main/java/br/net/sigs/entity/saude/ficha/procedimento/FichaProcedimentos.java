/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.ficha.procedimento;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.saude.ficha.util.LocalAtendimento;
import br.net.sigs.entity.saude.procedimento.Procedimento;
import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Gustavo Miranda
 */

@Entity
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
@Table(name = "FICHAPROCEDIMENTOS")
public class FichaProcedimentos implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -9190760509678441832L;
      
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(length = 44, nullable = false)
	private String uuid;
    
    @Column(length = 10)
    private String ine;
    
    @ManyToOne(optional = false)
	@JoinColumn(name="profissional_id", referencedColumnName="id")
	private Profissional profissionalResponsavel;
    
    @ManyToOne(optional = false)
	private Cbo cbo;
    
    @ManyToOne(optional = false)
    @JoinColumn(name="unidadebasicasaude_id", referencedColumnName="id")
    private UnidadeBasicaSaude ubs;

    @Column(nullable = false)
    private Long turno;
    
    @JsonFormat(pattern="yyyy-MM-dd")
    @Column(nullable = false)
    private Date data;
    
    @Column(name = "datahorarioinicio", nullable = false)
    private Date dataHorarioInicio;
    
    @Column(name = "datahorariofinal", nullable = false)
    private Date dataHorarioFinal;
    
    @ManyToOne(optional = false)
	private Cidadao cidadao;
    
    @ManyToOne(optional = false)
	@JoinColumn(name="localatendimento_codigo", referencedColumnName="codigo")
	private LocalAtendimento localAtendimento;
    
    private Boolean escutaInicial;
    
    @ManyToMany
    @JoinTable(name = "FICHAPROCEDIMENTOS_PROCEDIMENTOS",
    	joinColumns = @JoinColumn(name = "fichaprocedimento_id"),
        inverseJoinColumns = @JoinColumn(name = "procedimento_id")
    )
    private List<Procedimento> procedimento;
    
    @ManyToMany
    @JoinTable(name = "FICHAPROCEDIMENTOS_PROCEDIMENTOFIXO",
    	joinColumns = @JoinColumn(name = "fichaprocedimento_id"),
        inverseJoinColumns = @JoinColumn(name = "procedimentofixo_id")
    )
    private List<ProcedimentoFixo> procedimentosFixo;
    
    @Column(nullable = false)
	private Boolean importada;
    
}
