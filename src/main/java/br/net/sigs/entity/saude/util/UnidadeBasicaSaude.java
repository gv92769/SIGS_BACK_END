/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.util;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Entity
@Table(name = "UNIDADEBASICASAUDE")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class UnidadeBasicaSaude  implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -5838437881933743080L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    private String nome;
    
    private String cnes;
    
    private String bairro;
    
}
