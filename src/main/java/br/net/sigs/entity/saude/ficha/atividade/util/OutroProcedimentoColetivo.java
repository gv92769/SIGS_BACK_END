package br.net.sigs.entity.saude.ficha.atividade.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "OUTROPROCEDIMENTOCOLETIVO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class OutroProcedimentoColetivo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7633015435223260477L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String sigtap;
	
	@Column(nullable = false)
	private String descricao;
	
}
