package br.net.sigs.entity.saude.ficha.atendimento.odontologico.util;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "PROCEDIMENTOREALIZADO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ProcedimentoRealizado  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3645494651919974192L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="procedimentoodonto_id", referencedColumnName="id")
	private ProcedimentoOdonto procedimento;
	
	private Integer quantidade;
	
}
