/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.ficha.ectoscopia;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.net.sigs.entity.saude.ficha.util.AvaliacaoMedica;
import br.net.sigs.entity.saude.util.Cid10;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Entity
@Table(name = "ECTOSCOPIA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Ectoscopia extends AvaliacaoMedica{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 8384447939025211209L;
	
	@OneToMany(mappedBy = "ectoscopia", cascade = CascadeType.ALL)
    private List<ItemEctoscopia> itensEctoscopia = new ArrayList<>();
	
	@ManyToMany
	@JoinTable(
	    name = "CID10_ECTOSCOPIA", 
	    joinColumns = @JoinColumn(
	      name = "ectoscopia_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "cid10_id", referencedColumnName = "id"))
	private List<Cid10> listCid;
	
	@Column(length = 1000)
	private String observacao;

    public void adicionarItemEctoscopia(ItemEctoscopia item){
        this.getItensEctoscopia().add(item);
        item.setEctoscopia(this);
    }
    
}
