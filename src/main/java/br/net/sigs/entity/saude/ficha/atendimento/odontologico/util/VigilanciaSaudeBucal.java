package br.net.sigs.entity.saude.ficha.atendimento.odontologico.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "VIGILANCIASAUDEBUCAL")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class VigilanciaSaudeBucal implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2729744825491346171L;

	@Id
	private Long codigo;
	
	@Column(nullable = false)
	private String descricao;
	
}
