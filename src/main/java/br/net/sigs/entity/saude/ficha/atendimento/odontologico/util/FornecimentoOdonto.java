package br.net.sigs.entity.saude.ficha.atendimento.odontologico.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "FORNECIMENTOODONTOLOGICO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FornecimentoOdonto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3793206137377165771L;

	@Id
	private Long codigo;
	
	@Column(nullable = false)
	private String descricao;
	
}
