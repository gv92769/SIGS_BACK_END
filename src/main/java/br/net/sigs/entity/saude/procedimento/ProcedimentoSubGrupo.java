package br.net.sigs.entity.saude.procedimento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "PROCEDIMENTOSUBGRUPO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ProcedimentoSubGrupo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3299461306317992137L;

	
	@Id
    private Long id;
	
	@Column(nullable = false)
	private String codigo;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="grupo", referencedColumnName="id")
	private ProcedimentoGrupo grupo;
    
    @Column(nullable = false, length = 1000)
    private String descricao;
    
    @Column(nullable = false)
    private String dtcompetencia;
	
}
