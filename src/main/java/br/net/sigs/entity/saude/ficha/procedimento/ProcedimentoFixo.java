package br.net.sigs.entity.saude.ficha.procedimento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "PROCEDIMENTOFIXO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ProcedimentoFixo  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2359415342111773269L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name = "descricao", length = 255)
    private String descricao;
    
    @Column(name = "sigtap")
    private String sigtap;
    
    @Column(name = "codigoab")
    private String codigoAb;

}
