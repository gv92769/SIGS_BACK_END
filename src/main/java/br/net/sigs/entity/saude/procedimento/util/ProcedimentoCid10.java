package br.net.sigs.entity.saude.procedimento.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import br.net.sigs.entity.saude.procedimento.Procedimento;
import br.net.sigs.entity.saude.util.Cid10;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
@Table(name = "PROCEDIMENTO_CID10")
public class ProcedimentoCid10 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3321874130052244631L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@ManyToOne(optional = false)
	private Procedimento procedimento;
	
	@ManyToOne(optional = false)
	private Cid10 cid10;
	
	@Column(nullable = false)
    private String dtcompetencia;
    
    @Min(0)
    @Max(1)
    @Column(nullable = false)
    private Byte ativo;
    
    @Column(name = "stcidprincipal")
    private Boolean stCidPrincipal;
	
}
