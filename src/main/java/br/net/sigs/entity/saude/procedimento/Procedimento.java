/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.procedimento;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import br.net.sigs.entity.saude.procedimento.util.Complexidade;
import br.net.sigs.entity.saude.procedimento.util.ProcedimentoCbo;
import br.net.sigs.entity.saude.procedimento.util.ProcedimentoCid10;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Gabriel Vinicius
 */
@Entity
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
@Table(name = "PROCEDIMENTO")
public class Procedimento implements Serializable {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = -7663626893410473128L;

	@Id
    private Long id;
    
	@ManyToOne(optional = false)
	@JoinColumn(name="grupo", referencedColumnName="id")
	private ProcedimentoGrupo grupo;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="subgrupo", referencedColumnName="id")
	private ProcedimentoSubGrupo subGrupo;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="forma", referencedColumnName="id")
	private ProcedimentoFormaOrganizacional formaOrganizacional;
	
    @Column(name = "descricao", length = 1000)
    private String descricao;
    
    @Column(nullable = false)
    private String codigo;
    
    @Column(nullable = false)
    private String dtcompetencia;
    
    @Min(0)
    @Max(1)
    @Column(nullable = false)
    private Byte ativo;
    
    @Min(0)
    @Max(1)
    @Column(nullable = false)
    private Byte feminino;
    
    @Min(0)
    @Max(1)
    @Column(nullable = false)
    private Byte masculino;
    
    @ManyToOne
	private Complexidade complexidade;
    
    @OneToMany
    @JoinColumn(name="procedimento", referencedColumnName="id")
	private List<ProcedimentoCbo> listProcedimentoCbo;
    
    @OneToMany
    @JoinColumn(name="procedimento", referencedColumnName="id")
	private List<ProcedimentoCid10> listProcedimentoCid10;
    
    @Column(name ="tpprocedimento", nullable = true)
    private String tpProcedimento;
    
    @Column(name ="stexame")
    private Boolean stExame;
   
    public Procedimento(Long id, String descricao, String codigo) {
    	setId(id);
    	setDescricao(codigo + " - " + descricao);
    }
    
}
