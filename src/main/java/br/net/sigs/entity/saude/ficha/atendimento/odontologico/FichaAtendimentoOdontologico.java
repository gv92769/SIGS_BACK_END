package br.net.sigs.entity.saude.ficha.atendimento.odontologico;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.CondutaEncaminhamentoOdonto;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.FornecimentoOdonto;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.OutroProcedimentoRealizado;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.ProcedimentoRealizado;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.TipoConsultaOdonto;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.VigilanciaSaudeBucal;
import br.net.sigs.entity.saude.ficha.util.LocalAtendimento;
import br.net.sigs.entity.saude.ficha.util.TipoAtendimento;
import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "FICHAATENDIMENTOODONTOLOGICO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaAtendimentoOdontologico implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 547237647285300369L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 44, nullable = false)
	private String uuid;
	
	@Column(length = 10)
	private String ine;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="profissional_id", referencedColumnName="id")
	private Profissional profissionalResponsavel;
	
	@ManyToOne(optional = false)
	private Cbo cbo;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="unidadebasicasaude_id", referencedColumnName="id")
	private UnidadeBasicaSaude ubs;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(nullable = false)
	private Date data;
	
	@Column(name = "datahorarioinicio", nullable = false)
    private Date dataHorarioInicio;
    
    @Column(name = "datahorariofinal", nullable = false)
    private Date dataHorarioFinal;
	
	@Column(name = "numprontuario", length = 30)
	private String numProntuario;
	
	@ManyToOne(optional = false)
	private Cidadao cidadao;
	
	@Column(nullable = false)
	private Long turno;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="localatendimento_codigo", referencedColumnName="codigo")
	private LocalAtendimento localAtendimento;
	
	@Column(name = "necessidadesespeciais")
	private Boolean necessidadesEspeciais;
	
	private Boolean gestante;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="tipoatendimento_codigo", referencedColumnName="codigo")
	private TipoAtendimento tipoAtendimento;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="tipoconsultaodonto_codigo", referencedColumnName="codigo")
	private TipoConsultaOdonto tipoConsultaOdonto;
	
	@ManyToMany
	@JoinTable(
	    name = "FICHAATENDIMENTOODONTOLOGICO_VIGILANCIASAUDEBUCAL", 
	    joinColumns = @JoinColumn(
	      name = "fichaatendimentoodontologico_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "vigilanciasaudebucal_codigo", referencedColumnName = "codigo"))
	private List<VigilanciaSaudeBucal> listVigilanciaSaudeBucal;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="fichaatendimentoodontologico_id", referencedColumnName="id")
	private List<ProcedimentoRealizado> procedimentos;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="fichaatendimentoodontologico_id", referencedColumnName="id")
	private List<OutroProcedimentoRealizado> outrosProcedimentos;
	
	@ManyToMany
	@JoinTable(
	    name = "FICHAATENDIMENTOODONTOLOGICO_FORNECIMENTOODONTO", 
	    joinColumns = @JoinColumn(
	      name = "fichaatendimentoodontologico_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "fornecimentoodonto_codigo", referencedColumnName = "codigo"))
	private List<FornecimentoOdonto> fornecimentos;
	
	@ManyToMany
	@JoinTable(
	    name = "FICHAATENDIMENTOODONTOLOGICO_CONDUTAENCAMINHAMENTOODONTO", 
	    joinColumns = @JoinColumn(
	      name = "fichaatendimentoodontologico_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "condutaencaminhamentoodonto_codigo", referencedColumnName = "codigo"))
	private List<CondutaEncaminhamentoOdonto> condutas;
	
	@Column(nullable = false)
	private Boolean importada;
	
}
