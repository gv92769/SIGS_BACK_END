package br.net.sigs.entity.saude.ficha.atividade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.net.sigs.entity.saude.ficha.atividade.util.InstituicaoEnsino;
import br.net.sigs.entity.saude.ficha.atividade.util.OutroProcedimentoColetivo;
import br.net.sigs.entity.saude.ficha.atividade.util.PraticaSaude;
import br.net.sigs.entity.saude.ficha.atividade.util.PublicoAlvo;
import br.net.sigs.entity.saude.ficha.atividade.util.TemaReuniao;
import br.net.sigs.entity.saude.ficha.atividade.util.TemaSaude;
import br.net.sigs.entity.saude.ficha.atividade.util.TipoAtividadeColetiva;
import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "ATIVIDADECOLETIVA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class AtividadeColetiva implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7039584228947183440L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 44, nullable = false)
	private String uuid;
	
	@Column(length = 10)
	private String ine;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="profissional_id", referencedColumnName="id")
	private Profissional profissionalResponsavel;
	
	@ManyToOne(optional = false)
	private Cbo cbo;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="atividadecoletiva_id", referencedColumnName="id")
	private List<AtividadeColetivaProfissional> profissionais;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="unidadebasicasaude_id", referencedColumnName="id")
	private UnidadeBasicaSaude ubs;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(nullable = false)
	private Date data;
	
	@Column(nullable = false)
	private Long turno;
	
	@Column(name = "pseeducacao", nullable = false)
	private Boolean pseEducacao;
	
	@Column(name = "psesaude", nullable = false)
	private Boolean pseSaude;
	
	@Column(name = "numparticipantes", nullable = false)
	private Integer numParticipantes;

	@Column(name = "numavaliacoesalteradas", nullable = true)
	private Integer numAvaliacoesAlteradas;
	
	@ManyToOne(optional = true)
	@JoinColumn(name="unidadebasicasaude_localatividade_id", referencedColumnName="id")
	private UnidadeBasicaSaude cnesLocalAtividade;
	
	@ManyToOne
	@JoinColumn(name="instituicaoensino", referencedColumnName="inep")
	private InstituicaoEnsino instituicaoEnsino;
	
	@Column(name = "outralocalidade", nullable = true, length = 250)
	private String outraLocalidade;
	
	@ManyToOne(optional = false, fetch=FetchType.EAGER)
	@JoinColumn(name="tipoatividadecoletiva_codigo", referencedColumnName="codigo")
	private TipoAtividadeColetiva tipoAtividade;
	
	@ManyToMany
	@JoinTable(
	    name = "ATIVIDADECOLETIVA_TEMAREUNIAO", 
	    joinColumns = @JoinColumn(
	      name = "atividadecoletiva_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "temareuniao_codigo", referencedColumnName = "codigo"))
	private List<TemaReuniao> listTemaReuniao;
	
	@ManyToMany
	@JoinTable(
	    name = "ATIVIDADECOLETIVA_PUBLICOALVO", 
	    joinColumns = @JoinColumn(
	      name = "atividadecoletiva_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "publicoalvo_codigo", referencedColumnName = "codigo"))
	private List<PublicoAlvo> listPublicoAlvo;
	
	@ManyToMany
	@JoinTable(
	    name = "ATIVIDADECOLETIVA_TEMASAUDE", 
	    joinColumns = @JoinColumn(
	      name = "atividadecoletiva_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "temasaude_codigo", referencedColumnName = "codigo"))
	private List<TemaSaude> listTemaSaude;

	@ManyToOne(optional = true)
	@JoinColumn(name="praticasaude_codigo", referencedColumnName="codigo")
	private PraticaSaude praticaSaude;
	
	@ManyToOne(optional = true)
	@JoinColumn(name="outroprocedimentocoletivo_id", referencedColumnName="id")
	private OutroProcedimentoColetivo procedimentoColetivo;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="atividadecoletiva_id", referencedColumnName="id")
	private List<ParticipanteAtividadeColetiva> participantes;
	
	@Column(nullable = false)
	private Boolean importada;
	
}
