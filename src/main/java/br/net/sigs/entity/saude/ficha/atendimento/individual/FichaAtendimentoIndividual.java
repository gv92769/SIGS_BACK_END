package br.net.sigs.entity.saude.ficha.atendimento.individual;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "FICHAATENDIMENTOINDIVIDUAL")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaAtendimentoIndividual implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -693450784866568684L;

	@Id
	@Column(length = 44)
	private String uuid;
	
	@OneToOne(optional = false)
	@JoinColumn(name="atendimento_id", referencedColumnName="id")
	private AtendimentoIndividual atendimento;
	
	private Boolean importada;
	
}
