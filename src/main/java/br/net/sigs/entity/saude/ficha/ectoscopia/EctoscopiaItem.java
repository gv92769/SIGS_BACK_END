package br.net.sigs.entity.saude.ficha.ectoscopia;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "ECTOSCOPIA_ITEM")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class EctoscopiaItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -355971854763049157L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
    private String nome;
    
    @Column(nullable = false)
    private String grupo;
	
}
