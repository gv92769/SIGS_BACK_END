package br.net.sigs.entity.saude.ficha.exame;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "RESULTADOEXAME")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ResultadoExame implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4873524737164790613L;

	@Id
    private Long id;
    
    @Column(name = "dataresultado")
    private Date dataResultado;
    
    @Column(name = "datarealizacao")
    private Date dataRealizacao;
    
    @Column(length = 1000)
	private String observacaoResultado;
    
    @ManyToMany
	@JoinTable(
	    name = "DOCUMENTO_RESULTADOEXAME", 
	    joinColumns = @JoinColumn(
	      name = "resultadoexame_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "documento_id", referencedColumnName = "id"))
	private List<Documento> documentos;
    
    @JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private Solicitacao solicitacao;
	
}
