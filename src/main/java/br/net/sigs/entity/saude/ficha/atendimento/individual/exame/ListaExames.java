package br.net.sigs.entity.saude.ficha.atendimento.individual.exame;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "LISTAEXAMES")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ListaExames implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4153271632512353640L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String sigtap;
	
	private String descricao;
	
	private String codigoab;
	
}
