package br.net.sigs.entity.saude.ficha.atividade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.net.sigs.entity.cidadao.Cidadao;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "PARTICIPANTEATIVIDADECOLETIVA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ParticipanteAtividadeColetiva implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1552618801668225970L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = false)
	private Cidadao cidadao;
	
	@Column(nullable = true)
	private Double altura;
	
	@Column(nullable = true)
	private Double peso;
	
	@Column(name = "cessouhabitofumar", nullable = true)
	private Boolean cessouHabitoFumar;
	
	@Column(name = "abandonougrupo", nullable = true)
	private Boolean abandonouGrupo;
	
	@Column(name = "avaliacaoalterada", nullable = true)
	private Boolean avaliacaoAlterada;
	
}
