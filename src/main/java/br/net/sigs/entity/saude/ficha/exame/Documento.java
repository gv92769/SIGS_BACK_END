package br.net.sigs.entity.saude.ficha.exame;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "DOCUMENTO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Documento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8590520507684536480L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String url;
	
}
