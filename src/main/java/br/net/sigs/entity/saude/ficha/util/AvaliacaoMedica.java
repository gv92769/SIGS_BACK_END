/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.ficha.util;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.saude.util.TipoAvaliacaoMedica;
import br.net.sigs.entity.usuario.Profissional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Entity
@Table(name = "AVALIACAOMEDICA")
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class AvaliacaoMedica implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8476087544782548215L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "datarealizacao", nullable = false)
    private Date dataRealizacao;
    
    @Column(name = "datahorarioinicio", nullable = false)
    private Date dataHorarioInicio;
    
    @Column(name = "datahorariofinal", nullable = false)
    private Date dataHorarioFinal;

	@ManyToOne
    @JoinColumn(name = "id_cidadao", nullable = false)
    private Cidadao cidadao;
    
    @ManyToOne
    @JoinColumn(name = "id_profissional", nullable = false)
    private Profissional profissional;
    
    @Column(name = "tipoavaliacaomedica", nullable = false)
    private TipoAvaliacaoMedica tipoAvaliacaoMedica;
    
}
