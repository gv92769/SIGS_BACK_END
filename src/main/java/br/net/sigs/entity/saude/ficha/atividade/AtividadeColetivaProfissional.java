package br.net.sigs.entity.saude.ficha.atividade;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "ATIVIDADECOLETIVA_PROFISSIONAL")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class AtividadeColetivaProfissional {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="profissional_id", referencedColumnName="id")
	private Profissional profissional;
	
	@ManyToOne(optional = false)
	private Cbo cbo;
	
}
