/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.ficha.ectoscopia;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Entity
@Table(name = "ITEMECTOSCOPIA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ItemEctoscopia implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8326459836579118539L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
	@ManyToOne(optional = false)
	@JoinColumn(name="ectoscopia_item_id", referencedColumnName="id")
    private EctoscopiaItem item;
    
    private String observacao;
    
    private String auxiliar;
    
    private String opcao;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="id_ectoscopia")
    private Ectoscopia ectoscopia;
    
}
