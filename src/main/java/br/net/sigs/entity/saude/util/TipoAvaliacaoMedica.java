/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.util;

import lombok.Getter;

/**
 *
 * @author Naisses
 */
@Getter
public enum TipoAvaliacaoMedica {
    ECTOSCOPIA("Ectoscopia"),
    ATENDIMENTO_INDIVIDUAL("Atendimento Individual"),
	EVOLUCAO("Evolucao");
    
    private final String nome;

    private TipoAvaliacaoMedica(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
    
    public static TipoAvaliacaoMedica getTipoAvaliacaoMedicaPorNome(String nome){
        for (TipoAvaliacaoMedica t : values()) {
            if(t.getNome().equalsIgnoreCase(nome))
                return t;
        }
        return null;
    }
}
