package br.net.sigs.entity.saude.ficha.atendimento.odontologico.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "TIPOCONSULTAODONTO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class TipoConsultaOdonto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8796318585931617918L;

	@Id
	private Long codigo;
	
	@Column(nullable = false)
	private String descricao;
	
}
