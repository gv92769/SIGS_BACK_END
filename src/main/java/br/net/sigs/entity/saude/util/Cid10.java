/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Gabriel Vinicius
 */
@Entity
@Table(name = "CID10")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Cid10 implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -9071469831648370995L;

	@Id
    private Long id;
	
    @Column(length = 4)
    private String codigo;
    
    @Column(length = 400)
    private String descricao;
    
    private Byte sexo;
    
    private Boolean ativo;
  
    public Cid10(Long id, String codigo, String descricao) {
    	setId(id);
    	setCodigo(codigo);
    	setDescricao(codigo.concat( " - ").concat(descricao));
    }
    
}
