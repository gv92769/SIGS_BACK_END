package br.net.sigs.entity.saude.procedimento.util;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "COMPLEXIDADE")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Complexidade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8356764926761761846L;

	@Id
    private Long codigo;
	
	private String descricao;
	
	private String sigla;
	
}
