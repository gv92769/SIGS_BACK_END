/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.ficha.util;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Gustavo Miranda
 */
@Entity
@Table(name = "LOCALATENDIMENTO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class LocalAtendimento implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -8384151978378805961L;

	@Id
    private Long codigo;
    
    @Column(nullable = false)
    private String descricao;
    
}
