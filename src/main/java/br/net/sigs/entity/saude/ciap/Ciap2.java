/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.saude.ciap;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Gabriel Vinicius dos Santos
 */
@Entity
@Table(name = "CIAP2")
@NoArgsConstructor @Getter @Setter
public class Ciap2 implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 4066562624352615673L;

	@Id
    private Long id;
	
    private String codigo;
    
    @Column(length = 400)
    private String descricao;
    
    private Byte sexo;
    
    public Ciap2(Long id, String codigo, String descricao) {
    	setId(id);
    	setCodigo(codigo);
    	setDescricao(codigo.concat(" - ").concat(descricao));
    }
    
}
