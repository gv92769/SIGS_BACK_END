package br.net.sigs.entity.saude.ficha.atividade.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "PUBLICOALVO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class PublicoAlvo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -835878773267793626L;
	
	@Id
	private Long codigo;
	
	@Column(nullable = false)
	private String descricao;
	
}
