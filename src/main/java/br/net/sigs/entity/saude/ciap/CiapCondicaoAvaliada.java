package br.net.sigs.entity.saude.ciap;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "CIAPCONDICAOAVALIADA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CiapCondicaoAvaliada implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6446501788574393372L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(optional = true)
	private Ciap2 ciap2;
	
	private String descricao;
	
	private String codigoab;
	
	private String sexo;
	
}
