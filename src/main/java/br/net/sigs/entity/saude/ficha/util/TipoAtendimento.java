package br.net.sigs.entity.saude.ficha.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "TIPOATENDIMENTO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class TipoAtendimento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5158450846017119765L;

	@Id
	private Long codigo;
	
	@Column(nullable = false)
	private String descricao;
	
}
