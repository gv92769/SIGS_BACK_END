package br.net.sigs.entity.saude.ficha.atendimento.individual.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "CONDUTAENCAMINHAMENTO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CondutaEncaminhamento implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5304727660282999297L;

	@Id
	private Long codigo;
	
	@Column(nullable = false)
	private String descricao;
	
}
