package br.net.sigs.entity.saude.ficha.atendimento.individual.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "RACIONALIDADESAUDE")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class RacionalidadeSaude implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6575371111792815929L;

	@Id
	private Long codigo;
	
	@Column(nullable = false)
	private String descricao;
	
}
