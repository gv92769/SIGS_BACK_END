package br.net.sigs.entity.saude.ficha.atendimento.odontologico.util;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.net.sigs.entity.saude.procedimento.Procedimento;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "OUTROPROCEDIMENTOREALIZADO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class OutroProcedimentoRealizado  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 882472496445701871L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="procedimento_id", referencedColumnName="id")
	private Procedimento proced;
	
	private Integer quantidade;
	
	
}
