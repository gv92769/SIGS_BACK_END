package br.net.sigs.entity.saude.procedimento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "PROCEDIMENTOGRUPO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ProcedimentoGrupo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5575670527995037731L;

	@Id
    private Long id;
	
	@Column(nullable = false)
	private String codigo;
    
    @Column(nullable = false)
    private String descricao;
    
    @Column(nullable = false)
    private String dtcompetencia;   
	
}
