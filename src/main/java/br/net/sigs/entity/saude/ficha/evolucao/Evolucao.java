
package br.net.sigs.entity.saude.ficha.evolucao;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.net.sigs.entity.saude.ciap.Ciap2;
import br.net.sigs.entity.saude.ciap.CiapCondicaoAvaliada;
import br.net.sigs.entity.saude.ficha.atendimento.individual.util.AleitamentoMaterno;
import br.net.sigs.entity.saude.ficha.atendimento.individual.util.CondutaEncaminhamento;
import br.net.sigs.entity.saude.ficha.atendimento.individual.util.Nasf;
import br.net.sigs.entity.saude.ficha.atendimento.individual.util.RacionalidadeSaude;
import br.net.sigs.entity.saude.ficha.util.AvaliacaoMedica;
import br.net.sigs.entity.saude.ficha.util.LocalAtendimento;
import br.net.sigs.entity.saude.ficha.util.TipoAtendimento;
import br.net.sigs.entity.saude.procedimento.Procedimento;
import br.net.sigs.entity.saude.util.Cid10;
import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "EVOLUCAO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Evolucao extends AvaliacaoMedica {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -8367715255263425956L;
	
    public enum ModalidadeAD { AD1, AD2, AD3 };
    
    @ManyToOne(optional = false)
    private UnidadeBasicaSaude ubs;
    
    @ManyToOne(optional = false)
	private Cbo cbo;
    
    @Column(length = 10)
    private String ine;
    
    private Long turno;
    
    @ManyToOne(optional = false)
	@JoinColumn(name="localatendimento_codigo", referencedColumnName="codigo")
	private LocalAtendimento localAtendimento;
    
    @ManyToOne(optional = false)
	@JoinColumn(name="tipoatendimento_codigo", referencedColumnName="codigo")
	private TipoAtendimento tipoAtendimento;
    
    private Double peso;
    
    private Double altura;
    
    @ManyToOne
	@JoinColumn(name="aleitamentomaterno_codigo", referencedColumnName="codigo")
    private AleitamentoMaterno aleitamentoMaterno;
    
    private Date dum;
    
    @Column(name = "idadegestacional")
    private Integer idadeGestacional;
    
    @Column(name = "modalidadead")
    private ModalidadeAD modalidadeAD;
    
    @ManyToMany
	@JoinTable(
	    name = "EVOLUCAO_CIAPCONDICAOAVALIADA", 
	    joinColumns = @JoinColumn(
	      name = "evolucao_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "ciap_id", referencedColumnName = "id"))
    private List<CiapCondicaoAvaliada> ciaps;
    
    @ManyToOne(optional = true)
    private Ciap2 ciap2_1;
    
    @ManyToOne(optional = true)
    private Ciap2 ciap2_2;
    
    @ManyToOne(optional = true)
    private Cid10 cid10_1;
    
    @ManyToOne(optional = true)
    private Cid10 cid10_2;
    
    @Column(name = "vacinaemdia")
    private Boolean vacinacaoEmDia; 
    
    @Column(name = "ficouobservacao")
    private Boolean ficouObservacao;
    
    @ManyToMany
	@JoinTable(
	    name = "EVOLUCAO_NASF", 
	    joinColumns = @JoinColumn(
	      name = "evolucao_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "nasf_codigo", referencedColumnName = "codigo"))
    private List<Nasf> nasfs; 
    
    @ManyToMany
	@JoinTable(
	    name = "EVOLUCAO_CONDUTAENCAMINHAMENTO", 
	    joinColumns = @JoinColumn(
	      name = "evolucao_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "condutaencaminhamento_codigo", referencedColumnName = "codigo"))
    private List<CondutaEncaminhamento> condutas;
    
    @Column(name = "gravidezplanejada")
    private Boolean gravidezPlanejada;
    
    @Column(name = "gestasprevias")
    private Integer gestasPrevias;
    
    private Integer partos;
    
    @ManyToOne
	@JoinColumn(name="racionalidadesaude_codigo", referencedColumnName="codigo")
    private RacionalidadeSaude racionalidadeSaude;
    
    @Column(name = "perimetrocefalico")
    private Double perimetroCefalico;
    
    @JsonIgnore
    @OneToOne(mappedBy = "evolucao", fetch = FetchType.LAZY)
	private FichaEvolucao ficha;
    
    @ManyToMany
    @JoinTable(name = "EVOLUCAO_PROCEDIMENTOS",
    	joinColumns = @JoinColumn(name = "evolucao_id"),
        inverseJoinColumns = @JoinColumn(name = "procedimento_id")
    )
    private List<Procedimento> procedimentos;
    
    @Column(length = 3000)
    private String observacao;
}
