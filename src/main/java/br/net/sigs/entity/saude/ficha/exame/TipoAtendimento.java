package br.net.sigs.entity.saude.ficha.exame;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
*
* @author Gabriel Vinicius
*/
@AllArgsConstructor @Getter
public enum TipoAtendimento {
	ECTOSCOPIA(1, "Ectoscopia");
	
	private final int value;
    private final String nome;
    
    public static TipoAtendimento getTipoAtendimentoPorNome(String nome){
        for(TipoAtendimento tipo : values()){
            if(tipo.getNome().equalsIgnoreCase(nome))
                return tipo;
        }
        return null;
    }

}
