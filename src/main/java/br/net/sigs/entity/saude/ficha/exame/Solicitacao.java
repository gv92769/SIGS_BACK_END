package br.net.sigs.entity.saude.ficha.exame;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.saude.procedimento.Procedimento;
import br.net.sigs.entity.saude.procedimento.ProcedimentoSubGrupo;
import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.usuario.Profissional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "SOLICITACAO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Solicitacao implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7067440086969995998L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="cidadao_id", referencedColumnName="id")
	private Cidadao cidadao;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="profissional_id", referencedColumnName="id")
	private Profissional profissional;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="unidadebasicasaude_id", referencedColumnName="id")
	private UnidadeBasicaSaude ubs;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="subgrupo", referencedColumnName="id")
	private ProcedimentoSubGrupo subGrupo;
	
	@ManyToMany
	@JoinTable(
	    name = "PROCEDIMENTO_SOLICITACAO", 
	    joinColumns = @JoinColumn(
	      name = "solicitacao_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "procedimento_id", referencedColumnName = "id"))
	private List<Procedimento> procedimentos;
	
	@Enumerated(EnumType.STRING)
    private TipoAtendimento tipoAtendimento;
	
	@Column(name = "idatendimento")
	private Long idAtendimento;
	
	@Column(name = "datapedido", nullable = false)
    private Date data;
	
	@Column(length = 1000)
	private String observacao;
	
	@Column(name = "tipo_solicitacao", nullable = false)
	private String tipoSolicitacao;
	
	@Column(name = "namefile")
	private String nameFile;
	
	@JsonIgnore
	@OneToOne(mappedBy = "solicitacao", cascade = CascadeType.ALL)
	private ResultadoExame resultado;
	
}
