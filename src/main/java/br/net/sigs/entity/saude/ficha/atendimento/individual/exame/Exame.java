package br.net.sigs.entity.saude.ficha.atendimento.individual.exame;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "EXAME")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Exame implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -528191787819119928L;
	
	public enum Situacao { S, A };
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="exame_id", referencedColumnName="id")
	private ListaExames exame;
	
	private Situacao situacao;
	
}
