package br.net.sigs.entity.saude.ficha.atividade.util;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "INSTITUICAOENSINO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class InstituicaoEnsino implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 450479225616312690L;

	@Id
	private Long inep;
	
	private String nome;
	
}
