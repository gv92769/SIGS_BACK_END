/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.cidadao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import br.net.sigs.entity.cidadao.util.EstadoCivil;
import br.net.sigs.entity.cidadao.util.Etnia;
import br.net.sigs.entity.cidadao.util.Nacionalidade;
import br.net.sigs.entity.cidadao.util.RacaCor;
import br.net.sigs.entity.cidadao.util.Sexo;
import br.net.sigs.entity.saude.ficha.util.AvaliacaoMedica;
import br.net.sigs.entity.util.Municipio;
import br.net.sigs.entity.util.Pais;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 *
 * @author Naisses
 */
@Entity
@Table(name = "CIDADAO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Cidadao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8803702978141798712L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
	@Column(name="uuidorigem")
	private String uuidFichaOriginadora;
	
    @Column(nullable = false)
    private String nome;
    
    @Column(name = "numprontuario")
    private String numProntuario;
    
    @Column(name = "nomesocial")
    private String nomeSocial;
    
    @JsonFormat(pattern="yyyy-MM-dd")
    @Column(name = "datanascimento", nullable = false)
    private Date dataNascimento;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Sexo sexo;

    @Enumerated(EnumType.STRING)
    @Column(name = "estadocivil", nullable = true)
    private EstadoCivil estadoCivil;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Nacionalidade nacionalidade;
    
    @Column(name="estadonascimento")
    private String estadoNascimento;
    
    @ManyToOne
	@JoinColumn(name="municipionascimento", referencedColumnName="codigoibge")
    private Municipio municipioNascimento;
    
    @Column(name = "desconhecenomemae")
    private Boolean desconheceNomeMae;
    
    @Column(name = "nomemae", length = 70)
    private String nomeMae;
    
    @Column(nullable = true, length = 11)
    private String cpf;
    
    @Column(name = "cns", length = 15)
    private String cns;
    
    @Column(name = "numeronispispasep", length = 11)
    private String numeroNisPisPasep;
    
    @Column(name = "cnsresponsavel", length = 15)
    private String cnsResponsavelFamiliar;
    
    @ManyToOne
	@JoinColumn(name="paisnascimento", referencedColumnName="codigo")
    private Pais paisNascimento;
    
    @ManyToOne
	@JoinColumn(name="racacor", referencedColumnName="codigo")
    private RacaCor racaCor;
    
    @Column(name = "statusehresponsavel")
    private Boolean statusEhResponsavel;
    
    @ManyToOne
	@JoinColumn(name="etnia", referencedColumnName="codigo")
    private Etnia etnia;
    
    @Column(name = "nomepai", length = 70)
    private String nomePai;
    
    @Column(name = "desconhecenomepai")
    private Boolean desconheceNomePai;
    
    @JsonFormat(pattern="yyyy-MM-dd")
    @Column(name = "dtnaturalizacao")
    private Date dtNaturalizacao;
    
    @Column(name = "portarianaturalizacao", length = 16)
    private String portariaNaturalizacao;
    
    @JsonFormat(pattern="yyyy-MM-dd")
    @Column(name = "dtentradabrasil")
    private Date dtEntradaBrasil;
    
    @Column(length = 2)
    private String microarea;
    
    @Column(name = "stforaarea")
    private Boolean stForaArea;
    
    @Column(name = "cpfresponsavelfamiliar", length = 11)
    private String cpfResponsavelFamiliar;
    
    private String email;
    
    private String celular;
    
    private String telefone;
    
    @Embedded
    @AttributeOverrides(value = {
        @AttributeOverride(name = "numero", column = @Column(name = "rgnumero")),
        @AttributeOverride(name = "orgaoEmissor", column = @Column(name = "rgorgaoemissor")),
        @AttributeOverride(name = "uf", column = @Column(name = "rguf"))
    })
    private RG rg;
    
    @Embedded
    @AttributeOverrides(value = {
        @AttributeOverride(name = "numero", column = @Column(name = "tituloeleitornumero")),
        @AttributeOverride(name = "zona", column = @Column(name = "tituloeleitorzona")),
        @AttributeOverride(name = "secao", column = @Column(name = "tituloeleitorsecao")),
        @AttributeOverride(name = "uf", column = @Column(name = "tituloeleitoruf"))
    })
    private TituloEleitor tituloEleitor;

    @JsonIgnore
    @OneToOne(mappedBy = "cidadao", cascade = CascadeType.ALL)
    private MoradiaCidadao moradiaCidadao;
    
    @JsonIgnore
    @OneToOne(mappedBy = "cidadao", cascade = CascadeType.ALL)
    private InformacaoSocioDemografica informacaoSocioDemografica;

    @JsonIgnore
    @OneToOne(mappedBy = "cidadao", cascade = CascadeType.ALL)
    private CondicaoSaude condicaoSaude;

    @JsonIgnore
    @OneToOne(mappedBy = "cidadao", cascade = CascadeType.ALL)
    private SituacaoRua situacaoRua;

    @JsonIgnore
    @OneToMany(mappedBy = "cidadao")
    private List<AvaliacaoMedica> avaliacoesMedicas;
    
    private String ultimaficha;
    
    private Boolean ativo;
    
    public Cidadao(Long id, String nome, String cpf, String cns) {
    	this.id = id;
    	this.nome = nome;
    	this.cpf = cpf;
    	this.cns = cns;
    }
    
}
