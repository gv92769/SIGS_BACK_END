package br.net.sigs.entity.cidadao.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "IDENTIDADEGENERO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class IdentidadeGenero implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1721903431508465509L;

	@Id
    private Long codigo;
    
    @Column(nullable = false)
    private String descricao;
	
}
