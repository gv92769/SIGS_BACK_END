/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.cidadao;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.net.sigs.entity.domicilio.util.TipoLogradouro;
import br.net.sigs.entity.util.Municipio;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Entity
@Table(name = "MORADIACIDADAO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class MoradiaCidadao implements Serializable {
    
    

	/**
	 * 
	 */
	private static final long serialVersionUID = 8244996859965859000L;

	@Id
    private Long id;
    
	private String bairro;
	
	@Column(length = 8)
	private String cep;
	
	@ManyToOne
	@JoinColumn(name="codigoibge", referencedColumnName="codigoibge")
	private Municipio municipio;
	
	private String complemento;
	
	@Column(name = "nomelogradouro")
	private String nomeLogradouro;
	
	private String numero;
	
	@Column(name="estado")
    private String estado;
	
	@Column(name = "telefonecontato")
	private String telefoneContato;
	
	@Column(name = "telefoneresidencia")
	private String telefoneResidencia;
	
	@ManyToOne
	@JoinColumn(name="tipologradouro_codigo", referencedColumnName="codigo")
	private TipoLogradouro tipoLogradouro;
	
	@Column(name = "stsemnumero")
	private Boolean stSemNumero;
	
	@Column(name = "pontoreferencia")
	private String pontoReferencia;
	
	@Column(length = 2)
	private String microarea;
	
	@Column(name = "stforaarea")
	private Boolean stForaArea;
	
    @JsonIgnore
    @MapsId
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Cidadao cidadao;

}
