package br.net.sigs.entity.cidadao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.net.sigs.entity.cidadao.util.ConsideracaoPeso;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "CONDICAOSAUDETEMPO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CondicaoSaudeTemporaria implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5414841850287697390L;

	@Id
    private Long id;

    @ManyToOne
    @JoinColumn(name="consideracao_peso", referencedColumnName="codigo")
    private ConsideracaoPeso consideracaoPeso;

    @Column(name = "eh_dependente_alcool")
    private Boolean statusEhDependenteAlcool;

    @Column(name = "eh_fumante")
    private Boolean statusEhFumante;

    @Column(name = "eh_dependente_outras_drogas")
    private Boolean statusEhDependenteOutrasDrogas;

    @Column(name = "descricao_causa_internacao_em_12meses")
    private String descricaoCausaInternacaoEm12Meses;

    @Column(name = "descricao_outra_condicao_1")
    private String descricaoOutraCondicao1;

    @Column(name = "descricao_outra_condicao_2")
    private String descricaoOutraCondicao2;

    @Column(name = "descricao_outra_condicao_3")
    private String descricaoOutraCondicao3;

    @Column(name = "descricao_plantas_medicinais_usadas")
    private String descricaoPlantasMedicinaisUsadas;

    @Column(name = "maternidade_de_referencia")
    private String maternidadeDeReferencia;

    @Column(name = "eh_gestante")
    private Boolean statusEhGestante;

    @Column(name = "esta_acamado")
    private Boolean statusEstaAcamado;

    @Column(name = "teve_internado_em_12_meses")
    private Boolean statusTeveInternadoem12Meses;

    @Column(name = "status_usa_plantas_medicinais")
    private Boolean statusUsaPlantasMedicinais;

    @Column(name = "esta_domiciliado")
    private Boolean statusEstaDomiciliado;

    @JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private CondicaoSaude condicao;

}
