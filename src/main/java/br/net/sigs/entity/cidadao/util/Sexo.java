/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.cidadao.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Naisses
 */
@AllArgsConstructor @Getter
public enum Sexo {
    MASCULINO(0 ,"Masculino"),
    FEMININO(1, "Feminino");
    
	private final int value;
    private final String nome;
    
    public static Sexo getSexoPorNome(String nome){
        for(Sexo sexo : values()){
            if(sexo.getNome().equalsIgnoreCase(nome))
                return sexo;
        }
        return null;
    }
}
