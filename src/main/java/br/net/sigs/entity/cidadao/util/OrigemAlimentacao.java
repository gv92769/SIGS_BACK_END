package br.net.sigs.entity.cidadao.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ORIGEMALIMENTACAO") @NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class OrigemAlimentacao {

    @Id
    private Long codigo;

    @Column(length = 45)
    private String descricao;
}
