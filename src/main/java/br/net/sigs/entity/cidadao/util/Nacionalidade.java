package br.net.sigs.entity.cidadao.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
*
* @author Gabriel Vinicius
*/
@AllArgsConstructor @Getter
public enum Nacionalidade {
	Brasileira(1 ,"Brasileira"),
	Naturalizado(2, "Naturalizado"),
	Estrangeiro(3, "Estrangeiro");
    
	private final int value;
    private final String nome;
    
    public static Nacionalidade getNacionalidadePorNome(String nome){
        for(Nacionalidade nacionalidade : values()){
            if(nacionalidade.getNome().equalsIgnoreCase(nome))
                return nacionalidade;
        }
        return null;
    }
	
}
