/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.cidadao.util;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Embeddable
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class TipoSanguineo implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    public enum Grupo{
        A, B, AB, O
    }
    
    public enum FatorRh{
        POSITIVO, NEGATIVO
    }
    
    @Enumerated(EnumType.STRING)
    private Grupo grupo;
    
    @Enumerated(EnumType.STRING)
    private FatorRh fatorRh;    
    
}
