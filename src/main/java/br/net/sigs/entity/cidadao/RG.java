/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.cidadao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Embeddable
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class RG implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -8663119625575903443L;

	@Column(nullable = false)
    private String numero;
    
    @Column(nullable = false)
    private String orgaoEmissor;
    
    @Column(nullable = false)
    private String uf;

    public String imprimir(){
        return "Numero:" + numero + 
                " Órgao emissor:" + orgaoEmissor +
                " UF:" + uf;
    }
}
