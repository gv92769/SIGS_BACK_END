package br.net.sigs.entity.cidadao.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "DEFICIENCIA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Deficiencia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6361137331607712878L;

	@Id
    private Long codigo;
    
    @Column(nullable = false)
    private String descricao;
	
}
