package br.net.sigs.entity.cidadao.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "ETNIA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Etnia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7615035241977865078L;

	@Id
    private Long codigo;
    
    @Column(nullable = false)
    private String descricao;
	
}
