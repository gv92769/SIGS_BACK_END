package br.net.sigs.entity.cidadao.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TEMPOSITUACAORUA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class TempoSituacaoRua implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3758371454230145768L;

	@Id
    private Long codigo;

    @Column(length = 100)
    private String descricao;

}
