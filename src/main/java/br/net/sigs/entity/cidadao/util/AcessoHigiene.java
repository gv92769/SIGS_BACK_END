package br.net.sigs.entity.cidadao.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "ACESSOHIGIENE")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class AcessoHigiene implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3987794815784416677L;

	@Id
    private Long codigo;

    @Column(length = 45)
    private String descricao;

}
