package br.net.sigs.entity.cidadao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "FICHACADASTROINDIVIDUAL")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaCadastroIndividual implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2401572755817140705L;
	
	@Id
	@Column(length = 44)
	private String uuid;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="profissional_id", referencedColumnName="id")
	private Profissional profissionalResponsavel;
	
	@ManyToOne(optional = false)
	private Cbo cbo;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="unidadebasicasaude_id", referencedColumnName="id")
	private UnidadeBasicaSaude ubs;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="cidadao_id", referencedColumnName="id")
	private Cidadao cidadao;
	
	@Transient
	private InformacaoSocioDemografica informacao;

	@Transient
	private CondicaoSaude condicaoSaude;

	@Transient
	private SituacaoRua situacaoRua;
	
	@Column(length = 10)
	private String ine;
	
	@Column(name="fichaatualizada")
	private Boolean fichaAtualizada;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(nullable = false)
	private Date data;
	
	private Boolean importada;
	
}
