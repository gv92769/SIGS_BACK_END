/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.cidadao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Naisses
 */
@Entity
@Table(name = "CIDADAOBIOMETRIA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class BiometriaCidadao implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -6648271188273056935L;

	@JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    public enum Dedo{
        MINIMO_MAO_ESQUERDA,    //0
        ANELAR_MAO_ESQUERDA,    //1
        MEDIO_MAO_ESQUERDA,     //2
        INDICADOR_MAO_ESQUERDA, //3
        POLEGAR_MAO_ESQUERDA,   //4
        POLEGAR_MAO_DIREITA,    //5
        INDICADOR_MAO_DIREITA,  //6
        MEDIO_MAO_DIREITA,      //7
        ANELAR_MAO_DIREITA,     //8
        MINIMO_MAO_DIREITA      //9
    }
    
    @Id
    private Long id;
    
    @Column(name = "digitalprincipal", columnDefinition = "VARBINARY(4000)")
    private byte[] digitalPrincipal;
    
    @Column(name = "digitalsecundaria", columnDefinition = "VARBINARY(4000)")
    private byte[] digitalSecundaria;
    
    @Column(name = "dedoprincipal", columnDefinition = "TINYINT(1)")
    private Dedo dedoDigitalPrincipal;
    
    @Column(name = "dedosecundaria", columnDefinition = "TINYINT(1)")
    private Dedo dedoDigitalSecundaria;
    
    @JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private Cidadao cidadao;

}
