package br.net.sigs.entity.cidadao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.net.sigs.entity.cidadao.util.CursoMaisElevado;
import br.net.sigs.entity.cidadao.util.Deficiencia;
import br.net.sigs.entity.cidadao.util.IdentidadeGenero;
import br.net.sigs.entity.cidadao.util.OrientacaoSexual;
import br.net.sigs.entity.cidadao.util.PovoComunidadeTradicional;
import br.net.sigs.entity.cidadao.util.RelacaoParentesco;
import br.net.sigs.entity.cidadao.util.ResponsavelCrianca;
import br.net.sigs.entity.cidadao.util.SituacaoMercadoTrabalho;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "INFORMACAOSOCIODEMOGRAFICA")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class InformacaoSocioDemografica implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5438170491729864183L;
	
	@Id
    private Long id;
	
	@ManyToMany
	@JoinTable(
	    name = "DEFICIENCIA_CIDADAO", 
	    joinColumns = @JoinColumn(
	      name = "cidadao_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "deficiencia_codigo", referencedColumnName = "codigo"))
	private List<Deficiencia> deficiencias;
	
	@ManyToOne
	@JoinColumn(name="curso_codigo", referencedColumnName="codigo")
	private CursoMaisElevado grauInstrucaoCidadao;
	
	@ManyToOne
	private Cbo cbo;
	
	@ManyToOne
	@JoinColumn(name="orientacaosexual_codigo", referencedColumnName="codigo")
	private OrientacaoSexual orientacaoSexual;
	
	@ManyToOne
	@JoinColumn(name="comunidadetradicional_id", referencedColumnName="id")
	private PovoComunidadeTradicional povoComunidadeTradicional;
	
	@ManyToOne
	@JoinColumn(name="relacaoparentesco_codigo", referencedColumnName="codigo")
	private RelacaoParentesco relacaoParentesco;
	
	@ManyToOne
	@JoinColumn(name="situacaomercadotrabalho_codigo", referencedColumnName="codigo")
	private SituacaoMercadoTrabalho situacaoMercadoTrabalho;
	
	@Column(name = "statusdesejainformarorientacaosexual")
	private Boolean statusDesejaInformarOrientacaoSexual;
	
	@Column(name = "statusfrequentabenzedeira")
	private Boolean statusFrequentaBenzedeira;
	
	@Column(name = "statusfrequentaescola")
	private Boolean statusFrequentaEscola;
	
	@Column(name = "statusmembropovocomunidadetradicional")
	private Boolean statusMembroPovoComunidadeTradicional;
	
	@Column(name = "statusparticipagrupocomunitario")
	private Boolean statusParticipaGrupoComunitario;
	
	@Column(name = "statuspossuiplanosaudeprivado")
	private Boolean statusPossuiPlanoSaudePrivado;
	
	@Column(name = "statustemalgumadeficiencia")
	private Boolean statusTemAlgumaDeficiencia;
	
	@ManyToOne
	@JoinColumn(name="identidadegenero_codigo", referencedColumnName="codigo")
	private IdentidadeGenero identidadeGenero;
	
	@Column(name = "statusdesejainformaridentidadegenero")
	private Boolean statusDesejaInformarIdentidadeGenero;
	
	@ManyToMany
	@JoinTable(
	    name = "CIDADAO_RESPONSAVELCRIANCA", 
	    joinColumns = @JoinColumn(
	      name = "cidadao_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "responsavelcrianca_codigo", referencedColumnName = "codigo"))
	private List<ResponsavelCrianca> responsaveis;
	
	@Transient
	private Boolean ehResponsavel;
	
	@Transient
	private Integer idade;
	
	@JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private Cidadao cidadao;

}
