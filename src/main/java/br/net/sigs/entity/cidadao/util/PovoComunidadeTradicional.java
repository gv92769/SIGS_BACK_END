package br.net.sigs.entity.cidadao.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "POVOCOMUNIDADETRADICIONAL")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class PovoComunidadeTradicional implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3781578822593776009L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
    private String descricao;
	
}
