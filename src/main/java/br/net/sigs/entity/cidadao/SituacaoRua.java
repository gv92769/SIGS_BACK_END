package br.net.sigs.entity.cidadao;

import br.net.sigs.entity.cidadao.util.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "SITUACAORUA") @NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class SituacaoRua implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3091241294398457721L;

	@Id
    private Long id;

    @Column(name = "grau_parentesco_familiar_frequentado", length = 100)
    private String grauParentescoFamiliarFrequentado;

    @Column(name = "outra_instituicao_acompanha", length = 45)
    private String outraInstituicaoAcompanha;

    @ManyToMany
    @JoinTable(
            name = "SITUACAORUA_ACESSOHIGIENE",
            joinColumns = @JoinColumn(
                    name = "situacao_rua_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "acesso_higiene_codigo", referencedColumnName = "codigo"))
    private List<AcessoHigiene> listAcessoHigiene;

    @ManyToMany
    @JoinTable(
            name = "SITUACAORUA_ORIGEMALIMENTACAO",
            joinColumns = @JoinColumn(
                    name = "situacao_rua_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "origiem_alimentacao_codigo", referencedColumnName = "codigo"))
    private List<OrigemAlimentacao> listOrigemAlimentacao;

    @ManyToOne(optional = true)
    @JoinColumn(name="quantas_alimentacao", referencedColumnName="codigo")
    private QuantasAlimentacao quantasAlimentacao;

    @Column(name = "status_acompanhado_outra_instituicao")
    private Boolean statusAcompanhadoOutraInstituicao;

    @Column(name = "status_possui_referencia_familiar")
    private Boolean statusPossuiReferenciaFamiliar;

    @Column(name = "status_recebe_beneficio")
    private Boolean statusRecebeBeneficio;

    @Column(name = "status_situacao_rua", nullable = false)
    private Boolean statusSituacaoRua;

    @Column(name = "status_acesso_higiene_pessoal")
    private Boolean statusAcessoHigienePessoal;

    @Column(name = "status_visita_familiar_frequentemente")
    private Boolean statusVisitaFamiliarFrequentemente;

    @ManyToOne(optional = true)
    @JoinColumn(name="tempo_situacao_rua", referencedColumnName="codigo")
    private TempoSituacaoRua tempoSituacaoRua;

    @JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private Cidadao cidadao;
}
