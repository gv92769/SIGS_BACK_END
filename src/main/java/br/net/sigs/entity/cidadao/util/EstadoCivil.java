/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.cidadao.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Naisses
 */
@AllArgsConstructor @Getter
public enum EstadoCivil {
    SOLTEIRO("Solteiro"), 
    CASADO("Casado"), 
    SEPARADO("Separado"), 
    DIVORCIADO("Divorciado"),
    VIUVO("Viúvo");
    
    private final String nome;
    
    public static EstadoCivil getEstadoCivilPorNome(String nome){
        for(EstadoCivil e : values()){
            if(e.getNome().equalsIgnoreCase(nome))
                return e;
        }
        return null;
    }
}
