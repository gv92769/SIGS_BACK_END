package br.net.sigs.entity.cidadao;

import br.net.sigs.entity.cidadao.util.DoencaCardiaca;
import br.net.sigs.entity.cidadao.util.DoencaRespiratoria;
import br.net.sigs.entity.cidadao.util.DoencaRins;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "CONDICAOSAUDE")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CondicaoSaude implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    private Long id;

    @ManyToMany
    @JoinTable(
            name = "CONDICAOSAUDE_DOENCARINS",
            joinColumns = @JoinColumn(
                    name = "condicao_saude_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "doenca_codigo", referencedColumnName = "codigo"))
    private List<DoencaRins> listDoencaRins;

    @ManyToMany
    @JoinTable(
            name = "CONDICAOSAUDE_DOENCARESPIRATORIA",
            joinColumns = @JoinColumn(
                    name = "condicao_saude_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "doenca_codigo", referencedColumnName = "codigo"))
    private List<DoencaRespiratoria> listDoencaRespiratoria;

    @ManyToMany
    @JoinTable(
            name = "CONDICAOSAUDE_DOENCACARDIACA",
            joinColumns = @JoinColumn(
                    name = "condicao_saude_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "doenca_codigo", referencedColumnName = "codigo"))
    private List<DoencaCardiaca> listDoencaCardiaca;

    @Column(name = "tem_diabetes")
    private Boolean statusTemDiabetes;

    @Column(name = "tem_doenca_respiratoria")
    private Boolean statusTemDoencaRespiratoria;

    @Column(name = "tem_hanseniase")
    private Boolean statusTemHanseniase;

    @Column(name = "tem_hipertensao_arterial")
    private Boolean statusTemHipertensaoArterial;

    @Column(name = "tem_teve_cancer")
    private Boolean statusTemTeveCancer;

    @Column(name = "tem_teve_doencasrins")
    private Boolean statusTemTeveDoencasRins;

    @Column(name = "tem_tuberculose")
    private Boolean statusTemTuberculose;

    @Column(name = "teve_avc_derrame")
    private Boolean statusTeveAvcDerrame;

    @Column(name = "teve_doenca_cardiaca")
    private Boolean statusTeveDoencaCardiaca;

    @Column(name = "teve_infarto")
    private Boolean statusTeveInfarto;

    @Column(name = "status_tratamento_psiquico_problema_mental")
    private Boolean statusTratamentoPsiquicoOuProblemaMental;

    @Column(name = "usa_outras_praticas_integrativas_ou_complementares")
    private Boolean statusUsaOutrasPraticasIntegrativasOuComplementares;

    @OneToOne(mappedBy = "condicao", cascade = CascadeType.ALL)
    private CondicaoSaudeTemporaria condicaoTemporaria;

    @JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private Cidadao cidadao;
}
