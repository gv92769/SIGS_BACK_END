/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.usuario;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Naisses
 */
@Entity
@Table(name = "USUARIO")
public class Usuario implements Serializable, UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4080161632336637474L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotBlank(message = "login é obrigatório")
    private String login;
    
    @NotBlank
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String senha;
    
    @NotNull
    private Boolean ativo;
    
	@NotBlank
    private String nome;
    
    @NotBlank
    private String sobrenome;
    
    private String email;
    
    @Transient
    private String cnes;
    
    @Transient
    private String cbo;
    
    @Transient
    private Perfil perfil;
    
    @JsonIgnore
    @OneToOne(mappedBy = "usuario", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private DetalhesUsuario detalhesUsuario;

    public Usuario() {
    	
    }
    
    public Usuario(String login, String senha, Boolean ativo, String nome, String sobrenome, String email) {
        this.login = login;
        this.senha = senha;
        this.ativo = ativo;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.email = email;
    }
    
    public Usuario(Long id, String nome, String sobrenome,  String login, Boolean ativo) {
    	this.id = id;
    	this.nome = nome;
    	this.sobrenome = sobrenome;
    	this.login = login;
    	this.ativo = ativo;
    }
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCnes() {
		return cnes;
	}

	public void setCnes(String cnes) {
		this.cnes = cnes;
	}
	
	public String getCbo() {
		return cbo;
	}

	public void setCbo(String cbo) {
		this.cbo = cbo;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public DetalhesUsuario getDetalhesUsuario() {
		return detalhesUsuario;
	}

	public void setDetalhesUsuario(DetalhesUsuario detalhesUsuario) {
		this.detalhesUsuario = detalhesUsuario;
	}

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<>();
        
        if (getPerfil() != null)
        	authorities.add(new SimpleGrantedAuthority(getPerfil().getPerfil()));
        
        return authorities;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return this.senha;
    }

    @JsonIgnore
    @Override
    public String getUsername() {
        return this.login;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return this.ativo;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", login=" + login + ", senha=" + senha + ", ativo=" + ativo + ", perfil=" + perfil + '}';
    }
    
    @Transient
    @JsonIgnore
    public String getNomeCompleto(){
        return nome + " " + sobrenome;
    }
    
}
