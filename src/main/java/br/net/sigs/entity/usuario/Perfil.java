package br.net.sigs.entity.usuario;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "PERFIL")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Perfil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7060171631676436340L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String descricao;
	
	private String perfil;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
	    name = "OCUPACAO_PERFIL", 
	    joinColumns = @JoinColumn(
	      name = "perfil_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "cbo_id", referencedColumnName = "id"))
    private List<Cbo> listCbo;
	
	private Boolean ativo;

}
