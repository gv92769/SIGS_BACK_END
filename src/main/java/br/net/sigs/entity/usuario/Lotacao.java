package br.net.sigs.entity.usuario;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "LOTACAO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Lotacao implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3073925623792080134L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@ManyToOne(optional = false)
    @JoinColumn(name = "ubs", referencedColumnName = "id")
	private UnidadeBasicaSaude ubs;
	
	@Transient
	private String unidade;
	
	@ManyToOne(optional = false)
    private Cbo cbo;
	
	@Transient
	private String ocupacao;
	
	@ManyToOne(optional = false)
    @JoinColumn(name = "perfil", referencedColumnName = "id")
	private Perfil perfil;
	
	@Transient
	private String nomePerfil;
	
	@Transient
	private String descricao;
	
	@ManyToOne(optional = false)
    @JoinColumn(name = "id_profissional", nullable = false)
    private Profissional profissional;
	
	private Boolean ativo;

	public Lotacao(Long id, String ubs, String cbo, String perfil, Boolean ativo) {
		setId(id);
		setUnidade(ubs);
		setOcupacao(cbo);
		setNomePerfil(perfil);
		setAtivo(ativo);
	}
	
	public Lotacao(Long id, String ubs, String cbo, String perfil) {
		setId(id);
		setDescricao(ubs.concat(" - ").concat(cbo).concat(" - ").concat(perfil));
	}
	
}
