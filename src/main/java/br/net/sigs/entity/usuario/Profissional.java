/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.entity.usuario;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.net.sigs.entity.saude.ficha.util.AvaliacaoMedica;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@Entity
@Table(name = "PROFISSIONAL")
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Profissional extends DetalhesUsuario implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -5331041754408526809L;

	@Column(length = 11, nullable = false)
    private String cpf;
    
    @Column(length = 15, nullable = true)
    private String celular;
    
    @Column(name="cns", length = 15, nullable = false)
    private String cns;
    
    @OneToMany(mappedBy = "profissional")
    @JsonIgnore
    private List<Lotacao> lotacoes;
	
	@OneToMany(mappedBy = "profissional")
    @JsonIgnore
    private List<AvaliacaoMedica> avaliacoesMedicas;
	
	@Transient
	private String nome;
	
	@Column(name="crmCorem", nullable = true)
	private String crmCorem;
	
	public Profissional(Long id, String nome, String sobrenome) {
		setId(id);
		setNome(nome.concat(" ").concat(sobrenome));
	}
    
}
