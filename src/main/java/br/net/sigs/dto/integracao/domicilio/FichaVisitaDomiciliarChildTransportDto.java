package br.net.sigs.dto.integracao.domicilio;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaVisitaDomiciliarChildTransportDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long turno;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String numProntuario;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String cnsCidadao;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String cpfCidadao;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long dtNascimento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long sexo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean statusOutroProfissional;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Long> motivosVisita;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long desfecho;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String microArea;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean  stForaArea;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long tipoDeImovel;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double pesoAcompanhamentoNutricional;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double alturaAcompanhamentoNutricional;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer pressaoSistolica;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer pressaoDiastolica;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double temperatura;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer glicemia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long tipoGlicemia;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double latitude;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double longitude;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String uuidOrigemCadastroDomiciliar;
}
