package br.net.sigs.dto.integracao.cidadao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CondicoesDeSaudeTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String descricaoCausaInternacaoEm12Meses;
	
	@JsonInclude(Include.NON_EMPTY)
	private String descricaoOutraCondicao1;
	
	@JsonInclude(Include.NON_EMPTY)
	private String descricaoOutraCondicao2;
	
	@JsonInclude(Include.NON_EMPTY)
	private String descricaoOutraCondicao3;
	
	@JsonInclude(Include.NON_EMPTY)
	private String descricaoPlantasMedicinaisUsadas;
	
	@JsonInclude(Include.NON_NULL)
	private List<Long> doencaCardiaca;
	
	@JsonInclude(Include.NON_NULL)
	private List<Long> doencaRespiratoria;
	
	@JsonInclude(Include.NON_NULL)
	private List<Long> doencaRins;
	
	@JsonInclude(Include.NON_EMPTY)
	private String maternidadeDeReferencia;
	
	@JsonInclude(Include.NON_NULL)
	private Long situacaoPeso;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusEhDependenteAlcool;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusEhDependenteOutrasDrogas;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusEhFumante;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusEhGestante;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusEstaAcamado;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusEstaDomiciliado;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTemDiabetes;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTemDoencaRespiratoria;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTemHanseniase;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTemHipertensaoArterial;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTemTeveCancer;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTemTeveDoencasRins;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTemTuberculose;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTeveAvcDerrame;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTeveDoencaCardiaca;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTeveInfarto;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTeveInternadoEm12Meses;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusUsaOutrasPraticasIntegrativasOuComplementares;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusUsaPlantaMedicinais;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusDiagnosticoMental;
	
}
