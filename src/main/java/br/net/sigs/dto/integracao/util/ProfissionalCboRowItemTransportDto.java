package br.net.sigs.dto.integracao.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ProfissionalCboRowItemTransportDto {
	
	@JsonInclude(Include.NON_EMPTY)
	private String cnsProfissional;
	
	@JsonInclude(Include.NON_EMPTY)
	private String codigoCbo2002;
	
}
