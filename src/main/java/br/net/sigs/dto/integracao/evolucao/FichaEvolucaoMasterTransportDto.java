package br.net.sigs.dto.integracao.evolucao;

import br.net.sigs.dto.integracao.util.VariasLotacoesHeaderTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaEvolucaoMasterTransportDto {

	private VariasLotacoesHeaderTransportDto headerTransport;
	
	private EvolucaoDto evolucoes;
	
	private int tpCdsOrigem;
	
	private String uuidFicha;
	
}
