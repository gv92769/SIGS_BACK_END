package br.net.sigs.dto.integracao.atendimento.individual;

import br.net.sigs.dto.integracao.util.VariasLotacoesHeaderTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaAtendimentoIndividualMasterTransportDto {

	private VariasLotacoesHeaderTransportDto headerTransport;
	
	private AtendimentoIndivualDto atendimentosIndividuais;
	
	private int tpCdsOrigem;
	
	private String uuidFicha;
	
}
