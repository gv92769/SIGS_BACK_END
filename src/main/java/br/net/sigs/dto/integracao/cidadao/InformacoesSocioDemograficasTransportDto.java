package br.net.sigs.dto.integracao.cidadao;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class InformacoesSocioDemograficasTransportDto {

	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Long> deficienciasCidadao;
	
	@JsonInclude(Include.NON_NULL)
	private Long grauInstrucaoCidadao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String ocupacaoCodigoCbo2002;
	
	@JsonInclude(Include.NON_NULL)
	private Long orientacaoSexualCidadao;
	
	private String povoComunidadeTradicional;
	
	@JsonInclude(Include.NON_NULL)
	private Long relacaoParentescoCidadao;
	
	@JsonInclude(Include.NON_NULL)
	private Long situacaoMercadoTrabalhoCidadao;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusDesejaInformarOrientacaoSexual;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusFrequentaBenzedeira;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusFrequentaEscola;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusMembroPovoComunidadeTradicional;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusParticipaGrupoComunitario;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusPossuiPlanoSaudePrivado;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTemAlgumaDeficiencia;
	
	@JsonInclude(Include.NON_NULL)
	private Long identidadeGeneroCidadao;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusDesejaInformarIdentidadeGenero;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Long> responsavelPorCrianca;
	
}
