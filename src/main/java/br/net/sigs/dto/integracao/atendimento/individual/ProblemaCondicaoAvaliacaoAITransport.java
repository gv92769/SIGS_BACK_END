package br.net.sigs.dto.integracao.atendimento.individual;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ProblemaCondicaoAvaliacaoAITransport {

	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<String> ciaps;
	
	@JsonInclude(Include.NON_EMPTY)
	private String outroCiap1;
	
	@JsonInclude(Include.NON_EMPTY)
	private String outroCiap2;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cid10;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cid10_2;
	
}
