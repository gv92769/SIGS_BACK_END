package br.net.sigs.dto.integracao.domicilio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class EnderecoLocalPermanenciaTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String bairro;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cep;
	
	@JsonInclude(Include.NON_EMPTY)
	private String codigoIbgeMunicipio;
	
	@JsonInclude(Include.NON_EMPTY)
	private String complemento;
	
	@JsonInclude(Include.NON_EMPTY)
	private String nomeLogradouro;
	
	@JsonInclude(Include.NON_EMPTY)
	private String numero;
	
	@JsonInclude(Include.NON_EMPTY)
	private String numeroDneUf;
	
	@JsonInclude(Include.NON_EMPTY)
	private String telefoneContato;
	
	@JsonInclude(Include.NON_EMPTY)
	private String telefoneResidencia;
	
	@JsonInclude(Include.NON_EMPTY)
	private String tipoLogradouroNumeroDne;
	
	private Boolean stSemNumero;
	
	@JsonInclude(Include.NON_EMPTY)
	private String pontoReferencia;
	
	@JsonInclude(Include.NON_EMPTY)
	private String microArea;
	
	private Boolean stForaArea;
	
}
