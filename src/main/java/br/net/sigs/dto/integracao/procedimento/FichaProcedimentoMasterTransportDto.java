package br.net.sigs.dto.integracao.procedimento;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import br.net.sigs.dto.integracao.util.UnicaLotacaoHeaderTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaProcedimentoMasterTransportDto {

	private UnicaLotacaoHeaderTransportDto headerTransport;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<FichaProcedimentoChildTransportDto> atendProcedimentos;
	
	@JsonInclude(Include.NON_EMPTY)
	private String uuidFicha;
	
	@JsonInclude(Include.NON_NULL)
	private int tpCdsOrigem;
	
	@JsonInclude(Include.NON_NULL)
	private Long numTotalAfericaoPa;
	
	@JsonInclude(Include.NON_NULL)
	private Long numTotalAfericaoTemperatura;
	
	@JsonInclude(Include.NON_NULL)
	private Long numTotalCurativoSimples;
	
	@JsonInclude(Include.NON_NULL)
	private Long numTotalColetaMaterialParaExameLaboratorial;
	
	@JsonInclude(Include.NON_NULL)
	private Long numTotalGlicemiaCapilar;
	
	@JsonInclude(Include.NON_NULL)
	private Long numTotalMedicaoAltura;
	
	@JsonInclude(Include.NON_NULL)
	private Long numTotalMedicaoPeso;
	
}
