package br.net.sigs.dto.integracao.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class SistemaDto {

	@JsonInclude(Include.NON_EMPTY)
	private String contraChave = "MSCZ-SIGS-AB/0.2.2";
	
	@JsonInclude(Include.NON_EMPTY)
	private String uuidInstalacao = "MSCZ-SIGS-AB";
	
	@JsonInclude(Include.NON_EMPTY)
	private String cpfOuCnpj = "15031944/0001-01";
	
	@JsonInclude(Include.NON_EMPTY)
	private String nomeOuRazaoSocial = "Mscz Serviços Ortopédicos Ltda";
	
	@JsonInclude(Include.NON_EMPTY)
	private String fone = "(21)99900-3888";
	
	@JsonInclude(Include.NON_EMPTY)
	private String email = "cazezo@yahoo.com.br";
	
	@JsonInclude(Include.NON_EMPTY)
	private String versaoSistema = "0.2.2";
	
	@JsonInclude(Include.NON_EMPTY)
	private String nomeBancoDados = "MYSQL";
	
}
