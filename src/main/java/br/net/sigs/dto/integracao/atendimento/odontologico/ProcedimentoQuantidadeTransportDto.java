package br.net.sigs.dto.integracao.atendimento.odontologico;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ProcedimentoQuantidadeTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String coMsProcedimento;
	
	@JsonInclude(Include.NON_NULL)
	private int quantidade;
	
}
