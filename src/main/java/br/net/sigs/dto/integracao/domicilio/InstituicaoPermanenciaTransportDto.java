package br.net.sigs.dto.integracao.domicilio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class InstituicaoPermanenciaTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String nomeInstituicaoPermanencia;
	
	private Boolean stOutrosProfissionaisVinculados;
	
	@JsonInclude(Include.NON_EMPTY)
	private String nomeResponsavelTecnico;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cnsResponsavelTecnico;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cargoInstituicao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String telefoneResponsavelTecnico;
	
}
