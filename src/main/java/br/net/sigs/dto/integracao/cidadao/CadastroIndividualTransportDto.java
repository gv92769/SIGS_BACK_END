package br.net.sigs.dto.integracao.cidadao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import br.net.sigs.dto.integracao.util.UnicaLotacaoHeaderTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@JacksonXmlRootElement(localName = "ns4:cadastroIndividualTransport")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CadastroIndividualTransportDto {

	private CondicoesDeSaudeTransportDto condicoesDeSaude;
	
	private SituacaoDeRuaTransportDto emSituacaoDeRua;
	
	private Boolean fichaAtualizada;
	
	private IdentificacaoUsuarioCidadaoTransportDto identificacaoUsuarioCidadao;
	
	private InformacoesSocioDemograficasTransportDto informacoesSocioDemograficas;
	
	private SaidaCidadaoCadastroTransportDto saidaCidadaoCadastro;
	
	private Boolean statusTermoRecusaCadastroIndividualAtencaoBasica;
	
	@JsonInclude(Include.NON_NULL)
	private Integer tpCdsOrigem;
	
	@JsonInclude(Include.NON_EMPTY)
	private String uuid;
	
	@JsonInclude(Include.NON_EMPTY)
	private String uuidFichaOriginadora;
	
	private UnicaLotacaoHeaderTransportDto headerTransport;
	
}
