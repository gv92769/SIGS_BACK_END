package br.net.sigs.dto.integracao.domicilio;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import br.net.sigs.dto.integracao.util.InformacaoTransportDto;
import br.net.sigs.dto.integracao.util.SistemaDto;
import br.net.sigs.dto.integracao.util.VersaoTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JacksonXmlRootElement(localName = "ns3:dadoTransporteTransportXml")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaVisitaDomiciliarDTO extends InformacaoTransportDto {
    
    @JacksonXmlProperty(localName = "xmlns:ns4", isAttribute = true)
    private String ns4 = "http://esus.ufsc.br/fichavisitadomiciliarmaster";

    @JacksonXmlProperty(localName = "ns4:fichavisitadomiciliarMasterTransport")
    private FichaVisitaDomiciliarMasterTransportDto transport;

    @JacksonXmlProperty(localName = "ns2:remetente")
    private SistemaDto remetente = new SistemaDto();

    @JacksonXmlProperty(localName = "ns2:originadora")
    private SistemaDto originadora = new SistemaDto();

    @JacksonXmlProperty(localName = "versao")
    private VersaoTransportDto versao = new VersaoTransportDto();

}
