package br.net.sigs.dto.integracao.atendimento.odontologico;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import br.net.sigs.dto.integracao.util.VariasLotacoesHeaderTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaAtendimentoOdontologicoMasterTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String uuidFicha;
	
	@JsonInclude(Include.NON_NULL)
	private int tpCdsOrigem;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<FichaAtendimentoOdontologicoChildTransportDto> atendimentosOdontologicos;
	
	@JsonInclude(Include.NON_NULL)
	private VariasLotacoesHeaderTransportDto headerTransport;
	
}
