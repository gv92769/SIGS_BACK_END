package br.net.sigs.dto.integracao.atendimento.odontologico;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import br.net.sigs.dto.integracao.util.InformacaoTransportDto;
import br.net.sigs.dto.integracao.util.SistemaDto;
import br.net.sigs.dto.integracao.util.VersaoTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@JacksonXmlRootElement(localName = "ns3:dadoTransporteTransportXml")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaAtendimentoOdontologicoDto extends InformacaoTransportDto {

	@JacksonXmlProperty(localName = "xmlns:ns4", isAttribute = true)
	private String ns4 = "http://esus.ufsc.br/fichaatendimentoodontologicomaster";
	
	@JacksonXmlProperty(localName = "ns4:fichaAtendimentoOdontologicoMasterTransport")
	private FichaAtendimentoOdontologicoMasterTransportDto transport;
	
	@JacksonXmlProperty(localName = "ns2:remetente")
	private SistemaDto remetente = new SistemaDto();
	
	@JacksonXmlProperty(localName = "ns2:originadora")
	private SistemaDto originadora = new SistemaDto();
	
	@JacksonXmlProperty(localName = "versao")
	private VersaoTransportDto versao = new VersaoTransportDto();
	
}
