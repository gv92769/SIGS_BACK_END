package br.net.sigs.dto.integracao.domicilio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import br.net.sigs.dto.integracao.util.UnicaLotacaoHeaderTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaVisitaDomiciliarMasterTransportDto {

    private UnicaLotacaoHeaderTransportDto headerTransport;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JacksonXmlElementWrapper(useWrapping = false)
    private FichaVisitaDomiciliarChildTransportDto visitasDomiciliares;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String uuidFicha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int tpCdsOrigem;
}
