package br.net.sigs.dto.integracao.domicilio;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import br.net.sigs.dto.integracao.util.UnicaLotacaoHeaderTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CadastroDomiciliarTransportDto {
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	List<Long> animaisNoDomicilio;
	
	CondicaoMoradiaTransportDto condicaoMoradia;
	
	EnderecoLocalPermanenciaTransportDto enderecoLocalPermanencia;
	
	@JacksonXmlElementWrapper(useWrapping = false)
	List<FamiliaRowTransportDto> familias;
	
	private Boolean fichaAtualizada;
	
	@JsonInclude(Include.NON_EMPTY)
	private String quantosAnimaisNoDomicilio;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean stAnimaisNoDomicilio;
	
	private Boolean statusTermoRecusa;
	
	@JsonInclude(Include.NON_NULL)
	private Integer tpCdsOrigem;
	
	@JsonInclude(Include.NON_EMPTY)
	private String uuid;
	
	@JsonInclude(Include.NON_EMPTY)
	private String uuidFichaOriginadora;
	
	@JsonInclude(Include.NON_NULL)
	private Long tipoDeImovel;
	
	private InstituicaoPermanenciaTransportDto instituicaoPermanencia;
	
	private UnicaLotacaoHeaderTransportDto headerTransport;
	
}
