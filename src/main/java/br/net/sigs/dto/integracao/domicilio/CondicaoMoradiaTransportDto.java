package br.net.sigs.dto.integracao.domicilio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CondicaoMoradiaTransportDto {

	@JsonInclude(Include.NON_NULL)
	private Long abastecimentoAgua;
	
	@JsonInclude(Include.NON_NULL)
	private Long areaProducaoRural;
	
	@JsonInclude(Include.NON_NULL)
	private Long destinoLixo;
	
	@JsonInclude(Include.NON_NULL)
	private Long formaEscoamentoBanheiro;
	
	@JsonInclude(Include.NON_NULL)
	private Long localizacao;
	
	@JsonInclude(Include.NON_NULL)
	private Long materialPredominanteParedesExtDomicilio;
	
	@JsonInclude(Include.NON_EMPTY)
	private String nuComodos;
	
	@JsonInclude(Include.NON_EMPTY)
	private String nuMoradores;
	
	@JsonInclude(Include.NON_NULL)
	private Long situacaoMoradiaPosseTerra;
	
	private Boolean stDisponibilidadeEnergiaEletrica;
	
	@JsonInclude(Include.NON_NULL)
	private Long tipoAcessoDomicilio;
	
	@JsonInclude(Include.NON_NULL)
	private Long tipoDomicilio;
	
	@JsonInclude(Include.NON_NULL)
	private Long aguaConsumoDomicilio;
	
}
