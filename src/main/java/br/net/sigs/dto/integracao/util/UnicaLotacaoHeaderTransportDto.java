package br.net.sigs.dto.integracao.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class UnicaLotacaoHeaderTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String profissionalCNS;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cboCodigo_2002;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cnes;
	
	@JsonInclude(Include.NON_EMPTY)
	private String ine;
	
	@JsonInclude(Include.NON_NULL)
	private Long dataAtendimento;
	
	@JsonInclude(Include.NON_EMPTY)
	private String codigoIbgeMunicipio;
	
}
