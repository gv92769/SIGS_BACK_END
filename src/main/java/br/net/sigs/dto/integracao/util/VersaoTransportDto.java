package br.net.sigs.dto.integracao.util;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class VersaoTransportDto {

	@JacksonXmlProperty(isAttribute = true)
	private int major = 3;
	
	@JacksonXmlProperty(isAttribute = true)
	private int minor = 2;
	
	@JacksonXmlProperty(isAttribute = true)
	private int revision = 4;
	
}
