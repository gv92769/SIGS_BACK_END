package br.net.sigs.dto.integracao.atendimento.individual;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class AtendimentoIndivualDto {

	@JsonInclude(Include.NON_EMPTY)
	private String numeroProntuario;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cnsCidadao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cpfCidadao;
	
	@JsonInclude(Include.NON_NULL)
	private Long dataNascimento;
	
	@JsonInclude(Include.NON_NULL)
	private Long localDeAtendimento;
	
	@JsonInclude(Include.NON_NULL)
	private Long sexo;
	
	@JsonInclude(Include.NON_NULL)
	private Long turno;
	
	@JsonInclude(Include.NON_NULL)
	private Long tipoAtendimento;
	
	@JsonInclude(Include.NON_NULL)
	private Double pesoAcompanhamentoNutricional;
	
	@JsonInclude(Include.NON_NULL)
	private Double alturaAcompanhamentoNutricional;
	
	@JsonInclude(Include.NON_NULL)
	private Long aleitamentoMaterno;
	
	@JsonInclude(Include.NON_NULL)
	private Long dumDaGestante;
	
	@JsonInclude(Include.NON_NULL)
	private Integer idadeGestacional;
	
	@JsonInclude(Include.NON_NULL)
	private Long atencaoDomiciliarModalidade;
	
	@JsonInclude(Include.NON_NULL)
	private ProblemaCondicaoAvaliacaoAITransport problemaCondicaoAvaliada;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<ExameTransportDto> exame;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean vacinaEmDia;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean ficouEmObservacao;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Long> nasfs;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Long> condutas;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean stGravidezPlanejada;
	
	@JsonInclude(Include.NON_NULL)
	private Integer nuGestasPrevias;
	
	@JsonInclude(Include.NON_NULL)
	private Integer nuPartos;
	
	@JsonInclude(Include.NON_NULL)
	private Long racionalidadeSaude;
	
	@JsonInclude(Include.NON_NULL)
	private Double perimetroCefalico;
	
	private Long dataHoraInicialAtendimento;
	
	private Long dataHoraFinalAtendimento;
	
}
