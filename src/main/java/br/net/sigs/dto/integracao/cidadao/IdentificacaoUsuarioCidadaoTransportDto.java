package br.net.sigs.dto.integracao.cidadao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class IdentificacaoUsuarioCidadaoTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String nomeSocial;
	
	@JsonInclude(Include.NON_EMPTY)
	private String codigoIbgeMunicipioNascimento;
	
	@JsonInclude(Include.NON_NULL)
	private Long dataNascimentoCidadao;
	
	private Boolean desconheceNomeMae;
	
	@JsonInclude(Include.NON_EMPTY)
	private String emailCidadao;
	
	@JsonInclude(Include.NON_NULL)
	private Long nacionalidadeCidadao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String nomeCidadao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String nomeMaeCidadao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cnsCidadao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cpfCidadao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cnsResponsavelFamiliar;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cpfResponsavelFamiliar;
	
	@JsonInclude(Include.NON_EMPTY)
	private String telefoneCelular;
	
	@JsonInclude(Include.NON_EMPTY)
	private String numeroNisPisPasep;
	
	@JsonInclude(Include.NON_NULL)
	private Long paisNascimento;
	
	@JsonInclude(Include.NON_NULL)
	private Long racaCorCidadao;
	
	@JsonInclude(Include.NON_NULL)
	private Long sexoCidadao;
	
	private Boolean statusEhResponsavel;
	
	@JsonInclude(Include.NON_NULL)
	private Long etnia;
	
	@JsonInclude(Include.NON_EMPTY)
	private String nomePaiCidadao;
	
	private Boolean desconheceNomePai;
	
	@JsonInclude(Include.NON_NULL)
	private Long dtNaturalizacao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String portariaNaturalizacao;
	
	@JsonInclude(Include.NON_NULL)
	private Long dtEntradaBrasil;
	
	@JsonInclude(Include.NON_EMPTY)
	private String microArea;
	
	private Boolean stForaArea;
	
}
