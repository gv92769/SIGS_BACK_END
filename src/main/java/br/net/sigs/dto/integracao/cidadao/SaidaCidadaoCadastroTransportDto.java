package br.net.sigs.dto.integracao.cidadao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class SaidaCidadaoCadastroTransportDto {

	@JsonInclude(Include.NON_NULL)
	private Long motivoSaidaCidadao;
	
	@JsonInclude(Include.NON_NULL)
	private Long dataObito;
	
	@JsonInclude(Include.NON_EMPTY)
	private String numeroDO;
	
}
