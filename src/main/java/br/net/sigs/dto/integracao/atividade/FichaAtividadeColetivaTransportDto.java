package br.net.sigs.dto.integracao.atividade;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import br.net.sigs.dto.integracao.util.ProfissionalCboRowItemTransportDto;
import br.net.sigs.dto.integracao.util.UnicaLotacaoHeaderTransportDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaAtividadeColetivaTransportDto {
	
	@JsonInclude(Include.NON_EMPTY)
	private String uuidFicha;
	
	@JsonInclude(Include.NON_EMPTY)
	private String outraLocalidade;
	
	@JsonInclude(Include.NON_NULL)
	private Long inep;
	
	@JsonInclude(Include.NON_NULL)
	private int numParticipantes;
	
	@JsonInclude(Include.NON_NULL)
	private int numAvaliacoesAlteradas;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<ProfissionalCboRowItemTransportDto> profissionais;
	
	@JsonInclude(Include.NON_NULL)
	private Long atividadeTipo;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Long> temasParaReuniao;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Long> publicoAlvo;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<ParticipanteRowItemTransportDto> participantes;
	
	@JsonInclude(Include.NON_NULL)
	private int tbCdsOrigem;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cnesLocalAtividade;
	
	@JsonInclude(Include.NON_EMPTY)
	private String procedimento;
	
	@JsonInclude(Include.NON_NULL)
	private Long turno;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean pseEducacao;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean pseSaude;
	
	@JsonInclude(Include.NON_NULL)
	private UnicaLotacaoHeaderTransportDto headerTransport;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Long> temasParaSaude;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Long> praticasEmSaude;
	
}
