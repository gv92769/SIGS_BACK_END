package br.net.sigs.dto.integracao.cidadao;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class SituacaoDeRuaTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String grauParentescoFamiliarFrequentado;
	
	@JsonInclude(Include.NON_NULL)
	private List<Long> higienePessoalSituacaoRua;
	
	@JsonInclude(Include.NON_NULL)
	private List<Long> origemAlimentoSituacaoRua;
	
	@JsonInclude(Include.NON_EMPTY)
	private String outraInstituicaoQueAcompanha;
	
	@JsonInclude(Include.NON_NULL)
	private Long quantidadeAlimentacoesAoDiaSituacaoRua;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusAcompanhadoPorOutraInstituicao;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusPossuiReferenciaFamiliar;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusRecebeBeneficio;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusSituacaoRua;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusTemAcessoHigienePessoalSituacaoRua;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusVisitaFamiliarFrequentemente;
	
	@JsonInclude(Include.NON_NULL)
	private Long tempoSituacaoRua;
	
}
