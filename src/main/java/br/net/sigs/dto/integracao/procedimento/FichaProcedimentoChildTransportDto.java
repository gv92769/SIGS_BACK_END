package br.net.sigs.dto.integracao.procedimento;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaProcedimentoChildTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String numProntuario;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cnsCidadao;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cpfCidadao;
	
	@JsonInclude(Include.NON_NULL)
	private Long dtNascimento;
	
	@JsonInclude(Include.NON_NULL)
	private Long sexo;
	
	@JsonInclude(Include.NON_NULL)
	private Long localAtendimento;
	
	@JsonInclude(Include.NON_NULL)
	private Long turno;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean statusEscutaInicialOrientacao;
	
	@JsonInclude(Include.NON_NULL)
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<String> procedimentos;
	
	@JsonInclude(Include.NON_NULL)
	private Long dataHoraInicialAtendimento;
	
	@JsonInclude(Include.NON_NULL)
	private Long dataHoraFinalAtendimento;
	
}
