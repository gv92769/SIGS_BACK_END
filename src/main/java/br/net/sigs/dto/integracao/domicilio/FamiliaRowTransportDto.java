package br.net.sigs.dto.integracao.domicilio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FamiliaRowTransportDto {

	private Long dataNascimentoResponsavel;
	
	@JsonInclude(Include.NON_EMPTY)
	private String numeroCnsResponsavel;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cpfResponsavel;
	
	@JsonInclude(Include.NON_NULL)
	private int numeroMembrosFamilia;
	
	@JsonInclude(Include.NON_EMPTY)
	private String numeroProntuario;
	
	@JsonInclude(Include.NON_NULL)
	private Long rendaFamiliar;
	
	@JsonInclude(Include.NON_NULL)
	private Long resideDesde;
	
	private Boolean stMudanca;
	
}
