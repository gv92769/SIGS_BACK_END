package br.net.sigs.dto.integracao.atividade;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ParticipanteRowItemTransportDto {

	@JsonInclude(Include.NON_EMPTY)
	private String cnsParticipante;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cpfParticipante;
	
	@JsonInclude(Include.NON_NULL)
	private Long dataNascimento;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean avaliacaoAlterada;
	
	@JsonInclude(Include.NON_NULL)
	private Double peso;
	
	@JsonInclude(Include.NON_NULL)
	private Double altura;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean cessouHabitoFumar;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean abandonouGrupo;
	
	@JsonInclude(Include.NON_NULL)
	private Long sexo;
	
}
