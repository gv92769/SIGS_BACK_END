package br.net.sigs.dto.integracao.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class InformacaoTransportDto {

	@JacksonXmlProperty(localName = "xmlns:ns2", isAttribute = true)
	private String ns2 = "http://esus.ufsc.br/dadoinstalacao";
	
	@JacksonXmlProperty(localName = "xmlns:ns3", isAttribute = true)
	private String ns3 = "http://esus.ufsc.br/dadotransporte";
	
	@JsonInclude(Include.NON_EMPTY)
	private String uuidDadoSerializado;
	
	@JsonInclude(Include.NON_NULL)
	private Long tipoDadoSerializado;
	
	@JsonInclude(Include.NON_EMPTY)
	private String codIbge;
	
	@JsonInclude(Include.NON_EMPTY)
	private String cnesDadoSerializado;
	
	@JsonInclude(Include.NON_EMPTY)
	private String ineDadoSerializado;
	
	@JsonInclude(Include.NON_NULL)
	private Long numLote;
	
}
