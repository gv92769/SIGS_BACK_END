package br.net.sigs.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class AtividadeColetivaTabelaDto {

	private Long id;
	
	@JsonFormat(pattern="dd/MM/yyyy")
	private Date data;
	
	private String tipoAtividade;
	
	private String localidade;
	
	private Boolean importada;
	
	private Long idProfissional;
	
	public AtividadeColetivaTabelaDto(Long id, Date data, String descricao, String ubs, String instituicao, String outaLocalidade, Boolean importada, Long idProfissional) {
		setId(id);
		setData(data);
		setTipoAtividade(descricao);
		setLocalidade(outaLocalidade);
		setImportada(importada);
		setIdProfissional(idProfissional);
		
		if (ubs != null)
			setLocalidade(ubs);
		if (instituicao != null)
			setLocalidade(instituicao);
	}
	
}
