package br.net.sigs.dto;

import java.util.List;

import br.net.sigs.entity.domicilio.util.AbastecimentoAgua;
import br.net.sigs.entity.domicilio.util.AguaConsumoDomicilio;
import br.net.sigs.entity.domicilio.util.Animal;
import br.net.sigs.entity.domicilio.util.CondicaoPosseUsoTerra;
import br.net.sigs.entity.domicilio.util.DestinoLixo;
import br.net.sigs.entity.domicilio.util.FormaEscoamentoBanheiroSanitario;
import br.net.sigs.entity.domicilio.util.MaterialPredominanteConstrucao;
import br.net.sigs.entity.domicilio.util.RendaFamiliar;
import br.net.sigs.entity.domicilio.util.SituacaoMoradia;
import br.net.sigs.entity.domicilio.util.TipoAcessoDomicilio;
import br.net.sigs.entity.domicilio.util.TipoDomicilio;
import br.net.sigs.entity.domicilio.util.TipoImovel;
import br.net.sigs.entity.domicilio.util.TipoLogradouro;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FormCadastroDomiciliarDTO {

	private List<AbastecimentoAgua> opAbastecimentoAgua;
	
	private List<AguaConsumoDomicilio> opAguaConsumo;
	
	private List<Animal> opAnimal;
	
	private List<CondicaoPosseUsoTerra> opCondicaoPosse;
	
	private List<DestinoLixo> opDestinoLixo;
	
	private List<FormaEscoamentoBanheiroSanitario> opFormaEscoamento;
	
	private List<MaterialPredominanteConstrucao> opMaterialPredominante;
	
	private List<RendaFamiliar> opRendaFamiliar;
	
	private List<SituacaoMoradia> opSituacaoMoradia;
	
	private List<TipoAcessoDomicilio> opAcessoDomicilio;
	
	private List<TipoDomicilio> opDomicilio;
	
	private List<TipoImovel> opImovel;
	
	private List<TipoLogradouro> opLogradouro;
	
	private String[] opcoesEstados;
	
}
