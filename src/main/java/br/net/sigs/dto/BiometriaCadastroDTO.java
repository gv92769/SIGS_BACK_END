/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.dto;

import br.net.sigs.entity.cidadao.BiometriaCidadao;

/**
 *
 * @author Naisses
 */
public class BiometriaCadastroDTO {
    private String digital1;
    private String digital2;
    private String digital3;
    private String digital4;
    private BiometriaCidadao.Dedo dedo;
    private boolean principal;

    public String getDigital1() {
        return digital1;
    }

    public void setDigital1(String digital1) {
        this.digital1 = digital1;
    }

    public String getDigital2() {
        return digital2;
    }

    public void setDigital2(String digital2) {
        this.digital2 = digital2;
    }

    public String getDigital3() {
        return digital3;
    }

    public void setDigital3(String digital3) {
        this.digital3 = digital3;
    }

    public String getDigital4() {
        return digital4;
    }

    public void setDigital4(String digital4) {
        this.digital4 = digital4;
    }

    public BiometriaCidadao.Dedo getDedo() {
        return dedo;
    }

    public void setDedo(BiometriaCidadao.Dedo dedo) {
        this.dedo = dedo;
    }

    public boolean isPrincipal() {
        return principal;
    }

    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }

    
    
    
    
}
