/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.dto;

import java.util.List;

import br.net.sigs.entity.usuario.Perfil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 *
 * @author Naisses
 */
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class UsuarioDTO {
    
	private Long id;
    
    private String login;
    
    private String senha;
    
    private Boolean ativo;
    
    private String nome;
    
    private String sobrenome;
    
    private String email;
    
    private List<Perfil> perfis;
    
}
