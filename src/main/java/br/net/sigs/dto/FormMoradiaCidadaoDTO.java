package br.net.sigs.dto;

import br.net.sigs.entity.domicilio.util.TipoImovel;
import br.net.sigs.entity.domicilio.util.TipoLogradouro;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FormMoradiaCidadaoDTO {

    private List<TipoImovel> opImovel;

    private List<TipoLogradouro> opLogradouro;

    private String[] opcoesEstados;

}
