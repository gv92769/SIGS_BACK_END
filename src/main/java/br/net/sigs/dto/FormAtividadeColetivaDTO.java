package br.net.sigs.dto;

import java.util.List;

import br.net.sigs.entity.saude.ficha.atividade.util.PraticaSaude;
import br.net.sigs.entity.saude.ficha.atividade.util.PublicoAlvo;
import br.net.sigs.entity.saude.ficha.atividade.util.TemaReuniao;
import br.net.sigs.entity.saude.ficha.atividade.util.TemaSaude;
import br.net.sigs.entity.saude.ficha.atividade.util.TipoAtividadeColetiva;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FormAtividadeColetivaDTO {

	private List<TipoAtividadeColetiva> atividades;
	
	private List<TemaReuniao> temasReuniao;
	
	private List<PublicoAlvo> publicos;
	
	private List<TemaSaude> temasSaude;
	
	private List<PraticaSaude> praticas;
	
}
