package br.net.sigs.dto;

import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.entity.usuario.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class UsuarioProfissionalDto {

	private Usuario usuario;
	
	private Profissional profissional;
	
}
