/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class UsuarioLoginDTO {
    
	private String login;
    
    private String senha;
    
    private String cnes;

    public UsuarioLoginDTO(String login, String senha) {
        this.login = login;
        this.senha = senha;
    }
    
}
