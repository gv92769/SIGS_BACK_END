package br.net.sigs.dto;

import br.net.sigs.entity.cidadao.util.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class InformacoesSituacaoRuaDTO {

    private List<TempoSituacaoRua> listTempoSituacaoRua;

    private List<OrigemAlimentacao> listOrigemAlimentacao;

    private List<QuantasAlimentacao> QuantasAlimentacao;

    private List<AcessoHigiene> listAcessoHigiene;

}
