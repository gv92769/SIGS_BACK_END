package br.net.sigs.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.net.sigs.entity.saude.ficha.exame.TipoAtendimento;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class SolicitacaoTabelaDto {
	
	private Long id;
	
	private Long idProfissional;
	
	private String nome;
	
	private String cpf;
	
	private String cns;
	
	@JsonFormat(pattern="dd/MM/yyyy")
	private Date data;
	
	private String tipoAtendimento; 
	
	private String tipoExame;
	
	private Boolean exportada;
	
	private String tipoSolicitacao;
	
	private String arquivo;

	public SolicitacaoTabelaDto(Long id, Long idProfissional, String nome, String cpf, String cns, Date data, 
			TipoAtendimento tipoAtendimento, String descricao, String tipoSolicitacao, String arquivo) {
		this.id = id;
		this.idProfissional = idProfissional;
		this.nome = nome;
		this.cpf = cpf;
		this.cns = cns;
		this.data = data;
		this.tipoAtendimento = tipoAtendimento != null ? tipoAtendimento.getNome() : null;
		this.tipoExame = descricao;
		this.exportada = false;
		this.tipoSolicitacao = tipoSolicitacao;
		this.arquivo = arquivo;
	}
	
}
