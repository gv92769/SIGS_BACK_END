package br.net.sigs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class DomicilioFilterDto extends PageRequest {

	private String logradouro;
	
	private String numero;
	
	private String complemento;
	
	private String bairro;
	
	private String microarea;
	
	private String cep;
	
	private String cpfCns;
	
}
