package br.net.sigs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class CidadaoFilterDto extends PageRequest {

	private String nome;
	
	private String cpf;
	
	private String cns;
	
	private Boolean ativo;
	
}
