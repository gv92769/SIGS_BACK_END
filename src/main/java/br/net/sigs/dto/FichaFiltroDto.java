package br.net.sigs.dto;

import java.util.Date;

import br.net.sigs.entity.saude.procedimento.ProcedimentoSubGrupo;
import br.net.sigs.entity.usuario.Perfil;
import br.net.sigs.entity.util.Cbo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaFiltroDto extends PageRequest {

	private String nome;
	
	private String cpf;
	
	private String cns;
	
	private Date dataInicio;
	
	private Date dataFinal;
	
	private ProcedimentoSubGrupo tipoExame; 
	
	private String tipoAtendimento;
	
	private Boolean exportada;
	
	private Perfil perfil;
	
	private Cbo cbo;
	
}
