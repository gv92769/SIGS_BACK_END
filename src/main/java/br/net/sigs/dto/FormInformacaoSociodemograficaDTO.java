package br.net.sigs.dto;

import java.util.List;

import br.net.sigs.entity.cidadao.util.CursoMaisElevado;
import br.net.sigs.entity.cidadao.util.Deficiencia;
import br.net.sigs.entity.cidadao.util.IdentidadeGenero;
import br.net.sigs.entity.cidadao.util.OrientacaoSexual;
import br.net.sigs.entity.cidadao.util.PovoComunidadeTradicional;
import br.net.sigs.entity.cidadao.util.RelacaoParentesco;
import br.net.sigs.entity.cidadao.util.ResponsavelCrianca;
import br.net.sigs.entity.cidadao.util.SituacaoMercadoTrabalho;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FormInformacaoSociodemograficaDTO {
	
	private List<RelacaoParentesco> opcaoRelacaoParentesco;
	
	private List<CursoMaisElevado> opcaoCurso;
	
	private List<SituacaoMercadoTrabalho> opcaoSituacaoMercado;
	
	private List<ResponsavelCrianca> opcaoResponsavel;
	
	private List<PovoComunidadeTradicional> opcaoComunidade;
	
	private List<OrientacaoSexual> opcaoOrientacaoSexual;
	
	private List<IdentidadeGenero> opcaoIdentidadeGenero;
	
	private List<Deficiencia> opcaoDeficiencia;

}
