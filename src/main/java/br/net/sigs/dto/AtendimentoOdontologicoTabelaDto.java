package br.net.sigs.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class AtendimentoOdontologicoTabelaDto {

	private Long id;
	
	@JsonFormat(pattern="dd/MM/yyyy")
	private Date data;
	
	private String nome;
	
	private String tipoAtendimento;
	
	private String cpf;
	
	private String cns;
	
}
