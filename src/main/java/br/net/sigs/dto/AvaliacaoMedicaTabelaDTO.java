/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.net.sigs.entity.saude.util.TipoAvaliacaoMedica;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Naisses
 */
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class AvaliacaoMedicaTabelaDTO {
    
	private Long id;
    
	@JsonFormat(pattern="dd/MM/yyyy")
	private Date data;
    
	@JsonFormat(pattern="HH:mm:ss")
	private Date horario;
    
	private String profissional;
    
	private String paciente;
    
	private String tipo;
	
	private Boolean importada;
	
	private Long idProfissional;
	
	private String ocupacao;

    public AvaliacaoMedicaTabelaDTO(Long id, Date data, Date horario, String nome, String sobrenome, String cidadao, TipoAvaliacaoMedica tipoAvaliacaoMedica, 
    		Boolean importada, Long idProfissional) {
        this.id = id;
        this.data = data;
        this.horario = horario;
        this.profissional = nome.concat(" ").concat(sobrenome);
        this.paciente = cidadao;
        this.tipo = tipoAvaliacaoMedica.getNome();
        this.importada = importada;
        this.idProfissional = idProfissional;
    }
    
    public AvaliacaoMedicaTabelaDTO(Long id, Date data, Date horario, String nome, String sobrenome, String cidadao, TipoAvaliacaoMedica tipoAvaliacaoMedica, 
    		Long idProfissional) {
        this.id = id;
        this.data = data;
        this.horario = horario;
        this.profissional = nome.concat(" ").concat(sobrenome);
        this.paciente = cidadao;
        this.tipo = tipoAvaliacaoMedica.getNome();
        this.idProfissional = idProfissional;
    }
    
    public AvaliacaoMedicaTabelaDTO(Long id, Date data, Date horario, String nome, String sobrenome, String cidadao, TipoAvaliacaoMedica tipoAvaliacaoMedica, 
    		Long idProfissional, String ocupacao) {
        this.id = id;
        this.data = data;
        this.horario = horario;
        this.profissional = nome.concat(" ").concat(sobrenome);
        this.paciente = cidadao;
        this.tipo = tipoAvaliacaoMedica.getNome();
        this.idProfissional = idProfissional;
        this.ocupacao = ocupacao;
    }
    
}
