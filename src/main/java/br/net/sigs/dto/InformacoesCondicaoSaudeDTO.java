package br.net.sigs.dto;

import br.net.sigs.entity.cidadao.util.ConsideracaoPeso;
import br.net.sigs.entity.cidadao.util.DoencaCardiaca;
import br.net.sigs.entity.cidadao.util.DoencaRespiratoria;
import br.net.sigs.entity.cidadao.util.DoencaRins;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class InformacoesCondicaoSaudeDTO {

    private List<DoencaCardiaca> listCardiaca;

    private List<DoencaRins> listRins;

    private List<DoencaRespiratoria> listRespiratoria;

    private List<ConsideracaoPeso> consideracaoPeso;

}
