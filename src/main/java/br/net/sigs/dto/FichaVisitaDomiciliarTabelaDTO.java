package br.net.sigs.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FichaVisitaDomiciliarTabelaDTO {

    private Long id;

    @JsonFormat(pattern="dd/MM/yyyy")
    private Date data;

    private String profissional;

    private Long idProfissional;

    private String cidadao;

    private Boolean importada;

}
