package br.net.sigs.dto;

import java.util.List;

import br.net.sigs.entity.cidadao.util.EstadoCivil;
import br.net.sigs.entity.cidadao.util.Nacionalidade;
import br.net.sigs.entity.cidadao.util.RacaCor;
import br.net.sigs.entity.cidadao.util.Sexo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class FormCidadaoGeralDTO {

	private Sexo[] opcoesSexo;
	
	private EstadoCivil[] opcoesEstadoCivil;
	
	private String[] opcoesEstadosBrasil;
	
	private Nacionalidade[] opcoesNacionalidade;
	
	private List<RacaCor> opcoesRacaCor;
	
}
