package br.net.sigs.dto;

import br.net.sigs.entity.saude.procedimento.ProcedimentoGrupo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class SubGrupoFiltroDto {

    private Long id;

	private String codigo;
	
	private ProcedimentoGrupo grupo;

    private String descricao;
    
    private String dtcompetencia;
	
}
