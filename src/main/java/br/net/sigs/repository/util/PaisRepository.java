package br.net.sigs.repository.util;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.util.Pais;

/**
*
* @author Gabriel Vinicius
*/
public interface PaisRepository extends JpaRepository<Pais, Long> {

	@Query(value = "SELECT p FROM Pais p WHERE LOWER(p.nome) LIKE %:nome%")
	public List<Pais> findAllByNome(@Param("nome") String nome, Pageable pabeable);
	
}
