package br.net.sigs.repository.util;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.util.Municipio;

/**
*
* @author Gabriel Vinicius
*/
public interface MunicipioRepository extends JpaRepository<Municipio, Long> {

	@Query(value = "SELECT m FROM Municipio m WHERE m.uf = :uf AND LOWER(m.nome) LIKE %:nome%")
	public List<Municipio> findByUfAndNome(@Param("uf") String uf, @Param("nome") String nome, Pageable pageable);

}
