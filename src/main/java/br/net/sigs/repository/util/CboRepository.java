/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.util;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.util.Cbo;

/**
 *
 * @author Gabriel Vinicius
 */
public interface CboRepository extends JpaRepository<Cbo, Long>{
    
	@Query(value = "SELECT c FROM Cbo c WHERE LOWER(c.titulo) LIKE %:titulo%")
    public List<Cbo> findAllByTitulo(@Param("titulo") String titulo, Pageable pageable);
        
    @Query(value = "SELECT c FROM Perfil p "
    		+ " JOIN p.listCbo c "
    		+ " WHERE LOWER(c.titulo) LIKE %:titulo% AND p.id = :id")
    public List<Cbo> findAllByTituloByPerfil(@Param("titulo") String titulo, @Param("id") Long id, Pageable pageable);
    
    @Query(value = "SELECT l.cbo FROM Lotacao l "
    		+ " WHERE  l.profissional.id = :id")
    public List<Cbo> findAllByProfissional(Long id);

	public Optional<Cbo> findByCodigo(String codigo);
    
}
