package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.AguaConsumoDomicilio;

/**
*
* @author Gabriel Vincius
*/
public interface AguaConsumoDomicilioRepository extends JpaRepository<AguaConsumoDomicilio, Long> {

}
