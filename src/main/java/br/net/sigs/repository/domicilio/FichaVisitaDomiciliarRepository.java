package br.net.sigs.repository.domicilio;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.dto.FichaVisitaDomiciliarTabelaDTO;
import br.net.sigs.entity.domicilio.FichaVisitaDomiciliar;


public interface FichaVisitaDomiciliarRepository extends JpaRepository<FichaVisitaDomiciliar, Long> {

    public FichaVisitaDomiciliar findByUuid(String uuid);

    @Query(value = "SELECT new br.net.sigs.dto.FichaVisitaDomiciliarTabelaDTO(v.id, v.data, u.nome, p.id, "
            + " c.nome, v.importada) FROM FichaVisitaDomiciliar v "
            + " JOIN v.cidadao c JOIN v.profissionalResponsavel p JOIN p.usuario u "
            + " WHERE (:nome IS NULL OR LOWER(c.nome) LIKE %:nome%) AND  (:cpf IS NULL OR c.cpf = :cpf) AND (:cns IS NULL OR c.cns = :cns) "
            + " AND (:inicio IS NULL OR v.data >= :inicio) AND (:fim IS NULL OR v.data <= :fim) "
            + " AND v.importada = :exportada")
    public Page<FichaVisitaDomiciliarTabelaDTO> search(@Param("nome") String nome, @Param("cpf") String cpf, @Param("cns") String cns,
                                                           @Param("inicio") Date inicio, @Param("fim") Date fim, @Param("exportada") Boolean exportada, Pageable pageable);

    public List<FichaVisitaDomiciliar> findAllByImportada(Boolean importada);

    public Long countByImportada(boolean importada);
}
