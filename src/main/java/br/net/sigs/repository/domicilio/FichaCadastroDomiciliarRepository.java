package br.net.sigs.repository.domicilio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.Domicilio;
import br.net.sigs.entity.domicilio.FichaCadastroDomiciliar;

/**
*
* @author Gabriel Vincius
*/
public interface FichaCadastroDomiciliarRepository extends JpaRepository<FichaCadastroDomiciliar, String> {

	List<FichaCadastroDomiciliar> findByImportada(boolean importada);

	Long countByImportada(boolean importada);
	
	List<FichaCadastroDomiciliar> findAllByDomicilio(Domicilio domicilio);
	
}
