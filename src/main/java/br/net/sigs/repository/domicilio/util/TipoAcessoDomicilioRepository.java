package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.TipoAcessoDomicilio;

/**
*
* @author Gabriel Vincius
*/
public interface TipoAcessoDomicilioRepository extends JpaRepository<TipoAcessoDomicilio, Long> {

}
