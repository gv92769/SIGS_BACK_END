package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.TipoDomicilio;

/**
*
* @author Gabriel Vincius
*/
public interface TipoDomicilioRepository extends JpaRepository<TipoDomicilio, Long> {

}
