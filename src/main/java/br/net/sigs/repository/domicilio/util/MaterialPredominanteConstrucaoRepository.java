package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.MaterialPredominanteConstrucao;

/**
*
* @author Gabriel Vincius
*/
public interface MaterialPredominanteConstrucaoRepository extends JpaRepository<MaterialPredominanteConstrucao, Long> {

}
