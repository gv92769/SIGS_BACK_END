package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.AbastecimentoAgua;

/**
*
* @author Gabriel Vincius
*/
public interface AbastecimentoAguaRepository extends JpaRepository<AbastecimentoAgua, Long> {
	
}
