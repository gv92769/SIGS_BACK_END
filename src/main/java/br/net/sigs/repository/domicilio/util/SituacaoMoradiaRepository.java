package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.SituacaoMoradia;

/**
*
* @author Gabriel Vincius
*/
public interface SituacaoMoradiaRepository extends JpaRepository<SituacaoMoradia, Long> {

}
