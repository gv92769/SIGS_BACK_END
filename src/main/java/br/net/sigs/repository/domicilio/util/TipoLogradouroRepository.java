package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.TipoLogradouro;

/**
*
* @author Gabriel Vincius
*/
public interface TipoLogradouroRepository extends JpaRepository<TipoLogradouro, String> {

}
