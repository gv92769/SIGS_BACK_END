package br.net.sigs.repository.domicilio.util;

import br.net.sigs.entity.domicilio.Desfecho;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DesfechoRepository extends JpaRepository<Desfecho, Long> {
}
