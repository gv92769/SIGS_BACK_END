package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.CondicaoPosseUsoTerra;

/**
*
* @author Gabriel Vincius
*/
public interface CondicaoPosseUsoTerraRepository extends JpaRepository<CondicaoPosseUsoTerra, Long> {

}
