package br.net.sigs.repository.domicilio.util;


import br.net.sigs.entity.domicilio.MotivoVisita;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MotivoVisitaRepository extends JpaRepository<MotivoVisita, Long> {


}
