package br.net.sigs.repository.domicilio;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.Familia;

/**
*
* @author Gabriel Vincius
*/
public interface FamiliaRepository extends JpaRepository<Familia, Long> {

}
