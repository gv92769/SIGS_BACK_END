package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.DestinoLixo;

/**
*
* @author Gabriel Vincius
*/
public interface DestinoLixoRepository extends JpaRepository<DestinoLixo, Long> {

}
