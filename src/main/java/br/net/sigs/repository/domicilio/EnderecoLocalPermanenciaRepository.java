package br.net.sigs.repository.domicilio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.domicilio.EnderecoLocalPermanencia;

/**
*
* @author Gabriel Vincius
*/
public interface EnderecoLocalPermanenciaRepository extends JpaRepository<EnderecoLocalPermanencia, Long> {

	@Query(value = "SELECT new EnderecoLocalPermanencia(e.id, e.nomeLogradouro, e.numero, e.complemento, e.bairro, e.microarea) FROM Domicilio d "
			+ " JOIN d.endereco e JOIN d.familias f"
			+ " WHERE (:logradouro IS NULL OR LOWER(e.nomeLogradouro) LIKE %:logradouro%) AND (:numero IS NULL OR e.numero = :numero) AND "
			+ " (:complemento IS NULL OR LOWER(e.complemento) LIKE %:complemento%) AND (:bairro IS NULL OR LOWER(e.bairro) LIKE %:bairro%) "
			+ " AND (:microarea IS NULL OR e.microarea = :microarea) AND (:cep IS NULL OR e.cep = :cep) "
			+ " AND (:cpfCns IS NULL OR (f.cidadao.cpf = :cpfCns OR f.cidadao.cns = :cpfCns))")
	public Page<EnderecoLocalPermanencia> search(@Param("logradouro") String logradouro, @Param("numero") String numero, @Param("complemento") String complemento, 
			@Param("bairro") String bairro, @Param("microarea") String microarea, @Param("cep") String cep, @Param("cpfCns") String cpfCns, Pageable pageable);
	
}
