package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.Animal;

/**
*
* @author Gabriel Vincius
*/
public interface AnimalRepository extends JpaRepository<Animal, Long> {

}
