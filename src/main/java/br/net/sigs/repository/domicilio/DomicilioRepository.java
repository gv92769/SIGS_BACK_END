package br.net.sigs.repository.domicilio;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.Domicilio;

/**
*
* @author Gabriel Vincius
*/
public interface DomicilioRepository extends JpaRepository<Domicilio, Long> {

}
