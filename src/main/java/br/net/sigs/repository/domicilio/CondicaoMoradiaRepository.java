package br.net.sigs.repository.domicilio;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.CondicaoMoradia;

/**
*
* @author Gabriel Vincius
*/
public interface CondicaoMoradiaRepository extends JpaRepository<CondicaoMoradia, Long> {

}
