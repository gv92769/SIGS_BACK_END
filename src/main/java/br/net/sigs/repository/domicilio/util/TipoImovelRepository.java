package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.TipoImovel;

/**
*
* @author Gabriel Vincius
*/
public interface TipoImovelRepository extends JpaRepository<TipoImovel, Long> {

}
