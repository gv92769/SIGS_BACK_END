package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.FormaEscoamentoBanheiroSanitario;

/**
*
* @author Gabriel Vincius
*/
public interface FormaEscoamentoBanheiroSanitarioRepository extends JpaRepository<FormaEscoamentoBanheiroSanitario, Long> {

}
