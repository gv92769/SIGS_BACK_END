package br.net.sigs.repository.domicilio;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.InstituicaoPermanencia;

/**
*
* @author Gabriel Vincius
*/
public interface InstituicaoPermanenciaRepository extends JpaRepository<InstituicaoPermanencia, Long> {

}
