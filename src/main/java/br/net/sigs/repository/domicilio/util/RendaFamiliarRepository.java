package br.net.sigs.repository.domicilio.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.domicilio.util.RendaFamiliar;

/**
*
* @author Gabriel Vincius
*/
public interface RendaFamiliarRepository extends JpaRepository<RendaFamiliar, Long> {

}
