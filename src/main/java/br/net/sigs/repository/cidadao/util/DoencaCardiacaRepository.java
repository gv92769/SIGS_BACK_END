package br.net.sigs.repository.cidadao.util;

import br.net.sigs.entity.cidadao.util.DoencaCardiaca;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoencaCardiacaRepository extends JpaRepository<DoencaCardiaca, Long> {
}
