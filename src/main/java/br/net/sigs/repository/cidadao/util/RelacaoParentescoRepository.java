package br.net.sigs.repository.cidadao.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.util.RelacaoParentesco;

/**
*
* @author Gabriel Vinicius
*/
public interface RelacaoParentescoRepository extends JpaRepository<RelacaoParentesco, Long> {

}
