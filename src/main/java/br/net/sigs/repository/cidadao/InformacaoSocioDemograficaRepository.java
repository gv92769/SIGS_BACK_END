package br.net.sigs.repository.cidadao;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.InformacaoSocioDemografica;

/**
*
* @author Gabriel Vinicius
*/
public interface InformacaoSocioDemograficaRepository extends JpaRepository<InformacaoSocioDemografica, Long> {

}
