package br.net.sigs.repository.cidadao.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.util.OrientacaoSexual;

/**
*
* @author Gabriel Vinicius
*/
public interface OrientacaoSexualRepository extends JpaRepository<OrientacaoSexual, Long> {

}
