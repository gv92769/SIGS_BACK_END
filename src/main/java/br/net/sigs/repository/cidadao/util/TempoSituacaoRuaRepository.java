package br.net.sigs.repository.cidadao.util;

import br.net.sigs.entity.cidadao.util.TempoSituacaoRua;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TempoSituacaoRuaRepository extends JpaRepository<TempoSituacaoRua, Long> {
}
