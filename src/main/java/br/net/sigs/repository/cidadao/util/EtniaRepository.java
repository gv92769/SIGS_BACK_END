package br.net.sigs.repository.cidadao.util;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.cidadao.util.Etnia;

/**
*
* @author Gabriel Vinicius
*/
public interface EtniaRepository extends JpaRepository<Etnia, Long> {

	@Query(value = "SELECT e FROM Etnia e WHERE LOWER(e.descricao) LIKE %:descricao%")
	public List<Etnia> findAllByNome(@Param("descricao") String descricao, Pageable pageable);
	
}
