/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.cidadao;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.MoradiaCidadao;

/**
 *
 * @author Naisses
 */
public interface MoradiaCidadaoRepository extends JpaRepository<MoradiaCidadao, Long>{
    
}
