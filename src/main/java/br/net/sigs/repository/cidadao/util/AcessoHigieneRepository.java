package br.net.sigs.repository.cidadao.util;


import br.net.sigs.entity.cidadao.util.AcessoHigiene;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcessoHigieneRepository extends JpaRepository<AcessoHigiene, Long> {
}
