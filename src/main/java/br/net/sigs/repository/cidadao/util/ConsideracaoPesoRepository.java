package br.net.sigs.repository.cidadao.util;

import br.net.sigs.entity.cidadao.util.ConsideracaoPeso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsideracaoPesoRepository extends JpaRepository<ConsideracaoPeso, Long> {
}
