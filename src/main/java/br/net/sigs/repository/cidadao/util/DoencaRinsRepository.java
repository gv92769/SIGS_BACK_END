package br.net.sigs.repository.cidadao.util;

import br.net.sigs.entity.cidadao.util.DoencaRins;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoencaRinsRepository extends JpaRepository<DoencaRins, Long> {
}
