package br.net.sigs.repository.cidadao.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.util.PovoComunidadeTradicional;

/**
*
* @author Gabriel Vinicius
*/
public interface PovoComunidadeTradicionalRepository extends JpaRepository<PovoComunidadeTradicional, Long> {

}
