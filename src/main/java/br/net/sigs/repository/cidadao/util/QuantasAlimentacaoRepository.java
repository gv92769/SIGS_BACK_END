package br.net.sigs.repository.cidadao.util;

import br.net.sigs.entity.cidadao.util.QuantasAlimentacao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuantasAlimentacaoRepository extends JpaRepository<QuantasAlimentacao, Long> {
}
