package br.net.sigs.repository.cidadao.util;

import br.net.sigs.entity.cidadao.util.OrigemAlimentacao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrigemAlimentacaoRepository extends JpaRepository<OrigemAlimentacao, Long> {
}
