package br.net.sigs.repository.cidadao.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.util.CursoMaisElevado;

/**
*
* @author Gabriel Vinicius
*/
public interface CursoMaisElevadoRepository extends JpaRepository<CursoMaisElevado, Long> {

}
