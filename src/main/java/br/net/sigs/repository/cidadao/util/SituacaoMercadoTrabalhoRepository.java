package br.net.sigs.repository.cidadao.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.util.SituacaoMercadoTrabalho;

/**
*
* @author Gabriel Vinicius
*/
public interface SituacaoMercadoTrabalhoRepository extends JpaRepository<SituacaoMercadoTrabalho, Long> {

}
