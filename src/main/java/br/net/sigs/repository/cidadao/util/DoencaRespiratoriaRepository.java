package br.net.sigs.repository.cidadao.util;

import br.net.sigs.entity.cidadao.util.DoencaRespiratoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoencaRespiratoriaRepository extends JpaRepository<DoencaRespiratoria, Long> {
}
