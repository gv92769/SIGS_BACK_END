/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.cidadao;

import br.net.sigs.entity.cidadao.BiometriaCidadao;
import br.net.sigs.entity.cidadao.Cidadao;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Naisses
 */
public interface BiometriaCidadaoRepository extends JpaRepository<BiometriaCidadao, Long>{
    
    public BiometriaCidadao findByCidadao(Cidadao cidadao);
    
}
