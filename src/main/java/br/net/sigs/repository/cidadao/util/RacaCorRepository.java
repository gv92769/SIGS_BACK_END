package br.net.sigs.repository.cidadao.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.util.RacaCor;

/**
*
* @author Gabriel Vinicius
*/
public interface RacaCorRepository extends JpaRepository<RacaCor, Long> {

}
