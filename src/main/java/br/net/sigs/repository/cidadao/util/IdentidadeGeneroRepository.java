package br.net.sigs.repository.cidadao.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.util.IdentidadeGenero;

/**
*
* @author Gabriel Vinicius
*/
public interface IdentidadeGeneroRepository extends JpaRepository<IdentidadeGenero, Long> {

}
