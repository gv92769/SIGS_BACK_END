package br.net.sigs.repository.cidadao.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.util.Deficiencia;

/**
*
* @author Gabriel Vinicius
*/
public interface DeficienciaRepository extends JpaRepository<Deficiencia, Long> {

}
