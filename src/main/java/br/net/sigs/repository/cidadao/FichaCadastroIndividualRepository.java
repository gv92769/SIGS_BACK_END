package br.net.sigs.repository.cidadao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.cidadao.FichaCadastroIndividual;

/**
*
* @author Gabriel Vinicius
*/
public interface FichaCadastroIndividualRepository extends JpaRepository<FichaCadastroIndividual, String> {

	public List<FichaCadastroIndividual> findByImportada(Boolean importada);
	
	Long countByImportada(boolean importada);
	
	List<FichaCadastroIndividual> findAllByCidadao(Cidadao cidadao);
	
}
