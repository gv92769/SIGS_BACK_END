/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.cidadao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.cidadao.Cidadao;

/**
 *
 * @author Naisses
 */
public interface CidadaoRepository extends JpaRepository<Cidadao,Long>{
    
    @Query(value = "SELECT new Cidadao(c.id, c.nome, c.cpf, c.cns) FROM Cidadao c "
    		+ " WHERE (:nome IS NULL OR LOWER(c.nome) LIKE %:nome%) AND (:cpf IS NULL OR c.cpf LIKE :cpf%) "
    		+ " AND (:cns IS NULL OR c.cns LIKE :cns%) AND c.ativo = :ativo")
    public Page<Cidadao> search(@Param("nome") String nome, @Param("cpf") String cpf, @Param("cns") String cns, @Param("ativo") Boolean ativo, Pageable pageable);
    
	public Cidadao findByCns(String cns);
	
	public Cidadao findByCpf(String cpf);
	
	@Query(value = "SELECT c FROM Cidadao c "
    		+ " WHERE (:valor IS NULL OR ( c.cpf LIKE :valor% OR c.cns LIKE :valor%)) AND c.ativo = true")
	public List<Cidadao> findByCnsOrCpf(@Param("valor") String valor, Pageable pageable);
	
	@Query(value = "SELECT c FROM Cidadao c "
    		+ " WHERE (:valor IS NULL OR ( c.cpf LIKE :valor% OR c.cns LIKE :valor% OR c.nome LIKE :valor%)) AND c.ativo = true")
	public List<Cidadao> findByCnsOrCpfOrNome(@Param("valor") String valor, Pageable pageable);
    
}
