package br.net.sigs.repository.cidadao;

import br.net.sigs.entity.cidadao.CondicaoSaude;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CondicaoSaudeRepository extends JpaRepository<CondicaoSaude, Long> {
}
