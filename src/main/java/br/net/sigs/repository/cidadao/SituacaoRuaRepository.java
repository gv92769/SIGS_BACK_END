package br.net.sigs.repository.cidadao;

import br.net.sigs.entity.cidadao.SituacaoRua;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SituacaoRuaRepository extends JpaRepository<SituacaoRua, Long> {
}
