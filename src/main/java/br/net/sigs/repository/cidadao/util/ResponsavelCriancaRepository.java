package br.net.sigs.repository.cidadao.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.cidadao.util.ResponsavelCrianca;

/**
*
* @author Gabriel Vinicius
*/
public interface ResponsavelCriancaRepository extends JpaRepository<ResponsavelCrianca, Long> {

}
