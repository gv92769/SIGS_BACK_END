package br.net.sigs.repository.usuario;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.usuario.Perfil;

/**
*
* @author Gabriel Vincius
*/
public interface PerfilRepository extends JpaRepository<Perfil, Long> {

	public List<Perfil> findAllByAtivo(Boolean ativo);
	
	public Perfil findByPerfil(String perfil);
	
}
