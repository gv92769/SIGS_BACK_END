/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.usuario;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.usuario.Profissional;

/**
 *
 * @author Naisses
 */
public interface ProfissionalRepository extends JpaRepository<Profissional, Long> {

	public Profissional findByCns(String cns);
	
	public Profissional findByCpf(String cpf);
	
	@Query(value = "SELECT p FROM Profissional p "
			+ " JOIN p.lotacoes l"
			+ " WHERE p.id = :id AND l.ubs = :ubs")
	public Profissional findByIdAndListUbs(@Param("id") Long id, @Param("ubs") UnidadeBasicaSaude unidadeBasicaSaude);
	
	@Query(value = "SELECT new Profissional(p.id, u.nome, u.sobrenome) FROM Profissional p "
			+ " JOIN p.usuario u "
			+ " WHERE (LOWER(u.nome) LIKE %:valor% OR LOWER(u.sobrenome) LIKE %:valor%) OR p.cns LIKE %:valor% OR p.cpf LIKE %:valor%")
	public List<Profissional> search(@Param("valor") String valor, Pageable pageable);
	
	@Query(value = "SELECT new Profissional(p.id, u.nome, u.sobrenome) FROM Profissional p "
			+ " JOIN p.usuario u "
			+ " WHERE p.id = :id")
	public Profissional findByIdByLotacao(@Param("id") Long id);
    
}
