/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.usuario;

import br.net.sigs.entity.usuario.Usuario;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Naisses
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
    
   public  Optional<Usuario> findByLogin(String login);
    
    @Query(value = "SELECT new Usuario(u.id, u.nome, u.sobrenome, u.login, u.ativo) FROM Usuario u "
    		+ "	WHERE (:nome IS NULL OR LOWER(u.nome) LIKE %:nome% OR LOWER(u.sobrenome) LIKE %:nome%) "
    		+ " AND ( SELECT COUNT(*) FROM Profissional p WHERE (:cpf IS NULL OR p.cpf = :cpf) "
    		+ " AND (:cns IS NULL OR p.cns = :cns) AND p.id = u.id) = 1 AND u.ativo = :ativo")
    public Page<Usuario> search(@Param("nome") String nome, @Param("cpf") String cpf,  @Param("cns") String cns, @Param("ativo") Boolean ativo, Pageable pageable);
    
}
