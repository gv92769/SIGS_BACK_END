package br.net.sigs.repository.usuario;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.usuario.Lotacao;

/**
*
* @author Gabriel
*/
public interface LotacaoRepository extends JpaRepository<Lotacao, Long> {

	@Query(value = "SELECT new Lotacao(l.id, l.ubs.nome, l.cbo.titulo, l.perfil.descricao, l.ativo) FROM Lotacao l "
			+ " JOIN l.profissional p "
			+ " WHERE p.id = :id")
	public List<Lotacao> search(@Param("id") Long id);
	
	@Query(value = "SELECT new Lotacao(l.id, l.ubs.nome, l.cbo.titulo, l.perfil.descricao) FROM Lotacao l "
			+ " JOIN l.profissional p "
			+ " WHERE p.id = :id AND l.ativo = true ")
	public List<Lotacao> findAllByIdByAtivo(@Param("id") Long id);
	
}
