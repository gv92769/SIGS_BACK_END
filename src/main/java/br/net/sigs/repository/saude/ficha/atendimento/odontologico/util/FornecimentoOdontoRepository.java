package br.net.sigs.repository.saude.ficha.atendimento.odontologico.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.FornecimentoOdonto;

/**
*
* @author Gabriel Vinicius
*/
public interface FornecimentoOdontoRepository extends JpaRepository<FornecimentoOdonto, Long> {

}
