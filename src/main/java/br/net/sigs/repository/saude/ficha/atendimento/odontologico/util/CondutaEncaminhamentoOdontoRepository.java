package br.net.sigs.repository.saude.ficha.atendimento.odontologico.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.CondutaEncaminhamentoOdonto;

/**
*
* @author Gabriel Vinicius
*/
public interface CondutaEncaminhamentoOdontoRepository extends JpaRepository<CondutaEncaminhamentoOdonto, Long> {

}
