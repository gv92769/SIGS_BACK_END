package br.net.sigs.repository.saude.ficha.atendimento.odontologico.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.ProcedimentoOdonto;

/**
*
* @author Gabriel Vinicius
*/
public interface ProcedimentoOdontoRepository extends JpaRepository<ProcedimentoOdonto, Long> {

}
