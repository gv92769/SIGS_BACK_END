package br.net.sigs.repository.saude.ficha.procedimento;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.procedimento.ProcedimentoFixo;

/**
*
* @author Gabriel Vinicius
*/
public interface ProcedimentoFixoRepository extends JpaRepository<ProcedimentoFixo, Integer> {

}
