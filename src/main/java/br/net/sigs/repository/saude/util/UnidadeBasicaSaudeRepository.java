/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.saude.util;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;

/**
 *
 * @author Naisses
 */
public interface UnidadeBasicaSaudeRepository extends JpaRepository<UnidadeBasicaSaude, Integer>{
    
    public UnidadeBasicaSaude findByCnes(String cnes);
    
    @Query(value = "SELECT u FROM UnidadeBasicaSaude u WHERE LOWER(u.nome) LIKE %:nome%")
    public List<UnidadeBasicaSaude> findByNomeContaining(@Param("nome") String nome, Pageable pageable);
    
    public List<UnidadeBasicaSaude> findByBairroContaining(String bairro);
}
