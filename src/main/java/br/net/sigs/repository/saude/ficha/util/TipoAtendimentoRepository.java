package br.net.sigs.repository.saude.ficha.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.util.TipoAtendimento;

/**
*
* @author Gabriel Vinicius
*/
public interface TipoAtendimentoRepository extends JpaRepository<TipoAtendimento, Long> {

}
