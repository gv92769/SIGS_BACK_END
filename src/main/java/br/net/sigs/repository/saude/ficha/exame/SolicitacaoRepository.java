package br.net.sigs.repository.saude.ficha.exame;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.dto.SolicitacaoTabelaDto;
import br.net.sigs.entity.saude.ficha.exame.Solicitacao;
import br.net.sigs.entity.saude.ficha.exame.TipoAtendimento;
import br.net.sigs.entity.saude.procedimento.ProcedimentoSubGrupo;

/**
*
* @author Gabriel Vinicius
*/
public interface SolicitacaoRepository extends JpaRepository<Solicitacao, Long> {

	@Query(value = "SELECT new br.net.sigs.dto.SolicitacaoTabelaDto(s.id, p.id, c.nome, c.cpf, c.cns, s.data, s.tipoAtendimento,"
			+ " sg.descricao, s.tipoSolicitacao, s.nameFile) FROM Solicitacao s "
			+ " JOIN s.cidadao c JOIN s.profissional p JOIN p.usuario u JOIN s.subGrupo sg"
			+ " WHERE (:nome IS NULL OR LOWER(c.nome) LIKE %:nome%) AND  (:cpf IS NULL OR c.cpf = :cpf) AND (:cns IS NULL OR c.cns = :cns) "
			+ " AND (:inicio IS NULL OR s.data >= :inicio) AND (:fim IS NULL OR s.data <= :fim) "
			+ " AND (:subgrupo IS NULL OR sg = :subgrupo) AND (:tipoAtendimento IS NULL OR s.tipoAtendimento = :tipoAtendimento) ")
	public Page<SolicitacaoTabelaDto> search(@Param("nome") String nome, @Param("cpf") String cpf, @Param("cns") String cns, 
			@Param("inicio") Date inicio, @Param("fim") Date fim, @Param("subgrupo") ProcedimentoSubGrupo subgrupo, 
			@Param("tipoAtendimento") TipoAtendimento tipoAtendimento, Pageable pageable);
	
}
