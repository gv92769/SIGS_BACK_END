/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.saude.ficha.util;

import br.net.sigs.dto.AvaliacaoMedicaTabelaDTO;
import br.net.sigs.entity.saude.ficha.util.AvaliacaoMedica;
import br.net.sigs.entity.saude.util.TipoAvaliacaoMedica;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Naisses
 */
public interface AvaliacaoMedicaRepository extends JpaRepository<AvaliacaoMedica, Long> {
	
	@Query(value = "SELECT new br.net.sigs.dto.AvaliacaoMedicaTabelaDTO(a.id, a.dataRealizacao, a.dataHorarioInicio, u.nome, u.sobrenome, "
			+ " c.nome, a.tipoAvaliacaoMedica, false, p.id) FROM AvaliacaoMedica a JOIN a.cidadao c JOIN a.profissional p JOIN p.usuario u "
			+ " WHERE (:nome IS NULL OR LOWER(c.nome) LIKE %:nome%) AND  (:cpf IS NULL OR c.cpf = :cpf) AND (:cns IS NULL OR c.cns = :cns) "
			+ " AND (:inicio IS NULL OR a.dataRealizacao >= :inicio) AND (:fim IS NULL OR a.dataRealizacao <= :fim) AND a.tipoAvaliacaoMedica = :tipo")
	public Page<AvaliacaoMedicaTabelaDTO> search(@Param("nome") String nome, @Param("cpf") String cpf, @Param("cns") String cns, 
			@Param("inicio") Date inicio, @Param("fim") Date fim, @Param("tipo") TipoAvaliacaoMedica tipo,  Pageable pageable);
    
    public List<AvaliacaoMedica> findByDataRealizacaoAfter(LocalDate data);
    
    public Long countByDataRealizacaoAfter(Date data);
}
