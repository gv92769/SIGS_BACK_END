/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.saude.ficha.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.util.LocalAtendimento;

/**
 *
 * @author Gustavo Miranda
 */
public interface LocalAtendimentoRepository extends JpaRepository<LocalAtendimento, Long>{
    
}