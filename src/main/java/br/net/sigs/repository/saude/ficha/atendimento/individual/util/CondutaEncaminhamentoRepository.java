package br.net.sigs.repository.saude.ficha.atendimento.individual.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atendimento.individual.util.CondutaEncaminhamento;

/**
*
* @author Gabriel Vincius
*/
public interface CondutaEncaminhamentoRepository extends JpaRepository<CondutaEncaminhamento, Long> {

}
