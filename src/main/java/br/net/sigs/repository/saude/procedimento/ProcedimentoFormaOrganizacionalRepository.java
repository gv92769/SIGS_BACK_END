package br.net.sigs.repository.saude.procedimento;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.procedimento.ProcedimentoFormaOrganizacional;
import br.net.sigs.entity.saude.procedimento.ProcedimentoSubGrupo;

/**
*
* @author Gabriel Vincius
*/
public interface ProcedimentoFormaOrganizacionalRepository extends JpaRepository<ProcedimentoFormaOrganizacional, Long> {

	public List<ProcedimentoFormaOrganizacional> findAllBySubGrupo(ProcedimentoSubGrupo subgrupo);
	
}
