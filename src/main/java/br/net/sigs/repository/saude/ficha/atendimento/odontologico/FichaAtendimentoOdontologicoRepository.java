package br.net.sigs.repository.saude.ficha.atendimento.odontologico;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.dto.AtendimentoOdontologicoTabelaDto;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.FichaAtendimentoOdontologico;

/**
*
* @author Gabriel Vinicius
*/
public interface FichaAtendimentoOdontologicoRepository extends JpaRepository<FichaAtendimentoOdontologico, Long> {

	@Query(value = "SELECT new br.net.sigs.dto.AtendimentoOdontologicoTabelaDto(f.id, f.data, c.nome, tp.descricao, c.cpf, c.cns) FROM FichaAtendimentoOdontologico f "
			+ " JOIN f.cidadao c JOIN f.tipoAtendimento tp WHERE LOWER(c.nome) LIKE %:nome% GROUP BY f.id")
	public Page<AtendimentoOdontologicoTabelaDto> findAllByNome(@Param("nome") String nome, Pageable pageable);

	@Query(value = "SELECT new br.net.sigs.dto.AtendimentoOdontologicoTabelaDto(f.id, f.data, c.nome, tp.descricao, c.cpf, c.cns) FROM FichaAtendimentoOdontologico f "
			+ " JOIN f.cidadao c JOIN f.tipoAtendimento tp WHERE c.cpf = :cpf GROUP BY f.id")
	public Page<AtendimentoOdontologicoTabelaDto> findAllByCpf(@Param("cpf") String cpf, Pageable pageable);
	
	public FichaAtendimentoOdontologico findByUuid(String uuid);
	
	public List<FichaAtendimentoOdontologico> findAllByImportada(Boolean importada);
	
	public Long countByImportada(Boolean Importada);
	
}
