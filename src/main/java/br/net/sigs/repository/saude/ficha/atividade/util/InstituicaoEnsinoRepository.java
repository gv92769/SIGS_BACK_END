package br.net.sigs.repository.saude.ficha.atividade.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atividade.util.InstituicaoEnsino;

/**
*
* @author Naisses
*/
public interface InstituicaoEnsinoRepository extends JpaRepository<InstituicaoEnsino, Long> {

	public InstituicaoEnsino findByInep(Long inep);
	
}
