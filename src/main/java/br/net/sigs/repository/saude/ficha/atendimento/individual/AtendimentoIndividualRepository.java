/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.saude.ficha.atendimento.individual;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.dto.AvaliacaoMedicaTabelaDTO;
import br.net.sigs.entity.saude.ficha.atendimento.individual.AtendimentoIndividual;

/**
 *
 * @author Naisses
 */
public interface AtendimentoIndividualRepository extends JpaRepository<AtendimentoIndividual, Long>{
    
	@Query(value = "SELECT new br.net.sigs.dto.AvaliacaoMedicaTabelaDTO(a.id, a.dataRealizacao, a.dataHorarioInicio, u.nome, u.sobrenome, "
			+ " c.nome, a.tipoAvaliacaoMedica, f.importada, p.id) FROM AtendimentoIndividual a "
			+ " JOIN a.cidadao c JOIN a.profissional p JOIN p.usuario u JOIN a.ficha f"
			+ " WHERE (:nome IS NULL OR LOWER(c.nome) LIKE %:nome%) AND  (:cpf IS NULL OR c.cpf = :cpf) AND (:cns IS NULL OR c.cns = :cns) "
			+ " AND (:inicio IS NULL OR a.dataRealizacao >= :inicio) AND (:fim IS NULL OR a.dataRealizacao <= :fim) "
			+ " AND f.importada = :exportada")
	public Page<AvaliacaoMedicaTabelaDTO> search(@Param("nome") String nome, @Param("cpf") String cpf, @Param("cns") String cns, 
			@Param("inicio") Date inicio, @Param("fim") Date fim,  @Param("exportada") Boolean exportada, Pageable pageable);
	
}
