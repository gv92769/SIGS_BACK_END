package br.net.sigs.repository.saude.ficha.exame;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.exame.Documento;

/**
*
* @author Gabriel Vinicius
*/
public interface DocumentoRepository extends JpaRepository<Documento, Long> {

}
