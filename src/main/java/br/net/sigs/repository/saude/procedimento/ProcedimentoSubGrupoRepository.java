package br.net.sigs.repository.saude.procedimento;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.saude.procedimento.ProcedimentoGrupo;
import br.net.sigs.entity.saude.procedimento.ProcedimentoSubGrupo;

/**
*
* @author Gabriel Vincius
*/
public interface ProcedimentoSubGrupoRepository extends JpaRepository<ProcedimentoSubGrupo, Long> {

	public List<ProcedimentoSubGrupo> findAllByGrupo(ProcedimentoGrupo grupo);
	
	@Query(value = "SELECT new ProcedimentoSubGrupo(p.id, p.codigo, p.grupo, p.descricao, p.dtcompetencia) "
			+ " FROM ProcedimentoSubGrupo p JOIN p.grupo g"
			+ " WHERE (:grupo IS NULL OR g.id = :grupo) "
			+ " AND (:grupo IS NULL AND g.id IN :ids) "
			+ " AND (LOWER(p.descricao) LIKE %:texto% OR LOWER(p.codigo) LIKE %:texto%) " )
	public List<ProcedimentoSubGrupo> search(@Param("grupo") Long grupo, @Param("ids") List<Long> ids, Pageable pageable);
	
}
