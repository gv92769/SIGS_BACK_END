package br.net.sigs.repository.saude.ficha.atividade.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atividade.ParticipanteAtividadeColetiva;

/**
*
* @author Gabriel Vinicius
*/
public interface ParticipanteAtividadeColetivaRepository extends JpaRepository<ParticipanteAtividadeColetiva, Long> {

}
