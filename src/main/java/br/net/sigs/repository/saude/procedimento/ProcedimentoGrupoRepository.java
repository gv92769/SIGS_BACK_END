package br.net.sigs.repository.saude.procedimento;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.procedimento.ProcedimentoGrupo;

/**
*
* @author Gabriel Vincius
*/
public interface ProcedimentoGrupoRepository extends JpaRepository<ProcedimentoGrupo, Long> {

}
