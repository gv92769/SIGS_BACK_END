package br.net.sigs.repository.saude.ficha.atendimento.odontologico.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.ProcedimentoRealizado;

/**
*
* @author Gabriel Vinicius
*/
public interface ProcedimentoRealizadoRepository extends JpaRepository<ProcedimentoRealizado, Long> {

}
