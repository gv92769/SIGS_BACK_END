package br.net.sigs.repository.saude.ficha.exame;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.exame.ResultadoExame;

/**
*
* @author Gabriel Vinicius
*/
public interface ResultadoExameRepository extends JpaRepository<ResultadoExame, Long> {

}
