/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.saude.ficha.evolucao;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.dto.AvaliacaoMedicaTabelaDTO;
import br.net.sigs.entity.saude.ficha.evolucao.Evolucao;
import br.net.sigs.entity.util.Cbo;

/**
 *
 * @author Naisses
 */
public interface EvolucaoRepository extends JpaRepository<Evolucao, Long>{
    
	@Query(value = "SELECT DISTINCT new br.net.sigs.dto.AvaliacaoMedicaTabelaDTO(a.id, a.dataRealizacao, a.dataHorarioInicio, u.nome, u.sobrenome, "
			+ " c.nome, a.tipoAvaliacaoMedica, p.id) FROM Evolucao a "
			+ " JOIN a.cidadao c JOIN a.profissional p JOIN p.usuario u   JOIN a.cbo cb "
			+ " WHERE (:nome IS NULL OR LOWER(c.nome) LIKE %:nome%) AND  (:cpf IS NULL OR c.cpf = :cpf) AND (:cns IS NULL OR c.cns = :cns) "
			+ " AND (:inicio IS NULL OR a.dataRealizacao >= :inicio) AND (:fim IS NULL OR a.dataRealizacao <= :fim) "
			+ " AND (:cbo IS NULL OR cb = :cbo) ")
	public Page<AvaliacaoMedicaTabelaDTO> search(@Param("nome") String nome, @Param("cpf") String cpf, @Param("cns") String cns, 
			@Param("inicio") Date inicio, @Param("fim") Date fim,  @Param("cbo") Cbo cbo, Pageable pageable);
	
}
