/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.saude.ficha.procedimento;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.dto.FichaProcedimentoTabelaDto;
import br.net.sigs.entity.saude.ficha.procedimento.FichaProcedimentos;

/**
 *
 * @author Gabriel Vinicius
 */
public interface FichaProcedimentoRepository extends JpaRepository<FichaProcedimentos, Long> {
	
	@Query(value = "SELECT new br.net.sigs.dto.FichaProcedimentoTabelaDto(f.id, f.data, c.nome, c.cpf, c.cns, f.importada, f.profissionalResponsavel.id) FROM FichaProcedimentos f "
			+ " JOIN f.cidadao c "
			+ " WHERE (:nome IS NULL OR LOWER(c.nome) LIKE %:nome%) AND (:cpf IS NULL OR c.cpf = :cpf) AND (:cns IS NULL OR c.cns = :cns) "
			+ " AND (:inicio IS NULL OR f.data >= :inicio) AND (:fim IS NULL OR f.data <= :fim) "
			+ " AND f.importada = :exportada GROUP BY f.id")
	public Page<FichaProcedimentoTabelaDto> search(@Param("nome") String nome, @Param("cpf") String cpf, @Param("cns") String cns, 
			@Param("inicio") Date inicio, @Param("fim") Date fim, @Param("exportada") Boolean exportada, Pageable pageable);
	
	public FichaProcedimentos findByUuid(String uuid);
	
	public List<FichaProcedimentos> findAllByImportada(Boolean importada);
	
	public Long countByImportada(boolean importada);
    
}
