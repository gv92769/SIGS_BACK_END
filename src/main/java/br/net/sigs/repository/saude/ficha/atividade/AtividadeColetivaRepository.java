package br.net.sigs.repository.saude.ficha.atividade;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.dto.AtividadeColetivaTabelaDto;
import br.net.sigs.entity.saude.ficha.atividade.AtividadeColetiva;

/**
*
* @author Gabriel Vinicius
*/
public interface AtividadeColetivaRepository extends JpaRepository<AtividadeColetiva, Long> {
	
	@Query(value = "SELECT new br.net.sigs.dto.AtividadeColetivaTabelaDto(a.id, a.data, ta.descricao, ubs.nome, ens.nome, a.outraLocalidade, a.importada, a.profissionalResponsavel.id) "
			+ "FROM AtividadeColetiva a LEFT JOIN a.listPublicoAlvo pub  "
			+ " JOIN a.tipoAtividade ta LEFT JOIN a.cnesLocalAtividade ubs LEFT JOIN a.instituicaoEnsino ens JOIN a.ubs u"
			+ " WHERE (:publico IS NULL OR pub.codigo = :publico) "
			+ " AND (:inicio IS NULL OR a.data BETWEEN :inicio AND :fim) AND (:cnes IS NULL OR ubs.cnes = :cnes) "
			+ " AND (:inep IS NULL OR ens.inep = :inep) AND (:escola = false OR ens.inep IS NOT NULL) AND u.cnes = :ubs "
			+ " AND a.importada = :exportada GROUP BY a.id")
	public Page<AtividadeColetivaTabelaDto> search(@Param("publico") Long publico, @Param("inicio") Date dataInicio, @Param("fim") Date dataFim,
			@Param("cnes") String cnes, @Param("inep") Long inep, @Param("escola") Boolean escola, @Param("ubs") String ubs, @Param("exportada") Boolean exportada, Pageable pageable);

	public AtividadeColetiva findByUuid(String uuid);
	
	public List<AtividadeColetiva> findAllByImportada(Boolean importada);
	
	public Long countByImportada(boolean importada);
	
}
