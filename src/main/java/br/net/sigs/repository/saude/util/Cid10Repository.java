/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.saude.util;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.saude.util.Cid10;

/**
 *
 * @author Gabriel Vinicius
 */
public interface Cid10Repository extends JpaRepository<Cid10, Long>{
    
    @Query(value = "SELECT new Cid10(c.id, c.codigo, c.descricao) FROM Cid10 c "
    		+ " WHERE (LOWER(c.descricao) LIKE :valor% OR LOWER(c.codigo) LIKE :valor%) AND (c.sexo = :sexo OR c.sexo = 2)")
    public List<Cid10> search(@Param("valor") String valor, @Param("sexo") Byte sexo, Pageable pageable);
    
}
