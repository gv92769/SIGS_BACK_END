package br.net.sigs.repository.saude.ficha.atividade.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atividade.util.TemaReuniao;

/**
*
* @author Gabriel Vinicius
*/
public interface TemaReuniaoRepository extends JpaRepository<TemaReuniao, Long> {

}
