package br.net.sigs.repository.saude.ficha.atendimento.individual;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.net.sigs.entity.saude.ficha.atendimento.individual.FichaAtendimentoIndividual;
import br.net.sigs.entity.saude.ficha.util.AvaliacaoMedica;

/**
*
* @author Gabriel Vincius
*/
public interface FichaAtendimentoIndividualRepository extends JpaRepository<FichaAtendimentoIndividual, String> {

	public List<FichaAtendimentoIndividual> findAllByImportada(Boolean importada);
	
	public FichaAtendimentoIndividual findByAtendimento(AvaliacaoMedica avaliacao);
	
	public Long countByImportada(boolean importada);
	
	@Query(value = "SELECT f FROM FichaAtendimentoIndividual f "
			+ " WHERE f.atendimento.id = :id")
	public FichaAtendimentoIndividual findByIdAtendimento(Long id);
	
}
