package br.net.sigs.repository.saude.ficha.atendimento.individual.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atendimento.individual.util.AleitamentoMaterno;

/**
*
* @author Gabriel Vincius
*/
public interface AleitamentoMaternoRepository extends JpaRepository<AleitamentoMaterno, Long> {

}
