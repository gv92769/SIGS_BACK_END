/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.saude.ciap;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.saude.ciap.Ciap2;

/**
 *
 * @author Naisses
 */
public interface Ciap2Repository extends JpaRepository<Ciap2, String>{
  
	@Query(value = "SELECT new Ciap2(c.id, c.codigo, c.descricao) FROM Ciap2 c "
			+ " WHERE (LOWER(c.descricao) LIKE :valor% OR LOWER(c.codigo) LIKE :valor%) AND (c.sexo = :sexo OR c.sexo = 2) "
			+ " AND (SELECT COUNT(*) FROM CiapCondicaoAvaliada cc WHERE cc.ciap2.codigo = c.codigo OR cc.codigoab = c.codigo) = 0")
	public List<Ciap2> search(@Param("valor") String valor, @Param("sexo") Byte sexo, Pageable pageable);
	
}
