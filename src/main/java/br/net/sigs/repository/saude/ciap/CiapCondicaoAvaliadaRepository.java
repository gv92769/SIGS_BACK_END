package br.net.sigs.repository.saude.ciap;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ciap.CiapCondicaoAvaliada;

/**
*
* @author Gabriel Vincius
*/
public interface CiapCondicaoAvaliadaRepository extends JpaRepository<CiapCondicaoAvaliada, Long> {

}
