package br.net.sigs.repository.saude.ficha.atendimento.individual.exame;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.saude.ficha.atendimento.individual.exame.ListaExames;

/**
*
* @author Gabriel Vincius
*/
public interface ListaExamesRepository extends JpaRepository<ListaExames, Long> {

	@Query(value = "SELECT l FROM ListaExames l WHERE l.id NOT IN :ids")
	public List<ListaExames> findAllByNotInId(@Param("ids") List<Long> ids);
	
}
