/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.repository.saude.procedimento;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.net.sigs.entity.saude.procedimento.Procedimento;

/**
*
* @author Gabriel Vincius
*/
public interface ProcedimentoRepository extends JpaRepository<Procedimento, Long> {
    
	@Query(value = "SELECT new Procedimento(p.id, p.descricao, p.codigo) "
			+ " FROM Procedimento p JOIN p.grupo g JOIN p.subGrupo sb JOIN p.formaOrganizacional fo"
			+ " WHERE (:grupo IS NULL OR g.id = :grupo) AND (:grupo IS NULL AND g.id IN :ids) AND (:subgrupo IS NULL OR sb.id = :subgrupo) "
			+ " AND (:forma IS NULL OR fo.id = :forma) AND (LOWER(p.descricao) LIKE %:texto% OR LOWER(p.codigo) LIKE %:texto%) "
			+ " AND p.ativo = 1 AND ((:sexo = TRUE AND p.masculino = 1) OR (:sexo = FALSE AND p.feminino = 1))")
	public List<Procedimento> search(@Param("grupo") Long grupo, @Param("ids") List<Long> ids, @Param("subgrupo") Long subgrupo, @Param("forma") Long forma,
			@Param("texto") String descricao, @Param("sexo") Boolean sexo, Pageable pageable);
	
	@Query(value = "SELECT new Procedimento(p.id, p.descricao, p.codigo)  FROM Procedimento p "
			+ " WHERE (LOWER(p.descricao) LIKE %:texto% OR LOWER(p.codigo) LIKE %:texto%) "
			+ " AND p.ativo = 1 AND ((:sexo = TRUE AND p.masculino = 1) OR (:sexo = FALSE AND p.feminino = 1))")
	public List<Procedimento> searchOdonto(@Param("texto") String descricao, @Param("sexo") Boolean sexo, Pageable pageable);
	
}
