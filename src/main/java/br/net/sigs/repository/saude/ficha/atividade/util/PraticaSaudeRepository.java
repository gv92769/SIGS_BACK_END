package br.net.sigs.repository.saude.ficha.atividade.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atividade.util.PraticaSaude;

/**
*
* @author Gabriel Vinicius
*/
public interface PraticaSaudeRepository extends JpaRepository<PraticaSaude, Long> {

}
