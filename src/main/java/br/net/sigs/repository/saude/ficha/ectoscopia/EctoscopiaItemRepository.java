package br.net.sigs.repository.saude.ficha.ectoscopia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.ectoscopia.EctoscopiaItem;

/**
*
* @author Gabriel Vincius
*/
public interface EctoscopiaItemRepository extends JpaRepository<EctoscopiaItem, Long> {

	public List<EctoscopiaItem> findAllByGrupo(String grupo);
	
}
