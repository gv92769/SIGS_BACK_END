package br.net.sigs.repository.saude.ficha.atividade.util;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.sigs.entity.saude.ficha.atividade.util.OutroProcedimentoColetivo;

/**
*
* @author Gabriel Vinicius
*/
public interface OutroProcedimentoColetivoRepository extends JpaRepository<OutroProcedimentoColetivo, Long> {

	public OutroProcedimentoColetivo findBySigtap(String sigtap);
	
}
