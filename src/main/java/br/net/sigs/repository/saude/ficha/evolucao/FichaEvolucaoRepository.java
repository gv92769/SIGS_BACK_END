package br.net.sigs.repository.saude.ficha.evolucao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.net.sigs.entity.saude.ficha.evolucao.FichaEvolucao;
import br.net.sigs.entity.saude.ficha.util.AvaliacaoMedica;

/**
*
* @author Gabriel Vincius
*/
public interface FichaEvolucaoRepository extends JpaRepository<FichaEvolucao, String> {

	public List<FichaEvolucao> findAllByImportada(Boolean importada);
	
	public FichaEvolucao findByEvolucao(AvaliacaoMedica avaliacao);
	
	public Long countByImportada(boolean importada);
	
	@Query(value = "SELECT f FROM FichaEvolucao f "
			+ " WHERE f.evolucao.id = :id")
	public FichaEvolucao findByIdEvolucao(Long id);
	
}
