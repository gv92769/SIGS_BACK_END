/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import br.net.sigs.entity.usuario.Usuario;

/**
 *
 * @author Naisses
 */
@Component
public class JwtTokenProvider {
    
    private static final String CLAIM_AUTHORITY = "authority";
    private static final String CLAIM_CNES = "cnes";
    private static final String CLAIM_CBO = "cbo";
    private static final String CLAIM_PERFIL = "perfil";
    private static final String REFRESH_TOKEN_TYPE = "refresh_token";
    
    @Value("${security.jwt.secret}")
    private String jwtSecret;
    
    @Value("${security.jwt.accessTokenValidForInSeconds}")
    private Long jwtAccessTokenValidForInSeconds;
    
    @Value("${security.jwt.refreshTokenValidForInSeconds}")
    private Long jwtRefreshTokenValidForInSeconds;
    
    
    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }
    
    public String createAccessToken(UserDetails userDetails, String tipo){
        final String authorities = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
        System.out.println("authorities: " + authorities);
        
        Claims claims = Jwts.claims().setSubject(userDetails.getUsername());
        claims.put(CLAIM_AUTHORITY, authorities);
        
        if (tipo.equals("LOTACAO")) {
        	claims.put(CLAIM_CNES, ((Usuario) userDetails).getCnes());
        	claims.put(CLAIM_CBO, ((Usuario) userDetails).getCbo());
        	claims.put(CLAIM_PERFIL, ((Usuario) userDetails).getPerfil().getPerfil());
        }

        Date now = new Date();
        Date validity = new Date(now.getTime() + jwtAccessTokenValidForInSeconds*1000L);
        
        SecretKey key = Keys.hmacShaKeyFor(DatatypeConverter.parseBase64Binary(jwtSecret));

        return Jwts.builder()//
            .setClaims(claims)//
            .setIssuedAt(now)//
            .setExpiration(validity)
            .signWith(key)//
            .compact();
    }
    
    public String createRefreshToken(UserDetails userDetails, String tipo){        
        Claims claims = Jwts.claims().setSubject(userDetails.getUsername());
        claims.put(CLAIM_AUTHORITY, REFRESH_TOKEN_TYPE);
       
        if (tipo.equals("LOTACAO")) {
        	claims.put(CLAIM_CNES, ((Usuario) userDetails).getCnes());
        	claims.put(CLAIM_CBO, ((Usuario) userDetails).getCbo());
        	claims.put(CLAIM_PERFIL, ((Usuario) userDetails).getPerfil().getPerfil());
        }

        Date now = new Date();
        Date validity = new Date(now.getTime() + jwtRefreshTokenValidForInSeconds*1000L);
        
        SecretKey key = Keys.hmacShaKeyFor(DatatypeConverter.parseBase64Binary(jwtSecret));

        return Jwts.builder()//
            .setClaims(claims)//
            .setIssuedAt(now)//
            .setExpiration(validity)
            .signWith(key)//
            .compact();
    }
    
    public Authentication parseAccessToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
                .parseClaimsJws(token).getBody();

        String tokenSubject = claims.getSubject();
        String tokenAuthorities = (String) claims.get(CLAIM_AUTHORITY); 
        List<GrantedAuthority> tokenAuthoritiesList = AuthorityUtils.commaSeparatedStringToAuthorityList(tokenAuthorities);

        return new UsernamePasswordAuthenticationToken(tokenSubject, null, tokenAuthoritiesList);
    }
    
    public Authentication parseRefreshToken(String token){
        Claims claims = Jwts.parser()
                .require(CLAIM_AUTHORITY, REFRESH_TOKEN_TYPE)
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
                .parseClaimsJws(token).getBody();

        String tokenSubject = claims.getSubject();

        return new UsernamePasswordAuthenticationToken(tokenSubject, null, null);
    } 
    
    public String claimCnes(String token) {
    	Claims claims = Jwts.parser()
                .require(CLAIM_AUTHORITY, REFRESH_TOKEN_TYPE)
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
                .parseClaimsJws(token).getBody();
    	
    	return claims.get(CLAIM_CNES).toString();
    }
    
    public String claimCbo(String token) {
    	Claims claims = Jwts.parser()
                .require(CLAIM_AUTHORITY, REFRESH_TOKEN_TYPE)
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
                .parseClaimsJws(token).getBody();
    	
    	return claims.get(CLAIM_CBO).toString();
    }
    
    public String claimPerfil(String token) {
    	Claims claims = Jwts.parser()
                .require(CLAIM_AUTHORITY, REFRESH_TOKEN_TYPE)
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
                .parseClaimsJws(token).getBody();
    	
    	return claims.get(CLAIM_PERFIL).toString();
    }
    
}
