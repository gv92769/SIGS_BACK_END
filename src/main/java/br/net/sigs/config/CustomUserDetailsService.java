/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.config;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import br.net.sigs.repository.usuario.UsuarioRepository;

/**
 *
 * @author Naisses
 */
@Component
public class CustomUserDetailsService implements UserDetailsService{

    private final UsuarioRepository repository;

    public CustomUserDetailsService(UsuarioRepository repository) {
        this.repository = repository;
    }
    
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        System.out.println("=============UserDetails ===> loadUserByUsername");
        return repository.findByLogin(login).orElseThrow(
                () -> new UsernameNotFoundException("Login: " + login + " not found"));
    }
    
}
