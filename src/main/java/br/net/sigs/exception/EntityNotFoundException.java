/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.exception;


import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Naisses
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 9223331015181614323L;

	public static class ParamBuilder{
        private Map<String,Object> map = new LinkedHashMap<String, Object>();
        
        private ParamBuilder(String key, Object value){
            map.put(key, value);
        }
                
        public static <V> ParamBuilder with(String name, V value){
            return new ParamBuilder(name, value);
        }
        
        public <V> ParamBuilder and(String name, V value){
            map.put(name, value);
            return this;
        }

        @Override
        public String toString() {
            int i = 0;
            String s = "";
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if(i++ != 0)
                    s += ", ";
                s += key + " " + value;                
            }
            return s;
        }
        
        
    };
    
    private static final String MESSAGE = "Entity not found: ";
    
    public <T> EntityNotFoundException(Class<T> entityClass) {
        super(MESSAGE + entityClass.getSimpleName());
    }
    
    public <T,P> EntityNotFoundException(Class<T> entityClass, ParamBuilder param) {
        super(MESSAGE + entityClass.getSimpleName() + " with " + param);
    }
      
}
