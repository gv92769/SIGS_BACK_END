/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Naisses
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedExcetion extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 9213396247166959427L;

	public UnauthorizedExcetion() {
        super();
    }
    
    public UnauthorizedExcetion(String message) {
        super(message);
    }

    public UnauthorizedExcetion(String message, Throwable cause) {
        super(message, cause);
    }

    
    
}
