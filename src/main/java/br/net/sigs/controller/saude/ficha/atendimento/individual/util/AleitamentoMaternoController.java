package br.net.sigs.controller.saude.ficha.atendimento.individual.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atendimento.individual.util.AleitamentoMaterno;
import br.net.sigs.service.saude.ficha.atendimento.individual.util.AleitamentoMaternoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/aleitamentomaterno")
public class AleitamentoMaternoController {

	@Autowired
	private AleitamentoMaternoService service;
	
	@GetMapping
	public ResponseEntity<List<AleitamentoMaterno>> findAll() {
		return ResponseEntity.ok().body(service.findAll());
	}
	
}
