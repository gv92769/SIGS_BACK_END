package br.net.sigs.controller.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.ProcedimentoOdonto;
import br.net.sigs.service.saude.ficha.atendimento.odontologico.util.ProcedimentoOdontoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/procedimentoodonto")
public class ProcedimentoOdontoController {

	@Autowired
	private ProcedimentoOdontoService procedimentoOdontoService;
	
	@GetMapping
    public ResponseEntity<List<ProcedimentoOdonto>> findAll(){
        return ResponseEntity.ok().body(procedimentoOdontoService.findAll());
    }
	
}
