/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.saude.ficha.evolucao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.AvaliacaoMedicaTabelaDTO;
import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.saude.ficha.evolucao.Evolucao;
import br.net.sigs.entity.saude.ficha.evolucao.FichaEvolucao;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.saude.ficha.evolucao.EvolucaoService;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/evolucao")
public class EvolucaoController {
	
	@Autowired
	private EvolucaoService evolucaoService;
    
	@PostMapping("/search")
	@PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public Page<AvaliacaoMedicaTabelaDTO> search(@RequestBody FichaFiltroDto filter) {
    	return evolucaoService.search(filter);
    }
	
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<FichaEvolucao> findById(@PathVariable Long id){
        return ResponseEntity.ok().body(evolucaoService.findByIdEvolucao(id));
    }
    
    @GetMapping("/{id}/cidadao")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<Cidadao> findCidadaoByEvolucaoId(@PathVariable Long id){
        return evolucaoService.findById(id)
                .map( evolucao -> {
                    return ResponseEntity.ok().body(evolucao.getCidadao());
                })
                .orElseThrow(() -> new EntityNotFoundException(Evolucao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
    
    @PostMapping
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<Evolucao> create(@RequestBody Evolucao ficha){
        return ResponseEntity.status(HttpStatus.CREATED).body(evolucaoService.save(ficha));
    }
    
    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<Evolucao> update(@PathVariable("id") Long id, @RequestBody Evolucao evolucao){
        return evolucaoService.findById(id)
                .map( evolucaoFound -> {
                    return ResponseEntity.ok().body(evolucaoService.save(evolucao));
                }).orElseThrow(() -> new EntityNotFoundException(Evolucao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    } 
    
    @GetMapping("/exportada/{id}")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<Boolean> importada(@PathVariable("id") Long id) {
    	FichaEvolucao ficha = evolucaoService.findByIdEvolucao(id);
    	 return ResponseEntity.ok().body(ficha.getImportada());
    }
    

}

