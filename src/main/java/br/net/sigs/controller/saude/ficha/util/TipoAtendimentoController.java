package br.net.sigs.controller.saude.ficha.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.util.TipoAtendimento;
import br.net.sigs.service.saude.ficha.util.TipoAtendimentoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/tipoatendimento")
public class TipoAtendimentoController {
	
	@Autowired
	private TipoAtendimentoService tipoAtendimentoService;
	
	@PostMapping("/findallbyid")
    public ResponseEntity<List<TipoAtendimento>> findAllById(@RequestBody List<Long> ids){
        return ResponseEntity.ok().body(tipoAtendimentoService.findAllById(ids));
    }
	
}
