package br.net.sigs.controller.saude.procedimento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.procedimento.ProcedimentoFormaOrganizacional;
import br.net.sigs.entity.saude.procedimento.ProcedimentoSubGrupo;
import br.net.sigs.service.saude.procedimento.ProcedimentoFormaOrganizacionalService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/procedimentoformaorganizacional")
public class ProcedimentoFormaOrganizacionalController {

	@Autowired
	ProcedimentoFormaOrganizacionalService service;
	
	@PostMapping
	public ResponseEntity<List<ProcedimentoFormaOrganizacional>> findAllById(@RequestBody ProcedimentoSubGrupo subgrupo) {
		return ResponseEntity.ok().body(service.findAllBySubGrupo(subgrupo));
	}
	
}
