package br.net.sigs.controller.saude.ficha.atendimento.odontologico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.AtendimentoOdontologicoTabelaDto;
import br.net.sigs.dto.PageRequest;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.FichaAtendimentoOdontologico;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.saude.ficha.atendimento.odontologico.FichaAtendimentoOdontologicoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/fichaatendimentoodontologico")
public class FichaAtendimentoOdontologicoController {
	
	@Autowired
	private FichaAtendimentoOdontologicoService fichaAtendimentoOdontologicoService;

	@GetMapping("/{id}")
	@PreAuthorize("hasAnyRole('CIRURGIAO_DENTISTA')")
	public ResponseEntity<FichaAtendimentoOdontologico> findById(@PathVariable("id") Long id) {
		return fichaAtendimentoOdontologicoService.findById(id)
				 .map( fichaAtendimentoOdontologico -> {
					 return ResponseEntity.ok().body(fichaAtendimentoOdontologico);
				 }).orElseThrow(() -> new EntityNotFoundException(FichaAtendimentoOdontologico.class,
	                        EntityNotFoundException.ParamBuilder.with("id", id)));
	}
	
	@PostMapping
	@PreAuthorize("hasAnyRole('CIRURGIAO_DENTISTA')")
    public ResponseEntity<FichaAtendimentoOdontologico> create(@RequestBody FichaAtendimentoOdontologico fichaAtendimentoOdontologico) {
        return ResponseEntity.status(HttpStatus.CREATED).body(fichaAtendimentoOdontologicoService.save(fichaAtendimentoOdontologico));
    }
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAnyRole( 'CIRURGIAO_DENTISTA')")
    public ResponseEntity<FichaAtendimentoOdontologico> update(@PathVariable("id") Long id, @RequestBody FichaAtendimentoOdontologico fichaAtendimentoOdontologico) {
        return fichaAtendimentoOdontologicoService.findById(id)
                .map( fichaAtendimentoOdontologicoFound -> {
                    return ResponseEntity.ok().body(fichaAtendimentoOdontologicoService.save(fichaAtendimentoOdontologico));
                }).orElseThrow(() -> new EntityNotFoundException(FichaAtendimentoOdontologico.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyRole( 'CIRURGIAO_DENTISTA')")
    public void delete(@PathVariable("id") Long id) {
		fichaAtendimentoOdontologicoService.findById(id).orElseThrow(() -> new EntityNotFoundException(FichaAtendimentoOdontologico.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
		fichaAtendimentoOdontologicoService.delete(id);
    }

	@PostMapping("/findallbynome/{nome}")
	@PreAuthorize("hasAnyRole( 'CIRURGIAO_DENTISTA')")
	public ResponseEntity<Page<AtendimentoOdontologicoTabelaDto>> findAllByNome(@PathVariable("nome") String nome, @RequestBody PageRequest pageRequest) {
		return ResponseEntity.ok().body(fichaAtendimentoOdontologicoService.findAllByNome(nome, pageRequest));	
	}
	
	@PostMapping("/findallbycpf/{cpf}")
	@PreAuthorize("hasAnyRole( 'CIRURGIAO_DENTISTA')")
	public ResponseEntity<Page<AtendimentoOdontologicoTabelaDto>> findAllByCpf(@PathVariable("cpf") String cpf, @RequestBody PageRequest pageRequest) {
		return ResponseEntity.ok().body(fichaAtendimentoOdontologicoService.findAllByCpf(cpf, pageRequest));	
	}
	
}
