package br.net.sigs.controller.saude.procedimento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.SubGrupoFiltroDto;
import br.net.sigs.entity.saude.procedimento.ProcedimentoGrupo;
import br.net.sigs.entity.saude.procedimento.ProcedimentoSubGrupo;
import br.net.sigs.service.saude.procedimento.ProcedimentoSubGrupoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/procedimentosubgrupo")
public class ProcedimentoSubGrupoController {

	@Autowired
	private ProcedimentoSubGrupoService service;
	
	@PostMapping
	public ResponseEntity<List<ProcedimentoSubGrupo>> findAllByGrupo(@RequestBody ProcedimentoGrupo grupo) {
		return ResponseEntity.ok().body(service.findAllByGrupo(grupo));
	}
	
	@PostMapping("/search/subgrupo")
	public ResponseEntity<List<ProcedimentoSubGrupo>> searchExame(@RequestBody SubGrupoFiltroDto filtro) {
		return ResponseEntity.ok().body(service.searchSubGrupo(filtro));
	}
	
}
