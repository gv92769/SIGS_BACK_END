package br.net.sigs.controller.saude.ficha.util;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.dto.SolicitacaoTabelaDto;
import br.net.sigs.entity.saude.ficha.exame.ResultadoExame;
import br.net.sigs.entity.saude.ficha.exame.Solicitacao;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.saude.ficha.util.ExameService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/exame")
public class ExameController {

	@Autowired
	private ExameService service;
	
	@GetMapping("/solicitacao/{id}")
    public ResponseEntity<Solicitacao> solicitacaoFindById(@PathVariable("id") Long id) {
		return service.solicitacaoFindById(id)
                .map( solicitacao -> {
                    return ResponseEntity.ok().body(solicitacao);
                })
                .orElseThrow(() -> new EntityNotFoundException(Solicitacao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@GetMapping("/resultado/{id}")
    public ResponseEntity<ResultadoExame> resultadoFindById(@PathVariable("id") Long id) {
		return service.resultadoFindById(id)
                .map( solicitacao -> {
                    return ResponseEntity.ok().body(solicitacao);
                })
                .orElseThrow(() -> new EntityNotFoundException(ResultadoExame.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@PostMapping("/solicitacao")
	public ResponseEntity<Solicitacao> saveSolicitacao(@RequestBody Solicitacao solicitacao) {
		return ResponseEntity.ok().body(service.saveSolicitacao(solicitacao, null));
	}
	
	@PostMapping(value ="/laudo",  consumes = {"multipart/form-data"})
	@ResponseBody
	public ResponseEntity<Solicitacao> saveLaudo(
	        @RequestPart("properties") @Valid Solicitacao properties,
	        @RequestPart("file") @Valid MultipartFile file){
		return ResponseEntity.ok().body(service.saveSolicitacao(properties, file));
	}
	
	@PutMapping("/{id}/resultado")
    public ResponseEntity<ResultadoExame> updateResultado(@PathVariable("id") Long id, @RequestBody ResultadoExame resultado){
        return service.solicitacaoFindById(id)
                .map( solicitacao -> {
                	resultado.setSolicitacao(solicitacao);
                    return ResponseEntity.ok().body(service.saveResultado(resultado));
                }).orElseThrow(() -> new EntityNotFoundException(ResultadoExame.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@PutMapping("/{id}/solicitacao")
    public ResponseEntity<Solicitacao> updateSolicitacao(@PathVariable("id") Long id, @RequestBody Solicitacao solicitacao){
        return service.solicitacaoFindById(id)
                .map( solicitacaoEntity -> {
                    return ResponseEntity.ok().body(service.saveSolicitacao(solicitacao, null));
                }).orElseThrow(() -> new EntityNotFoundException(Solicitacao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@DeleteMapping("/solicitacao/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSolicitacao(@PathVariable("id") Long id) {
		Optional<Solicitacao> solicitacao = service.solicitacaoFindById(id);
		if (solicitacao.isPresent())
			service.deleteSolicitacao(solicitacao.get());
    }
	
	@PostMapping("/search")
	public ResponseEntity<Page<SolicitacaoTabelaDto>> search(@RequestBody FichaFiltroDto filter) {
		return ResponseEntity.ok().body(service.search(filter));
	}
	
}
