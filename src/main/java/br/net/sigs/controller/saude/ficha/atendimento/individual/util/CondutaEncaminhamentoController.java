package br.net.sigs.controller.saude.ficha.atendimento.individual.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atendimento.individual.util.CondutaEncaminhamento;
import br.net.sigs.service.saude.ficha.atendimento.individual.util.CondutaEncaminhamentoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/condutaencaminhamento")
public class CondutaEncaminhamentoController {

	@Autowired
	private CondutaEncaminhamentoService service;
	
	@GetMapping
	public ResponseEntity<List<CondutaEncaminhamento>> findAll() {
		return ResponseEntity.ok().body(service.findAll());
	}
	
	@PostMapping
	public ResponseEntity<List<CondutaEncaminhamento>> findAllByid(@RequestBody List<Long> ids) {
		return ResponseEntity.ok().body(service.findAllById(ids));
	}
	
}
