/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.saude.ciap;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ciap.Ciap2;
import br.net.sigs.service.saude.ciap.Ciap2Service;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/ciap2")
public class Ciap2Controller {

    @Autowired
    private Ciap2Service service;

    @GetMapping("/search/{valor}/{sexo}")
	public ResponseEntity<List<Ciap2>> search(@PathVariable String valor, @PathVariable Byte sexo) {
		return ResponseEntity.ok().body(service.search(valor, sexo));
	}
    
}
