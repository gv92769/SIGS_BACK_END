package br.net.sigs.controller.saude.ficha.ectoscopia;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.ectoscopia.EctoscopiaItem;
import br.net.sigs.service.saude.ficha.ectoscopia.EctoscopiaItemService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/ectoscopiaitem")
public class EctoscopiaItemController {

	@Autowired
	private EctoscopiaItemService service;
	
	@GetMapping("/grupo/{grupo}")
	@PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
	public ResponseEntity<List<EctoscopiaItem>> findAllByGrupo(@PathVariable String grupo) {
		return ResponseEntity.ok().body(service.findAllByGrupo(grupo));
	}
	
}
