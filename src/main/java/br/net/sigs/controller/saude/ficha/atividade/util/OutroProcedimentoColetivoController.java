package br.net.sigs.controller.saude.ficha.atividade.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atividade.util.OutroProcedimentoColetivo;
import br.net.sigs.service.saude.ficha.atividade.util.OutroProcedimentoColetivoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/outroprocedimentocoletivo")
public class OutroProcedimentoColetivoController {

	@Autowired
	private OutroProcedimentoColetivoService outroProcedimentoService;
	
	@GetMapping("/sigtap/{sigtap}")
    public ResponseEntity<OutroProcedimentoColetivo> findBySigtap(@PathVariable String sigtap){
        return ResponseEntity.ok().body(outroProcedimentoService.findBySigtap(sigtap));
    }
	
}
