package br.net.sigs.controller.saude.ciap;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ciap.CiapCondicaoAvaliada;
import br.net.sigs.service.saude.ciap.CiapCondicaoAvaliadaService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/ciapcondicaoavaliada")
public class CiapCondicaoAvaliadaController {

	@Autowired
	private CiapCondicaoAvaliadaService service;
	
	@GetMapping
	public ResponseEntity<List<CiapCondicaoAvaliada>> findAll() {
		return ResponseEntity.ok().body(service.findAll());
	}
	
	@PostMapping
	public ResponseEntity<List<CiapCondicaoAvaliada>> findAllById(@RequestBody List<Long> ids) {
		return ResponseEntity.ok().body(service.findAllById(ids));
	}
	
}
