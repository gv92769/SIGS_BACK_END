package br.net.sigs.controller.saude.ficha.atividade.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atividade.util.InstituicaoEnsino;
import br.net.sigs.service.saude.ficha.atividade.util.InstituicaoEnsinoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/instituicaoensino")
public class InstituicaoEnsinoController {

	@Autowired
	private InstituicaoEnsinoService instituicaoEnsinoService;
	
	@GetMapping("/inep/{inep}")
    public ResponseEntity<InstituicaoEnsino> findByInep(@PathVariable Long inep){
        return ResponseEntity.ok().body(instituicaoEnsinoService.findByInep(inep));
    }
	
}
