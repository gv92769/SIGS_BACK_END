package br.net.sigs.controller.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.TipoConsultaOdonto;
import br.net.sigs.service.saude.ficha.atendimento.odontologico.util.TipoConsultaOdontoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/tipoconsultaodonto")
public class TipoConsultaOdontoController {

	@Autowired
	private TipoConsultaOdontoService tipoConsultaOdontoService;
	
	@GetMapping
    public ResponseEntity<List<TipoConsultaOdonto>> findAll(){
        return ResponseEntity.ok().body(tipoConsultaOdontoService.findAll());
    }
	
}
