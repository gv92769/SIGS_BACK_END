package br.net.sigs.controller.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.CondutaEncaminhamentoOdonto;
import br.net.sigs.service.saude.ficha.atendimento.odontologico.util.CondutaEncaminhamentoOdontoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/contudaencaminhamentoodonto")
public class CondutaEncaminhamentoOdontoController {

	@Autowired
	private CondutaEncaminhamentoOdontoService condutaEncaminhamentoOdontoService;
	
	@PostMapping("/findallbyid")
    public ResponseEntity<List<CondutaEncaminhamentoOdonto>> findAllById(@RequestBody List<Long> ids){
        return ResponseEntity.ok().body(condutaEncaminhamentoOdontoService.findAllById(ids));
    }
	
}
