/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.saude.ficha.util;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.AvaliacaoMedicaTabelaDTO;
import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.entity.saude.ficha.util.AvaliacaoMedica;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.saude.ficha.util.AvaliacaoMedicaService;

/**
 *
 * @author Gabriel Vinicius
 */
@RestController
@RequestMapping("/avaliacoesmedicas")
public class AvaliacaoMedicaController {
    
    @Autowired
    private AvaliacaoMedicaService service;
    
    @PostMapping("/search/{tipo}")
    public Page<AvaliacaoMedicaTabelaDTO> search(@RequestBody FichaFiltroDto filter, @PathVariable("tipo") String tipo) {
    	return service.search(filter, tipo);
    }
    
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public void delete(@PathVariable("id") Long id){
    	Optional<AvaliacaoMedica> avaliacaoOp = service.findById(id);
    	
    	if (avaliacaoOp.isPresent())
    		service.delete(avaliacaoOp.get());
    	else
    		new EntityNotFoundException(AvaliacaoMedica.class,
                        EntityNotFoundException.ParamBuilder.with("id", id));
    } 
    
}
