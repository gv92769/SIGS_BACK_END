package br.net.sigs.controller.saude.ficha.atendimento.individual.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atendimento.individual.util.RacionalidadeSaude;
import br.net.sigs.service.saude.ficha.atendimento.individual.util.RacionalidadeSaudeService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/racionalidadesaude")
public class RacionalidadeSaudeController {

	@Autowired
	private RacionalidadeSaudeService service;
	
	@GetMapping
	public ResponseEntity<List<RacionalidadeSaude>> findAll() {
		return ResponseEntity.ok().body(service.findAll());
	}
	
}
