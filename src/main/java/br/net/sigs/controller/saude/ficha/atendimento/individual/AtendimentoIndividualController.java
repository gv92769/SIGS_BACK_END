/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.saude.ficha.atendimento.individual;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.AvaliacaoMedicaTabelaDTO;
import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.saude.ficha.atendimento.individual.AtendimentoIndividual;
import br.net.sigs.entity.saude.ficha.atendimento.individual.FichaAtendimentoIndividual;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.saude.ficha.atendimento.individual.AtendimentoIndividualService;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/atendimentosindividuais")
public class AtendimentoIndividualController {
	
	@Autowired
	private AtendimentoIndividualService atendimentoIndividualService;
    
	@PostMapping("/search")
	@PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public Page<AvaliacaoMedicaTabelaDTO> search(@RequestBody FichaFiltroDto filter) {
    	return atendimentoIndividualService.search(filter);
    }
	
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<FichaAtendimentoIndividual> findById(@PathVariable Long id){
        return ResponseEntity.ok().body(atendimentoIndividualService.findByIdAtendimento(id));
    }
    
    @GetMapping("/{id}/cidadao")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<Cidadao> findCidadaoByAtendimentoId(@PathVariable Long id){
        return atendimentoIndividualService.findById(id)
                .map( atendimento -> {
                    return ResponseEntity.ok().body(atendimento.getCidadao());
                })
                .orElseThrow(() -> new EntityNotFoundException(AtendimentoIndividual.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
    
    @PostMapping
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<AtendimentoIndividual> create(@RequestBody AtendimentoIndividual ficha){
        return ResponseEntity.status(HttpStatus.CREATED).body(atendimentoIndividualService.save(ficha));
    }
    
    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<AtendimentoIndividual> update(@PathVariable("id") Long id, @RequestBody AtendimentoIndividual atendimento){
        return atendimentoIndividualService.findById(id)
                .map( atendimentoFound -> {
                    return ResponseEntity.ok().body(atendimentoIndividualService.save(atendimento));
                }).orElseThrow(() -> new EntityNotFoundException(AtendimentoIndividual.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    } 
    
    @GetMapping("/exportada/{id}")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<Boolean> importada(@PathVariable("id") Long id) {
    	FichaAtendimentoIndividual ficha = atendimentoIndividualService.findByIdAtendimento(id);
    	 return ResponseEntity.ok().body(ficha.getImportada());
    }
    

}