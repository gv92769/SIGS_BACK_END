package br.net.sigs.controller.saude.procedimento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.procedimento.ProcedimentoGrupo;
import br.net.sigs.service.saude.procedimento.ProcedimentoGrupoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/procedimentogrupo")
public class ProcedimentoGrupoController {

	@Autowired
	private ProcedimentoGrupoService service;
	
	@PostMapping
	public ResponseEntity<List<ProcedimentoGrupo>> findAllById(@RequestBody List<Long> ids) {
		return ResponseEntity.ok().body(service.findAllById(ids));
	}
	
}
