package br.net.sigs.controller.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.FornecimentoOdonto;
import br.net.sigs.service.saude.ficha.atendimento.odontologico.util.FornecimentoOdontoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/fornecimentoodonto")
public class FornecimentoOdontoController {

	@Autowired
	private FornecimentoOdontoService fornecimentoOdontoService;
	
	@GetMapping
    public ResponseEntity<List<FornecimentoOdonto>> findAll(){
        return ResponseEntity.ok().body(fornecimentoOdontoService.findAll());
    }
	
}
