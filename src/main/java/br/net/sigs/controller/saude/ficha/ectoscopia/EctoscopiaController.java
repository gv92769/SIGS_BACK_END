/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.saude.ficha.ectoscopia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.ectoscopia.Ectoscopia;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.saude.ficha.ectoscopia.EctoscopiaService;

/**
 *
 * @author Gabriel Vinicius
 */
@RestController
@RequestMapping("/avaliacoesmedicas/ectoscopias")
public class EctoscopiaController {
    
    @Autowired
    private EctoscopiaService service;
    
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<Ectoscopia> findById(@PathVariable Long id){
        return service.findById(id)
                .map( ectoscopia -> ResponseEntity.ok().body(ectoscopia))
                .orElseThrow(() -> new EntityNotFoundException(Ectoscopia.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
    
    @PostMapping
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<Ectoscopia> create(@RequestBody Ectoscopia ectoscopia){
        return ResponseEntity.status(HttpStatus.CREATED).body(service.create(ectoscopia));
    }
    
    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<Ectoscopia> update(@PathVariable("id") Long id, @RequestBody Ectoscopia ectoscopia){
        return service.findById(id)
                .map( ectoscopiaFound -> {
                    return ResponseEntity.ok().body(service.update(ectoscopia));
                }).orElseThrow(() -> new EntityNotFoundException(Ectoscopia.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
       
}
