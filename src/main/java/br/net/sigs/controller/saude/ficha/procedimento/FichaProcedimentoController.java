package br.net.sigs.controller.saude.ficha.procedimento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.dto.FichaProcedimentoTabelaDto;
import br.net.sigs.entity.saude.ficha.procedimento.FichaProcedimentos;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.saude.ficha.procedimento.FichaProcedimentoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/fichaprocedimento")
public class FichaProcedimentoController {

	@Autowired
	private FichaProcedimentoService fichaProcedimentoService;
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAnyRole('AUXILIAR_SAUDE',  'ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', "
			+ "'PSICOLOGO', 'TECNICO_SAUDE')")
	public ResponseEntity<FichaProcedimentos> findById(@PathVariable("id") Long id) {
		return fichaProcedimentoService.findById(id)
				 .map( fichaProcedimentos -> {
					 return ResponseEntity.ok().body(fichaProcedimentos);
				 }).orElseThrow(() -> new EntityNotFoundException(FichaProcedimentos.class,
	                        EntityNotFoundException.ParamBuilder.with("id", id)));
	}
	
	@PostMapping
	@PreAuthorize("hasAnyRole('AUXILIAR_SAUDE',  'ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', "
			+ "'PSICOLOGO', 'TECNICO_SAUDE')")
    public ResponseEntity<FichaProcedimentos> create(@RequestBody FichaProcedimentos fichaProcedimento) {
        return ResponseEntity.status(HttpStatus.CREATED).body(fichaProcedimentoService.save(fichaProcedimento));
    }
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAnyRole('AUXILIAR_SAUDE',  'ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', "
			+ "'PSICOLOGO', 'TECNICO_SAUDE')")
    public ResponseEntity<FichaProcedimentos> update(@PathVariable("id") Long id, @RequestBody FichaProcedimentos fichaProcedimento) {
        return fichaProcedimentoService.findById(id)
                .map( fichaProcedimentosFound -> {
                    return ResponseEntity.ok().body(fichaProcedimentoService.save(fichaProcedimento));
                }).orElseThrow(() -> new EntityNotFoundException(FichaProcedimentos.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyRole('AUXILIAR_SAUDE',  'ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', "
			+ "'PSICOLOGO', 'TECNICO_SAUDE')")
    public void delete(@PathVariable("id") Long id) {
		fichaProcedimentoService.findById(id).orElseThrow(() -> new EntityNotFoundException(FichaProcedimentos.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
		fichaProcedimentoService.delete(id);
    }
	
	@PostMapping("/search")
	@PreAuthorize("hasAnyRole('AUXILIAR_SAUDE',  'ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', "
			+ "'PSICOLOGO', 'TECNICO_SAUDE')")
	public ResponseEntity<Page<FichaProcedimentoTabelaDto>> search(@RequestBody FichaFiltroDto filtro) {
		return ResponseEntity.ok().body(fichaProcedimentoService.search(filtro));
	}
	
}
