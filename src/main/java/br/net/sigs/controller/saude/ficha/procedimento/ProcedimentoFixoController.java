package br.net.sigs.controller.saude.ficha.procedimento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.procedimento.ProcedimentoFixo;
import br.net.sigs.service.saude.ficha.procedimento.ProcedimentoFixoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/procedimetofixo")
public class ProcedimentoFixoController {

	@Autowired
	private ProcedimentoFixoService procedimentoFixoService;
	
	@PostMapping("/findallbyid")
    public ResponseEntity<List<ProcedimentoFixo>> findAllById(@RequestBody List<Integer> ids){
        return ResponseEntity.ok().body(procedimentoFixoService.findAllById(ids));
    }
	
}
