package br.net.sigs.controller.saude.ficha.atividade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.AtividadeColetivaFilterDto;
import br.net.sigs.dto.AtividadeColetivaTabelaDto;
import br.net.sigs.dto.FormAtividadeColetivaDTO;
import br.net.sigs.entity.saude.ficha.atividade.AtividadeColetiva;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.saude.ficha.atividade.AtividadeColetivaService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/atividadecoletiva")
public class AtividadeColetivaController {

	@Autowired
	private AtividadeColetivaService atividadeColetivaService;
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
	public ResponseEntity<AtividadeColetiva> findById(@PathVariable Long id) {
		return atividadeColetivaService.findById(id)
					.map( atvidadeColetiva -> {
						return ResponseEntity.ok().body( atvidadeColetiva);
					}).orElseThrow(() -> new EntityNotFoundException(AtividadeColetiva.class,
	                        EntityNotFoundException.ParamBuilder.with("id", id)));
	}
	
	@PostMapping("/search")
	@PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
	public ResponseEntity<Page<AtividadeColetivaTabelaDto>> search(@RequestBody AtividadeColetivaFilterDto filter) {
		return ResponseEntity.ok().body(atividadeColetivaService.search(filter));
	}
	
	@PostMapping
	@PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<AtividadeColetiva> create(@RequestBody AtividadeColetiva atividadeColetiva){
        return ResponseEntity.status(HttpStatus.CREATED).body(atividadeColetivaService.save(atividadeColetiva));
    }
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public ResponseEntity<AtividadeColetiva> update(@PathVariable("id") Long id, @RequestBody AtividadeColetiva atividadeColetiva){
        return atividadeColetivaService.findById(id)
                .map( atividadeColetivaFound -> {
                    return ResponseEntity.ok().body(atividadeColetivaService.save(atividadeColetiva));
                }).orElseThrow(() -> new EntityNotFoundException(AtividadeColetiva.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyRole('ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO')")
    public void delete(@PathVariable("id") Long id){
		atividadeColetivaService.findById(id).orElseThrow(() -> new EntityNotFoundException(AtividadeColetiva.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
		atividadeColetivaService.delete(id);
    }
	
	@GetMapping("/init")
    public ResponseEntity<FormAtividadeColetivaDTO> init() {
        return ResponseEntity.ok().body(atividadeColetivaService.init());
    }

}
