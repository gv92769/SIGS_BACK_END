package br.net.sigs.controller.saude.ficha.atividade.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atividade.util.PublicoAlvo;
import br.net.sigs.service.saude.ficha.atividade.util.PublicoAlvoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/publicoalvo")
public class PublicoAlvoController {

	@Autowired
	private PublicoAlvoService publicoAlvoService;
	
	@GetMapping
    public ResponseEntity<List<PublicoAlvo>> findAll() {
		return ResponseEntity.ok().body(publicoAlvoService.findAll());
	}
	
}
