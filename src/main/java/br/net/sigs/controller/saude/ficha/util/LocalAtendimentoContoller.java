package br.net.sigs.controller.saude.ficha.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.util.LocalAtendimento;
import br.net.sigs.service.saude.ficha.util.LocalAtendimentoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/localatendimento")
public class LocalAtendimentoContoller {

	@Autowired
	private LocalAtendimentoService localAtendimentoService;
	
	@GetMapping
    public ResponseEntity<List<LocalAtendimento>> findAll(){
        return ResponseEntity.ok().body(localAtendimentoService.findAll());
    }
	
}
