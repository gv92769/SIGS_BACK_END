package br.net.sigs.controller.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.VigilanciaSaudeBucal;
import br.net.sigs.service.saude.ficha.atendimento.odontologico.util.VigilanciaSaudeBucalService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/vigilanciasaudebucal")
public class VigilanciaSaudeBucalController {

	@Autowired
	private VigilanciaSaudeBucalService vigilanciaSaudeBucalService;
	
	@GetMapping
    public ResponseEntity<List<VigilanciaSaudeBucal>> findAll(){
        return ResponseEntity.ok().body(vigilanciaSaudeBucalService.findAll());
    }
	
}
