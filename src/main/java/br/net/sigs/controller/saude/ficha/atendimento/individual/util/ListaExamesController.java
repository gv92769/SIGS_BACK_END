package br.net.sigs.controller.saude.ficha.atendimento.individual.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.saude.ficha.atendimento.individual.exame.ListaExames;
import br.net.sigs.service.saude.ficha.atendimento.individual.util.ListaExamesService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/listaexames")
public class ListaExamesController {

	@Autowired
	private ListaExamesService service;
	
	@GetMapping
	public ResponseEntity<List<ListaExames>> findAll() {
		return ResponseEntity.ok().body(service.findAll());
	}
	
	@PostMapping("/findAllById")
	public ResponseEntity<List<ListaExames>> findAllById(@RequestBody List<Long> ids) {
		return ResponseEntity.ok().body(service.findAllById(ids));
	}
	
	@PostMapping("/findAllByNotInId")
	public ResponseEntity<List<ListaExames>> findAllByNotInId(@RequestBody List<Long> ids) {
		return ResponseEntity.ok().body(service.findAllByNotInId(ids));
	}
	
}
