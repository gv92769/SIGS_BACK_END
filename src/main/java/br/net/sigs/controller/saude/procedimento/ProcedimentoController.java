package br.net.sigs.controller.saude.procedimento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.ProcedimentoFiltroDto;
import br.net.sigs.entity.saude.procedimento.Procedimento;
import br.net.sigs.service.saude.procedimento.ProcedimentoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/procedimento")
public class ProcedimentoController {

	@Autowired
	private ProcedimentoService service;
	
	@PostMapping("/search")
	public ResponseEntity<List<Procedimento>> search(@RequestBody ProcedimentoFiltroDto filtro) {
		return ResponseEntity.ok().body(service.searchProcedimento(filtro));
	}
	
	@PostMapping("/search/exame")
	public ResponseEntity<List<Procedimento>> searchExame(@RequestBody ProcedimentoFiltroDto filtro) {
		return ResponseEntity.ok().body(service.searchExame(filtro));
	}
	
	@PostMapping("/search/odonto")
	public ResponseEntity<List<Procedimento>> searchOdonto(@RequestBody ProcedimentoFiltroDto filtro) {
		return ResponseEntity.ok().body(service.searchOdonto(filtro));
	}
	
}
