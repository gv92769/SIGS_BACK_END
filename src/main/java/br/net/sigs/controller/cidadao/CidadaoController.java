/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.cidadao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.CidadaoFilterDto;
import br.net.sigs.dto.FormCidadaoGeralDTO;
import br.net.sigs.dto.FormInformacaoSociodemograficaDTO;
import br.net.sigs.dto.FormMoradiaCidadaoDTO;
import br.net.sigs.dto.InformacoesCondicaoSaudeDTO;
import br.net.sigs.dto.InformacoesSituacaoRuaDTO;
import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.cidadao.CondicaoSaude;
import br.net.sigs.entity.cidadao.CondicaoSaudeTemporaria;
import br.net.sigs.entity.cidadao.FichaCadastroIndividual;
import br.net.sigs.entity.cidadao.InformacaoSocioDemografica;
import br.net.sigs.entity.cidadao.MoradiaCidadao;
import br.net.sigs.entity.cidadao.SituacaoRua;
import br.net.sigs.entity.domicilio.util.SituacaoImovel;
import br.net.sigs.entity.domicilio.util.SituacaoImovel.SituacaoImovelTipo;
import br.net.sigs.entity.util.Estado;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.repository.cidadao.CondicaoSaudeRepository;
import br.net.sigs.repository.cidadao.InformacaoSocioDemograficaRepository;
import br.net.sigs.repository.cidadao.MoradiaCidadaoRepository;
import br.net.sigs.repository.cidadao.SituacaoRuaRepository;
import br.net.sigs.service.cidadao.CidadaoService;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/cidadaos")
public class CidadaoController {
    
    @Autowired
    private CidadaoService service;
    
    @Autowired
    private MoradiaCidadaoRepository moradiaCidadaoRepository;
    
    @Autowired
    private InformacaoSocioDemograficaRepository informacaoRepository;

    @Autowired
    private CondicaoSaudeRepository condicaoSaudeRepository;

    @Autowired
    private SituacaoRuaRepository situacaoRuaRepository;

    @PostMapping
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<FichaCadastroIndividual> create(@RequestBody FichaCadastroIndividual ficha){
        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(ficha));
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Cidadao> findById(@PathVariable Long id){
        return service.findById(id)
                .map( cidadao -> ResponseEntity.ok().body(cidadao))
                .orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @PostMapping("/search")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<Page<Cidadao>> search(@RequestBody CidadaoFilterDto filter){
        return ResponseEntity.ok().body(service.search(filter));
    }
    
    @GetMapping("/filtro/{valor}")
    public ResponseEntity<List<Cidadao>> filtro(@PathVariable String valor) {
    	 return ResponseEntity.ok().body(service.filtro(valor));
    }
   
    @GetMapping("/completo/{id}")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<Map<String,Object>> findByIdWithRelationships(@PathVariable Long id){
        return service.findById(id)
                .map( cidadao -> 
                {
                    MoradiaCidadao moradiaCidadao = cidadao.getMoradiaCidadao();
        
                    Map<String,Object> map = new HashMap<>();
                    Object moradiaToMap = moradiaCidadao==null ? "{}" : moradiaCidadao;
                    map.put("cidadao", cidadao);
                    map.put("moradia", moradiaToMap);
                    return ResponseEntity.ok().body(map);
                })
                .orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
    
    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<FichaCadastroIndividual> update(@PathVariable("id") Long id, @RequestBody FichaCadastroIndividual ficha) {
        return service.findById(id)
                .map( cidadaoFound -> {
                    return ResponseEntity.ok().body(service.save(ficha));
                }).orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", ficha.getCidadao().getId())));
    }
    
    @PostMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public void delete(@RequestBody Long id) {
        service.delete(id);
    }
    
    @GetMapping("/initCidadaogeral")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<FormCidadaoGeralDTO> initCidadaogeral() {
        return ResponseEntity.ok().body(service.initCidadaogeral());
    }
    
    @GetMapping("/{id}/informacaosociodemografica")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<InformacaoSocioDemografica> findByIdInformacaoSocioDemografica(@PathVariable Long id){
        return service.findById(id)
                .map( cidadao -> {
                    return ResponseEntity.ok().body(service.getInformacaoSocioDemografica(cidadao));
                })
                .orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @PutMapping("/{id}/informacaosociodemografica")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<InformacaoSocioDemografica> updateInformacaoSocioDemografica(@PathVariable("id") Long id, @RequestBody FichaCadastroIndividual ficha){
        return service.findById(id)
                .map( cidadao -> {
                    ficha.getInformacao().setCidadao(cidadao);
                    InformacaoSocioDemografica inf = informacaoRepository.save(service.verificarCampo(ficha.getInformacao()));
                    cidadao.setInformacaoSocioDemografica(inf);
                    ficha.setCidadao(cidadao);
                    service.save(ficha);
                    return ResponseEntity.ok().body(inf);
                }).orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
    
    @GetMapping("/initInformacaosociodemografica")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<FormInformacaoSociodemograficaDTO> initInformacaoSociodemografica() {
        return ResponseEntity.ok().body(service.initInformacaoSociodemografica());
    }

    @PutMapping("/{id}/condicaosaude")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<CondicaoSaude> updateCondicaoSaude(@PathVariable("id") Long id, @RequestBody FichaCadastroIndividual ficha){
        return service.findById(id)
                .map( cidadao -> {
                    ficha.getCondicaoSaude().setCidadao(cidadao);
                    ficha.getCondicaoSaude().getCondicaoTemporaria().setCondicao(ficha.getCondicaoSaude());

                    if (ficha.getCondicaoSaude().getId() != null)
                        ficha.getCondicaoSaude().getCondicaoTemporaria().setId(ficha.getCondicaoSaude().getId());

                    CondicaoSaude condicao = condicaoSaudeRepository.save(ficha.getCondicaoSaude());
                    cidadao.setCondicaoSaude(condicao);
                    ficha.setCidadao(cidadao);
                    service.save(ficha);
                    return ResponseEntity.ok().body(condicao);
                }).orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @GetMapping("/{id}/condicaosaude")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<CondicaoSaude> findByIdCondicaoSaude(@PathVariable Long id){
        return service.findById(id)
                .map( cidadao -> {
                    CondicaoSaude condicao = cidadao.getCondicaoSaude();

                    if (condicao == null)
                        condicao = new CondicaoSaude();
                    else
                        condicao.setCondicaoTemporaria(new CondicaoSaudeTemporaria());

                    return ResponseEntity.ok().body(condicao);
                })
                .orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @GetMapping("/initCondicaosaude")
    public ResponseEntity<InformacoesCondicaoSaudeDTO> initCondicaosaude() {
        return ResponseEntity.ok().body(service.initCondicaosaude());
    }

    @GetMapping("/initSituacaoRua")
    public ResponseEntity<InformacoesSituacaoRuaDTO> initSituacaoRua() {
        return ResponseEntity.ok().body(service.initSituacaoRua());
    }

    @GetMapping("/{id}/situacaorua")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<SituacaoRua> findByIdSituacaoRua(@PathVariable Long id){
        return service.findById(id)
                .map( cidadao -> {
                    return ResponseEntity.ok().body(cidadao.getSituacaoRua());
                })
                .orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @PutMapping("/{id}/situacaorua")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<SituacaoRua> updateSituacaoRua(@PathVariable("id") Long id, @RequestBody FichaCadastroIndividual ficha){
        return service.findById(id)
                .map( cidadao -> {
                    ficha.getSituacaoRua().setCidadao(cidadao);
                    SituacaoRua situacao = situacaoRuaRepository.save(ficha.getSituacaoRua());
                    cidadao.setSituacaoRua(situacao);
                    ficha.setCidadao(cidadao);
                    service.save(ficha);
                    return ResponseEntity.ok().body(situacao);
                }).orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @GetMapping("/{id}/moradia")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<MoradiaCidadao> findByIdMoradia(@PathVariable Long id){
        return service.findById(id)
                .map( cidadao -> {
                    return ResponseEntity.ok().body(cidadao.getMoradiaCidadao());
                })
                .orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @PutMapping("/{id}/moradia")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<MoradiaCidadao> updateMoradia(@PathVariable("id") Long id, @RequestBody MoradiaCidadao moradia){
        return service.findById(id)
                .map( cidadao -> {
                    moradia.setCidadao(cidadao);
                    cidadao.setMoradiaCidadao(moradia);
                    moradiaCidadaoRepository.save(moradia);
                    return ResponseEntity.ok().body(moradia);
                }).orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @GetMapping("/initMoradiaCidadao")
    @PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ADMINISTRATIVO', 'ENFERMEIRO')")
    public ResponseEntity<FormMoradiaCidadaoDTO> initMoradiaCidadao() {
        return ResponseEntity.ok().body(service.initMoradiaCidadao());
    }

    @GetMapping("/opcoesEstado")
    public ResponseEntity<Estado[]> findAllOpcoesEstado(){
        return ResponseEntity.ok().body(Estado.values());
    }
    
    @GetMapping("/opcoesSituacaoImovelTipo")
    public ResponseEntity<SituacaoImovelTipo[]> findAllOpcoesSituacaoImovelTipo(){
        return ResponseEntity.ok().body(SituacaoImovel.SituacaoImovelTipo.values());
    }
    
}
