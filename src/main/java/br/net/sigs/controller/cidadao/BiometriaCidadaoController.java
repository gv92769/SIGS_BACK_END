/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.cidadao;

import br.net.sigs.dto.BiometriaCadastroDTO;
import br.net.sigs.dto.BiometriaDTO;
import br.net.sigs.entity.cidadao.BiometriaCidadao;
import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.exception.BadRequestException;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.repository.cidadao.BiometriaCidadaoRepository;
import br.net.sigs.repository.cidadao.CidadaoRepository;
import br.net.sigs.service.cidadao.BiometriaServico;

import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/cidadaos/biometria")
public class BiometriaCidadaoController {

    @Autowired
    private BiometriaCidadaoRepository biometriaCidadaoRepository;

    @Autowired
    private CidadaoRepository cidadaoRepository;

    @GetMapping("/possui/{id}")
    public ResponseEntity< Map<String,Object>> possuiBiometria(@PathVariable Long id){
        Map<String,Object> result = new HashMap<>();
        result.put("possuiDigitalPrincipal", false);
        result.put("possuiDigitalSecundaria", false);
        result.put("dedoDigitalPrincipal", -1);
        result.put("dedoDigitalSecundaria", -1);
        biometriaCidadaoRepository.findById(id).map(biometria -> {
            result.put("possuiDigitalPrincipal", biometria.getDigitalPrincipal() != null);
            result.put("possuiDigitalSecundaria", biometria.getDigitalSecundaria() != null);
            result.put("dedoDigitalPrincipal", biometria.getDigitalPrincipal()!= null ? biometria.getDedoDigitalPrincipal() : -1);
            result.put("dedoDigitalSecundaria", biometria.getDigitalSecundaria() != null ? biometria.getDedoDigitalSecundaria() : -1);
            return ResponseEntity.ok().body(result);
        });
        return ResponseEntity.ok().body(result); 
    }
    
    @PostMapping("/cadastrar/{id}")
    public ResponseEntity cadastrarOuAtualizar(@RequestBody BiometriaCadastroDTO biometriaCadastroDto, @PathVariable Long id) {
        Fmd fmd;
        try {
            BiometriaServico biometriaServico = new BiometriaServico();
            Fmd[] fmdCandidates = biometriaServico.toFmdArray(biometriaCadastroDto);
            fmd = biometriaServico.executeEnrollment(fmdCandidates);
        } catch (IOException | UareUException ex) {
            //Logger.getLogger(BiometriaCidadaoController.class.getName()).log(Level.SEVERE, null, ex);
            //erro na biometria
            throw new BadRequestException("Erro no processamento da biometria.");
        }
        if (fmd == null) {
            //nao foi possivel cadastrar, biometrias sem qualidade suficiente, refazer!
            throw new BadRequestException("Biometria não possui qualidade suficiente");
        }
        return cidadaoRepository.findById(id)
                .map((cidadao) -> {
                    BiometriaCidadao biometriaCidadao = biometriaCidadaoRepository.findByCidadao(cidadao);
                    if (biometriaCidadao == null) {
                        biometriaCidadao = new BiometriaCidadao();
                        biometriaCidadao.setCidadao(cidadao);
                    }
                    if (biometriaCadastroDto.isPrincipal()) {
                        biometriaCidadao.setDigitalPrincipal(fmd.getData());
                        biometriaCidadao.setDedoDigitalPrincipal(biometriaCadastroDto.getDedo());
                    } else {
                        biometriaCidadao.setDigitalSecundaria(fmd.getData());
                        biometriaCidadao.setDedoDigitalSecundaria(biometriaCadastroDto.getDedo());
                    }
                    biometriaCidadaoRepository.save(biometriaCidadao);
                    return new ResponseEntity(HttpStatus.OK);
                }).orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @PostMapping("/identificar")
    public ResponseEntity identificar(@RequestBody BiometriaDTO biometriaDto) throws UareUException {
        BiometriaServico biometriaServico = new BiometriaServico();
        Fmd fmd;
        try {
            fmd = biometriaServico.pngStringToFmd(biometriaDto.getDigital());
        } catch (IOException | UareUException ex) {
            //Logger.getLogger(BiometriaCidadaoController.class.getName()).log(Level.SEVERE, null, ex);
            throw new BadRequestException("Erro no processamento da biometria.");
        }
        List<BiometriaCidadao> biometrias = biometriaCidadaoRepository.findAll();
        Map<String,Object> response = new HashMap<>();
        
        //Digital Principal
        List<BiometriaCidadao> biometriasPrincipais = biometrias.stream().filter(b -> b.getDigitalPrincipal()!=null).collect(Collectors.toList());
        if(!biometriasPrincipais.isEmpty()){
            Fmd[] fmdsPrincipal = biometriaServico.toFmdArray(biometriasPrincipais, true);
            BiometriaServico.FmdIdentificationResult resultPrincipais = biometriaServico.executeIdentification(fmd, fmdsPrincipal);
            int[] indexesMatchedPrincipal = resultPrincipais.getIndexesMatched();
            if (indexesMatchedPrincipal.length > 0) {
                int indice = indexesMatchedPrincipal[0];
                Cidadao cidadao = biometriasPrincipais.get(indice).getCidadao();
                response.put("matched", true);
                response.put("idCidadao", cidadao.getId());
                return ResponseEntity.ok(response);
            }
        }
        
        //Digital Secundaria
        List<BiometriaCidadao> biometriasSecundarias = biometrias.stream().filter(b -> b.getDigitalSecundaria()!=null).collect(Collectors.toList());
        if(!biometriasSecundarias.isEmpty()){
            Fmd[] fmdsSecundaria = biometriaServico.toFmdArray(biometriasSecundarias, false);
            BiometriaServico.FmdIdentificationResult resultSecundarias = biometriaServico.executeIdentification(fmd, fmdsSecundaria);
            int[] indexesMatchedSecundaria = resultSecundarias.getIndexesMatched();
            if (indexesMatchedSecundaria.length > 0) {
                int indice = indexesMatchedSecundaria[0];
                Cidadao cidadao = biometriasSecundarias.get(indice).getCidadao();
                response.put("matched", true);
                response.put("idCidadao", cidadao.getId());
                return ResponseEntity.ok(response);
            }
        }
        
        response.put("matched", false);
        response.put("idCidadao", null);
        
        return ResponseEntity.ok(response);
    }

    @PostMapping("/verificar/{id}")
    public ResponseEntity verificar(@RequestBody BiometriaDTO biometriaDto, @PathVariable Long id) throws UareUException {
        BiometriaServico biometriaServico = new BiometriaServico();
        Fmd fmd;
        try {
            fmd = biometriaServico.pngStringToFmd(biometriaDto.getDigital());
        } catch (IOException | UareUException ex) {
            //Logger.getLogger(BiometriaCidadaoController.class.getName()).log(Level.SEVERE, null, ex);
            throw new BadRequestException("Erro no processamento da biometria.");
        }
        return cidadaoRepository.findById(id)
                .map((cidadao) -> {
                    BiometriaCidadao biometriaCidadao = biometriaCidadaoRepository.findByCidadao(cidadao);
                    if (biometriaCidadao == null) {
                        throw new EntityNotFoundException(BiometriaCidadao.class);
                    }
                    Engine engine = UareUGlobal.GetEngine();
                    Fmd fmdPrincipal;
                    try {
                        fmdPrincipal = biometriaServico.toFmd(biometriaCidadao, true);
                    } catch (UareUException ex) {
                        //Logger.getLogger(BiometriaCidadaoController.class.getName()).log(Level.SEVERE, null, ex);
                        throw new BadRequestException("Erro no processamento da biometria cadastrada.");
                    }
                    try {
                        Map<String, Boolean> result = new HashMap<>();
                        if (fmdPrincipal != null) {
                            BiometriaServico.FmdComparisonResult comparisonResultPrincipal = biometriaServico.compareFmds(engine, fmd, fmdPrincipal);
                            if (comparisonResultPrincipal.isFmdMatched()) {
                                result.put("matched", true);
                                return ResponseEntity.ok(result);
                            }
                        }
                    } catch (UareUException ex) {
                        //Logger.getLogger(BiometriaCidadaoController.class.getName()).log(Level.SEVERE, null, ex);
                        throw new BadRequestException("Erro no processamento de comparação das biometrias.");
                    }
                    Fmd fmdSecundaria;
                    try {
                        fmdSecundaria = biometriaServico.toFmd(biometriaCidadao, false);
                    } catch (UareUException ex) {
                        //Logger.getLogger(BiometriaCidadaoController.class.getName()).log(Level.SEVERE, null, ex);
                        throw new BadRequestException("Erro no processamento da biometria cadastrada.");
                    }
                    try {
                        Map<String, Boolean> result = new HashMap<>();
                        if (fmdSecundaria != null) {
                            BiometriaServico.FmdComparisonResult comparisonResultSecundaria = biometriaServico.compareFmds(engine, fmd, fmdSecundaria);
                            if (comparisonResultSecundaria.isFmdMatched()) {
                                result.put("matched", true);
                                return ResponseEntity.ok(result);
                            }
                        }
                    } catch (UareUException ex) {
                        //Logger.getLogger(BiometriaCidadaoController.class.getName()).log(Level.SEVERE, null, ex);
                        throw new BadRequestException("Erro no processamento de comparação das biometrias.");
                    }
                    Map<String,Boolean> result = new HashMap<>();
                    result.put("matched", false);
                    return ResponseEntity.ok(result);
                })
                .orElseThrow(() -> new EntityNotFoundException(Cidadao.class,
                    EntityNotFoundException.ParamBuilder.with("id", id)));
    }
    
    @DeleteMapping("/{id}/{principal}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarBiometria(@PathVariable("id") Long id, @PathVariable("principal") boolean principal){
        BiometriaCidadao biometriaCidadao = biometriaCidadaoRepository.getOne(id);
        if(principal){
            biometriaCidadao.setDigitalPrincipal(null);
            biometriaCidadao.setDedoDigitalPrincipal(null);
        }
        else{
            biometriaCidadao.setDigitalSecundaria(null);
            biometriaCidadao.setDedoDigitalSecundaria(null);
        }
        biometriaCidadaoRepository.save(biometriaCidadao);
    }
}
