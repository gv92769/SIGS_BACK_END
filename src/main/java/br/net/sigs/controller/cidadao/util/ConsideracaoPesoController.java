package br.net.sigs.controller.cidadao.util;

import br.net.sigs.entity.cidadao.util.ConsideracaoPeso;
import br.net.sigs.service.cidadao.util.ConsideracaoPesoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/consideracaopeso")
public class ConsideracaoPesoController {

    @Autowired
    private ConsideracaoPesoService service;

    @GetMapping
    public ResponseEntity<List<ConsideracaoPeso>> findAll(){
        return ResponseEntity.ok().body(service.findAll());
    }
}
