/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.util;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.service.util.EstatisticasServico;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/estatisticas")
public class EstatisticasController {
    
    @Autowired
    private EstatisticasServico estatisticasServico;
    
    @GetMapping("/inicio")
    public ResponseEntity<Map<String,Object>> getInformacoesTelaIncial(){
        long totalCidadaos = estatisticasServico.getTotalDeCidadaosCadastrados();
        long totalCidadaosComBiometria = estatisticasServico.getTotalDeCidadaosComBiometriaCadastrados();
        long totalAvaliacoes = estatisticasServico.getTotalAvaliacoesMedicasCadastradas();
        long totalAvaliacoesHoje = estatisticasServico.getTotalAvaliacoesMedicasCadastradasHoje();
        long totalAvaliacoes7dias = estatisticasServico.getTotalAvaliacoesMedicasCadastradasUltimos7Dias();
        long totalAvaliacoes30dias = estatisticasServico.getTotalAvaliacoesMedicasCadastradasUltimos30Dias();
        Map<String,Object> response = new HashMap<>();
        response.put("ncidadaos", totalCidadaos);
        response.put("ncidadaosComBiometria", totalCidadaosComBiometria);
        response.put("navaliacoes", totalAvaliacoes);
        response.put("navaliacoesHoje", totalAvaliacoesHoje);
        response.put("navaliacoes7dias", totalAvaliacoes7dias);
        response.put("navaliacoes30dias", totalAvaliacoes30dias);
        return ResponseEntity.ok(response);
    }
}
