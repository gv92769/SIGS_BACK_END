package br.net.sigs.controller.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.util.Municipio;
import br.net.sigs.service.util.MunicipioService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/municipio")
public class MunicipioController {

	@Autowired
	private MunicipioService municipioService;
	
	@GetMapping("/uf/{uf}/nome/{nome}")
	public ResponseEntity<List<Municipio>> findByUfAndNome(@PathVariable String uf, @PathVariable String nome) {
		return ResponseEntity.ok().body(municipioService.findByUfAndNome(uf, nome));
	}
	
}
