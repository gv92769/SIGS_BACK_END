package br.net.sigs.controller.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.service.util.ExportacaoService;

/**
*
* @author Naisses
*/
@RestController
@RequestMapping("/exportacao")
public class ExportacaoController {

	@Autowired
	private ExportacaoService service;
	
	@GetMapping
    @PreAuthorize("hasAnyRole('ADMINISTRADOR')")
	public void getFile(HttpServletResponse response) throws IOException {
	    String address = service.gerarExportacao();
	    File file = new File(address);
	    response.setContentType("application/zip");
	    response.setHeader("Content-disposition", "attachment; filename=" + file.getName());
	    response.setContentLength((int) file.length());
	    OutputStream out = response.getOutputStream();
	    FileInputStream in = new FileInputStream(file);
	    IOUtils.copy(in,out);
	    out.close();
	    in.close();
	    file.delete();
	}
	
	@GetMapping("/count")
	@PreAuthorize("hasAnyRole('ADMINISTRADOR')")
	public ResponseEntity<Long> countByImportada() {
		return ResponseEntity.ok().body(service.countByImportada());
	}
	
}
