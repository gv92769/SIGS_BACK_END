package br.net.sigs.controller.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.util.Pais;
import br.net.sigs.service.util.PaisService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/pais")
public class PaisController {

	@Autowired
	private PaisService paisService;
	
	@GetMapping("/nome/{nome}")
	public ResponseEntity<List<Pais>> findAllByNome(@PathVariable String nome) {
		return ResponseEntity.ok().body(paisService.findAllByNome(nome));
	}
	
}
