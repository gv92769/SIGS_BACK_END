/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.service.util.InfoService;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/info")
public class InfoController {
    
	@Autowired
    private InfoService infoService;
    
    @GetMapping("/ping")
    public ResponseEntity<Map<String,Integer>> ping(){
        Map<String,Integer> response = new HashMap<>();
        response.put("ping", 1);
        return ResponseEntity.ok(response);
    }
    
    @GetMapping("/me")
    public ResponseEntity<UserDetails> currentUser(@AuthenticationPrincipal Authentication authentication){
        return ResponseEntity.ok(infoService.currentUser(authentication));
    }
    
    @GetMapping("/hello")
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    public String hello(){
        return "hello admnistrador";
    }
    
}
