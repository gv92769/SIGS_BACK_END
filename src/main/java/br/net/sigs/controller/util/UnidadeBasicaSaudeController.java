/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.util;

import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.repository.saude.util.UnidadeBasicaSaudeRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/ubs")
public class UnidadeBasicaSaudeController {
    
    @Autowired
    private UnidadeBasicaSaudeRepository repository;
    
    @GetMapping
    public ResponseEntity<List<UnidadeBasicaSaude>> findAll(){
        return ResponseEntity.ok(repository.findAll());
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Optional<UnidadeBasicaSaude>> findById(@PathVariable Integer id){
        return ResponseEntity.ok(repository.findById(id));
    }
    
    @GetMapping("/cnes/{cnes}")
    public ResponseEntity<UnidadeBasicaSaude> findByCnes(@PathVariable String cnes){
        return ResponseEntity.ok(repository.findByCnes(cnes));
    }
    
    @GetMapping("/nome/{nome}")
    public ResponseEntity<List<UnidadeBasicaSaude>> findAllByNome(@PathVariable String nome){
    	Pageable pageable = org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("nome")));
        return ResponseEntity.ok(repository.findByNomeContaining(nome.toLowerCase(), pageable));
    }
    
    @GetMapping("/bairro/{bairro}")
    public ResponseEntity<List<UnidadeBasicaSaude>> findByBairro(@PathVariable String bairro){
        return ResponseEntity.ok(repository.findByBairroContaining(bairro));
    }
}
