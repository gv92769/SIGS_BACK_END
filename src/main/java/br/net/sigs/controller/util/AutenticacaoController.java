/*
s * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.config.CustomUserDetailsService;
import br.net.sigs.config.JwtTokenProvider;
import br.net.sigs.dto.LotacaoDto;
import br.net.sigs.dto.UsuarioLoginDTO;
import br.net.sigs.entity.usuario.Lotacao;
import br.net.sigs.entity.usuario.Usuario;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.exception.UnauthorizedExcetion;
import br.net.sigs.repository.usuario.LotacaoRepository;
import br.net.sigs.repository.usuario.UsuarioRepository;
import br.net.sigs.service.usuario.PerfilService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/autenticacao")
//@CrossOrigin(origins = {"http://localhost:4200","http://localhost"})
public class AutenticacaoController {
    
    private final static String KEY_ACCESS_TOKEN = "access_token";
    
    private final static String KEY_REFRESH_TOKEN = "refresh_token";
    
    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private JwtTokenProvider tokenProvider;
    
    @Autowired
    private CustomUserDetailsService customUserDetailsService;
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private LotacaoRepository lotacaoRepository;
    
    @Autowired
	private PerfilService perfilService;
    
    @PostMapping("/login")
    public ResponseEntity<Map<String,String>> login(@RequestBody UsuarioLoginDTO usuarioLogin) {
    	return  usuarioRepository.findByLogin(usuarioLogin.getLogin())
	    	 .map( user -> {
			        Authentication authentication = authenticationManager.authenticate(
			                new UsernamePasswordAuthenticationToken(
			                        usuarioLogin.getLogin(),
			                        usuarioLogin.getSenha()
			                )
			        );
			        
			        SecurityContextHolder.getContext().setAuthentication(authentication);
			        
			        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
			        user = (Usuario) userDetails;
			        userDetails = user;
			        
			        String accessToken = tokenProvider.createAccessToken(userDetails, "LOGIN");
			        String refreshToken = tokenProvider.createRefreshToken(userDetails, "LOGIN");
			        Map<String,String> tokenMap = new HashMap<>();
			        tokenMap.put(KEY_ACCESS_TOKEN, accessToken);
			        tokenMap.put(KEY_REFRESH_TOKEN, refreshToken);
			        
			        return ResponseEntity.ok(tokenMap);
	    	}) .orElseThrow(() -> new EntityNotFoundException(Usuario.class,
                    EntityNotFoundException.ParamBuilder.with("login", usuarioLogin.getLogin())));
    }
    
    @PostMapping("/lotacao")
    public ResponseEntity<Map<String,String>> lotacao(@RequestBody LotacaoDto lotacaoDto) {
    	Authentication authentication = null;
    	Lotacao lotacao = null;
    	
    	try{
            authentication = tokenProvider.parseRefreshToken(lotacaoDto.getToken());
            lotacao = lotacaoRepository.getOne(lotacaoDto.getId());
        } catch(ExpiredJwtException e){
            throw new UnauthorizedExcetion("expired_refresh_token");
        } catch(JwtException e){
            throw new UnauthorizedExcetion("invalid_refresh_token");
        }
    	
    	String login = (String) authentication.getPrincipal();
        UserDetails userDetails = customUserDetailsService.loadUserByUsername(login);
        
        Usuario user = (Usuario) userDetails;
        user.setCnes(lotacao.getUbs().getCnes());
        user.setCbo(lotacao.getCbo().getCodigo());
        user.setPerfil(lotacao.getPerfil());
        userDetails =  user;
    	
        if(!userDetails.isEnabled()){
            throw new UnauthorizedExcetion("user_inactive");
        }
        
        String accessToken = tokenProvider.createAccessToken(userDetails, "LOTACAO");
        String refreshToken = tokenProvider.createRefreshToken(userDetails, "LOTACAO");
        Map<String,String> tokenMap = new HashMap<>();
        tokenMap.put(KEY_ACCESS_TOKEN, accessToken);
        tokenMap.put(KEY_REFRESH_TOKEN, refreshToken);
        
        return ResponseEntity.ok(tokenMap);
    }
    
    @PostMapping("/refreshtoken")
    public ResponseEntity<Map<String,String>> refreshToken(@RequestBody String refreshToken) {
        Authentication authentication = null;
        String cnes = null;
        String cbo = null;
        String perfil = null;
        
        try{
            authentication = tokenProvider.parseRefreshToken(refreshToken);
            cnes = tokenProvider.claimCnes(refreshToken);
            cbo = tokenProvider.claimCbo(refreshToken);
            perfil = tokenProvider.claimPerfil(refreshToken);
        } catch(ExpiredJwtException e){
            throw new UnauthorizedExcetion("expired_refresh_token");
        } catch(JwtException e){
            throw new UnauthorizedExcetion("invalid_refresh_token");
        }
        
        String login = (String) authentication.getPrincipal();
        UserDetails userDetails = customUserDetailsService.loadUserByUsername(login);
        
        Usuario user = (Usuario) userDetails;
        user.setCnes(cnes);
        user.setCbo(cbo);
        user.setPerfil(perfilService.findByPerfil(perfil));
        userDetails =  user;
        
        if(!userDetails.isEnabled()){
            throw new UnauthorizedExcetion("user_inactive");
        }
        
        String accessToken = tokenProvider.createAccessToken(userDetails, "LOTACAO");
        Map<String,String> tokenMap = new HashMap<>();
        tokenMap.put(KEY_ACCESS_TOKEN, accessToken);
        
        return ResponseEntity.ok(tokenMap);
    }
    
}
