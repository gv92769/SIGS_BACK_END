/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.util;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.util.Cbo;
import br.net.sigs.service.util.CboService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/cbo")
public class CboController {
    
    @Autowired
    private CboService service;
    
    @GetMapping("/codigo/{codigo}")
    public ResponseEntity<Optional<Cbo>> findByCodigo(@PathVariable String codigo){
        return ResponseEntity.ok(service.findByCodigo(codigo));
    }
    
    @GetMapping("/titulo/{titulo}")
    public ResponseEntity<List<Cbo>> findAllByTitulo(@PathVariable String titulo){
        return ResponseEntity.ok(service.findAllByTitulo(titulo));
    }
    
    @GetMapping("/titulo/{titulo}/id/{id}")
    public ResponseEntity<List<Cbo>> findAllByTituloByPerfil(@PathVariable String titulo, @PathVariable Long id) {
    	return ResponseEntity.ok(service.findAllByTituloByPerfil(titulo, id));
    }
    
    @GetMapping("/findallbyprofissional/{id}")
    public ResponseEntity<List<Cbo>> findAllByProfissional(@PathVariable Long id) {
    	return ResponseEntity.ok(service.findAllByProfissional(id));
    }
    
}
