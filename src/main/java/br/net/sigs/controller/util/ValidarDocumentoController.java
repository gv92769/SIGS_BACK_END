package br.net.sigs.controller.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.service.util.ValidarDocumentoService;

/**
*
* @author Gabriel Santos
*/
@RestController
@RequestMapping("/validardocumentos")
public class ValidarDocumentoController {
	
	@Autowired
	private ValidarDocumentoService service;
	
	@GetMapping("id/{id}/cns/{cns}/tipo/{tipo}")
    public ResponseEntity<Boolean> validaCns(@PathVariable Long id, @PathVariable String cns, @PathVariable String tipo){
        return ResponseEntity.ok().body(service.validaCns(id, cns, tipo));       
    }
	
	@GetMapping("id/{id}/cpf/{cpf}/tipo/{tipo}")
    public ResponseEntity<Boolean> validaCpf(@PathVariable Long id, @PathVariable String cpf, @PathVariable String tipo){
        return ResponseEntity.ok().body(service.validaCpf(id, cpf, tipo));       
    }
	
}
