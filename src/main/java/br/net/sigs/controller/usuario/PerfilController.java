package br.net.sigs.controller.usuario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.usuario.Perfil;
import br.net.sigs.service.usuario.PerfilService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/perfil")
public class PerfilController {

	@Autowired
	private PerfilService service;
	
	@GetMapping("/findallbyativo")
	@PreAuthorize("hasAnyRole('ADMINISTRADOR', 'ENFERMEIRO', 'FARMACEUTICO',  'FISIOTERAPIA', 'MASSOTERAPEUTA', 'FONOAUDIOLOGO', 'MEDICO', 'NUTRICIONISTA', 'PSICOLOGO', 'ROLE_ASSISTENTE_SOCIAL')")
	public ResponseEntity<List<Perfil>> findAllByAtivo() {
		return ResponseEntity.ok().body(service.findAllByAtivo());
	}
	
}
