package br.net.sigs.controller.usuario;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.usuario.Lotacao;
import br.net.sigs.entity.usuario.Usuario;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.usuario.LotacaoService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/lotacao")
public class LotacaoController {

	@Autowired
    private LotacaoService service;
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAnyRole('ADMINISTRADOR')")
	public ResponseEntity<Lotacao> findById(@PathVariable("id") Long id) {
		 return service.findById(id)
	                .map( lotacao -> {
	                    return ResponseEntity.ok().body(lotacao);
	                }).orElseThrow(() -> new EntityNotFoundException(Lotacao.class,
	                        EntityNotFoundException.ParamBuilder.with("id", id)));
	}
	
	
	@PostMapping
	@PreAuthorize("hasAnyRole('ADMINISTRADOR')")
    public ResponseEntity<Lotacao> save(@RequestBody Lotacao lotacao) {
    	return ResponseEntity.ok().body(service.save(lotacao));
    }
	
	@PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRADOR')")
    public ResponseEntity<Lotacao> update(@PathVariable("id") Long id, @RequestBody Lotacao lotacao) {
        return service.findById(id)
                .map( lotacaoFound -> {
                    return ResponseEntity.ok().body(service.save(lotacao));
                }).orElseThrow(() -> new EntityNotFoundException(Lotacao.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@GetMapping("/search/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRADOR')")
    public ResponseEntity<List<Lotacao>> search(@PathVariable("id") Long id) {
        return ResponseEntity.ok().body(service.search(id));
    }
    
	@GetMapping("/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRADOR')")
    public void delete(@PathVariable("id") Long id) {
    	Optional<Lotacao> lotacaoOp = service.findById(id);
    	if (lotacaoOp.isPresent())
    		service.delete(lotacaoOp.get());
    	else {
    		new EntityNotFoundException(Usuario.class,
                        EntityNotFoundException.ParamBuilder.with("id", id));
    	}
    }
	
	@GetMapping("/perfis/{id}")
	public ResponseEntity<List<Lotacao>> findAllByIdByAtivo(@PathVariable("id") Long id) {
		return ResponseEntity.ok().body(service.findAllByIdByAtivo(id));
	}
	
	
}
