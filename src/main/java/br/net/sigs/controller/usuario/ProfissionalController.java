package br.net.sigs.controller.usuario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.usuario.ProfissionalService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/profissional")
public class ProfissionalController {

	@Autowired
	private ProfissionalService profissionalService;
	
	@GetMapping("/{id}")
    public ResponseEntity<Profissional> findById(@PathVariable Long id){
        return profissionalService.findById(id)
	        		 .map( profissional -> {
	        			 return ResponseEntity.ok().body(profissional);
	        		  }).orElseThrow(() -> new EntityNotFoundException(Profissional.class,
		                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@GetMapping("/search/{valor}")
	public ResponseEntity<List<Profissional>> search(@PathVariable String valor) {
		return ResponseEntity.ok().body(profissionalService.search(valor));
	}
	
}
