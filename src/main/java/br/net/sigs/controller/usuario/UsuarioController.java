/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.controller.usuario;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.UsuarioFilterDto;
import br.net.sigs.dto.UsuarioProfissionalDto;
import br.net.sigs.entity.usuario.Usuario;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.repository.usuario.UsuarioRepository;
import br.net.sigs.service.usuario.UsuarioService;

/**
 *
 * @author Naisses
 */
@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
    
    @Autowired
    private UsuarioRepository repository;
    
    @Autowired
    private UsuarioService usuarioService;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @GetMapping("/{id}")
    public ResponseEntity<Usuario> findById(@PathVariable Long id){
        return usuarioService.findById(id)
                .map( usuario -> ResponseEntity.ok().body(usuario))
                .orElseThrow(() -> new EntityNotFoundException(Usuario.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
    
    @PostMapping("/search")
    @PreAuthorize("hasAnyRole('ADMINISTRADOR')")
    public ResponseEntity<Page<Usuario>> search(@RequestBody UsuarioFilterDto filter) {
    	return ResponseEntity.ok().body(usuarioService.search(filter));
    }
    
    @GetMapping("/login/{login}")
    @PreAuthorize("hasAnyRole('ADMINISTRADOR')")
    public ResponseEntity<Usuario> findByLogin(@PathVariable String login){
        return repository.findByLogin(login)
                .map( usuario -> ResponseEntity.ok().body(usuario))
                .orElseThrow(() -> new EntityNotFoundException(Usuario.class,
                        EntityNotFoundException.ParamBuilder.with("login", login)));
    }
    
    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRADOR')")
    public ResponseEntity<UsuarioProfissionalDto> create(@Valid @RequestBody UsuarioProfissionalDto usuarioProfissionalDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(usuarioService.create(usuarioProfissionalDto));
    }
    
    @PostMapping("/delete")
    @PreAuthorize("hasAnyRole('ADMINISTRADOR')")
    public void delete(@RequestBody Long id){
    	Optional<Usuario> usuarioOp = usuarioService.findById(id);
    	if (usuarioOp.isPresent())
    		usuarioService.delete(usuarioOp.get());
    	else {
    		new EntityNotFoundException(Usuario.class,
                        EntityNotFoundException.ParamBuilder.with("id", id));
    	}
    }
    
    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRADOR')")
    public ResponseEntity<UsuarioProfissionalDto> update(@PathVariable("id") Long id, @RequestBody UsuarioProfissionalDto usuarioProfissionalDto){
    	if(usuarioProfissionalDto.getUsuario().getSenha() != null){
    		usuarioProfissionalDto.getUsuario().setSenha(passwordEncoder.encode(usuarioProfissionalDto.getUsuario().getSenha()));
        }
    	
        return usuarioService.findById(id)
                .map( usuarioFound -> {
                	usuarioProfissionalDto.getUsuario().setSenha(usuarioProfissionalDto.getUsuario().getSenha() == null ? 
                			usuarioFound.getSenha() : usuarioProfissionalDto.getUsuario().getSenha());
                    return ResponseEntity.ok().body(usuarioService.save(usuarioProfissionalDto));
                }).orElseThrow(() -> new EntityNotFoundException(Usuario.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

}
