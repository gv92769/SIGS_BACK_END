package br.net.sigs.controller.domicilio.util;

import br.net.sigs.entity.domicilio.Desfecho;
import br.net.sigs.service.domicilio.util.DesfechoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/desfecho")
public class DesfechoController {

    @Autowired
    private DesfechoService service;

    @GetMapping
    public ResponseEntity<List<Desfecho>> findAll() {
        return ResponseEntity.ok().body(service.findAll());
    }
}
