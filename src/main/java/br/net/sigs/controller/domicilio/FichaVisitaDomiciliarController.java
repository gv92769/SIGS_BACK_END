package br.net.sigs.controller.domicilio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.dto.FichaVisitaDomiciliarTabelaDTO;
import br.net.sigs.entity.domicilio.FichaVisitaDomiciliar;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.service.domicilio.FichaVisitaDomiciliarService;



@RestController
@RequestMapping("/fichavisitadomiciliar")
public class FichaVisitaDomiciliarController {

    @Autowired
    private FichaVisitaDomiciliarService service;

    @PostMapping
    public ResponseEntity<FichaVisitaDomiciliar> create(@RequestBody FichaVisitaDomiciliar ficha) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(ficha));
    }

    @GetMapping("/{id}")
    public ResponseEntity<FichaVisitaDomiciliar> findById(@PathVariable Long id){
        return service.findById(id)
                .map( ficha -> ResponseEntity.ok().body(ficha))
                .orElseThrow(() -> new EntityNotFoundException(FichaVisitaDomiciliar.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @PutMapping("/{id}")
    public ResponseEntity<FichaVisitaDomiciliar> update(@PathVariable Long id, @RequestBody FichaVisitaDomiciliar ficha){
        return service.findById(id)
                .map( fichaFound -> ResponseEntity.ok().body(service.save(ficha)))
                .orElseThrow(() -> new EntityNotFoundException(FichaVisitaDomiciliar.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
      service.delete(id);
    }
  
    @PostMapping("/search")
    public ResponseEntity<Page<FichaVisitaDomiciliarTabelaDTO>> search(@RequestBody FichaFiltroDto filter) {
        return ResponseEntity.ok().body(service.search(filter));
    }

    @GetMapping("/exportada/{id}")
    public ResponseEntity<Boolean> importada(@PathVariable("id") Long id) {
        return service.findById(id)
            .map( ficha -> ResponseEntity.ok().body(ficha.getImportada()))
            .orElseThrow(() -> new EntityNotFoundException(FichaVisitaDomiciliar.class,
                EntityNotFoundException.ParamBuilder.with("id", id)));
    }
  
}