package br.net.sigs.controller.domicilio.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.entity.domicilio.MotivoVisita;
import br.net.sigs.service.domicilio.util.MotivoVisitaService;

@RestController
@RequestMapping("/motivovisita")
public class MotivoVisitaController {

    @Autowired
    private MotivoVisitaService service;

    @PostMapping
    public ResponseEntity<List<MotivoVisita>> findAllById(@RequestBody List<Long> codigo) {
        return ResponseEntity.ok().body(service.findAllById(codigo));
    }

}
