package br.net.sigs.controller.domicilio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.sigs.dto.DomicilioFilterDto;
import br.net.sigs.dto.FormCadastroDomiciliarDTO;
import br.net.sigs.entity.domicilio.Domicilio;
import br.net.sigs.entity.domicilio.EnderecoLocalPermanencia;
import br.net.sigs.entity.domicilio.FichaCadastroDomiciliar;
import br.net.sigs.exception.EntityNotFoundException;
import br.net.sigs.repository.domicilio.DomicilioRepository;
import br.net.sigs.service.domicilio.CadastroDomiciliarService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/cadastrodomiciliar")
public class CadastroDomiciliarController {

	@Autowired
	private CadastroDomiciliarService service;
	
	@Autowired
	private DomicilioRepository domicilioRepository;
	
	@PostMapping
	@PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ENFERMEIRO')")
	public ResponseEntity<FichaCadastroDomiciliar> create(@RequestBody FichaCadastroDomiciliar ficha) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(ficha));
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ENFERMEIRO')")
	public ResponseEntity<Domicilio> findById(@PathVariable Long id) {
		return ResponseEntity.ok().body(service.findById(id));
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ENFERMEIRO')")
    public ResponseEntity<FichaCadastroDomiciliar> update(@PathVariable("id") Long id, @RequestBody FichaCadastroDomiciliar ficha){
        return domicilioRepository.findById(id)
                .map( domicilioFound -> {
                    return ResponseEntity.ok().body(service.save(ficha));
                }).orElseThrow(() -> new EntityNotFoundException(Domicilio.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
    }
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ENFERMEIRO')")
    public void delete(@PathVariable("id") Long id){
		domicilioRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(Domicilio.class,
                        EntityNotFoundException.ParamBuilder.with("id", id)));
		service.delete(id);
    }

	@PostMapping("/search")
	@PreAuthorize("hasAnyRole('AGENTE_SAUDE', 'ENFERMEIRO')")
	public Page<EnderecoLocalPermanencia> search(@RequestBody DomicilioFilterDto filter) {
		return service.search(filter);
	}
	
	@GetMapping("/init")
    public ResponseEntity<FormCadastroDomiciliarDTO> init() {
        return ResponseEntity.ok().body(service.init());
    }
	
}
