package br.net.sigs.service.util;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import br.net.sigs.dto.PageRequest;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class SearchService {

	public Pageable pageable(PageRequest page) {
		Pageable pageable = org.springframework.data.domain.PageRequest.of(page.getPageNumber(), page.getSize(), Sort.by(Order.asc("id")));
		
		if (page.getSort() != null) {
			if (page.getSort().equals("asc"))
				pageable = org.springframework.data.domain.PageRequest.of(page.getPageNumber(), page.getSize(), Sort.by(Order.asc(page.getColumn())));
			else
				pageable = org.springframework.data.domain.PageRequest.of(page.getPageNumber(), page.getSize(), Sort.by(Order.desc(page.getColumn())));
		}
	
		return pageable;
	}
	
}
