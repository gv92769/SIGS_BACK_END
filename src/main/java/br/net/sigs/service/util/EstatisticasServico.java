/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.service.util;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.repository.cidadao.BiometriaCidadaoRepository;
import br.net.sigs.repository.cidadao.CidadaoRepository;
import br.net.sigs.repository.saude.ficha.util.AvaliacaoMedicaRepository;

/**
 *
 * @author Naisses
 */
@Service
public class EstatisticasServico {
    
    @Autowired
    private CidadaoRepository cidadaoRepository;
    
    @Autowired
    private AvaliacaoMedicaRepository avaliacaoMedicaRepository;
    
    @Autowired
    private BiometriaCidadaoRepository biometriaCidadaoRepository;
    
    public long getTotalDeCidadaosCadastrados(){
        return cidadaoRepository.count();
    }
    
    public long getTotalAvaliacoesMedicasCadastradas(){
        return avaliacaoMedicaRepository.count();
    }
    
    public long getTotalDeCidadaosComBiometriaCadastrados(){
        return biometriaCidadaoRepository.count();
    }
    
    public long getTotalAvaliacoesMedicasCadastradasHoje(){
        return getTotalAvaliacoesMedicasCadastradasNUltimosDias(1);
    }
    
    public long getTotalAvaliacoesMedicasCadastradasUltimos7Dias(){
        return getTotalAvaliacoesMedicasCadastradasNUltimosDias(7);
    }
    
    public long getTotalAvaliacoesMedicasCadastradasUltimos30Dias(){
        return getTotalAvaliacoesMedicasCadastradasNUltimosDias(30);
    }
    
    private long getTotalAvaliacoesMedicasCadastradasNUltimosDias(int n){
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DAY_OF_WEEK, -n);
        Long total = avaliacaoMedicaRepository.countByDataRealizacaoAfter(calendar.getTime());
        return total;
    }
    
}
