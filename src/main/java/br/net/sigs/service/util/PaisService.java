package br.net.sigs.service.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.util.Pais;
import br.net.sigs.repository.util.PaisRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class PaisService {

	@Autowired
	private PaisRepository paisRepository;
	
	public List<Pais> findAllByNome(String nome) {
		return paisRepository.findAllByNome(nome.toLowerCase(), org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("nome"))));
	}
	
}
