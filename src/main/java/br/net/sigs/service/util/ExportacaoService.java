package br.net.sigs.service.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import br.net.sigs.service.domicilio.FichaVisitaDomiciliarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.dto.integracao.util.InformacaoTransportDto;
import br.net.sigs.dto.integracao.util.LotacaoHeaderTransportDto;
import br.net.sigs.dto.integracao.util.UnicaLotacaoHeaderTransportDto;
import br.net.sigs.dto.integracao.util.VariasLotacoesHeaderTransportDto;
import br.net.sigs.entity.saude.util.UnidadeBasicaSaude;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.entity.util.Cbo;
import br.net.sigs.service.cidadao.CidadaoService;
import br.net.sigs.service.domicilio.CadastroDomiciliarService;
import br.net.sigs.service.saude.ficha.atendimento.individual.AtendimentoIndividualService;
import br.net.sigs.service.saude.ficha.atendimento.odontologico.FichaAtendimentoOdontologicoService;
import br.net.sigs.service.saude.ficha.atividade.AtividadeColetivaService;
import br.net.sigs.service.saude.ficha.procedimento.FichaProcedimentoService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ExportacaoService {

	@Autowired
	private CidadaoService cidadao;
	
	@Autowired
	private CadastroDomiciliarService cadastroDomiciliar;
	
	@Autowired
	private AtendimentoIndividualService atendimentoIndividual;
	
	@Autowired
	private FichaAtendimentoOdontologicoService fichaAtendimentoOdontologico;
	
	@Autowired
	private AtividadeColetivaService atividadeColetiva;
	
	@Autowired
	private FichaProcedimentoService fichaProcedimento;

	@Autowired
	private FichaVisitaDomiciliarService fichaVisitaDomiciliar;
	
	public UnicaLotacaoHeaderTransportDto headerTrasport(Profissional profissional, Cbo cbo, UnidadeBasicaSaude ubs, Long dataAtendimento) {
		UnicaLotacaoHeaderTransportDto header = new UnicaLotacaoHeaderTransportDto();
		
		header.setCboCodigo_2002(cbo.getCodigo());
		header.setCnes(ubs.getCnes());
		header.setCodigoIbgeMunicipio("3302007");
		header.setDataAtendimento(dataAtendimento);
		header.setProfissionalCNS(profissional.getCns());
		
		return header;
	}
	
	public LotacaoHeaderTransportDto lotacaoHeader(Profissional profissional, Cbo cbo, UnidadeBasicaSaude ubs) {
		LotacaoHeaderTransportDto lotacao = new LotacaoHeaderTransportDto();
		
		lotacao.setCboCodigo_2002(cbo.getCodigo());
		lotacao.setProfissionalCNS(profissional.getCns());
		lotacao.setCnes(ubs.getCnes());
		
		return lotacao;
	}
	
	public VariasLotacoesHeaderTransportDto headerTrasport(List<Profissional> profissionais, Cbo cbo,UnidadeBasicaSaude ubs, Long dataAtendimento) {
		VariasLotacoesHeaderTransportDto header = new VariasLotacoesHeaderTransportDto();
		
		header.setCodigoIbgeMunicipio("3302007");
		header.setDataAtendimento(dataAtendimento);
		header.setLotacaoFormPrincipal(lotacaoHeader(profissionais.get(0), cbo, ubs));
		header.setLotacaoFormAtendimentoCompartilhado(profissionais.size() > 1 ? lotacaoHeader(profissionais.get(1), cbo, ubs) : null);
		
		return header;
	}
	
	public InformacaoTransportDto informacaoTransport(InformacaoTransportDto dado, String cnes, String uuid, Long tipoDado, String ine) {
		dado.setCnesDadoSerializado(cnes);
		dado.setCodIbge("3302007");
		dado.setIneDadoSerializado(ine);
		dado.setNumLote(null);
		dado.setTipoDadoSerializado(tipoDado);
		dado.setUuidDadoSerializado(uuid);
		
		return dado;
	}
	
	public void zip(ZipOutputStream zipOut, String srcFile) throws IOException {
		File fileToZip = new File(srcFile);
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
        zipOut.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
	}
	
	public String gerarExportacao() {
		String path = "fichas-exportacao-" + new Date().getTime() + ".zip";
		
		try (FileOutputStream fos = new FileOutputStream(path);
			     ZipOutputStream zipOut = new ZipOutputStream(fos);) {
			 
			cidadao.gerarExportacao(zipOut);
			cadastroDomiciliar.gerarExportacao(zipOut);
			atendimentoIndividual.gerarExportacao(zipOut);
			fichaAtendimentoOdontologico.gerarExportacao(zipOut);
			atividadeColetiva.gerarExportacao(zipOut);
			fichaProcedimento.gerarExportacao(zipOut);
			fichaVisitaDomiciliar.gerarExportacao(zipOut);
			
		 } catch (Exception e) {
				e.printStackTrace();
		}
        
        return path;
	}
	
	public Long countByImportada() {
		return cidadao.countByImportada() + cadastroDomiciliar.countByImportada() + atendimentoIndividual.countByImportada() + fichaAtendimentoOdontologico.countByImportada()
			+ atividadeColetiva.countByImportada() + fichaProcedimento.countByImportada();
	}
	
}
