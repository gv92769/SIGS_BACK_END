package br.net.sigs.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.repository.cidadao.CidadaoRepository;
import br.net.sigs.repository.usuario.ProfissionalRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ValidarDocumentoService {

	@Autowired
    private CidadaoRepository cidadaoRepository;
	
	@Autowired
    private ProfissionalRepository profissionalRepository;
	
	public boolean validaCns(String cns){
		float soma;
		float resto, dv;
		String pis = new String("");
		String resultado = new String("");
		pis = cns.substring(0,11);

		soma = ((Integer.valueOf(pis.substring(0,1)).intValue()) * 15) +
		((Integer.valueOf(pis.substring(1,2)).intValue()) * 14) +
		((Integer.valueOf(pis.substring(2,3)).intValue()) * 13) +
		((Integer.valueOf(pis.substring(3,4)).intValue()) * 12) +
		((Integer.valueOf(pis.substring(4,5)).intValue()) * 11) +
		((Integer.valueOf(pis.substring(5,6)).intValue()) * 10) +
		((Integer.valueOf(pis.substring(6,7)).intValue()) * 9) +
		((Integer.valueOf(pis.substring(7,8)).intValue()) * 8) +
		((Integer.valueOf(pis.substring(8,9)).intValue()) * 7) +
		((Integer.valueOf(pis.substring(9,10)).intValue()) * 6) +
		((Integer.valueOf(pis.substring(10,11)).intValue()) * 5);

		resto = soma % 11;
		dv = 11 - resto;

		if (dv == 11){
		dv = 0;
		}

		if (dv == 10){
			soma = ((Integer.valueOf(pis.substring(0,1)).intValue()) * 15) +
			((Integer.valueOf(pis.substring(1,2)).intValue()) * 14) +
			((Integer.valueOf(pis.substring(2,3)).intValue()) * 13) +
			((Integer.valueOf(pis.substring(3,4)).intValue()) * 12) +
			((Integer.valueOf(pis.substring(4,5)).intValue()) * 11) +
			((Integer.valueOf(pis.substring(5,6)).intValue()) * 10) +
			((Integer.valueOf(pis.substring(6,7)).intValue()) * 9) +
			((Integer.valueOf(pis.substring(7,8)).intValue()) * 8) +
			((Integer.valueOf(pis.substring(8,9)).intValue()) * 7) +
			((Integer.valueOf(pis.substring(9,10)).intValue()) * 6) +
			((Integer.valueOf(pis.substring(10,11)).intValue()) * 5) + 2;
	
			resto = soma % 11;
			dv = 11 - resto;
			resultado = pis + "001" + String.valueOf((int)dv);
		}
		else{
			resultado = pis + "000" + String.valueOf((int)dv);
		}

		if (! cns.equals(resultado)){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean validaCnsProv(String cns){
		float resto,soma;

		soma = ((Integer.valueOf(cns.substring(0,1)).intValue()) * 15) +
		((Integer.valueOf(cns.substring(1,2)).intValue()) * 14) +
		((Integer.valueOf(cns.substring(2,3)).intValue()) * 13) +
		((Integer.valueOf(cns.substring(3,4)).intValue()) * 12) +
		((Integer.valueOf(cns.substring(4,5)).intValue()) * 11) +
		((Integer.valueOf(cns.substring(5,6)).intValue()) * 10) +
		((Integer.valueOf(cns.substring(6,7)).intValue()) * 9) +
		((Integer.valueOf(cns.substring(7,8)).intValue()) * 8) +
		((Integer.valueOf(cns.substring(8,9)).intValue()) * 7) +
		((Integer.valueOf(cns.substring(9,10)).intValue()) * 6) +
		((Integer.valueOf(cns.substring(10,11)).intValue()) * 5) +
		((Integer.valueOf(cns.substring(11,12)).intValue()) * 4) +
		((Integer.valueOf(cns.substring(12,13)).intValue()) * 3) +
		((Integer.valueOf(cns.substring(13,14)).intValue()) * 2) +
		((Integer.valueOf(cns.substring(14,15)).intValue()) * 1);

		resto = soma % 11;

		if (resto != 0){
			return false;
		}
		else{
			return true;
		}
	}

	public boolean validaCns(Long id, String cns, String tipo){
		if (cns.trim().length() != 15){
			return false ;
		}
		
		String primeiroDigito = cns.substring(0, 1);
		Boolean valido = false;
		
		if (primeiroDigito.equals("1") || primeiroDigito.equals("2"))
			valido = validaCns(cns);
		else {
			if (primeiroDigito.equals("7") || primeiroDigito.equals("8") || primeiroDigito.equals("9"))
				valido = validaCnsProv(cns);
			else
				return valido;
		}
		
		if(id != -1 && valido == true && tipo.equals("CIDADAO")) {
			Cidadao cidadao = cidadaoRepository.findByCns(cns);
			return cidadao == null ? true : cidadao != null && id != 0 && cidadao.getId().equals(id) ? true : false;
		} 
		
		if(id != -1 && valido == true && tipo.equals("PROFISSIONAL")) {
			Profissional profissional = profissionalRepository.findByCns(cns);
			return profissional == null ? true : profissional != null && id != 0 && profissional.getId().equals(id) ? true : false;
		} 
		
		return valido;
	}

	
	public boolean validaCpf(String cpf) {
		if (cpf.trim().length() != 11){
			return false;
		}
		
	    int Soma = 0;
	    int Resto;
	    
	    if (cpf == "00000000000") 
	    	return false;

	    for (int i = 1; i <= 9; i++) 
	    	Soma = Soma + Integer.parseInt(cpf.substring(i-1, i)) * (11 - i);
	  
	    Resto = (Soma * 10) % 11;

	    if ((Resto == 10) || (Resto == 11))  
	    	Resto = 0;
	    if (Resto != Integer.parseInt(cpf.substring(9, 10)) ) 
	    	return false;

	    Soma = 0;
	    
	    for (int i = 1; i <= 10; i++) 
	    	Soma = Soma + Integer.parseInt(cpf.substring(i-1, i)) * (12 - i);
	    
	    Resto = (Soma * 10) % 11;

	    if ((Resto == 10) || (Resto == 11))  
	    	Resto = 0;
	    if (Resto != Integer.parseInt(cpf.substring(10, 11) ) ) 
	    	return false;
	    
	    return true;
	}
	
	public boolean validaCpf(Long id, String cpf, String tipo) {
		Boolean valido = validaCpf(cpf);
		
		if(id != -1 && valido == true && tipo.equals("CIDADAO")) {
			Cidadao cidadao = cidadaoRepository.findByCpf(cpf);
			return cidadao == null ? true : cidadao != null && id != 0 && cidadao.getId().equals( id) ? true : false;
		} 
		
		if(id != -1 && valido == true && tipo.equals("PROFISSIONAL")) {
			Profissional profissional = profissionalRepository.findByCpf(cpf);
			return profissional == null ? true : profissional != null && id != 0 && profissional.getId().equals(id) ? true : false;
		} 
		
		return valido;
	}
	
}
