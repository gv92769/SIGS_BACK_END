package br.net.sigs.service.util;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.util.Cbo;
import br.net.sigs.repository.util.CboRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class CboService {
	
	@Autowired
    private CboRepository repository;

    public Optional<Cbo> findByCodigo(String codigo){
        return repository.findByCodigo(codigo);
    }
    
    public List<Cbo> findAllByTitulo(String titulo){
    	Pageable pageable = org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("titulo")));
        return repository.findAllByTitulo(titulo.toLowerCase(), pageable);
    }
	
    public List<Cbo> findAllByTituloByPerfil(String titulo, Long id) {   	
    	 return repository.findAllByTituloByPerfil(titulo, id, org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("id"))));
    }
    
    public List<Cbo> findAllByProfissional(Long id) {
    	 return repository.findAllByProfissional(id);
    }
    
}
