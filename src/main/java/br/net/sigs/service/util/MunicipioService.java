package br.net.sigs.service.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.util.Municipio;
import br.net.sigs.repository.util.MunicipioRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class MunicipioService {

	@Autowired
	private MunicipioRepository municipioRepository;

	public List<Municipio> findByUfAndNome(String uf, String nome) {
		return municipioRepository.findByUfAndNome(uf, nome.toLowerCase(), 
				org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("nome"))));
	}
	
}
