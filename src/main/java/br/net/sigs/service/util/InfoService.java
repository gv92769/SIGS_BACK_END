package br.net.sigs.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import br.net.sigs.config.CustomUserDetailsService;
import br.net.sigs.entity.usuario.Usuario;
import br.net.sigs.service.usuario.PerfilService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class InfoService {

	@Autowired
    private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private PerfilService perfilService;
	
	public UserDetails currentUser(@AuthenticationPrincipal Authentication authentication) {
		String login = (String) authentication.getPrincipal();
		
		UserDetails userDetails = customUserDetailsService.loadUserByUsername(login);
		
		Usuario user = (Usuario) userDetails;
        user.setPerfil(!authentication.getAuthorities().isEmpty() ? perfilService.findByPerfil(authentication.getAuthorities().toArray()[0].toString()) : null);
        userDetails =  user;
		
		return userDetails;
	}
	
}
