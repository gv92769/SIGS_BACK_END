package br.net.sigs.service.domicilio;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import br.net.sigs.dto.FichaVisitaDomiciliarTabelaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.dto.integracao.domicilio.FichaVisitaDomiciliarChildTransportDto;
import br.net.sigs.dto.integracao.domicilio.FichaVisitaDomiciliarDTO;
import br.net.sigs.dto.integracao.domicilio.FichaVisitaDomiciliarMasterTransportDto;
import br.net.sigs.entity.domicilio.FichaVisitaDomiciliar;
import br.net.sigs.repository.domicilio.FichaVisitaDomiciliarRepository;
import br.net.sigs.service.util.ExportacaoService;
import br.net.sigs.service.util.SearchService;

@Service
public class FichaVisitaDomiciliarService {

    @Autowired
    private SearchService searchService;

    @Autowired
    private FichaVisitaDomiciliarRepository repository;

    @Autowired
    private ExportacaoService exportacaoService;

    public FichaVisitaDomiciliar save(FichaVisitaDomiciliar ficha) {
        if (ficha.getUuid() == null) {
            ficha.setUuid(gerarUuid(ficha.getUbs().getCnes()));
            ficha.setImportada(false);
        }
        return repository.save(ficha);
    }

    public String gerarUuid(String cnes) {
        String uuid = null;

        do {
            uuid = cnes.concat("-".concat(UUID.randomUUID().toString()));
        } while(repository.findByUuid(uuid) != null);

        return uuid;
    }

    public Optional<FichaVisitaDomiciliar> findById(Long id) {
        return repository.findById(id);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
    
    public Page<FichaVisitaDomiciliarTabelaDTO> search(FichaFiltroDto filter) {
        return repository.search(filter.getNome() != null ? filter.getNome().toLowerCase() : null, filter.getCpf(), filter.getCns(), filter.getDataInicio(), filter.getDataFinal(),
            filter.getExportada(), searchService.pageable(filter));
    }

    public FichaVisitaDomiciliarChildTransportDto fichaVisitaDomiciliarChild(FichaVisitaDomiciliar ficha) {
        FichaVisitaDomiciliarChildTransportDto dado = new FichaVisitaDomiciliarChildTransportDto();

        dado.setCnsCidadao(ficha.getCidadao().getCns());
        dado.setCpfCidadao(dado.getCnsCidadao() == null ? ficha.getCidadao().getCpf() : null);
        if (ficha.getMotivos().size() > 0) {
            dado.setMotivosVisita(ficha.getMotivos().stream().map(motivo -> motivo.getCodigo()).collect(Collectors.toList()));
        }
        dado.setDesfecho(ficha.getDesfecho().getCodigo());
        dado.setSexo((long) ficha.getCidadao().getSexo().getValue());
        dado.setDtNascimento(ficha.getCidadao().getDataNascimento().getTime());
        dado.setStatusOutroProfissional(ficha.getStatusOutroProfissional());
        dado.setNumProntuario(ficha.getCidadao().getNumProntuario());
        dado.setTurno(ficha.getTurno());

        return dado;
    }

    public FichaVisitaDomiciliarMasterTransportDto fichaVisitaDomiciliarMaster(FichaVisitaDomiciliar ficha) {
        FichaVisitaDomiciliarMasterTransportDto dado = new FichaVisitaDomiciliarMasterTransportDto();

        dado.setHeaderTransport(exportacaoService.headerTrasport(ficha.getProfissionalResponsavel(),ficha.getCbo(),  ficha.getUbs(), ficha.getData().getTime()));
        dado.setTpCdsOrigem(3);
        dado.setUuidFicha(ficha.getUuid());
        dado.setVisitasDomiciliares(fichaVisitaDomiciliarChild(ficha));

        return dado;
    }

    public void gerarExportacao(ZipOutputStream zipOut) throws JsonGenerationException, JsonMappingException, IOException  {
        List<FichaVisitaDomiciliar> fichas = repository.findAllByImportada(Boolean.FALSE);

        for (FichaVisitaDomiciliar ficha: fichas) {
            FichaVisitaDomiciliarDTO dado = new FichaVisitaDomiciliarDTO();
            dado = (FichaVisitaDomiciliarDTO) exportacaoService.informacaoTransport(dado, ficha.getUbs().getCnes(), ficha.getUuid(), 7L, ficha.getIne());
            dado.setTransport(fichaVisitaDomiciliarMaster(ficha));

            XmlMapper xmlMapper = new XmlMapper();
            File file = new File(ficha.getUuid() + ".esus.xml");
            xmlMapper.writeValue(file, dado);
            exportacaoService.zip(zipOut, file.getAbsolutePath());
            Files.deleteIfExists(file.toPath());

            ficha.setImportada(Boolean.TRUE);
            repository.save(ficha);
        }
    }

    public Long countByImportada() {
        return repository.countByImportada(Boolean.FALSE);
    }

}
