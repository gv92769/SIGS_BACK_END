package br.net.sigs.service.domicilio.util;

import br.net.sigs.entity.domicilio.Desfecho;
import br.net.sigs.repository.domicilio.util.DesfechoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DesfechoService {

    @Autowired
    private DesfechoRepository repository;

    public List<Desfecho> findAll() {
        return repository.findAll();
    }

}
