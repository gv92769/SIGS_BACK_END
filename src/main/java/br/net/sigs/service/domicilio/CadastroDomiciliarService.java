package br.net.sigs.service.domicilio;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import br.net.sigs.dto.DomicilioFilterDto;
import br.net.sigs.dto.FormCadastroDomiciliarDTO;
import br.net.sigs.dto.integracao.domicilio.CadastroDomiciliarDto;
import br.net.sigs.dto.integracao.domicilio.CadastroDomiciliarTransportDto;
import br.net.sigs.dto.integracao.domicilio.CondicaoMoradiaTransportDto;
import br.net.sigs.dto.integracao.domicilio.EnderecoLocalPermanenciaTransportDto;
import br.net.sigs.dto.integracao.domicilio.FamiliaRowTransportDto;
import br.net.sigs.dto.integracao.domicilio.InstituicaoPermanenciaTransportDto;
import br.net.sigs.entity.domicilio.CondicaoMoradia;
import br.net.sigs.entity.domicilio.Domicilio;
import br.net.sigs.entity.domicilio.EnderecoLocalPermanencia;
import br.net.sigs.entity.domicilio.Familia;
import br.net.sigs.entity.domicilio.FichaCadastroDomiciliar;
import br.net.sigs.entity.domicilio.InstituicaoPermanencia;
import br.net.sigs.entity.domicilio.util.AbastecimentoAgua;
import br.net.sigs.entity.domicilio.util.AguaConsumoDomicilio;
import br.net.sigs.entity.domicilio.util.CondicaoPosseUsoTerra;
import br.net.sigs.entity.domicilio.util.DestinoLixo;
import br.net.sigs.entity.domicilio.util.FormaEscoamentoBanheiroSanitario;
import br.net.sigs.entity.domicilio.util.MaterialPredominanteConstrucao;
import br.net.sigs.entity.domicilio.util.RendaFamiliar;
import br.net.sigs.entity.domicilio.util.SituacaoMoradia;
import br.net.sigs.entity.domicilio.util.TipoAcessoDomicilio;
import br.net.sigs.entity.domicilio.util.TipoDomicilio;
import br.net.sigs.entity.util.Estado;
import br.net.sigs.repository.domicilio.CondicaoMoradiaRepository;
import br.net.sigs.repository.domicilio.DomicilioRepository;
import br.net.sigs.repository.domicilio.EnderecoLocalPermanenciaRepository;
import br.net.sigs.repository.domicilio.FichaCadastroDomiciliarRepository;
import br.net.sigs.repository.domicilio.InstituicaoPermanenciaRepository;
import br.net.sigs.repository.domicilio.util.AbastecimentoAguaRepository;
import br.net.sigs.repository.domicilio.util.AguaConsumoDomicilioRepository;
import br.net.sigs.repository.domicilio.util.AnimalRepository;
import br.net.sigs.repository.domicilio.util.CondicaoPosseUsoTerraRepository;
import br.net.sigs.repository.domicilio.util.DestinoLixoRepository;
import br.net.sigs.repository.domicilio.util.FormaEscoamentoBanheiroSanitarioRepository;
import br.net.sigs.repository.domicilio.util.MaterialPredominanteConstrucaoRepository;
import br.net.sigs.repository.domicilio.util.RendaFamiliarRepository;
import br.net.sigs.repository.domicilio.util.SituacaoMoradiaRepository;
import br.net.sigs.repository.domicilio.util.TipoAcessoDomicilioRepository;
import br.net.sigs.repository.domicilio.util.TipoDomicilioRepository;
import br.net.sigs.repository.domicilio.util.TipoImovelRepository;
import br.net.sigs.repository.domicilio.util.TipoLogradouroRepository;
import br.net.sigs.service.util.ExportacaoService;
import br.net.sigs.service.util.SearchService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class CadastroDomiciliarService {

	@Autowired
	private FichaCadastroDomiciliarRepository repository;
	
	@Autowired
	private DomicilioRepository domicilioRepository;
	
	@Autowired
	private InstituicaoPermanenciaRepository instituicaoRepository;
	
	@Autowired
	private EnderecoLocalPermanenciaRepository enderecoRepository;
	
	@Autowired
	private CondicaoMoradiaRepository condicaoRepository;
	
	@Autowired
	private ExportacaoService exportacaoService;
	
	@Autowired
	private SearchService searchService;
	
	@Autowired
	private AbastecimentoAguaRepository abastecimentoAguaRepository;
	
	@Autowired
	private AguaConsumoDomicilioRepository aguaConsumoRepository;
	
	@Autowired
	private AnimalRepository animalRepository;
	
	@Autowired
	private CondicaoPosseUsoTerraRepository condicaoPosseRepository;
	
	@Autowired
	private DestinoLixoRepository destinoLixoRepository;
	
	@Autowired
	private FormaEscoamentoBanheiroSanitarioRepository formaEscoamentoRepository;
	
	@Autowired
	private MaterialPredominanteConstrucaoRepository materialPredominanteRepository;
	
	@Autowired
	private RendaFamiliarRepository rendaFamiliarRepository;
	
	@Autowired
	private SituacaoMoradiaRepository situacaoMoradiaRepository;
	
	@Autowired
	private TipoAcessoDomicilioRepository acessoDomicilioRepository;
	
	@Autowired
	private TipoDomicilioRepository tipoDomicilioRepository;
	
	@Autowired
	private TipoImovelRepository tipoImovelRepository;
	
	@Autowired
	private TipoLogradouroRepository tipoLogradouroRepository;
	
	private Integer qntMembros = 0;
	
	public FichaCadastroDomiciliar verificarCampo(FichaCadastroDomiciliar ficha) {	
		ficha.getDomicilio().setAnimaisDomicilio(!ficha.getDomicilio().getAnimaisDomicilio().isEmpty() ? ficha.getDomicilio().getAnimaisDomicilio() : null);	
		ficha.getDomicilio().setFamilias(familiasDtoToFamilias(ficha.getDomicilio().getFamilias()));
		Long codigo = ficha.getDomicilio().getTipoImovel().getCodigo();
		
		if (codigo != 2 && codigo != 3 && codigo != 4 && codigo != 5 && codigo != 6 && codigo != 12 && codigo != 99) {
			Integer quant = 0;
			if (ficha.getDomicilio().getCondicao().getNuMoradores() != null)
				quant = Integer.valueOf(ficha.getDomicilio().getCondicao().getNuMoradores());
			
			ficha.getDomicilio().getCondicao().setNuMoradores(quant == 0 ? qntMembros.toString() : quant < qntMembros ? qntMembros.toString() : quant.toString());
		}
		
		return ficha;
	}
	
	public List<Familia> familiasDtoToFamilias(List<Familia> familias) {
		for (Familia familia: familias) {
			if ((familia.getNumeroMembrosFamilia()!= null && familia.getNumeroMembrosFamilia() == 0) || familia.getNumeroMembrosFamilia() == null )
				qntMembros +=  1;
			else
				qntMembros += familia.getNumeroMembrosFamilia();
			
			if (familia.getCodRendaFamiliar() != null)
				familia.setRendaFamiliar(new RendaFamiliar(familia.getCodRendaFamiliar(), ""));
		}
		return familias;
	}
	
	public String gerarUuid(String cnes) {
		String uuid = null;
		
		do {
			uuid = cnes.concat("-".concat(UUID.randomUUID().toString()));
		} while(repository.findById(uuid).isPresent());
		
		return uuid;
	 }
	
	public InstituicaoPermanencia save(InstituicaoPermanencia instituicao) {
		return instituicaoRepository.save(instituicao);
	}
	
	public EnderecoLocalPermanencia save(EnderecoLocalPermanencia endereco) {
		return enderecoRepository.save(endereco);
	}
	
	public CondicaoMoradia CondicaoMoradiaDtoToCondicaoMoradia(CondicaoMoradia condicao) {
		condicao.setAbastecimentoAgua(condicao.getCodAbastecimentoAgua() == null ? null : new AbastecimentoAgua(condicao.getCodAbastecimentoAgua(), ""));
		condicao.setAguaDomicilio(condicao.getCodAguaDomicilio() == null ? null : new AguaConsumoDomicilio(condicao.getCodAguaDomicilio(), ""));
		condicao.setDestinoLixo(condicao.getCodDestinoLixo() == null ? null : new DestinoLixo(condicao.getCodDestinoLixo(), ""));
		condicao.setFormaEscoamentoBanheiro(condicao.getCodFormaEscoamentoBanheiro() == null ? null : new FormaEscoamentoBanheiroSanitario(condicao.getCodFormaEscoamentoBanheiro(), ""));
		condicao.setMaterialPredominante(condicao.getCodMaterialPredominante() == null ? null : new MaterialPredominanteConstrucao(condicao.getCodMaterialPredominante(), ""));
		condicao.setPosseUso(condicao.getCodPosseUso() == null ? null : new CondicaoPosseUsoTerra(condicao.getCodPosseUso(), ""));
		condicao.setSituacaoMoradia(condicao.getCodSituacaoMoradia() == null ? null : new SituacaoMoradia(condicao.getCodSituacaoMoradia(), ""));
		condicao.setTipoAcessoDomicilio(condicao.getCodTipoAcessoDomicilio() == null ? null : new TipoAcessoDomicilio(condicao.getCodTipoAcessoDomicilio(), ""));
		condicao.setTipoDomicilio(condicao.getCodTipoDomicilio() == null ? null : new TipoDomicilio(condicao.getCodTipoDomicilio(), ""));
		
		return condicao;
	}
	
	public CondicaoMoradia save(CondicaoMoradia condicao) {
		return condicaoRepository.save(CondicaoMoradiaDtoToCondicaoMoradia(condicao));
	}
	
	public FichaCadastroDomiciliar save(FichaCadastroDomiciliar ficha) {	
		ficha.setUuid(gerarUuid(ficha.getUbs().getCnes()));
		ficha.setUuidFichaOriginadora(ficha.getDomicilio().getId() != null ? ficha.getDomicilio().getUltimaficha() : ficha.getUuid());
		ficha.getDomicilio().setUltimaficha(ficha.getUuid());
		ficha.getDomicilio().setFicha(ficha.getDomicilio().getId() != null ? ficha.getDomicilio().getFicha() : ficha.getUuid());
		ficha = verificarCampo(ficha);
		
		InstituicaoPermanencia instituicao = null;
		if (ficha.getDomicilio().getInstituicao() != null && ficha.getDomicilio().getInstituicao().getId() == null) {
			instituicao = ficha.getDomicilio().getInstituicao();
			ficha.getDomicilio().setInstituicao(null);
		}
		
		EnderecoLocalPermanencia endereco = null;
		if (ficha.getDomicilio().getEndereco() != null && ficha.getDomicilio().getEndereco().getId() == null) {
			endereco = ficha.getDomicilio().getEndereco();
			ficha.getDomicilio().setEndereco(null);
		}
		
		CondicaoMoradia condicao = ficha.getDomicilio().getCondicao();
		ficha.getDomicilio().setCondicao(null);
	
		ficha = repository.save(ficha);
		
		if (instituicao != null && instituicao.getNomeResponsavelTecnico() != null && !instituicao.getNomeResponsavelTecnico().isEmpty()) {
			instituicao.setDomicilio(ficha.getDomicilio());
			ficha.getDomicilio().setInstituicao(save(instituicao));
		}
		
		if (endereco != null) {
			endereco.setDomicilio(ficha.getDomicilio());
			ficha.getDomicilio().setEndereco(save(endereco));
		}
		
		if (condicao != null && condicao.getLocalizacaoMoradia() != null) {
			condicao.setDomicilio(ficha.getDomicilio());
			ficha.getDomicilio().setCondicao(save(condicao));
		}
		
		return ficha;
	}
	
	public List<Familia> familiasToFamiliasDto(List<Familia> familias) {
		for (Familia familia: familias)
			if (familia.getRendaFamiliar() != null)
				familia.setCodRendaFamiliar(familia.getRendaFamiliar().getCodigo());
		
		return familias;
	}
	
	public CondicaoMoradia CondicaoMoradiaToCondicaoMoradiaDto(CondicaoMoradia condicao) {
		condicao.setCodAbastecimentoAgua(condicao.getAbastecimentoAgua() == null ? null : condicao.getAbastecimentoAgua().getCodigo());
		condicao.setCodAguaDomicilio(condicao.getAguaDomicilio() == null ? null : condicao.getAguaDomicilio().getCodigo());
		condicao.setCodDestinoLixo(condicao.getDestinoLixo() == null ? null : condicao.getDestinoLixo().getCodigo());
		condicao.setCodFormaEscoamentoBanheiro(condicao.getFormaEscoamentoBanheiro() == null ? null : condicao.getFormaEscoamentoBanheiro().getCodigo());
		condicao.setCodMaterialPredominante(condicao.getMaterialPredominante() == null ? null : condicao.getMaterialPredominante().getCodigo());
		condicao.setCodPosseUso(condicao.getPosseUso() == null ? null : condicao.getPosseUso().getCodigo());
		condicao.setCodSituacaoMoradia(condicao.getSituacaoMoradia() == null ? null : condicao.getSituacaoMoradia().getCodigo());
		condicao.setCodTipoAcessoDomicilio(condicao.getTipoAcessoDomicilio() == null ? null : condicao.getTipoAcessoDomicilio().getCodigo());
		condicao.setCodTipoDomicilio(condicao.getTipoDomicilio() == null ? null : condicao.getTipoDomicilio().getCodigo());
		
		return condicao;
	}
	
	public Domicilio findById(Long id) {
		Optional<Domicilio> domicilioOp = domicilioRepository.findById(id);
		Domicilio domicilio = null;
		
		if (domicilioOp.isPresent()) {
			domicilio = domicilioOp.get();
			domicilio.setFamilias(familiasToFamiliasDto(domicilio.getFamilias()));
			domicilio.setCondicao(domicilio.getCondicao() != null ? CondicaoMoradiaToCondicaoMoradiaDto(domicilio.getCondicao()) : domicilio.getCondicao());
		}
		return domicilio;
	}
	
	public void delete(Long id) {
		Optional<Domicilio> domicilio =  domicilioRepository.findById(id);
		
		if (domicilio.isPresent())
			repository.deleteAll(repository.findAllByDomicilio(domicilio.get()));
	}
	
	public CadastroDomiciliarTransportDto cadastroDomiciliarTransport(FichaCadastroDomiciliar ficha) {
		CadastroDomiciliarTransportDto cadastro = new CadastroDomiciliarTransportDto();
		
		if (!ficha.getDomicilio().getAnimaisDomicilio().isEmpty())
			cadastro.setAnimaisNoDomicilio(ficha.getDomicilio().getAnimaisDomicilio().stream().map(animal -> animal.getCodigo()).collect(Collectors.toList()));
		
		cadastro.setCondicaoMoradia(ficha.getDomicilio().getCondicao() == null ? null : condicaoMoradiaTransport(ficha.getDomicilio().getCondicao()));
		cadastro.setEnderecoLocalPermanencia(enderecoLocalPermanencia(ficha.getDomicilio().getEndereco()));
		cadastro.setFamilias(familias(ficha.getDomicilio().getFamilias()));
		cadastro.setFichaAtualizada(ficha.getFichaAtualizada());
		cadastro.setQuantosAnimaisNoDomicilio(ficha.getDomicilio().getQuantosAnimaisDomicilio() != null ? ficha.getDomicilio().getQuantosAnimaisDomicilio().toString() : null);
		cadastro.setStAnimaisNoDomicilio(ficha.getDomicilio().getStAnimaisDomicilio());
		cadastro.setStatusTermoRecusa(ficha.getStatusTermoRecusa());
		cadastro.setTpCdsOrigem(3);
		cadastro.setUuid(ficha.getUuid());
		cadastro.setUuidFichaOriginadora(ficha.getUuidFichaOriginadora());
		cadastro.setTipoDeImovel(ficha.getDomicilio().getTipoImovel().getCodigo());
		cadastro.setInstituicaoPermanencia(ficha.getDomicilio().getInstituicao() == null ? null : instituicaoPermanenciaTransport(ficha.getDomicilio().getInstituicao()));
		cadastro.setHeaderTransport(exportacaoService.headerTrasport(ficha.getProfissionalResponsavel(), ficha.getCbo(), ficha.getUbs(), ficha.getData().getTime()));
		
		return cadastro;
	}
	
	public CondicaoMoradiaTransportDto condicaoMoradiaTransport(CondicaoMoradia condicao) {
		CondicaoMoradiaTransportDto transport = new CondicaoMoradiaTransportDto();
		
		transport.setAbastecimentoAgua(condicao.getAbastecimentoAgua() == null ? null : condicao.getAbastecimentoAgua().getCodigo());
		transport.setAguaConsumoDomicilio(condicao.getAguaDomicilio() == null ? null : condicao.getAguaDomicilio().getCodigo());
		transport.setAreaProducaoRural(condicao.getPosseUso() == null ? null : condicao.getPosseUso().getCodigo());
		transport.setDestinoLixo(condicao.getDestinoLixo() == null ? null : condicao.getDestinoLixo().getCodigo());
		transport.setFormaEscoamentoBanheiro(condicao.getFormaEscoamentoBanheiro() == null ? null : condicao.getFormaEscoamentoBanheiro().getCodigo());
		transport.setLocalizacao((long) condicao.getLocalizacaoMoradia().getValue());
		transport.setMaterialPredominanteParedesExtDomicilio(condicao.getMaterialPredominante() == null ? null : condicao.getMaterialPredominante().getCodigo());
		transport.setNuComodos(condicao.getNuComodos());
		transport.setNuMoradores(condicao.getNuMoradores());
		transport.setSituacaoMoradiaPosseTerra(condicao.getSituacaoMoradia() == null ? null : condicao.getSituacaoMoradia().getCodigo());
		transport.setStDisponibilidadeEnergiaEletrica(condicao.getStDisponibilidadeEnergiaEletrica());
		transport.setTipoAcessoDomicilio(condicao.getTipoAcessoDomicilio() == null ? null : condicao.getTipoAcessoDomicilio().getCodigo());
		transport.setTipoDomicilio(condicao.getTipoDomicilio() == null ? null : condicao.getTipoDomicilio().getCodigo());
		
		return transport;
	}
	
	public EnderecoLocalPermanenciaTransportDto enderecoLocalPermanencia(EnderecoLocalPermanencia endereco) {
		EnderecoLocalPermanenciaTransportDto transport = new EnderecoLocalPermanenciaTransportDto();
		
		transport.setBairro(endereco.getBairro());
		transport.setCep(endereco.getCep());
		transport.setCodigoIbgeMunicipio(endereco.getMunicipio().getCodigoIbge().toString());
		transport.setComplemento(endereco.getComplemento());
		transport.setMicroArea(endereco.getMicroarea());
		transport.setNomeLogradouro(endereco.getNomeLogradouro());
		transport.setNumero(endereco.getNumero());
		transport.setNumeroDneUf(Estado.getEstadoPorSigla(endereco.getEstado()).getDne());
		transport.setPontoReferencia(endereco.getPontoReferencia());
		transport.setStForaArea(endereco.getStForaArea());
		transport.setStSemNumero(endereco.getStSemNumero());
		transport.setTelefoneContato(endereco.getTelefoneContato());
		transport.setTelefoneResidencia(endereco.getTelefoneResidencia());
		transport.setTipoLogradouroNumeroDne(endereco.getTipoLogradouro().getCodigo());
		
		return transport;
	}
	
	public List<FamiliaRowTransportDto> familias(List<Familia> familias) {
		List<FamiliaRowTransportDto> list = new ArrayList<>();
		
		for (Familia familia: familias) {
			FamiliaRowTransportDto object = new FamiliaRowTransportDto();
			
			object.setCpfResponsavel(familia.getCidadao().getCns() == null ? familia.getCidadao().getCpf() : null);
			object.setDataNascimentoResponsavel(familia.getCidadao().getDataNascimento().getTime());
			object.setNumeroCnsResponsavel(familia.getCidadao().getCns());
			object.setNumeroMembrosFamilia(familia.getNumeroMembrosFamilia() == null ? 1 : familia.getNumeroMembrosFamilia());
			object.setNumeroProntuario(familia.getNumeroProntuario());
			object.setRendaFamiliar(familia.getRendaFamiliar() == null ? null : familia.getRendaFamiliar().getCodigo());
			object.setResideDesde(familia.getResideDesde() == null ? null : familia.getResideDesde().getTime());
			object.setStMudanca(familia.getStMudanca());
			
			list.add(object);
		}
		
		return list;
	}
	
	public InstituicaoPermanenciaTransportDto instituicaoPermanenciaTransport(InstituicaoPermanencia instituicao) {
		InstituicaoPermanenciaTransportDto transport = new InstituicaoPermanenciaTransportDto();
		
		transport.setCargoInstituicao(instituicao.getCargoInstituicao());
		transport.setCnsResponsavelTecnico(instituicao.getCnsResponsavelTecnico());
		transport.setNomeInstituicaoPermanencia(instituicao.getNomeInstituicaoPermanencia());
		transport.setNomeResponsavelTecnico(instituicao.getNomeResponsavelTecnico());
		transport.setStOutrosProfissionaisVinculados(instituicao.getStOutrosProfissionaisVinculados());
		transport.setTelefoneResponsavelTecnico(instituicao.getTelefoneResponsavelTecnico());
		
		return transport;
	}
	
	public void gerarExportacao(ZipOutputStream zipOut) throws JsonGenerationException, JsonMappingException, IOException  {
			List<FichaCadastroDomiciliar> fichas = repository.findByImportada(false);
			for (FichaCadastroDomiciliar ficha: fichas) {
				CadastroDomiciliarDto dado = new CadastroDomiciliarDto();
				
				dado = (CadastroDomiciliarDto) exportacaoService.informacaoTransport(dado, ficha.getUbs().getCnes(), ficha.getUuid(), 3L, ficha.getIne());
				dado.setTrasport(cadastroDomiciliarTransport(ficha));
				
				XmlMapper xmlMapper = new XmlMapper();
				File file = new File(ficha.getUuid() + ".esus.xml");
			    xmlMapper.writeValue(file, dado);
			    exportacaoService.zip(zipOut, file.getAbsolutePath());
			    Files.deleteIfExists(file.toPath());
			    
			    ficha.setImportada(Boolean.TRUE);
			    repository.save(ficha);
			}
	}
	
	public Page<EnderecoLocalPermanencia> search(DomicilioFilterDto filter) {
		return enderecoRepository.search(filter.getLogradouro() == null ? null : filter.getLogradouro().toLowerCase(), filter.getNumero(), 
				filter.getComplemento() == null ? null : filter.getComplemento().toLowerCase(), filter.getBairro() == null ? null : filter.getBairro().toLowerCase(),
				filter.getMicroarea(), filter.getCep(), filter.getCpfCns(), searchService.pageable(filter));
	}
	
	public Long countByImportada() {
		return repository.countByImportada(false);
	}
	
	public FormCadastroDomiciliarDTO init() {
		return new FormCadastroDomiciliarDTO(abastecimentoAguaRepository.findAll(), aguaConsumoRepository.findAll(), animalRepository.findAll(), 
				condicaoPosseRepository.findAll(), destinoLixoRepository.findAll(), formaEscoamentoRepository.findAll(), 
				materialPredominanteRepository.findAll(), rendaFamiliarRepository.findAll(), situacaoMoradiaRepository.findAll(), 
				acessoDomicilioRepository.findAll(), tipoDomicilioRepository.findAll(), tipoImovelRepository.findAll(), 
				tipoLogradouroRepository.findAll(), Estado.valuesForSigla());
	}
	
}
