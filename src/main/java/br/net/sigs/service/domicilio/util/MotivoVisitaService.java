package br.net.sigs.service.domicilio.util;


import br.net.sigs.entity.domicilio.MotivoVisita;
import br.net.sigs.repository.domicilio.util.MotivoVisitaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MotivoVisitaService {

    @Autowired
    private MotivoVisitaRepository repository;

    public List<MotivoVisita> findAllById(List<Long> codigo) {
        return repository.findAllById(codigo);
    }

}
