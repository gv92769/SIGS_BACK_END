package br.net.sigs.service.cidadao.util;

import br.net.sigs.entity.cidadao.util.ConsideracaoPeso;
import br.net.sigs.repository.cidadao.util.ConsideracaoPesoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsideracaoPesoService {

    @Autowired
    private ConsideracaoPesoRepository repository;

    public List<ConsideracaoPeso> findAll() {
        return repository.findAll();
    }
}
