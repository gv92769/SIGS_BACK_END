package br.net.sigs.service.cidadao.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.cidadao.util.Etnia;
import br.net.sigs.repository.cidadao.util.EtniaRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class EtniaService {

	@Autowired
	private EtniaRepository etniaRepository;
	
	public List<Etnia> findAllByNome(String descricao) {
		return etniaRepository.findAllByNome(descricao.toLowerCase(), org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("descricao"))));
	}
	
}
