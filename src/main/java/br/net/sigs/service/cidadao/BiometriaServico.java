/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.service.cidadao;

import br.net.sigs.dto.BiometriaCadastroDTO;
import br.net.sigs.entity.cidadao.BiometriaCidadao;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import javax.imageio.ImageIO;

/**
 *
 * @author Naisses
 */
public class BiometriaServico {

    public class FmdComparisonResult {

        private final int falseMatchRate;
        private final boolean fmdMatched;

        public FmdComparisonResult(int falseMatchRate, boolean fmdMatched) {
            this.falseMatchRate = falseMatchRate;
            this.fmdMatched = fmdMatched;
        }

        public int getFalseMatchRate() {
            return falseMatchRate;
        }

        public boolean isFmdMatched() {
            return fmdMatched;
        }
    }

    public class FmdIdentificationResult {

        private final int[] indexesMatched;

        public FmdIdentificationResult(int[] indexesMatched) {
            this.indexesMatched = indexesMatched;
        }

        public int[] getIndexesMatched() {
            return indexesMatched;
        }

    }

    private class SimpleEnrollmentCallback implements Engine.EnrollmentCallback {

        private Fmd[] fmdCandidates;
        private int indexCandidate = 0;

        public SimpleEnrollmentCallback(Fmd[] fmdCandidates) {
            this.fmdCandidates = fmdCandidates;
        }

        @Override
        public Engine.PreEnrollmentFmd GetFmd(Fmd.Format format) {
            System.out.println("Engine.PreEnrollmentFmd GetFmd : " + indexCandidate);
            if (indexCandidate >= fmdCandidates.length) {
                return null;
            } else {
                Engine.PreEnrollmentFmd preFmd = new Engine.PreEnrollmentFmd();
                preFmd.fmd = fmdCandidates[indexCandidate];
                preFmd.view_index = 0;
                indexCandidate++;
                return preFmd;
            }
        }
    }

    public static final int IMAGE_RESOLUTION_DPI = 700;
    public static final int CBEFF_ID = 3407615;

    public Fmd pngStringToFmd(String dataString) throws IOException, UareUException {
        byte[] bytes = Base64.getDecoder().decode(dataString.getBytes());
        ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
        BufferedImage image = ImageIO.read(stream);

        WritableRaster raster = image.getRaster();
        DataBufferByte data = (DataBufferByte) raster.getDataBuffer();
        byte[] imageBytes = data.getData();

        Engine engine = UareUGlobal.GetEngine();
        Fmd fmd = engine.CreateFmd(
                imageBytes,
                image.getWidth(),
                image.getHeight(),
                IMAGE_RESOLUTION_DPI,
                0,
                CBEFF_ID,
                Fmd.Format.ISO_19794_2_2005
        );

        return fmd;
    }

    public Fmd pngFileToFmd(String file) throws IOException, UareUException {
        BufferedImage image = ImageIO.read(new File(file));
        WritableRaster raster = image.getRaster();
        DataBufferByte data = (DataBufferByte) raster.getDataBuffer();
        byte[] imageBytes = data.getData();

        Engine engine = UareUGlobal.GetEngine();
        Fmd fmd = engine.CreateFmd(
                imageBytes,
                image.getWidth(),
                image.getHeight(),
                IMAGE_RESOLUTION_DPI,
                0,
                CBEFF_ID,
                Fmd.Format.ISO_19794_2_2005
        );

        return fmd;
    }

    public FmdComparisonResult compareFmds(Engine engine, Fmd fmd1, Fmd fmd2) throws UareUException {
        int falsematch_rate = engine.Compare(fmd1, 0, fmd2, 0);
        int target_falsematch_rate = Engine.PROBABILITY_ONE / 100000; //target rate is 0.00001
        boolean fmdMatched = falsematch_rate < target_falsematch_rate;
        FmdComparisonResult fmdComparisonResult = new FmdComparisonResult(falsematch_rate, fmdMatched);
        return fmdComparisonResult;
    }

    public FmdIdentificationResult executeIdentification(Fmd fmd, Fmd[] fmdPool) throws UareUException {
        Engine engine = UareUGlobal.GetEngine();
        int falsepositive_rate = Engine.PROBABILITY_ONE / 100000;
        Engine.Candidate[] candidates = engine.Identify(fmd, 0, fmdPool, falsepositive_rate, fmdPool.length);
        int[] res = new int[candidates.length];
        for (int i = 0; i < candidates.length; i++) {
            res[i] = candidates[i].fmd_index;
        }
        FmdIdentificationResult fmdIdentificationResult = new FmdIdentificationResult(res);
        return fmdIdentificationResult;
    }

    public Fmd executeEnrollment(Fmd[] fmds) throws UareUException {
        Engine engine = UareUGlobal.GetEngine();
        SimpleEnrollmentCallback enrollmentCallback = new SimpleEnrollmentCallback(fmds);
        Fmd fmdFinal = engine.CreateEnrollmentFmd(Fmd.Format.ISO_19794_2_2005, enrollmentCallback);
        return fmdFinal;
    }
    
    public Fmd[] toFmdArray(BiometriaCadastroDTO dto) throws IOException, UareUException{
        Fmd fmd1 = pngStringToFmd(dto.getDigital1());
        Fmd fmd2 = pngStringToFmd(dto.getDigital2());
        Fmd fmd3 = pngStringToFmd(dto.getDigital3());
        Fmd fmd4 = pngStringToFmd(dto.getDigital4());
        Fmd[] fmds = {fmd1, fmd2, fmd3, fmd4};
        return fmds;
    }
    
    public Fmd[] toFmdArray(List<BiometriaCidadao> biometrias, boolean isDigitalPrincipal) throws UareUException{
        Fmd[] fmds = new Fmd[biometrias.size()];
        if(isDigitalPrincipal){
            int i = 0;
            for (BiometriaCidadao biometria : biometrias) {
                Fmd fmd = UareUGlobal.GetImporter().ImportFmd(biometria.getDigitalPrincipal(), Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                fmds[i] = fmd;
                i++;
            }
            return fmds;
        }
        else{
            int i = 0;
            for (BiometriaCidadao biometria : biometrias) {
                Fmd fmd = UareUGlobal.GetImporter().ImportFmd(biometria.getDigitalSecundaria(), Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                fmds[i] = fmd;
                i++;
            }
            return fmds;
        }
    }
    
    public Fmd toFmd(BiometriaCidadao biometria, boolean isDigitalPrincipal) throws UareUException{
        byte[] data;
        if(isDigitalPrincipal){
            data = biometria.getDigitalPrincipal();
        }
        else{
            data = biometria.getDigitalSecundaria();
        }
        if(data==null)
            return null;
        
        Fmd fmd = UareUGlobal.GetImporter().ImportFmd(data, Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
        return fmd;
    }
}
