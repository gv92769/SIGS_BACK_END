package br.net.sigs.service.cidadao;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import br.net.sigs.dto.CidadaoFilterDto;
import br.net.sigs.dto.FormCidadaoGeralDTO;
import br.net.sigs.dto.FormInformacaoSociodemograficaDTO;
import br.net.sigs.dto.FormMoradiaCidadaoDTO;
import br.net.sigs.dto.InformacoesCondicaoSaudeDTO;
import br.net.sigs.dto.InformacoesSituacaoRuaDTO;
import br.net.sigs.dto.integracao.cidadao.CadastroIndividualDto;
import br.net.sigs.dto.integracao.cidadao.CadastroIndividualTransportDto;
import br.net.sigs.dto.integracao.cidadao.CondicoesDeSaudeTransportDto;
import br.net.sigs.dto.integracao.cidadao.IdentificacaoUsuarioCidadaoTransportDto;
import br.net.sigs.dto.integracao.cidadao.InformacoesSocioDemograficasTransportDto;
import br.net.sigs.dto.integracao.cidadao.SituacaoDeRuaTransportDto;
import br.net.sigs.entity.cidadao.Cidadao;
import br.net.sigs.entity.cidadao.CondicaoSaude;
import br.net.sigs.entity.cidadao.FichaCadastroIndividual;
import br.net.sigs.entity.cidadao.InformacaoSocioDemografica;
import br.net.sigs.entity.cidadao.SituacaoRua;
import br.net.sigs.entity.cidadao.util.EstadoCivil;
import br.net.sigs.entity.cidadao.util.Nacionalidade;
import br.net.sigs.entity.cidadao.util.RelacaoParentesco;
import br.net.sigs.entity.cidadao.util.Sexo;
import br.net.sigs.entity.util.Estado;
import br.net.sigs.repository.cidadao.CidadaoRepository;
import br.net.sigs.repository.cidadao.FichaCadastroIndividualRepository;
import br.net.sigs.repository.cidadao.util.AcessoHigieneRepository;
import br.net.sigs.repository.cidadao.util.ConsideracaoPesoRepository;
import br.net.sigs.repository.cidadao.util.CursoMaisElevadoRepository;
import br.net.sigs.repository.cidadao.util.DeficienciaRepository;
import br.net.sigs.repository.cidadao.util.DoencaCardiacaRepository;
import br.net.sigs.repository.cidadao.util.DoencaRespiratoriaRepository;
import br.net.sigs.repository.cidadao.util.DoencaRinsRepository;
import br.net.sigs.repository.cidadao.util.IdentidadeGeneroRepository;
import br.net.sigs.repository.cidadao.util.OrientacaoSexualRepository;
import br.net.sigs.repository.cidadao.util.OrigemAlimentacaoRepository;
import br.net.sigs.repository.cidadao.util.PovoComunidadeTradicionalRepository;
import br.net.sigs.repository.cidadao.util.QuantasAlimentacaoRepository;
import br.net.sigs.repository.cidadao.util.RacaCorRepository;
import br.net.sigs.repository.cidadao.util.RelacaoParentescoRepository;
import br.net.sigs.repository.cidadao.util.ResponsavelCriancaRepository;
import br.net.sigs.repository.cidadao.util.SituacaoMercadoTrabalhoRepository;
import br.net.sigs.repository.cidadao.util.TempoSituacaoRuaRepository;
import br.net.sigs.repository.domicilio.util.TipoImovelRepository;
import br.net.sigs.repository.domicilio.util.TipoLogradouroRepository;
import br.net.sigs.service.util.ExportacaoService;
import br.net.sigs.service.util.SearchService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class CidadaoService {

	@Autowired
	private FichaCadastroIndividualRepository repository;
	 
	@Autowired
	private CidadaoRepository cidadaorepository;
	
	@Autowired
	private ExportacaoService exportacaoService;
	
	@Autowired
	private SearchService searchService;

	@Autowired
	private DoencaCardiacaRepository cardiacaRepository;

	@Autowired
	private DoencaRespiratoriaRepository respiratoriaRepository;

	@Autowired
	private DoencaRinsRepository rinsRepository;

	@Autowired
	private ConsideracaoPesoRepository consideracaoPesoRepository;

	@Autowired
	private AcessoHigieneRepository higieneRepository;

	@Autowired
	private OrigemAlimentacaoRepository origemAlimentacaoRepository;

	@Autowired
	private QuantasAlimentacaoRepository qtdAlimentacaoRepository;

	@Autowired
	private TempoSituacaoRuaRepository tempoSituacaoRuaRepository;

	@Autowired
	private RelacaoParentescoRepository relacaoParentescoRepository;
	
	@Autowired
	private CursoMaisElevadoRepository cursoMaisElevadoRepository;
	
	@Autowired
	private SituacaoMercadoTrabalhoRepository  situacaoMercadoTrabalhoRepository;
	
	@Autowired
	private ResponsavelCriancaRepository  responsavelCriancaRepository;
	
	@Autowired
	private PovoComunidadeTradicionalRepository  povoComunidadeTradicionalRepository;
	
	@Autowired
	private OrientacaoSexualRepository  orientacaoSexualRepository;
	
	@Autowired
	private IdentidadeGeneroRepository  identidadeGeneroRepository;
	
	@Autowired
	private DeficienciaRepository  deficienciaRepository;
	
	@Autowired
	private RacaCorRepository  racaCorRepository;

	@Autowired
	private TipoImovelRepository tipoImovelRepository;

	@Autowired
	private TipoLogradouroRepository tipoLogradouroRepository;
	
	public FichaCadastroIndividual verificarCampo(FichaCadastroIndividual ficha) {
		ficha.getCidadao().setEtnia(ficha.getCidadao().getEtnia() == null || ficha.getCidadao().getEtnia().getCodigo() == null ? null : ficha.getCidadao().getEtnia());
		ficha.getCidadao().setMunicipioNascimento(ficha.getCidadao().getMunicipioNascimento() == null || 
				ficha.getCidadao().getMunicipioNascimento().getCodigoIbge() == null ? null : ficha.getCidadao().getMunicipioNascimento());
		
		return ficha;
	}
	
	public InformacaoSocioDemografica verificarCampo(InformacaoSocioDemografica informacao) {
		informacao.setDeficiencias(!informacao.getDeficiencias().isEmpty() ? informacao.getDeficiencias() : null);
		informacao.setGrauInstrucaoCidadao(informacao.getGrauInstrucaoCidadao() == null || (informacao.getGrauInstrucaoCidadao() != null && 
				informacao.getGrauInstrucaoCidadao().getCodigo() == null) ? null : informacao.getGrauInstrucaoCidadao());
		informacao.setIdentidadeGenero(informacao.getIdentidadeGenero() == null || 
				(informacao.getIdentidadeGenero() != null && informacao.getIdentidadeGenero().getCodigo() == null) ? null : informacao.getIdentidadeGenero());
		informacao.setCbo(informacao.getCbo() == null || 
				(informacao.getCbo() != null && informacao.getCbo().getCodigo() == null) ? null : informacao.getCbo());
		informacao.setOrientacaoSexual(informacao.getOrientacaoSexual() == null || 
				(informacao.getOrientacaoSexual() != null && informacao.getOrientacaoSexual().getCodigo() == null) ? null : informacao.getOrientacaoSexual());
		informacao.setPovoComunidadeTradicional(informacao.getPovoComunidadeTradicional() == null || 
				(informacao.getPovoComunidadeTradicional() != null && informacao.getPovoComunidadeTradicional().getId() == null) ? null : informacao.getPovoComunidadeTradicional());
		informacao.setRelacaoParentesco(informacao.getRelacaoParentesco() == null || 
				(informacao.getRelacaoParentesco() != null && informacao.getRelacaoParentesco().getCodigo() == null) ? null : informacao.getRelacaoParentesco());
		informacao.setResponsaveis(!informacao.getResponsaveis().isEmpty()? informacao.getResponsaveis() : null);
		informacao.setSituacaoMercadoTrabalho(informacao.getSituacaoMercadoTrabalho() == null || 
				(informacao.getSituacaoMercadoTrabalho() != null && informacao.getSituacaoMercadoTrabalho().getCodigo() == null) ? null : informacao.getSituacaoMercadoTrabalho());
		
		return informacao;
	}
	
	public String gerarUuid(String cnes) {
		String uuid = null;
		
		do {
			uuid = cnes.concat("-".concat(UUID.randomUUID().toString()));
		} while(repository.findById(uuid).isPresent());
		
		return uuid;
	 }
	
	public FichaCadastroIndividual save(FichaCadastroIndividual ficha) {	
		FichaCadastroIndividual ultFicha = null;
		if (ficha.getCidadao().getId() != null) {
			Optional<FichaCadastroIndividual> fichaOp = null;
			if (ficha.getCidadao().getUltimaficha() != null)
				fichaOp = repository.findById(ficha.getCidadao().getUltimaficha());
			
			ultFicha = fichaOp != null && fichaOp.isPresent() == Boolean.TRUE ? fichaOp.get() : null;
		}
		String uuid = null;
		if (ultFicha == null || (ultFicha != null && ultFicha.getImportada() == Boolean.TRUE))
			uuid = gerarUuid(ficha.getUbs().getCnes());
		else
			uuid = ultFicha.getUuid();
		ficha.setUuid(uuid);
		ficha.getCidadao().setUuidFichaOriginadora(ficha.getCidadao().getUuidFichaOriginadora() == null ? uuid : ficha.getCidadao().getUuidFichaOriginadora());
		ficha.setFichaAtualizada(!ficha.getUuid().equals(ficha.getCidadao().getUuidFichaOriginadora()) ? Boolean.TRUE : Boolean.FALSE);
		ficha.getCidadao().setUltimaficha(ficha.getUuid());
		
		return repository.save(verificarCampo(ficha));
	}
	
	public Optional<Cidadao> findById(Long id) {
		return cidadaorepository.findById(id);
	}
	
	public InformacaoSocioDemografica getInformacaoSocioDemografica(Cidadao cidadao) {
		InformacaoSocioDemografica informacao = cidadao.getInformacaoSocioDemografica() == null ? 
    			new InformacaoSocioDemografica() : cidadao.getInformacaoSocioDemografica();
    	
    	if (cidadao.getStatusEhResponsavel())
    		informacao.setRelacaoParentesco(new RelacaoParentesco());
    	
    	SimpleDateFormat fd = new SimpleDateFormat("yyyy-MM-dd");
    	String date = fd.format(cidadao.getDataNascimento());
    	
    	LocalDate date1 = LocalDate.of(Integer.parseInt(date.substring(0, 4)),  Integer.parseInt(date.substring(5, 7)), Integer.parseInt(date.substring(8, 10)));
    	LocalDate date2 = LocalDate.now();
    	informacao.setIdade((int) (ChronoUnit.DAYS.between(date1, date2)/365));
    	
    	if (informacao.getIdade() > 9)
    		informacao.setResponsaveis(null);
    	
    	informacao.setEhResponsavel(cidadao.getStatusEhResponsavel());
    	
    	return informacao;
	}

	public CadastroIndividualTransportDto cadastroTransport(FichaCadastroIndividual ficha) {
		CadastroIndividualTransportDto cadastro = new CadastroIndividualTransportDto();
		
		cadastro.setFichaAtualizada(ficha.getFichaAtualizada());
		cadastro.setStatusTermoRecusaCadastroIndividualAtencaoBasica(Boolean.FALSE);
		cadastro.setUuid(ficha.getUuid());
		cadastro.setUuidFichaOriginadora(ficha.getCidadao().getUuidFichaOriginadora());
		cadastro.setTpCdsOrigem(3);
		cadastro.setIdentificacaoUsuarioCidadao(identificacaoTransport(ficha.getCidadao()));
		
		if (ficha.getCidadao().getInformacaoSocioDemografica() != null)
			cadastro.setInformacoesSocioDemograficas(informacaoTransport(ficha.getCidadao().getInformacaoSocioDemografica()));
		else {
			InformacoesSocioDemograficasTransportDto informacao = new InformacoesSocioDemograficasTransportDto();
			informacao.setStatusTemAlgumaDeficiencia(Boolean.FALSE);
			informacao.setStatusFrequentaEscola(Boolean.FALSE);
			cadastro.setInformacoesSocioDemograficas(informacao);
		}

		if (ficha.getCidadao().getCondicaoSaude() != null)
			cadastro.setCondicoesDeSaude(condicaoTransport(ficha.getCidadao().getCondicaoSaude()));

		//implementar situacao de rua na próxima versão
		SituacaoDeRuaTransportDto situacao = new SituacaoDeRuaTransportDto();
		situacao.setStatusSituacaoRua(Boolean.FALSE);
		cadastro.setEmSituacaoDeRua(situacao);
			
		
		cadastro.setHeaderTransport(exportacaoService.headerTrasport(ficha.getProfissionalResponsavel(), ficha.getCbo(), ficha.getUbs(), ficha.getData().getTime()));
		
		return cadastro;
	}
	
	public IdentificacaoUsuarioCidadaoTransportDto identificacaoTransport(Cidadao cidadao) {
		IdentificacaoUsuarioCidadaoTransportDto identificacao = new IdentificacaoUsuarioCidadaoTransportDto();
		
		identificacao.setNomeSocial(cidadao.getNomeSocial());
		identificacao.setCodigoIbgeMunicipioNascimento(cidadao.getMunicipioNascimento() != null ? cidadao.getMunicipioNascimento().getCodigoIbge().toString() : null);
		identificacao.setDataNascimentoCidadao(cidadao.getDataNascimento().getTime());
		identificacao.setDesconheceNomeMae(cidadao.getDesconheceNomeMae());
		identificacao.setEmailCidadao(cidadao.getEmail());
		identificacao.setNacionalidadeCidadao((long) cidadao.getNacionalidade().getValue());
		identificacao.setNomeCidadao(cidadao.getNome());
		identificacao.setNomeMaeCidadao(cidadao.getDesconheceNomeMae() == Boolean.TRUE ? null : cidadao.getNomeMae());
		identificacao.setTelefoneCelular(cidadao.getCelular() == null ? cidadao.getTelefone() : cidadao.getCelular());
		identificacao.setNumeroNisPisPasep(cidadao.getNumeroNisPisPasep());
		identificacao.setPaisNascimento(cidadao.getPaisNascimento().getCodigo());
		identificacao.setRacaCorCidadao(cidadao.getRacaCor().getCodigo());
		identificacao.setSexoCidadao((long) cidadao.getSexo().getValue());
		identificacao.setStatusEhResponsavel(cidadao.getStatusEhResponsavel());
		identificacao.setEtnia(cidadao.getEtnia() != null ? cidadao.getEtnia().getCodigo() : null);
		identificacao.setNomePaiCidadao(cidadao.getNomePai() != null ? cidadao.getNomePai() : null);
		identificacao.setDesconheceNomePai(cidadao.getDesconheceNomePai());
		identificacao.setDtNaturalizacao(cidadao.getDtNaturalizacao() != null ? cidadao.getDtNaturalizacao().getTime() : null);
		identificacao.setPortariaNaturalizacao(cidadao.getPortariaNaturalizacao());
		identificacao.setDtEntradaBrasil(cidadao.getDtEntradaBrasil() != null ? cidadao.getDtEntradaBrasil().getTime() : null);
		identificacao.setMicroArea(cidadao.getMicroarea());
		identificacao.setStForaArea(cidadao.getStForaArea());
		
		if (cidadao.getStatusEhResponsavel() == Boolean.FALSE) {
			if (cidadao.getCnsResponsavelFamiliar() != null)
				identificacao.setCnsResponsavelFamiliar(cidadao.getCnsResponsavelFamiliar());
			else 
				identificacao.setCpfResponsavelFamiliar(cidadao.getCpfResponsavelFamiliar());
		}
		
		if (cidadao.getCns() != null)
			identificacao.setCnsCidadao(cidadao.getCns());
		else
			identificacao.setCpfCidadao(cidadao.getCpf());

		return identificacao;
	}

	public CondicoesDeSaudeTransportDto condicaoTransport(CondicaoSaude condicao) {
		CondicoesDeSaudeTransportDto condicaoDto = new CondicoesDeSaudeTransportDto();

		condicaoDto.setDescricaoOutraCondicao1(condicao.getCondicaoTemporaria().getDescricaoOutraCondicao1());
		condicaoDto.setDescricaoOutraCondicao2(condicao.getCondicaoTemporaria().getDescricaoOutraCondicao2());
		condicaoDto.setDescricaoOutraCondicao3(condicao.getCondicaoTemporaria().getDescricaoOutraCondicao3());
		condicaoDto.setDescricaoCausaInternacaoEm12Meses(condicao.getCondicaoTemporaria().getDescricaoCausaInternacaoEm12Meses());
		condicaoDto.setDescricaoPlantasMedicinaisUsadas(condicao.getCondicaoTemporaria().getDescricaoPlantasMedicinaisUsadas());
		condicaoDto.setSituacaoPeso(condicao.getCondicaoTemporaria().getConsideracaoPeso() != null ? condicao.getCondicaoTemporaria().getConsideracaoPeso().getCodigo() : null);
		condicaoDto.setStatusEhDependenteAlcool(condicao.getCondicaoTemporaria().getStatusEhDependenteAlcool());
		condicaoDto.setStatusEhFumante(condicao.getCondicaoTemporaria().getStatusEhFumante());
		condicaoDto.setStatusEhDependenteOutrasDrogas(condicao.getCondicaoTemporaria().getStatusEhDependenteOutrasDrogas());
		condicaoDto.setMaternidadeDeReferencia(condicao.getCondicaoTemporaria().getMaternidadeDeReferencia());
		condicaoDto.setStatusEhGestante(condicao.getCondicaoTemporaria().getStatusEhGestante());
		condicaoDto.setStatusEstaAcamado(condicao.getCondicaoTemporaria().getStatusEstaAcamado());
		condicaoDto.setStatusTeveInternadoEm12Meses(condicao.getCondicaoTemporaria().getStatusTeveInternadoem12Meses());
		condicaoDto.setStatusUsaPlantaMedicinais(condicao.getCondicaoTemporaria().getStatusUsaPlantasMedicinais());
		condicaoDto.setStatusEstaDomiciliado(condicao.getCondicaoTemporaria().getStatusEstaDomiciliado());

		if (condicao.getListDoencaCardiaca().size() > 0)
			condicaoDto.setDoencaCardiaca(condicao.getListDoencaCardiaca().stream().map(cardiaca -> cardiaca.getCodigo()).collect(Collectors.toList()));

		if (condicao.getListDoencaRespiratoria().size() > 0)
			condicaoDto.setDoencaRespiratoria(condicao.getListDoencaRespiratoria().stream().map(respiratoria -> respiratoria.getCodigo()).collect(Collectors.toList()));

		if (condicao.getListDoencaRins().size() > 0)
			condicaoDto.setDoencaRins(condicao.getListDoencaRins().stream().map(rins -> rins.getCodigo()).collect(Collectors.toList()));

		return condicaoDto;
	}
	
	public SituacaoDeRuaTransportDto situacaoTransport(SituacaoRua situacao) {
		SituacaoDeRuaTransportDto situacaoDto = new SituacaoDeRuaTransportDto();
		
		situacaoDto.setGrauParentescoFamiliarFrequentado(situacao.getGrauParentescoFamiliarFrequentado());
		situacaoDto.setOutraInstituicaoQueAcompanha(situacao.getOutraInstituicaoAcompanha());
		situacaoDto.setStatusPossuiReferenciaFamiliar(situacao.getStatusAcompanhadoOutraInstituicao());
		situacaoDto.setStatusPossuiReferenciaFamiliar(situacao.getStatusPossuiReferenciaFamiliar());
		situacaoDto.setStatusRecebeBeneficio(situacao.getStatusRecebeBeneficio());
		situacaoDto.setStatusSituacaoRua(situacao.getStatusSituacaoRua());
		situacaoDto.setStatusTemAcessoHigienePessoalSituacaoRua(situacao.getStatusAcessoHigienePessoal());
		situacaoDto.setStatusVisitaFamiliarFrequentemente(situacao.getStatusVisitaFamiliarFrequentemente());
		situacaoDto.setTempoSituacaoRua(situacao.getTempoSituacaoRua() == null ? null : situacao.getTempoSituacaoRua().getCodigo());
		situacaoDto.setQuantidadeAlimentacoesAoDiaSituacaoRua(situacao.getQuantasAlimentacao() == null ? null : situacao.getQuantasAlimentacao().getCodigo());
		
		
		if (situacao.getListAcessoHigiene().size() > 0)
			situacaoDto.setHigienePessoalSituacaoRua(situacao.getListAcessoHigiene().stream().map(op -> op.getCodigo()).collect(Collectors.toList()));
		
		if (situacao.getListOrigemAlimentacao().size() > 0)
			situacaoDto.setOrigemAlimentoSituacaoRua(situacao.getListOrigemAlimentacao().stream().map(op -> op.getCodigo()).collect(Collectors.toList()));
		
	
		return situacaoDto;
	}
	
	public InformacoesSocioDemograficasTransportDto informacaoTransport(InformacaoSocioDemografica informacao) {
		InformacoesSocioDemograficasTransportDto inf = new InformacoesSocioDemograficasTransportDto();
		
		if (informacao.getDeficiencias() != null) {
				inf.setDeficienciasCidadao(informacao.getDeficiencias().stream().map(def -> def.getCodigo()).collect(Collectors.toList()));
		}
		
		inf.setGrauInstrucaoCidadao(informacao.getGrauInstrucaoCidadao() != null ? informacao.getGrauInstrucaoCidadao().getCodigo() : null);
		inf.setOcupacaoCodigoCbo2002(informacao.getCbo() != null ? informacao.getCbo().getCodigo() : null);
		inf.setOrientacaoSexualCidadao(informacao.getOrientacaoSexual() != null ? informacao.getOrientacaoSexual().getCodigo() : null);
		inf.setPovoComunidadeTradicional(informacao.getPovoComunidadeTradicional() != null ? informacao.getPovoComunidadeTradicional().getDescricao() : null);
		inf.setRelacaoParentescoCidadao(informacao.getRelacaoParentesco() != null ? informacao.getRelacaoParentesco().getCodigo() : null);
		inf.setSituacaoMercadoTrabalhoCidadao(informacao.getSituacaoMercadoTrabalho() != null ? informacao.getSituacaoMercadoTrabalho().getCodigo() : null);
		inf.setStatusDesejaInformarOrientacaoSexual(informacao.getStatusDesejaInformarOrientacaoSexual());
		inf.setStatusFrequentaBenzedeira(informacao.getStatusFrequentaBenzedeira());
		inf.setStatusFrequentaEscola(informacao.getStatusFrequentaEscola());
		inf.setStatusMembroPovoComunidadeTradicional(informacao.getStatusMembroPovoComunidadeTradicional());
		inf.setStatusParticipaGrupoComunitario(informacao.getStatusParticipaGrupoComunitario());
		inf.setStatusPossuiPlanoSaudePrivado(informacao.getStatusPossuiPlanoSaudePrivado());
		inf.setStatusTemAlgumaDeficiencia(informacao.getStatusTemAlgumaDeficiencia());
		inf.setIdentidadeGeneroCidadao(informacao.getIdentidadeGenero() != null ? informacao.getIdentidadeGenero().getCodigo() : null);
		inf.setStatusDesejaInformarIdentidadeGenero(informacao.getStatusDesejaInformarIdentidadeGenero());
		
		if (informacao.getResponsaveis() != null) {
				inf.setResponsavelPorCrianca(informacao.getResponsaveis().stream().map(resp -> resp.getCodigo()).collect(Collectors.toList()));
		}
		
		return inf;
	}
	
	public void gerarExportacao(ZipOutputStream zipOut) throws JsonGenerationException, JsonMappingException, IOException {
			List<FichaCadastroIndividual> fichas = repository.findByImportada(false);
			
			for (FichaCadastroIndividual ficha: fichas) {
				CadastroIndividualDto dado = new CadastroIndividualDto();
				
				dado = (CadastroIndividualDto) exportacaoService.informacaoTransport(dado, ficha.getUbs().getCnes(), ficha.getUuid(), 2L, ficha.getIne());
				dado.setTrasport(cadastroTransport(ficha));
				
				XmlMapper xmlMapper = new XmlMapper();
				File file = new File(ficha.getUuid() + ".esus.xml");
			    xmlMapper.writeValue(file, dado);
			    exportacaoService.zip(zipOut, file.getAbsolutePath());
			    Files.deleteIfExists(file.toPath());
			    
			    ficha.setImportada(Boolean.TRUE);
			    repository.save(ficha);
			}
	}
	
	public Page<Cidadao> search(CidadaoFilterDto filter) {
		return cidadaorepository.search(filter.getNome(), filter.getCpf(), filter.getCns(), filter.getAtivo(), searchService.pageable(filter));
	}
	
	 public void delete(Long id) {
	        Optional<Cidadao>  cidadaoOp = cidadaorepository.findById(id);
	        
	        if (cidadaoOp.isPresent()) {
	        	Cidadao cidadao = cidadaoOp.get();
	        	cidadao.setAtivo(false);
	        	cidadaorepository.save(cidadao);
	        }
	  }
	
	 public List<Cidadao> filtro(String valor) {
		 return cidadaorepository.findByCnsOrCpfOrNome(valor, org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("nome"))));
	 }
	 
	 public Long countByImportada() {
		 return repository.countByImportada(Boolean.FALSE);
	 }

	 public InformacoesCondicaoSaudeDTO initCondicaosaude() {
		 return new InformacoesCondicaoSaudeDTO(cardiacaRepository.findAll(), rinsRepository.findAll(),
				 respiratoriaRepository.findAll(), consideracaoPesoRepository.findAll());
	 }
	 
	 public FormInformacaoSociodemograficaDTO initInformacaoSociodemografica() {
		 return new FormInformacaoSociodemograficaDTO(relacaoParentescoRepository.findAll(), cursoMaisElevadoRepository.findAll(), 
				 situacaoMercadoTrabalhoRepository.findAll(),responsavelCriancaRepository.findAll(), povoComunidadeTradicionalRepository.findAll(), 
				 orientacaoSexualRepository.findAll(), identidadeGeneroRepository.findAll(), deficienciaRepository.findAll());
	 }
	 
	 public FormCidadaoGeralDTO initCidadaogeral() {
		 return new FormCidadaoGeralDTO(Sexo.values(), EstadoCivil.values(), Estado.valuesForSigla(), Nacionalidade.values(), racaCorRepository.findAll());
	 }

	public InformacoesSituacaoRuaDTO initSituacaoRua() {
		return new InformacoesSituacaoRuaDTO(tempoSituacaoRuaRepository.findAll(), origemAlimentacaoRepository.findAll(),
				qtdAlimentacaoRepository.findAll(), higieneRepository.findAll());
	}
	
	public FormMoradiaCidadaoDTO initMoradiaCidadao() {
		return new FormMoradiaCidadaoDTO(tipoImovelRepository.findAll(), tipoLogradouroRepository.findAll(),
				Estado.valuesForSigla());
	}
	
}
