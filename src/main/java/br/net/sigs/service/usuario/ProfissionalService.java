package br.net.sigs.service.usuario;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.repository.usuario.ProfissionalRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ProfissionalService {

	@Autowired
	private ProfissionalRepository profissionalRepository;
	
	public Optional<Profissional> findById(Long id) {
		return profissionalRepository.findById(id);
	}
	
	public List<Profissional> search(String valor) {
		return profissionalRepository.search(valor.toLowerCase(), org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("id"))));
	}
	
}
