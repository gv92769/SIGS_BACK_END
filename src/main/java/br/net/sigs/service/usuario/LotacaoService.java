package br.net.sigs.service.usuario;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.usuario.Lotacao;
import br.net.sigs.repository.usuario.LotacaoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class LotacaoService {

	@Autowired
	private LotacaoRepository repository;
	
	public Optional<Lotacao> findById(Long id) {
		return repository.findById(id);
	}
	
	public Lotacao save(Lotacao lotacao) {
		return repository.save(lotacao);
	}
	
	public List<Lotacao> search(Long id) {
		return repository.search(id);
	}
	
	public void delete(Lotacao lotacao) {
		repository.delete(lotacao);
	}
	
	public List<Lotacao> findAllByIdByAtivo(Long id) {
		return repository.findAllByIdByAtivo(id);
	}

}
