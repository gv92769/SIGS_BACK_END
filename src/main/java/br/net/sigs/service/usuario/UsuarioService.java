package br.net.sigs.service.usuario;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.net.sigs.dto.UsuarioFilterDto;
import br.net.sigs.dto.UsuarioProfissionalDto;
import br.net.sigs.entity.usuario.Usuario;
import br.net.sigs.exception.BadRequestException;
import br.net.sigs.repository.usuario.ProfissionalRepository;
import br.net.sigs.repository.usuario.UsuarioRepository;
import br.net.sigs.service.util.SearchService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class UsuarioService {

	@Autowired
    private UsuarioRepository repository;
	
	@Autowired
    private ProfissionalRepository profissionalrepository;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private SearchService searchService;
	
    public UsuarioProfissionalDto create(UsuarioProfissionalDto usuarioProfissional) {
    	usuarioProfissional.getUsuario().setSenha(passwordEncoder.encode(usuarioProfissional.getUsuario().getSenha()));
	    Optional<Usuario> usuarioFound = repository.findByLogin(usuarioProfissional.getUsuario().getLogin());
	    
	    if(usuarioFound.isPresent()){
	        throw new BadRequestException("Usuário already exists with the provided login");
	    }
	    
	    return save(usuarioProfissional);
    }
    
    public Optional<Usuario> findById(Long id) {
    	return repository.findById(id);
    }
    
    public UsuarioProfissionalDto save(UsuarioProfissionalDto usuarioProfissional) {
    	usuarioProfissional.setUsuario(repository.save(usuarioProfissional.getUsuario()));
    	
		usuarioProfissional.getProfissional().setUsuario(usuarioProfissional.getUsuario());
		profissionalrepository.save(usuarioProfissional.getProfissional());

    	return usuarioProfissional;
    }
    
    public Page<Usuario> search(UsuarioFilterDto filter) {
    	return repository.search(filter.getNome() == null ? null : filter.getNome().toLowerCase(), filter.getCpf(), filter.getCns(), filter.getAtivo(), searchService.pageable(filter));
    }
    
    public void delete(Usuario usuario) {
    	usuario.setAtivo(Boolean.FALSE);
    	repository.save(usuario);
    }
    
}
