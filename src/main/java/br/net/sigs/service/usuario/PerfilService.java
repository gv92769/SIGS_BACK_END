package br.net.sigs.service.usuario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.usuario.Perfil;
import br.net.sigs.repository.usuario.PerfilRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class PerfilService {

	@Autowired
	private PerfilRepository repository;
	
	public List<Perfil> findAllByAtivo() {
		return repository.findAllByAtivo(true);
	}
	
	public Perfil findByPerfil(String perfil) {
		return repository.findByPerfil(perfil);
	}
	
}
