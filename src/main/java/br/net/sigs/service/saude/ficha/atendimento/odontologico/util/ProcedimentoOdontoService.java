package br.net.sigs.service.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.ProcedimentoOdonto;
import br.net.sigs.repository.saude.ficha.atendimento.odontologico.util.ProcedimentoOdontoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ProcedimentoOdontoService {

	@Autowired
	private ProcedimentoOdontoRepository procedimentoOdontoRepository;
	
	public List<ProcedimentoOdonto> findAll() {
		return procedimentoOdontoRepository.findAll();
	}
	
}
