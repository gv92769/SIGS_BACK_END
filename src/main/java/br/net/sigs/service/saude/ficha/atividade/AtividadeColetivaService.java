package br.net.sigs.service.saude.ficha.atividade;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import br.net.sigs.dto.AtividadeColetivaFilterDto;
import br.net.sigs.dto.AtividadeColetivaTabelaDto;
import br.net.sigs.dto.FormAtividadeColetivaDTO;
import br.net.sigs.dto.integracao.atividade.AtividadeColetivaDto;
import br.net.sigs.dto.integracao.atividade.FichaAtividadeColetivaTransportDto;
import br.net.sigs.dto.integracao.atividade.ParticipanteRowItemTransportDto;
import br.net.sigs.dto.integracao.util.ProfissionalCboRowItemTransportDto;
import br.net.sigs.entity.saude.ficha.atividade.AtividadeColetiva;
import br.net.sigs.entity.saude.ficha.atividade.ParticipanteAtividadeColetiva;
import br.net.sigs.repository.saude.ficha.atividade.AtividadeColetivaRepository;
import br.net.sigs.repository.saude.ficha.atividade.util.PraticaSaudeRepository;
import br.net.sigs.repository.saude.ficha.atividade.util.PublicoAlvoRepository;
import br.net.sigs.repository.saude.ficha.atividade.util.TemaReuniaoRepository;
import br.net.sigs.repository.saude.ficha.atividade.util.TemaSaudeRepository;
import br.net.sigs.repository.saude.ficha.atividade.util.TipoAtividadeColetivaRepository;
import br.net.sigs.service.util.ExportacaoService;
import br.net.sigs.service.util.SearchService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class AtividadeColetivaService {

	@Autowired
	private AtividadeColetivaRepository atividadeColetivaRepository;
	
	@Autowired
	private ExportacaoService exportacaoService;
	
	@Autowired
	private SearchService searchService;
	
	@Autowired
	private TipoAtividadeColetivaRepository tipoAtividadeColetivaRepository;
	
	@Autowired
	private TemaReuniaoRepository temaReuniaoRepository;
	
	@Autowired
	private PublicoAlvoRepository publicoAlvoRepository;
	
	@Autowired
	private TemaSaudeRepository temaSaudeRepository;
	
	@Autowired
	private PraticaSaudeRepository praticaSaudeRepository;

	public String gerarUuid(String cnes) {
		String uuid = null;
		
		do {
			uuid = cnes.concat("-".concat(UUID.randomUUID().toString()));
		} while(atividadeColetivaRepository.findByUuid(uuid) != null);

		return uuid;
	 }
	
	public AtividadeColetiva verificarCampo(AtividadeColetiva atividadeColetiva) {
		atividadeColetiva.setImportada(atividadeColetiva.getUuid() == null ? Boolean.FALSE : atividadeColetiva.getImportada());
		atividadeColetiva.setUuid(atividadeColetiva.getUuid() == null ? gerarUuid(atividadeColetiva.getUbs().getCnes()) : atividadeColetiva.getUuid());
		atividadeColetiva.setInstituicaoEnsino(atividadeColetiva.getInstituicaoEnsino() == null || 
				(atividadeColetiva.getInstituicaoEnsino() != null && atividadeColetiva.getInstituicaoEnsino().getInep() != null) ? atividadeColetiva.getInstituicaoEnsino() : null);
	 	atividadeColetiva.setCnesLocalAtividade(atividadeColetiva.getCnesLocalAtividade() == null || 
	 			(atividadeColetiva.getCnesLocalAtividade() != null && atividadeColetiva.getCnesLocalAtividade().getId() != null) ? atividadeColetiva.getCnesLocalAtividade()  : null);
	    atividadeColetiva.setListPublicoAlvo(atividadeColetiva.getListPublicoAlvo().isEmpty() ? null : atividadeColetiva.getListPublicoAlvo());
	    atividadeColetiva.setListTemaReuniao(atividadeColetiva.getListTemaReuniao().isEmpty() ? null : atividadeColetiva.getListTemaReuniao());
	    atividadeColetiva.setListTemaSaude(atividadeColetiva.getListTemaSaude().isEmpty() ? null : atividadeColetiva.getListTemaSaude());
	    atividadeColetiva.setParticipantes(atividadeColetiva.getParticipantes().isEmpty() ? null : atividadeColetiva.getParticipantes());
	    atividadeColetiva.setProfissionais(atividadeColetiva.getProfissionais().isEmpty() ? null : atividadeColetiva.getProfissionais());
	    atividadeColetiva.setPraticaSaude(atividadeColetiva.getPraticaSaude() == null || 
	    		(atividadeColetiva.getPraticaSaude() != null && atividadeColetiva.getPraticaSaude().getCodigo() != null) ? atividadeColetiva.getPraticaSaude() : null);
	    atividadeColetiva.setProcedimentoColetivo(atividadeColetiva.getProcedimentoColetivo() == null || 
	    		(atividadeColetiva.getProcedimentoColetivo() != null && atividadeColetiva.getProcedimentoColetivo().getId() != null) ? atividadeColetiva.getProcedimentoColetivo() : null);
		atividadeColetiva.setNumParticipantes(atividadeColetiva.getParticipantes() != null && atividadeColetiva.getNumParticipantes() < atividadeColetiva.getParticipantes().size() 
				? atividadeColetiva.getParticipantes().size() : atividadeColetiva.getNumParticipantes());
	    
		return atividadeColetiva;
	}
	
	public Optional<AtividadeColetiva> findById(Long id) {
		return atividadeColetivaRepository.findById(id);
	}
	
	public AtividadeColetiva save(AtividadeColetiva atividadeColetiva) {
		return atividadeColetivaRepository.save(verificarCampo(atividadeColetiva));
	}
	
	public Page<AtividadeColetivaTabelaDto> search(AtividadeColetivaFilterDto filter) {	
		return atividadeColetivaRepository.search(filter.getPublicoAlvo(), filter.getDataInicio(), filter.getDataFim(), 
				filter.getCnes(), filter.getInep(), filter.getEscola(), filter.getUbs(), filter.getExportada(), searchService.pageable(filter));
	}

	public void delete(Long id) {
		atividadeColetivaRepository.deleteById(id);
	}
	
	public List<ParticipanteRowItemTransportDto> participantes(List<ParticipanteAtividadeColetiva> participantes) {
		List<ParticipanteRowItemTransportDto> participantesTransport = new ArrayList<>();
		
		for (ParticipanteAtividadeColetiva participante: participantes) {
			ParticipanteRowItemTransportDto part = new ParticipanteRowItemTransportDto();
			
			part.setAbandonouGrupo(participante.getAbandonouGrupo());
			part.setAltura(participante.getAltura());
			part.setAvaliacaoAlterada(participante.getAvaliacaoAlterada());
			part.setCessouHabitoFumar(participante.getCessouHabitoFumar());
			part.setCnsParticipante(participante.getCidadao().getCns());
			part.setCpfParticipante(part.getCnsParticipante() == null ? participante.getCidadao().getCpf() : null);
			part.setDataNascimento(participante.getCidadao().getDataNascimento().getTime());
			part.setPeso(participante.getPeso());
			part.setSexo((long) participante.getCidadao().getSexo().getValue());
			
			participantesTransport.add(part);
		}
		
		return participantesTransport;
	}
	
	public FichaAtividadeColetivaTransportDto transport(AtividadeColetiva atividade) {
		FichaAtividadeColetivaTransportDto ficha = new FichaAtividadeColetivaTransportDto();
		
		ficha.setAtividadeTipo(atividade.getTipoAtividade() != null ? atividade.getTipoAtividade().getCodigo() : null);
		ficha.setCnesLocalAtividade(atividade.getCnesLocalAtividade() != null ? atividade.getCnesLocalAtividade().getCnes() : null);
		ficha.setHeaderTransport(exportacaoService.headerTrasport(atividade.getProfissionalResponsavel(), atividade.getCbo(), atividade.getUbs(), atividade.getData().getTime()));
		ficha.setInep(atividade.getInstituicaoEnsino() != null ? atividade.getInstituicaoEnsino().getInep() : null);
		
		if (atividade.getNumAvaliacoesAlteradas() != null)
			ficha.setNumAvaliacoesAlteradas(atividade.getNumAvaliacoesAlteradas());
		
		ficha.setNumParticipantes(atividade.getNumParticipantes());
		ficha.setOutraLocalidade(atividade.getOutraLocalidade());
		ficha.setParticipantes(atividade.getParticipantes() != null ? participantes(atividade.getParticipantes()) : null);
		
		List<Long> array = new ArrayList<>();
		if (atividade.getPraticaSaude() != null) {
			array.add(atividade.getPraticaSaude().getCodigo());
		}
		ficha.setPraticasEmSaude(!array.isEmpty()  ? array  :  null);
		
		ficha.setProcedimento(atividade.getProcedimentoColetivo() != null ? atividade.getProcedimentoColetivo().getSigtap().replaceAll("[.-]", "") : null);
		
		if (atividade.getProfissionais() != null)	
			ficha.setProfissionais(atividade.getProfissionais().stream().map(profissional -> 
				new ProfissionalCboRowItemTransportDto(profissional.getProfissional().getCns(), profissional.getCbo().getCodigo())).collect(Collectors.toList()));
		
		ficha.setPseEducacao(atividade.getPseEducacao());
		ficha.setPseSaude(atividade.getPseSaude());
		
		if (atividade.getListPublicoAlvo() != null)
			ficha.setPublicoAlvo(atividade.getListPublicoAlvo().stream().map(publicoAlvo -> publicoAlvo.getCodigo()).collect(Collectors.toList()));
		
		ficha.setTbCdsOrigem(3);
		
		if (atividade.getListTemaReuniao() != null)
			ficha.setTemasParaReuniao(atividade.getListTemaReuniao().stream().map(temaReuniao -> temaReuniao.getCodigo()).collect(Collectors.toList()));
		
		if (atividade.getListTemaSaude() != null)
			ficha.setTemasParaSaude(atividade.getListTemaSaude().stream().map(temaSaude -> temaSaude.getCodigo()).collect(Collectors.toList()));
		
		ficha.setTurno(atividade.getTurno());
		ficha.setUuidFicha(atividade.getUuid());
		
		return ficha;
	}
	
	public void gerarExportacao(ZipOutputStream zipOut) throws JsonGenerationException, JsonMappingException, IOException  {		 
			 List<AtividadeColetiva> fichas = atividadeColetivaRepository.findAllByImportada(Boolean.FALSE);
			 
			 for (AtividadeColetiva ficha: fichas) {
				 AtividadeColetivaDto dado = new AtividadeColetivaDto();
				 dado = (AtividadeColetivaDto) exportacaoService.informacaoTransport(dado, ficha.getUbs().getCnes(), ficha.getUuid(), 6L, ficha.getIne());
				 dado.setTransport(transport(ficha));
				 
				 XmlMapper xmlMapper = new XmlMapper();
				 File file = new File(ficha.getUuid() + ".esus.xml");
				 xmlMapper.writeValue(file, dado);
				 exportacaoService.zip(zipOut, file.getAbsolutePath());
				 
				 Files.deleteIfExists(file.toPath());
				 
				 ficha.setImportada(Boolean.TRUE);
				 atividadeColetivaRepository.save(ficha);
			 }
	 }
	
	public Long countByImportada() {
		 return atividadeColetivaRepository.countByImportada(Boolean.FALSE);
	 }
	
	public FormAtividadeColetivaDTO init() {
		return new FormAtividadeColetivaDTO(tipoAtividadeColetivaRepository.findAll(), temaReuniaoRepository.findAll(), publicoAlvoRepository.findAll(),
				temaSaudeRepository.findAll(), praticaSaudeRepository.findAll());
	}
	
}
