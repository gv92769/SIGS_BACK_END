package br.net.sigs.service.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.CondutaEncaminhamentoOdonto;
import br.net.sigs.repository.saude.ficha.atendimento.odontologico.util.CondutaEncaminhamentoOdontoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class CondutaEncaminhamentoOdontoService {
	
	@Autowired
	private CondutaEncaminhamentoOdontoRepository condutaEncaminhamentoOdontoRepository;
	
	public List<CondutaEncaminhamentoOdonto> findAllById(List<Long> ids) {
		return condutaEncaminhamentoOdontoRepository.findAllById(ids);
	}
	
}
