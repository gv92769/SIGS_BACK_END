package br.net.sigs.service.saude.ciap;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ciap.CiapCondicaoAvaliada;
import br.net.sigs.repository.saude.ciap.CiapCondicaoAvaliadaRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class CiapCondicaoAvaliadaService {

	@Autowired
	private CiapCondicaoAvaliadaRepository repository;
	
	public List<CiapCondicaoAvaliada> findAll() {
		return repository.findAll();
	}
	
	public List<CiapCondicaoAvaliada> findAllById(List<Long> ids) {
		return repository.findAllById(ids);
	}
	
}
