/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.service.saude.ficha.util;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.net.sigs.dto.AvaliacaoMedicaTabelaDTO;
import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.entity.saude.ficha.atendimento.individual.FichaAtendimentoIndividual;
import br.net.sigs.entity.saude.ficha.util.AvaliacaoMedica;
import br.net.sigs.entity.saude.util.TipoAvaliacaoMedica;
import br.net.sigs.repository.saude.ficha.atendimento.individual.FichaAtendimentoIndividualRepository;
import br.net.sigs.repository.saude.ficha.util.AvaliacaoMedicaRepository;
import br.net.sigs.service.util.SearchService;

/**
 *
 * @author Naisses
 */
@Service
public class AvaliacaoMedicaService {
    
	@Autowired
    private AvaliacaoMedicaRepository repository;
	
	@Autowired
	 private FichaAtendimentoIndividualRepository fichaRepository;
	
	@Autowired
	private SearchService searchService;
    
    public Page<AvaliacaoMedicaTabelaDTO> search(FichaFiltroDto filter, String tipo) {
    	return repository.search(filter.getNome() != null ? filter.getNome().toLowerCase() : null, filter.getCpf(), filter.getCns(), filter.getDataInicio(), filter.getDataFinal(),
    			TipoAvaliacaoMedica.getTipoAvaliacaoMedicaPorNome(tipo), searchService.pageable(filter));
    }
    
    public void delete(AvaliacaoMedica avaliacao) {
    	if (avaliacao.getTipoAvaliacaoMedica().equals(TipoAvaliacaoMedica.ATENDIMENTO_INDIVIDUAL)) {
    		FichaAtendimentoIndividual ficha = fichaRepository.findByAtendimento(avaliacao);
    		fichaRepository.delete(ficha);
    	}
    	repository.delete(avaliacao);
    }
    
    public Optional<AvaliacaoMedica> findById(Long id) {
    	return repository.findById(id);
    }
    
}
