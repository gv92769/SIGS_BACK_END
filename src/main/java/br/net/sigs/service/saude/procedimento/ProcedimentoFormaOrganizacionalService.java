package br.net.sigs.service.saude.procedimento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.procedimento.ProcedimentoFormaOrganizacional;
import br.net.sigs.entity.saude.procedimento.ProcedimentoSubGrupo;
import br.net.sigs.repository.saude.procedimento.ProcedimentoFormaOrganizacionalRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ProcedimentoFormaOrganizacionalService {

	@Autowired
	private ProcedimentoFormaOrganizacionalRepository repository;
	
	public List<ProcedimentoFormaOrganizacional> findAllBySubGrupo(ProcedimentoSubGrupo subgrupo) {
		return repository.findAllBySubGrupo(subgrupo);
	}
	
}
