package br.net.sigs.service.saude.ficha.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.util.LocalAtendimento;
import br.net.sigs.repository.saude.ficha.util.LocalAtendimentoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class LocalAtendimentoService {

	@Autowired
	private LocalAtendimentoRepository localAtendimentoRepository;

	public List<LocalAtendimento> findAll() {
		return localAtendimentoRepository.findAll();
	}
	
}
