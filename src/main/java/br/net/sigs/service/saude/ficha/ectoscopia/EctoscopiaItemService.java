package br.net.sigs.service.saude.ficha.ectoscopia;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.ectoscopia.EctoscopiaItem;
import br.net.sigs.repository.saude.ficha.ectoscopia.EctoscopiaItemRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class EctoscopiaItemService {

	@Autowired
	private EctoscopiaItemRepository repository;
	
	public List<EctoscopiaItem> findAllByGrupo(String grupo) {
		return repository.findAllByGrupo(grupo);
	}
	
}
