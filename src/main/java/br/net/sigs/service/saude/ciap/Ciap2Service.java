package br.net.sigs.service.saude.ciap;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ciap.Ciap2;
import br.net.sigs.repository.saude.ciap.Ciap2Repository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class Ciap2Service {

	@Autowired
    private Ciap2Repository repository;
	
	public List<Ciap2> search(String valor, Byte sexo) {
		return repository.search(valor.toLowerCase(), sexo, org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("descricao")))); 
	}
	
}
