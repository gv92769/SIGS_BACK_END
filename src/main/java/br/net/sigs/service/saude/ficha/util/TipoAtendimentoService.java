package br.net.sigs.service.saude.ficha.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.util.TipoAtendimento;
import br.net.sigs.repository.saude.ficha.util.TipoAtendimentoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class TipoAtendimentoService {

	@Autowired
	private TipoAtendimentoRepository tipoAtendimentoRepository;

	public List<TipoAtendimento> findAllById(List<Long> ids) {
		return tipoAtendimentoRepository.findAllById(ids);
	}
	
}
