package br.net.sigs.service.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.FornecimentoOdonto;
import br.net.sigs.repository.saude.ficha.atendimento.odontologico.util.FornecimentoOdontoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class FornecimentoOdontoService {

	@Autowired
	private FornecimentoOdontoRepository fornecimentoOdontoRepository;

	public List<FornecimentoOdonto> findAll() {
		return fornecimentoOdontoRepository.findAll();
	}
	
}
