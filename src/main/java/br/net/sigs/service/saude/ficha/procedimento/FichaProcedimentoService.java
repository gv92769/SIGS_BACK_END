package br.net.sigs.service.saude.ficha.procedimento;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.dto.FichaProcedimentoTabelaDto;
import br.net.sigs.dto.integracao.procedimento.FichaProcedimentoChildTransportDto;
import br.net.sigs.dto.integracao.procedimento.FichaProcedimentoDto;
import br.net.sigs.dto.integracao.procedimento.FichaProcedimentoMasterTransportDto;
import br.net.sigs.entity.saude.ficha.procedimento.FichaProcedimentos;
import br.net.sigs.entity.saude.ficha.procedimento.ProcedimentoFixo;
import br.net.sigs.entity.saude.procedimento.Procedimento;
import br.net.sigs.repository.saude.ficha.procedimento.FichaProcedimentoRepository;
import br.net.sigs.service.util.ExportacaoService;
import br.net.sigs.service.util.SearchService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class FichaProcedimentoService {

	@Autowired
	private FichaProcedimentoRepository fichaProcedimentoRepository;
	
	@Autowired
	private SearchService searchService;
	
	@Autowired
	private ExportacaoService exportacaoService;
	
	public Optional<FichaProcedimentos> findById(Long id) {
		return fichaProcedimentoRepository.findById(id);
	}
	
	public String gerarUuid(String cnes) {
		String uuid = null;
		
		do {
			uuid = cnes.concat("-".concat(UUID.randomUUID().toString()));
		} while(fichaProcedimentoRepository.findByUuid(uuid) != null);
		
		return uuid;
	 }
	
	public FichaProcedimentos save(FichaProcedimentos fichaProcedimentos) {
		fichaProcedimentos.setUuid(fichaProcedimentos.getUuid() == null ? gerarUuid(fichaProcedimentos.getUbs().getCnes()) : fichaProcedimentos.getUuid());
		fichaProcedimentos.setImportada(fichaProcedimentos.getImportada() == null ? Boolean.FALSE : fichaProcedimentos.getImportada());
		return fichaProcedimentoRepository.save(fichaProcedimentos);
	}
	
	public void delete(Long id) {
		fichaProcedimentoRepository.deleteById(id);
	}
	
	public Page<FichaProcedimentoTabelaDto> search(FichaFiltroDto filtro) {
		return fichaProcedimentoRepository.search(filtro.getNome() != null ? filtro.getNome().toLowerCase() : null, filtro.getCpf(), filtro.getCns(), 
				filtro.getDataInicio(), filtro.getDataFinal(), filtro.getExportada(), searchService.pageable(filtro));
	}
	
	public FichaProcedimentoChildTransportDto fichaProcedimentoChild(FichaProcedimentos ficha) {
		FichaProcedimentoChildTransportDto dado = new FichaProcedimentoChildTransportDto();
		
		dado.setCnsCidadao(ficha.getCidadao().getCns());
		dado.setCpfCidadao(dado.getCnsCidadao() == null ? ficha.getCidadao().getCpf() : null);
		dado.setDataHoraFinalAtendimento(ficha.getDataHorarioFinal().getTime());
		dado.setDataHoraInicialAtendimento(ficha.getDataHorarioInicio().getTime());
		dado.setDtNascimento(ficha.getCidadao().getDataNascimento().getTime());
		dado.setLocalAtendimento(ficha.getLocalAtendimento().getCodigo());
		dado.setNumProntuario(ficha.getCidadao().getNumProntuario());
		
		dado.setProcedimentos(new ArrayList<>());
		for (ProcedimentoFixo procedimento: ficha.getProcedimentosFixo())
			dado.getProcedimentos().add(procedimento.getSigtap() != null && !procedimento.getSigtap().equals("") ? 
					procedimento.getSigtap().replaceAll("[.-]", "") : procedimento.getCodigoAb());
		
		for (Procedimento procedimento: ficha.getProcedimento())
			dado.getProcedimentos().add(procedimento.getCodigo());
		
		dado.setSexo((long) ficha.getCidadao().getSexo().getValue());
		dado.setStatusEscutaInicialOrientacao(ficha.getEscutaInicial());
		dado.setTurno(ficha.getTurno());
		
		return dado;
	}
	
	public FichaProcedimentoMasterTransportDto fichaProcedimentoMaster(FichaProcedimentos ficha) {
		FichaProcedimentoMasterTransportDto dado = new FichaProcedimentoMasterTransportDto();
		
		dado.setAtendProcedimentos(new ArrayList<>());
		dado.getAtendProcedimentos().add(fichaProcedimentoChild(ficha));
		dado.setHeaderTransport(exportacaoService.headerTrasport(ficha.getProfissionalResponsavel(),ficha.getCbo(),  ficha.getUbs(), ficha.getData().getTime()));
		dado.setTpCdsOrigem(3);
		dado.setUuidFicha(ficha.getUuid());
		
		return dado;
	}
	
	public void gerarExportacao(ZipOutputStream zipOut) throws JsonGenerationException, JsonMappingException, IOException  {
			List<FichaProcedimentos> fichas = fichaProcedimentoRepository.findAllByImportada(Boolean.FALSE);
			
			for (FichaProcedimentos ficha: fichas) {
				FichaProcedimentoDto dado = new FichaProcedimentoDto();
				dado = (FichaProcedimentoDto) exportacaoService.informacaoTransport(dado, ficha.getUbs().getCnes(), ficha.getUuid(), 7L, ficha.getIne());
				dado.setTransport(fichaProcedimentoMaster(ficha));
				
				XmlMapper xmlMapper = new XmlMapper();
				File file = new File(ficha.getUuid() + ".esus.xml");
				xmlMapper.writeValue(file, dado);
				exportacaoService.zip(zipOut, file.getAbsolutePath());
				 Files.deleteIfExists(file.toPath());
				
				ficha.setImportada(Boolean.TRUE);
				fichaProcedimentoRepository.save(ficha);
			}
	}
	
	public Long countByImportada() {
		return fichaProcedimentoRepository.countByImportada(Boolean.FALSE);
	}
	
}
