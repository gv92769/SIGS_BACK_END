package br.net.sigs.service.saude.ficha.atividade.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atividade.util.OutroProcedimentoColetivo;
import br.net.sigs.repository.saude.ficha.atividade.util.OutroProcedimentoColetivoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class OutroProcedimentoColetivoService {
	
	
	@Autowired
	private OutroProcedimentoColetivoRepository outroProcedimentoRepository;
	
	public OutroProcedimentoColetivo findBySigtap(String sigtap) {
		return outroProcedimentoRepository.findBySigtap(sigtap);
	}
	
}
