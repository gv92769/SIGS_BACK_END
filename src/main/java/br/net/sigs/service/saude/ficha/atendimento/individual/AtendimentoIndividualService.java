package br.net.sigs.service.saude.ficha.atendimento.individual;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import br.net.sigs.dto.AvaliacaoMedicaTabelaDTO;
import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.dto.integracao.atendimento.individual.AtendimentoIndividualMasterDto;
import br.net.sigs.dto.integracao.atendimento.individual.AtendimentoIndivualDto;
import br.net.sigs.dto.integracao.atendimento.individual.ExameTransportDto;
import br.net.sigs.dto.integracao.atendimento.individual.FichaAtendimentoIndividualMasterTransportDto;
import br.net.sigs.dto.integracao.atendimento.individual.ProblemaCondicaoAvaliacaoAITransport;
import br.net.sigs.entity.saude.ficha.atendimento.individual.AtendimentoIndividual;
import br.net.sigs.entity.saude.ficha.atendimento.individual.FichaAtendimentoIndividual;
import br.net.sigs.entity.saude.ficha.atendimento.individual.exame.Exame;
import br.net.sigs.entity.saude.util.TipoAvaliacaoMedica;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.repository.saude.ficha.atendimento.individual.AtendimentoIndividualRepository;
import br.net.sigs.repository.saude.ficha.atendimento.individual.FichaAtendimentoIndividualRepository;
import br.net.sigs.service.util.ExportacaoService;
import br.net.sigs.service.util.SearchService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class AtendimentoIndividualService {

	 @Autowired
	 private AtendimentoIndividualRepository atendimentoIndividualRepository;
	 
	 @Autowired
	 private FichaAtendimentoIndividualRepository repository;
	 
	 @Autowired
	 private ExportacaoService exportacaoService;

	 @Autowired
	 private SearchService searchService;
	    
	 public Page<AvaliacaoMedicaTabelaDTO> search(FichaFiltroDto filter) {
		 return atendimentoIndividualRepository.search(filter.getNome() != null ? filter.getNome().toLowerCase() : null, filter.getCpf(), filter.getCns(), filter.getDataInicio(), filter.getDataFinal(),
			 filter.getExportada(), searchService.pageable(filter));
	 }
	 
	 public String gerarUuid(String cnes) {
		String uuid = null;
		
		do {
			uuid = cnes.concat("-".concat(UUID.randomUUID().toString()));
		} while(repository.findById(uuid).isPresent());
		
		return uuid;
	 }
	 
	 public AtendimentoIndividual verificarCampos(AtendimentoIndividual atendimento) {
		 atendimento.setAleitamentoMaterno(atendimento.getAleitamentoMaterno() == null || (atendimento.getAleitamentoMaterno() != null && 
				 atendimento.getAleitamentoMaterno().getCodigo() == null) ? null : atendimento.getAleitamentoMaterno());
		 atendimento.setCiaps(atendimento.getCiaps().isEmpty() ? null : atendimento.getCiaps());
		 atendimento.setListExame( atendimento.getListExame().isEmpty() ? null : atendimento.getListExame());
		 atendimento.setNasfs(atendimento.getNasfs().isEmpty() ? null : atendimento.getNasfs());
		 atendimento.setRacionalidadeSaude(atendimento.getRacionalidadeSaude() == null || (atendimento.getRacionalidadeSaude() != null && 
				 atendimento.getRacionalidadeSaude().getCodigo() == null) ? null : atendimento.getRacionalidadeSaude());
		 
		 return atendimento;
	 }
	
	 public Optional<AtendimentoIndividual> findById(Long id) {
		 return atendimentoIndividualRepository.findById(id);
	 }
	 
	 public FichaAtendimentoIndividual findByIdAtendimento(Long id) {
		return  repository.findByIdAtendimento(id);
	 }
	 
	 public AtendimentoIndividual save(AtendimentoIndividual atendimento) {
		 atendimento.setTipoAvaliacaoMedica(TipoAvaliacaoMedica.ATENDIMENTO_INDIVIDUAL);
		 atendimento = atendimentoIndividualRepository.save(verificarCampos(atendimento));
		 
		 if (repository.findByAtendimento(atendimento) == null) {
			 FichaAtendimentoIndividual ficha = new FichaAtendimentoIndividual();
			 ficha.setUuid(gerarUuid(atendimento.getUbs().getCnes()));
			 ficha.setAtendimento(atendimento);
			 ficha.setImportada(Boolean.FALSE);
			 repository.save(ficha);
		 }
		 
		 return atendimento;
	 }
	 
	 public AtendimentoIndivualDto exame(AtendimentoIndividual atendimento, AtendimentoIndivualDto atend) {
		 atend.setExame(atendimento.getListExame() != null || atendimento.getOutrosSia1() != null || atendimento.getOutrosSia2() != null || 
				 atendimento.getOutrosSia3() != null || atendimento.getOutrosSia4() != null ? new ArrayList<ExameTransportDto>() : null);
		 
		 for (Exame exame: atendimento.getListExame()) {
			 ExameTransportDto exTransport = new ExameTransportDto();
			 
			 exTransport.setCodigoExame(exame.getExame().getSigtap() != null &&  exame.getExame().getSigtap().equals( "")  ? 
					 exame.getExame().getSigtap().replaceAll("[.-]", "") : exame.getExame().getCodigoab());
			 exTransport.setSolicitadoAvaliado(new ArrayList<>());
			 exTransport.getSolicitadoAvaliado().add(exame.getSituacao().toString());
			 
			 atend.getExame().add(exTransport);
		 }
		 
		 if (atendimento.getOutrosSia1() != null) {
			 ExameTransportDto exTransport = new ExameTransportDto();
			 
			 exTransport.setSolicitadoAvaliado(new ArrayList<>());
			 exTransport.setCodigoExame(atendimento.getOutrosSia1Codigo().getCodigo());
			 exTransport.getSolicitadoAvaliado().add(atendimento.getOutrosSia1().toString());
			 
			 atend.getExame().add(exTransport);
		 }
		 
		 if (atendimento.getOutrosSia2() != null) {
			 ExameTransportDto exTransport = new ExameTransportDto();
			 
			 exTransport.setSolicitadoAvaliado(new ArrayList<>());
			 exTransport.setCodigoExame(atendimento.getOutrosSia2Codigo().getCodigo());
			 exTransport.getSolicitadoAvaliado().add(atendimento.getOutrosSia2().toString());
			 
			 atend.getExame().add(exTransport);
		 }
		 
		 if (atendimento.getOutrosSia3() != null) {
			 ExameTransportDto exTransport = new ExameTransportDto();
			 
			 exTransport.setSolicitadoAvaliado(new ArrayList<>());
			 exTransport.setCodigoExame(atendimento.getOutrosSia3Codigo().getCodigo());
			 exTransport.getSolicitadoAvaliado().add(atendimento.getOutrosSia3().toString());
			 
			 atend.getExame().add(exTransport);
		 }
		 
		 if (atendimento.getOutrosSia4() != null) {
			 ExameTransportDto exTransport = new ExameTransportDto();
			 
			 exTransport.setSolicitadoAvaliado(new ArrayList<>());
			 exTransport.setCodigoExame(atendimento.getOutrosSia4Codigo().getCodigo());
			 exTransport.getSolicitadoAvaliado().add(atendimento.getOutrosSia4().toString());
			 
			 atend.getExame().add(exTransport);
		 }
		 
		 return atend;
	 }
	 
	 public ProblemaCondicaoAvaliacaoAITransport problemaCondicaoAvaliada(AtendimentoIndividual atendimento) {
		 ProblemaCondicaoAvaliacaoAITransport problema = new ProblemaCondicaoAvaliacaoAITransport();
		 
		 if (atendimento.getCiaps() != null)
			 problema.setCiaps(atendimento.getCiaps().stream().map(ciap -> ciap.getCodigoab()).collect(Collectors.toList()));
		 
		 problema.setOutroCiap1(atendimento.getCiap2_1() != null ? atendimento.getCiap2_1().getCodigo() : null);
		 problema.setOutroCiap2(atendimento.getCiap2_2() != null ? atendimento.getCiap2_2().getCodigo() : null);
		 problema.setCid10(atendimento.getCid10_1() != null ? atendimento.getCid10_1().getCodigo() : null);
		 problema.setCid10_2(atendimento.getCid10_2() != null ? atendimento.getCid10_2().getCodigo() : null);
		 
		 return problema;
	 }
	 
	 public AtendimentoIndivualDto atendimento(AtendimentoIndividual atendimento) {
		 AtendimentoIndivualDto atend = new AtendimentoIndivualDto();
		 
		 atend.setAleitamentoMaterno(atendimento.getAleitamentoMaterno() != null ? atendimento.getAleitamentoMaterno().getCodigo() : null);
		 atend.setAlturaAcompanhamentoNutricional(atendimento.getAltura());
		 atend.setAtencaoDomiciliarModalidade(atendimento.getModalidadeAD() != null ? (long) (atendimento.getModalidadeAD().ordinal() + 1) : null);
		 atend.setCnsCidadao(atendimento.getCidadao().getCns());
		 atend.setCpfCidadao(atend.getCnsCidadao() == null ? atendimento.getCidadao().getCpf() : null);
		 atend.setDataHoraInicialAtendimento(atendimento.getDataHorarioInicio().getTime());
		 atend.setDataHoraFinalAtendimento(atendimento.getDataHorarioFinal().getTime());
		 
		 if (atendimento.getCondutas() != null)
			 atend.setCondutas(atendimento.getCondutas().stream().map(conduta -> conduta.getCodigo()).collect(Collectors.toList()));
		 
		 atend.setDataNascimento(atendimento.getCidadao().getDataNascimento().getTime());
		 atend.setDumDaGestante(atendimento.getDum() != null ? atendimento.getDum().getTime() : null);
		 
		 atend = exame(atendimento, atend);
		 
		 atend.setFicouEmObservacao(atendimento.getFicouObservacao());
		 atend.setIdadeGestacional(atendimento.getIdadeGestacional());
		 atend.setLocalDeAtendimento(atendimento.getLocalAtendimento().getCodigo());
		 
		 if (atendimento.getNasfs() != null)
			 atend.setNasfs(atendimento.getNasfs().stream().map(nasf -> nasf.getCodigo()).collect(Collectors.toList()));
		 
		 atend.setNuGestasPrevias(atendimento.getGestasPrevias());
		 atend.setNumeroProntuario(atendimento.getCidadao().getNumProntuario());
		 atend.setNuPartos(atendimento.getPartos());
		 
		 atend.setPerimetroCefalico(atendimento.getPerimetroCefalico());
		 atend.setPesoAcompanhamentoNutricional(atendimento.getPeso());
		 atend.setProblemaCondicaoAvaliada(problemaCondicaoAvaliada(atendimento));
		 atend.setRacionalidadeSaude(atendimento.getRacionalidadeSaude() != null ? atendimento.getRacionalidadeSaude().getCodigo() : null);
		 atend.setSexo((long) atendimento.getCidadao().getSexo().getValue());
		 atend.setStGravidezPlanejada(atendimento.getGravidezPlanejada());
		 atend.setTipoAtendimento(atendimento.getTipoAtendimento() != null ? atendimento.getTipoAtendimento().getCodigo() : null);
		 atend.setTurno(atendimento.getTurno());
		 atend.setVacinaEmDia(atendimento.getVacinacaoEmDia());
		 
		 return atend;
	 }
	 
	 public FichaAtendimentoIndividualMasterTransportDto transport(FichaAtendimentoIndividual ficha) {
		 FichaAtendimentoIndividualMasterTransportDto transport = new FichaAtendimentoIndividualMasterTransportDto();
		 
		 transport.setAtendimentosIndividuais(atendimento(ficha.getAtendimento()));
		 
		 List<Profissional> profissionais = new ArrayList<Profissional>();
		 profissionais.add(ficha.getAtendimento().getProfissional());
		 
		 transport.setHeaderTransport(exportacaoService.headerTrasport(profissionais, ficha.getAtendimento().getCbo(),ficha.getAtendimento().getUbs(), 
				 ficha.getAtendimento().getDataRealizacao().getTime()));
		 
		 transport.setTpCdsOrigem(3);
		 transport.setUuidFicha(ficha.getUuid());
		 
		 return transport;
	 }
	 
	 public void gerarExportacao(ZipOutputStream zipOut) throws JsonGenerationException, JsonMappingException, IOException {
			List<FichaAtendimentoIndividual> fichas = repository.findAllByImportada(false);
			
			for (FichaAtendimentoIndividual ficha: fichas) {
				AtendimentoIndividualMasterDto dado = new AtendimentoIndividualMasterDto();
				dado = (AtendimentoIndividualMasterDto) exportacaoService.informacaoTransport(dado, ficha.getAtendimento().getUbs().getCnes(), 
						ficha.getUuid(), 4L, ficha.getAtendimento().getIne());
				dado.setTransport(transport(ficha));
				
				XmlMapper xmlMapper = new XmlMapper();
				File file = new File(ficha.getUuid() + ".esus.xml");
				xmlMapper.writeValue(file, dado);
				exportacaoService.zip(zipOut, file.getAbsolutePath());
				 Files.deleteIfExists(file.toPath());
				
				ficha.setImportada(Boolean.TRUE);
			    repository.save(ficha);
			}
	 }
	 
	 public Long countByImportada() {
		 return repository.countByImportada(Boolean.FALSE);
	 }
	 
}
