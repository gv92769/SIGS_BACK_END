package br.net.sigs.service.saude.ficha.atendimento.odontologico;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import br.net.sigs.dto.AtendimentoOdontologicoTabelaDto;
import br.net.sigs.dto.PageRequest;
import br.net.sigs.dto.integracao.atendimento.odontologico.FichaAtendimentoOdontologicoChildTransportDto;
import br.net.sigs.dto.integracao.atendimento.odontologico.FichaAtendimentoOdontologicoDto;
import br.net.sigs.dto.integracao.atendimento.odontologico.FichaAtendimentoOdontologicoMasterTransportDto;
import br.net.sigs.dto.integracao.atendimento.odontologico.ProcedimentoQuantidadeTransportDto;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.FichaAtendimentoOdontologico;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.OutroProcedimentoRealizado;
import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.ProcedimentoRealizado;
import br.net.sigs.entity.usuario.Profissional;
import br.net.sigs.repository.saude.ficha.atendimento.odontologico.FichaAtendimentoOdontologicoRepository;
import br.net.sigs.service.util.ExportacaoService;
import br.net.sigs.service.util.SearchService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class FichaAtendimentoOdontologicoService {

	@Autowired
	private FichaAtendimentoOdontologicoRepository fichaAtendimentoOdontologicoRepository;
	
	@Autowired
	private SearchService searchService;
	
	@Autowired
	private ExportacaoService exportacaoService;
	
	public String gerarUuid(String cnes) {
		String uuid = null;
		
		do {
			uuid = cnes.concat("-".concat(UUID.randomUUID().toString()));
		} while(fichaAtendimentoOdontologicoRepository.findByUuid(uuid) != null);
		
		return uuid;
	 }
	
	public Optional<FichaAtendimentoOdontologico> findById(Long id) {
		return fichaAtendimentoOdontologicoRepository.findById(id);
	}
	
	public FichaAtendimentoOdontologico save(FichaAtendimentoOdontologico fichaAtendimentoOdontologico) {
		fichaAtendimentoOdontologico.setUuid(fichaAtendimentoOdontologico.getUuid() == null ? gerarUuid(fichaAtendimentoOdontologico.getUbs().getCnes()) :
			fichaAtendimentoOdontologico.getUuid());
		fichaAtendimentoOdontologico.setImportada(fichaAtendimentoOdontologico.getImportada() == null ? Boolean.FALSE : fichaAtendimentoOdontologico.getImportada());
		return fichaAtendimentoOdontologicoRepository.save(fichaAtendimentoOdontologico);
	}
	
	public void delete(Long id) {
		fichaAtendimentoOdontologicoRepository.deleteById(id);
	}

	public Page<AtendimentoOdontologicoTabelaDto> findAllByNome(String nome, PageRequest pageRequest) {
		return fichaAtendimentoOdontologicoRepository.findAllByNome(nome.toLowerCase(), searchService.pageable(pageRequest));
	}
	
	public Page<AtendimentoOdontologicoTabelaDto> findAllByCpf(String cpf, PageRequest pageRequest) {
		return fichaAtendimentoOdontologicoRepository.findAllByCpf(cpf, searchService.pageable(pageRequest));
	}
	
	public List<ProcedimentoQuantidadeTransportDto> procedimentosRealizados(List<ProcedimentoRealizado> procedimentos, List<OutroProcedimentoRealizado> outrosProcedimentos) {
		List<ProcedimentoQuantidadeTransportDto> list = new ArrayList<ProcedimentoQuantidadeTransportDto>();
		
		for (ProcedimentoRealizado procedimento: procedimentos) {
			ProcedimentoQuantidadeTransportDto dado = new ProcedimentoQuantidadeTransportDto();
			
			dado.setCoMsProcedimento(procedimento.getProcedimento().getSigtap() == null || procedimento.getProcedimento().getSigtap().equals("")
				? procedimento.getProcedimento().getCodigoab() : procedimento.getProcedimento().getSigtap().replaceAll("[.-]", ""));
			dado.setQuantidade(procedimento.getQuantidade());
			
			list.add(dado);
		}
		
		for (OutroProcedimentoRealizado procedimento: outrosProcedimentos) {
			ProcedimentoQuantidadeTransportDto dado = new ProcedimentoQuantidadeTransportDto();
			
			dado.setCoMsProcedimento(procedimento.getProced().getCodigo());
			dado.setQuantidade(procedimento.getQuantidade());
			
			list.add(dado);
		}
		
		return list;
	}
	
	public FichaAtendimentoOdontologicoChildTransportDto atendimentoOdontologico(FichaAtendimentoOdontologico ficha) {
		FichaAtendimentoOdontologicoChildTransportDto transport = new FichaAtendimentoOdontologicoChildTransportDto();
		
		transport.setCnsCidadao(ficha.getCidadao().getCns());
		transport.setCpfCidadao(transport.getCnsCidadao() == null ? ficha.getCidadao().getCpf() : null);
		transport.setDataHoraFinalAtendimento(ficha.getDataHorarioFinal().getTime());
		transport.setDataHoraInicialAtendimento(ficha.getDataHorarioInicio().getTime());
		transport.setDtNascimento(ficha.getCidadao().getDataNascimento().getTime());
		transport.setGestante(ficha.getGestante());
		transport.setLocalAtendimento(ficha.getLocalAtendimento().getCodigo());
		transport.setNecessidadesEspeciais(ficha.getNecessidadesEspeciais());
		transport.setNumProntuario(ficha.getCidadao().getNumProntuario());
		transport.setProcedimentosRealizados(procedimentosRealizados(ficha.getProcedimentos(), ficha.getOutrosProcedimentos()));
		transport.setSexo((long) ficha.getCidadao().getSexo().getValue()); 
		transport.setTipoAtendimento(ficha.getTipoAtendimento().getCodigo());
		
		transport.setTiposConsultaOdonto(new ArrayList<>());
		transport.getTiposConsultaOdonto().add(ficha.getTipoConsultaOdonto().getCodigo());
		
		if (ficha.getCondutas() != null) 
			transport.setTiposEncamOdonto(ficha.getCondutas().stream().map(conduta -> conduta.getCodigo()).collect(Collectors.toList()));
		
		if (ficha.getFornecimentos() != null) 
			transport.setTiposFornecimOdonto(ficha.getFornecimentos().stream().map(fornecimento -> fornecimento.getCodigo()).collect(Collectors.toList()));
		
		if (ficha.getListVigilanciaSaudeBucal() != null) 
			transport.setTiposVigilanciaSaudeBucal(ficha.getListVigilanciaSaudeBucal().stream().map(vigilancia -> vigilancia.getCodigo()).collect(Collectors.toList()));
		
		transport.setTurno(ficha.getTurno());
		
		return transport;
	}
	
	public FichaAtendimentoOdontologicoMasterTransportDto transport(FichaAtendimentoOdontologico ficha) {
		FichaAtendimentoOdontologicoMasterTransportDto transport = new FichaAtendimentoOdontologicoMasterTransportDto();
		
		transport.setAtendimentosOdontologicos(new ArrayList<>());
		transport.getAtendimentosOdontologicos().add(atendimentoOdontologico(ficha));
		
		List<Profissional> profissionais = new ArrayList<>();
		profissionais.add(ficha.getProfissionalResponsavel());
		transport.setHeaderTransport(exportacaoService.headerTrasport(profissionais, ficha.getCbo(), ficha.getUbs(), ficha.getData().getTime()));
		
		transport.setTpCdsOrigem(3);
		transport.setUuidFicha(ficha.getUuid());
		
		return transport;
	}
	
	public void gerarExportacao( ZipOutputStream zipOut) throws IOException {
			 List<FichaAtendimentoOdontologico> fichas = fichaAtendimentoOdontologicoRepository.findAllByImportada(Boolean.FALSE);
			
			 for (FichaAtendimentoOdontologico ficha: fichas) {
					FichaAtendimentoOdontologicoDto dado = new FichaAtendimentoOdontologicoDto();
					dado = (FichaAtendimentoOdontologicoDto) exportacaoService.informacaoTransport(dado, ficha.getUbs().getCnes(), ficha.getUuid(), 5L, ficha.getIne());
					dado.setTransport(transport(ficha));
					
					XmlMapper xmlMapper = new XmlMapper();
					File file = new File(ficha.getUuid() + ".esus.xml");
					xmlMapper.writeValue(file, dado);
					exportacaoService.zip(zipOut, file.getAbsolutePath());
					 Files.deleteIfExists(file.toPath());
					
					ficha.setImportada(Boolean.TRUE);
					save(ficha);
				}
	}
	
	public Long countByImportada() {
		return fichaAtendimentoOdontologicoRepository.countByImportada(Boolean.FALSE);
	}
	
}
