package br.net.sigs.service.saude.ficha.atendimento.individual.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atendimento.individual.exame.ListaExames;
import br.net.sigs.repository.saude.ficha.atendimento.individual.exame.ListaExamesRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ListaExamesService {

	@Autowired
	private ListaExamesRepository repository;
	
	public List<ListaExames> findAll() {
		return repository.findAll();
	}

	public List<ListaExames> findAllById(List<Long> ids) {
		return repository.findAllById(ids);
	}
	
	public List<ListaExames> findAllByNotInId(List<Long> ids) {
		return repository.findAllByNotInId(ids);
	}
	
}
