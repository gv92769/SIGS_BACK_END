package br.net.sigs.service.saude.ficha.atividade.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atividade.util.PublicoAlvo;
import br.net.sigs.repository.saude.ficha.atividade.util.PublicoAlvoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class PublicoAlvoService {

	@Autowired
	private PublicoAlvoRepository publicoAlvoRepository;
	
	public List<PublicoAlvo> findAll() {
		return publicoAlvoRepository.findAll();
	}
	
}
