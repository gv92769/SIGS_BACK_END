package br.net.sigs.service.saude.procedimento;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import br.net.sigs.dto.SubGrupoFiltroDto;
import br.net.sigs.entity.saude.procedimento.ProcedimentoGrupo;
import br.net.sigs.entity.saude.procedimento.ProcedimentoSubGrupo;
import br.net.sigs.repository.saude.procedimento.ProcedimentoSubGrupoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ProcedimentoSubGrupoService {

	@Autowired
	private ProcedimentoSubGrupoRepository repository;
	
	public List<ProcedimentoSubGrupo> findAllByGrupo(ProcedimentoGrupo grupo) {
		return repository.findAllByGrupo(grupo);
	}
	
	public List<ProcedimentoSubGrupo> search(List<Long> ids, SubGrupoFiltroDto filtro) {
		return repository.search(filtro.getGrupo() != null ?  filtro.getGrupo().getId() :  null, ids, org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("descricao"))));
	}
	
	public List<ProcedimentoSubGrupo> searchSubGrupo(SubGrupoFiltroDto filtro) {
		List<Long> ids = new ArrayList<>();
		ids.add(2L);
		return search(ids, filtro);
	}
}
