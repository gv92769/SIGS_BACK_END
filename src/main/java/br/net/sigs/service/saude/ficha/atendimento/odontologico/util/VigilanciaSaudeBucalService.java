package br.net.sigs.service.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.VigilanciaSaudeBucal;
import br.net.sigs.repository.saude.ficha.atendimento.odontologico.util.VigilanciaSaudeBucalRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class VigilanciaSaudeBucalService {

	@Autowired
	private VigilanciaSaudeBucalRepository vigilanciaSaudeBucalRepository;

	public List<VigilanciaSaudeBucal> findAll() {
		return vigilanciaSaudeBucalRepository.findAll();
	}
	
}
