/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.net.sigs.service.saude.ficha.ectoscopia;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.ectoscopia.Ectoscopia;
import br.net.sigs.entity.saude.ficha.ectoscopia.ItemEctoscopia;
import br.net.sigs.entity.saude.util.TipoAvaliacaoMedica;
import br.net.sigs.repository.saude.ficha.ectoscopia.EctoscopiaRepository;


/**
 *
 * @author Gabriel Vinicius
 */
@Service
public class EctoscopiaService {
	
	@Autowired
    private EctoscopiaRepository repository;

    public Optional<Ectoscopia> findById(Long id) {
    	return repository.findById(id);
    }
	
    public Ectoscopia create(Ectoscopia ectoscopia) {
        ectoscopia.setTipoAvaliacaoMedica(TipoAvaliacaoMedica.ECTOSCOPIA);
        return repository.save(itens(ectoscopia));   
    }
    
    public Ectoscopia itens( Ectoscopia ectoscopia) {
    	for (ItemEctoscopia item :  ectoscopia.getItensEctoscopia()) {
	        item.setEctoscopia(ectoscopia);
	    }
    	
    	return ectoscopia;
    }
    
    public Ectoscopia update(Ectoscopia ectoscopia) {
	    return repository.save(itens(ectoscopia));
	}
    
}
