package br.net.sigs.service.saude.ficha.util;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.dto.SolicitacaoTabelaDto;
import br.net.sigs.entity.saude.ficha.exame.ResultadoExame;
import br.net.sigs.entity.saude.ficha.exame.Solicitacao;
import br.net.sigs.entity.saude.ficha.exame.TipoAtendimento;
import br.net.sigs.repository.saude.ficha.exame.ResultadoExameRepository;
import br.net.sigs.repository.saude.ficha.exame.SolicitacaoRepository;
import br.net.sigs.service.util.SearchService;
import br.net.sigs.service.util.StorageService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ExameService {

	@Autowired
	private SolicitacaoRepository solicitacaoRepository;
	
	@Autowired
	private ResultadoExameRepository resultadoRepository;
	
	@Autowired
	private SearchService searchService;
	
	@Autowired
	private StorageService storageService;
	
	 public Optional< Solicitacao> solicitacaoFindById(Long id) {
		 return solicitacaoRepository.findById(id);
     }
	 
	 public Optional< ResultadoExame> resultadoFindById(Long id) {
		 return resultadoRepository.findById(id);
     }
	 
	 public Solicitacao saveSolicitacao(Solicitacao solicitacao, MultipartFile file) {
		 String nameFile = null;
		 if (file != null) {
			 nameFile = storageService.uploadFile(file, "exame/laudo/");
			 solicitacao.setNameFile(nameFile);	
		 }
		 	 
		 return solicitacaoRepository.save(solicitacao);
	 }
	 
	 public ResultadoExame saveResultado(ResultadoExame resultado) {
		 return resultadoRepository.save(resultado);
	 }
	 
	 public void deleteSolicitacao(Solicitacao solicitacao) {
		 solicitacaoRepository.delete(solicitacao);
	 }
	 
	 public Page<SolicitacaoTabelaDto> search(FichaFiltroDto filter) {
		 return solicitacaoRepository.search(filter.getNome(), filter.getCpf(), filter.getCns(), filter.getDataInicio(), 
				 filter.getDataFinal(), filter.getTipoExame(), TipoAtendimento.getTipoAtendimentoPorNome(filter.getTipoAtendimento()), 
				 searchService.pageable(filter));
	 }
	
}
