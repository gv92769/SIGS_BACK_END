package br.net.sigs.service.saude.procedimento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.procedimento.ProcedimentoGrupo;
import br.net.sigs.repository.saude.procedimento.ProcedimentoGrupoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ProcedimentoGrupoService {

	@Autowired
	private ProcedimentoGrupoRepository repository;
	
	public List<ProcedimentoGrupo> findAllById(List<Long> ids) {
		return repository.findAllById(ids);
	}
	
}
