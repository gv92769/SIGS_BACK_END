package br.net.sigs.service.saude.ficha.evolucao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.net.sigs.dto.AvaliacaoMedicaTabelaDTO;
import br.net.sigs.dto.FichaFiltroDto;
import br.net.sigs.dto.integracao.evolucao.ProblemaCondicaoAvaliacaoAITransport;
import br.net.sigs.dto.integracao.evolucao.EvolucaoDto;
import br.net.sigs.dto.integracao.evolucao.FichaEvolucaoMasterTransportDto;
import br.net.sigs.entity.saude.ficha.evolucao.Evolucao;
import br.net.sigs.entity.saude.ficha.evolucao.FichaEvolucao;
import br.net.sigs.entity.saude.util.TipoAvaliacaoMedica;
import br.net.sigs.entity.usuario.Profissional;

import br.net.sigs.repository.saude.ficha.evolucao.EvolucaoRepository;
import br.net.sigs.repository.saude.ficha.evolucao.FichaEvolucaoRepository;
import br.net.sigs.service.util.ExportacaoService;
import br.net.sigs.service.util.SearchService;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class EvolucaoService {

	 @Autowired
	 private EvolucaoRepository evolucaoRepository;
	 
	 @Autowired
	 private FichaEvolucaoRepository repository;
	 
	 @Autowired
	 private ExportacaoService exportacaoService;

	 @Autowired
	 private SearchService searchService;
    
     public Page<AvaliacaoMedicaTabelaDTO> search(FichaFiltroDto filter) {
    	return evolucaoRepository.search(filter.getNome() != null ? filter.getNome().toLowerCase() : null, filter.getCpf(), filter.getCns(), filter.getDataInicio(), filter.getDataFinal(),
    			  filter.getCbo(), searchService.pageable(filter));
     }
	 
	 public String gerarUuid(String cnes) {
		String uuid = null;
		
		do {
			uuid = cnes.concat("-".concat(UUID.randomUUID().toString()));
		} while(repository.findById(uuid).isPresent());
		
		return uuid;
	 }
	 
	 public Evolucao verificarCampos(Evolucao evolucao) {
		 evolucao.setAleitamentoMaterno(evolucao.getAleitamentoMaterno() == null || 
		(evolucao.getAleitamentoMaterno() != null && 
		 evolucao.getAleitamentoMaterno().getCodigo() == null) ? null : evolucao.getAleitamentoMaterno());
		 evolucao.setCiaps(evolucao.getCiaps().isEmpty() ? null : evolucao.getCiaps());
		 evolucao.setNasfs(evolucao.getNasfs().isEmpty() ? null : evolucao.getNasfs());
		 evolucao.setRacionalidadeSaude(evolucao.getRacionalidadeSaude() == null || 
		(evolucao.getRacionalidadeSaude() != null && 
		 evolucao.getRacionalidadeSaude().getCodigo() == null) ? null : evolucao.getRacionalidadeSaude());
		 
		 return evolucao;
	 }
	
	 public Optional<Evolucao> findById(Long id) {
		 return evolucaoRepository.findById(id);
	 }
	 
	 public FichaEvolucao findByIdEvolucao(Long id) {
		return  repository.findByIdEvolucao(id);
	 }
	 
	 public Evolucao save(Evolucao evolucao) {
		 evolucao.setTipoAvaliacaoMedica(TipoAvaliacaoMedica.EVOLUCAO);
		 evolucao = evolucaoRepository.save(verificarCampos(evolucao));
		 
		 if (repository.findByEvolucao(evolucao) == null) {
			 FichaEvolucao ficha = new FichaEvolucao();
			 ficha.setUuid(gerarUuid(evolucao.getUbs().getCnes()));
			 ficha.setEvolucao(evolucao);
			 ficha.setImportada(Boolean.FALSE);
			 repository.save(ficha);
		 }
		 
		 return evolucao;
	 }

	 
	 public ProblemaCondicaoAvaliacaoAITransport problemaCondicaoAvaliada(Evolucao evolucao) {
		 ProblemaCondicaoAvaliacaoAITransport problema = new ProblemaCondicaoAvaliacaoAITransport();
		 
		 if (evolucao.getCiaps() != null)
			 problema.setCiaps(evolucao.getCiaps().stream().map(ciap -> ciap.getCodigoab()).collect(Collectors.toList()));
		 
		 problema.setOutroCiap1(evolucao.getCiap2_1() != null ? evolucao.getCiap2_1().getCodigo() : null);
		 problema.setOutroCiap2(evolucao.getCiap2_2() != null ? evolucao.getCiap2_2().getCodigo() : null);
		 problema.setCid10(evolucao.getCid10_1() != null ? evolucao.getCid10_1().getCodigo() : null);
		 problema.setCid10_2(evolucao.getCid10_2() != null ? evolucao.getCid10_2().getCodigo() : null);
		 
		 return problema;
	 }
	 
	 public EvolucaoDto evolucao(Evolucao evolucao) {
		 EvolucaoDto evolucaoDto = new EvolucaoDto();
		 
		 evolucaoDto.setAleitamentoMaterno(evolucao.getAleitamentoMaterno() != null ? evolucao.getAleitamentoMaterno().getCodigo() : null);
		 evolucaoDto.setAlturaAcompanhamentoNutricional(evolucao.getAltura());
		 evolucaoDto.setAtencaoDomiciliarModalidade(evolucao.getModalidadeAD() != null ? (long) (evolucao.getModalidadeAD().ordinal() + 1) : null);
		 evolucaoDto.setCnsCidadao(evolucao.getCidadao().getCns());
		 evolucaoDto.setCpfCidadao(evolucaoDto.getCnsCidadao() == null ? evolucao.getCidadao().getCpf() : null);
		 evolucaoDto.setDataHoraInicialAtendimento(evolucao.getDataHorarioInicio().getTime());
		 evolucaoDto.setDataHoraFinalAtendimento(evolucao.getDataHorarioFinal().getTime());
		 
		 if (evolucao.getCondutas() != null)
			 evolucaoDto.setCondutas(evolucao.getCondutas().stream().map(conduta -> conduta.getCodigo()).collect(Collectors.toList()));
		 
		 evolucaoDto.setDataNascimento(evolucao.getCidadao().getDataNascimento().getTime());
		 evolucaoDto.setDumDaGestante(evolucao.getDum() != null ? evolucao.getDum().getTime() : null);
		 
		 evolucaoDto.setFicouEmObservacao(evolucao.getFicouObservacao());
		 evolucaoDto.setIdadeGestacional(evolucao.getIdadeGestacional());
		 evolucaoDto.setLocalDeAtendimento(evolucao.getLocalAtendimento().getCodigo());
		 
		 if (evolucao.getNasfs() != null)
			 evolucaoDto.setNasfs(evolucao.getNasfs().stream().map(nasf -> nasf.getCodigo()).collect(Collectors.toList()));
		 
		 evolucaoDto.setNuGestasPrevias(evolucao.getGestasPrevias());
		 evolucaoDto.setNumeroProntuario(evolucao.getCidadao().getNumProntuario());
		 evolucaoDto.setNuPartos(evolucao.getPartos());
		 
		 evolucaoDto.setPerimetroCefalico(evolucao.getPerimetroCefalico());
		 evolucaoDto.setPesoAcompanhamentoNutricional(evolucao.getPeso());
		 evolucaoDto.setProblemaCondicaoAvaliada(problemaCondicaoAvaliada(evolucao));
		 evolucaoDto.setRacionalidadeSaude(evolucao.getRacionalidadeSaude() != null ? evolucao.getRacionalidadeSaude().getCodigo() : null);
		 evolucaoDto.setSexo((long) evolucao.getCidadao().getSexo().getValue());
		 evolucaoDto.setStGravidezPlanejada(evolucao.getGravidezPlanejada());
		 evolucaoDto.setTipoAtendimento(evolucao.getTipoAtendimento() != null ? evolucao.getTipoAtendimento().getCodigo() : null);
		 evolucaoDto.setTurno(evolucao.getTurno());
		 evolucaoDto.setVacinaEmDia(evolucao.getVacinacaoEmDia());
		 
		 return evolucaoDto;
	 }
	 
	 public FichaEvolucaoMasterTransportDto transport(FichaEvolucao ficha) {
		 FichaEvolucaoMasterTransportDto transport = new FichaEvolucaoMasterTransportDto();
		 
		 transport.setEvolucoes(evolucao(ficha.getEvolucao()));
		 
		 List<Profissional> profissionais = new ArrayList<Profissional>();
		 profissionais.add(ficha.getEvolucao().getProfissional());
		 
		 transport.setHeaderTransport(exportacaoService.headerTrasport(profissionais, ficha.getEvolucao().getCbo(),ficha.getEvolucao().getUbs(), 
				 ficha.getEvolucao().getDataRealizacao().getTime()));
		 
		 transport.setTpCdsOrigem(3);
		 transport.setUuidFicha(ficha.getUuid());
		 
		 return transport;
	 }
	 
	 /*public void gerarExportacao(ZipOutputStream zipOut) throws JsonGenerationException, JsonMappingException, IOException {
			List<FichaAtendimentoIndividual> fichas = repository.findAllByImportada(false);
			
			for (FichaAtendimentoIndividual ficha: fichas) {
				AtendimentoIndividualMasterDto dado = new AtendimentoIndividualMasterDto();
				dado = (AtendimentoIndividualMasterDto) exportacaoService.informacaoTransport(dado, ficha.getAtendimento().getUbs().getCnes(), 
						ficha.getUuid(), 4L, ficha.getAtendimento().getIne());
				dado.setTransport(transport(ficha));
				
				XmlMapper xmlMapper = new XmlMapper();
				File file = new File(ficha.getUuid() + ".esus.xml");
				xmlMapper.writeValue(file, dado);
				exportacaoService.zip(zipOut, file.getAbsolutePath());
				 Files.deleteIfExists(file.toPath());
				
				ficha.setImportada(Boolean.TRUE);
			    repository.save(ficha);
			}
	 }*/
	 
	 public Long countByImportada() {
		 return repository.countByImportada(Boolean.FALSE);
	 }
	 
}
