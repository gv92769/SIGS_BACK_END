package br.net.sigs.service.saude.ficha.atendimento.individual.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atendimento.individual.util.AleitamentoMaterno;
import br.net.sigs.repository.saude.ficha.atendimento.individual.util.AleitamentoMaternoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class AleitamentoMaternoService {

	@Autowired
	private AleitamentoMaternoRepository repository;
	
	public List<AleitamentoMaterno> findAll() {
		return repository.findAll();
	}
	
}
