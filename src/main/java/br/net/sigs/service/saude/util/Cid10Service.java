package br.net.sigs.service.saude.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import br.net.sigs.entity.saude.util.Cid10;
import br.net.sigs.repository.saude.util.Cid10Repository;

@Service
public class Cid10Service {

	@Autowired
    private Cid10Repository repository;
	
	@GetMapping("/search/{valor}")
    public List<Cid10> search(String valor, Byte sexo) {
		return repository.search(valor.toLowerCase(), sexo, org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("descricao")))); 
    }
	
}
