package br.net.sigs.service.saude.procedimento;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import br.net.sigs.dto.ProcedimentoFiltroDto;
import br.net.sigs.entity.saude.procedimento.Procedimento;
import br.net.sigs.repository.saude.procedimento.ProcedimentoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ProcedimentoService {

	@Autowired
	private ProcedimentoRepository repository;
	
	public List<Procedimento> searchProcedimento(ProcedimentoFiltroDto filtro) {
		List<Long> ids = new ArrayList<>();
		
		ids.add(1L);
		ids.add(2L);
		ids.add(3L);
		ids.add(4L);
		
		return search(ids, filtro);
	}
	
	public List<Procedimento> search(List<Long> ids, ProcedimentoFiltroDto filtro) {
		return repository.search(filtro.getGrupo() != null ?  filtro.getGrupo().getId() :  null, ids, filtro.getSubgrupo() != null ? filtro.getSubgrupo().getId() : null , 
				filtro.getForma() != null ? filtro.getForma().getId() : null, filtro.getTexto().toLowerCase(), 
				filtro.getSexo(), org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("descricao"))));
	}

	public List<Procedimento> searchExame(ProcedimentoFiltroDto filtro) {
		List<Long> ids = new ArrayList<>();
		ids.add(2L);
		return search(ids, filtro);
	}
	
	public List<Procedimento> searchOdonto(ProcedimentoFiltroDto filtro) {
		return repository.searchOdonto(filtro.getTexto().toLowerCase(), filtro.getSexo(), org.springframework.data.domain.PageRequest.of(0, 10, Sort.by(Order.asc("descricao"))));
	}
	
}
