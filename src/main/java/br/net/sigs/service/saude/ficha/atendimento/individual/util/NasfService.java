package br.net.sigs.service.saude.ficha.atendimento.individual.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atendimento.individual.util.Nasf;
import br.net.sigs.repository.saude.ficha.atendimento.individual.util.NasfRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class NasfService {

	@Autowired
	private NasfRepository repository;
	
	public List<Nasf> findAll() {
		return repository.findAll();
	}
	
}
