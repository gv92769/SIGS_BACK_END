package br.net.sigs.service.saude.ficha.procedimento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.procedimento.ProcedimentoFixo;
import br.net.sigs.repository.saude.ficha.procedimento.ProcedimentoFixoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ProcedimentoFixoService {
	
	@Autowired
	private ProcedimentoFixoRepository procedimentoFixoRepository;
	
	public List<ProcedimentoFixo> findAllById(List<Integer> ids) {
		return procedimentoFixoRepository.findAllById(ids);
	}

}
