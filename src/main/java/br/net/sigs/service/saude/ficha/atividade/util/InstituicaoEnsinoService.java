package br.net.sigs.service.saude.ficha.atividade.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atividade.util.InstituicaoEnsino;
import br.net.sigs.repository.saude.ficha.atividade.util.InstituicaoEnsinoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class InstituicaoEnsinoService {
	
	@Autowired
	private InstituicaoEnsinoRepository instituicaoEnsinoRepository;
	
	public InstituicaoEnsino findByInep(Long inep) {
		return instituicaoEnsinoRepository.findByInep(inep);
	}

}
