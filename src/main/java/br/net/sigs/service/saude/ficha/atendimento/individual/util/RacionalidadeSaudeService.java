package br.net.sigs.service.saude.ficha.atendimento.individual.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atendimento.individual.util.RacionalidadeSaude;
import br.net.sigs.repository.saude.ficha.atendimento.individual.util.RacionalidadeSaudeRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class RacionalidadeSaudeService {

	@Autowired
	private RacionalidadeSaudeRepository repository;
	
	public List<RacionalidadeSaude> findAll() {
		return repository.findAll();
	}
	
}
