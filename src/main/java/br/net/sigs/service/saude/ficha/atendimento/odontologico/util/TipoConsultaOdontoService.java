package br.net.sigs.service.saude.ficha.atendimento.odontologico.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.sigs.entity.saude.ficha.atendimento.odontologico.util.TipoConsultaOdonto;
import br.net.sigs.repository.saude.ficha.atendimento.odontologico.util.TipoConsultaOdontoRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class TipoConsultaOdontoService {

	@Autowired
	private TipoConsultaOdontoRepository tipoConsultaOdontoRepository;

	public List<TipoConsultaOdonto> findAll() {
		return tipoConsultaOdontoRepository.findAll();
	}
	
}
